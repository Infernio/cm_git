package ic2.api.item;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;

public interface IElectricItemManager {

	int charge(ItemStack itemStack, int amount, int tier, boolean ignoreTransferLimit, boolean simulate);

	int discharge(ItemStack itemStack, int amount, int tier, boolean ignoreTransferLimit, boolean simulate);

	int getCharge(ItemStack itemStack);

	boolean canUse(ItemStack itemStack, int amount);

	boolean use(ItemStack itemStack, int amount, EntityLivingBase entity);

	void chargeFromArmor(ItemStack itemStack, EntityLivingBase entity);

	String getToolTip(ItemStack itemStack);
}
