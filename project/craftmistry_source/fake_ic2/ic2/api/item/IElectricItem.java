package ic2.api.item;

import net.minecraft.item.ItemStack;

public interface IElectricItem {

	boolean canProvideEnergy(ItemStack itemStack);

	int getChargedItemId(ItemStack itemStack);

	int getEmptyItemId(ItemStack itemStack);

	int getMaxCharge(ItemStack itemStack);

	int getTier(ItemStack itemStack);

	int getTransferLimit(ItemStack itemStack);
}

