package ic2.api.item;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public final class ElectricItem {

	public static IElectricItemManager manager;

	public static IElectricItemManager rawManager;

	@Deprecated
	public static int charge(ItemStack itemStack, int amount, int tier, boolean ignoreTransferLimit, boolean simulate)
	{
		return 0;
	}

	@Deprecated
	public static int discharge(ItemStack itemStack, int amount, int tier, boolean ignoreTransferLimit, boolean simulate)
	{
		return 0;
	}

	@Deprecated
	public static boolean canUse(ItemStack itemStack, int amount)
	{
		return true;
	}

	@Deprecated
	public static boolean use(ItemStack itemStack, int amount, EntityPlayer player)
	{
		return true;
	}

	@Deprecated
	public static void chargeFromArmor(ItemStack itemStack, EntityPlayer player) {}

	static
	{
		manager = new IElectricItemManager()
		{
			@Override
			public boolean use(ItemStack itemStack, int amount, EntityLivingBase entity)
			{
				return true;
			}
			
			@Override
			public String getToolTip(ItemStack itemStack)
			{
				return "";
			}
			
			@Override
			public int getCharge(ItemStack itemStack)
			{
				return 0;
			}
			
			@Override
			public int discharge(ItemStack itemStack, int amount, int tier, boolean ignoreTransferLimit, boolean simulate)
			{
				return 0;
			}
			
			@Override
			public void chargeFromArmor(ItemStack itemStack, EntityLivingBase entity) {}
			
			@Override
			public int charge(ItemStack itemStack, int amount, int tier, boolean ignoreTransferLimit, boolean simulate)
			{
				return 0;
			}
			
			@Override
			public boolean canUse(ItemStack itemStack, int amount)
			{
				return true;
			}
		};
		rawManager = manager;
	}
}

