package com.infernio.Craftmistry.common.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.lib.StringLib;

public class CMTab extends CreativeTabs {

	public CMTab()
	{
		super(CreativeTabs.creativeTabArray.length, StringLib.MODNAME);
	}

	@Override
	public String getTranslatedTabLabel()
	{
		return StringLib.MODNAME;
	}

	@Override
	public Item getTabIconItem()
	{
		return ModItems.testTubeSolid;
	}

}
