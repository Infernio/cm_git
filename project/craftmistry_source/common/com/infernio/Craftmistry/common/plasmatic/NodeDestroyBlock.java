package com.infernio.Craftmistry.common.plasmatic;

import net.minecraft.world.World;

import com.infernio.Craftmistry.api.plasmatic.BuildNode;

public class NodeDestroyBlock extends BuildNode {

	@Override
	public void apply(World world, int x, int y, int z)
	{
		world.setBlockToAir(x, y, z);
	}

	@Override
	public String getIdentifier()
	{
		return PlasmaticLoader.DESTROY_BLOCK_ID;
	}

}
