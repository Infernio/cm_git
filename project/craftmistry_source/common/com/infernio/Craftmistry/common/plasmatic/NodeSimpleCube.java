package com.infernio.Craftmistry.common.plasmatic;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.plasmatic.BuildNode;

public class NodeSimpleCube extends BuildNode {

	private Block block;
	private int blockMeta;
	private int diameter;

	public NodeSimpleCube(Block block, int blockMeta, int diameter)
	{
		this.block = block;
		this.blockMeta = blockMeta;
		this.diameter = diameter;
	}

	@Override
	public void apply(World world, int x, int y, int z)
	{
		int radius = (this.diameter - 1) / 2;
		for(int x1 = -radius; x1 <= radius; ++x1)
		{
			for(int y1 = -radius; y1 <= radius; ++y1)
			{
				for(int z1 = -radius; z1 <= radius; ++z1)
				{
					Block block = world.getBlock(x + x1, y + y1, z + z1);
					if(block == null || block.isAir(world, x + x1, y + y1, z + z1))
					{
						world.setBlock(x + x1, y + y1, z + z1, this.block);
						world.setBlockMetadataWithNotify(x + x1, y + y1, z + z1, this.blockMeta, 3);
					}
				}
			}
		}
	}

	@Override
	public String getIdentifier()
	{
		return PlasmaticLoader.SIMPLE_CUBE_ID;
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setInteger("BlockID", Block.getIdFromBlock(this.block));
		tag.setShort("BlockMeta", (short)this.blockMeta);
		tag.setInteger("Diameter", this.diameter);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		this.block = Block.getBlockById(tag.getInteger("BlockID"));
		this.blockMeta = tag.getShort("BlockMeta");
		this.diameter = tag.getInteger("Diameter");
	}

}
