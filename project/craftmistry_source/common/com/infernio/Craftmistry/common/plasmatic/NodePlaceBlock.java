package com.infernio.Craftmistry.common.plasmatic;

import net.minecraft.block.Block;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.plasmatic.BuildNode;

public class NodePlaceBlock extends BuildNode {

	private Block block;
	private int blockMeta;

	public NodePlaceBlock(Block block, int blockMeta)
	{
		this.block = block;
		this.blockMeta = blockMeta;
	}

	@Override
	public void apply(World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
		if(block == null || block.isAir(world, x, y, z))
		{
			world.setBlock(x, y, z, this.block);
			world.setBlockMetadataWithNotify(x, y, z, this.blockMeta, 3);
		}
	}

	@Override
	public String getIdentifier()
	{
		return PlasmaticLoader.PLACE_BLOCK_ID;
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setInteger("BlockID", Block.getIdFromBlock(this.block));
		tag.setShort("BlockMeta", (short)this.blockMeta);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		this.block = Block.getBlockById(tag.getInteger("BlockID"));
		this.blockMeta = tag.getShort("BlockMeta");
	}

}
