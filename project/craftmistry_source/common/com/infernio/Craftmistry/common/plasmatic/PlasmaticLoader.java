package com.infernio.Craftmistry.common.plasmatic;

import net.minecraft.init.Blocks;

import com.infernio.Craftmistry.api.plasmatic.BuilderRegistry;
import com.infernio.Craftmistry.api.plasmatic.INodeBuilder;

public class PlasmaticLoader {

	static INodeBuilder builder = new CMNodeBuilder();

	public static final String PLACE_BLOCK_ID = "PlaceBlockSimple";
	public static final String DESTROY_BLOCK_ID = "DestroyBlockSimple";
	public static final String SIMPLE_CUBE_ID = "SimpleCube";

	public static void init()
	{
		BuilderRegistry.registerNodeBuilder(PLACE_BLOCK_ID, builder);
		BuilderRegistry.registerNodeBuilder(DESTROY_BLOCK_ID, builder);
		BuilderRegistry.registerNodeBuilder(SIMPLE_CUBE_ID, builder);

		BuilderRegistry.registerNode(new NodePlaceBlock(Blocks.air, 0));
		BuilderRegistry.registerNode(new NodeDestroyBlock());
		BuilderRegistry.registerNode(new NodeSimpleCube(Blocks.air, 0, 0));
	}

}
