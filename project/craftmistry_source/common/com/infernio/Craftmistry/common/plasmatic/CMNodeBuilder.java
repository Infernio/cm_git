package com.infernio.Craftmistry.common.plasmatic;

import net.minecraft.init.Blocks;

import com.infernio.Craftmistry.api.plasmatic.BuildNode;
import com.infernio.Craftmistry.api.plasmatic.INodeBuilder;

public class CMNodeBuilder implements INodeBuilder {

	@Override
	public BuildNode buildNode(String identifier)
	{
		if(identifier.equals(PlasmaticLoader.PLACE_BLOCK_ID))
		{
			return new NodePlaceBlock(Blocks.air, 0);	
		}
		else if(identifier.equals(PlasmaticLoader.DESTROY_BLOCK_ID))
		{
			return new NodeDestroyBlock();
		}
		else if(identifier.equals(PlasmaticLoader.SIMPLE_CUBE_ID))
		{
			return new NodeSimpleCube(Blocks.air, 0, 0);
		}
		return null;
	}

}
