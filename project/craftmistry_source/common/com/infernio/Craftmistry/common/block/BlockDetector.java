package com.infernio.Craftmistry.common.block;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileDetector;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.block.BlockTile;

public class BlockDetector extends BlockTile {

	public BlockDetector()
	{
		super(Material.rock);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setHardness(3.5F);
		this.setBlockName(StringLib.BLOCK_DETECTOR_NAME);
	}

	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
		super.onBlockPlacedBy(world, x, y, z, entity, stack);
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileDetector)
		{
			((TileDetector)tile).redstoneConfig = 15;
		}
    }

    @Override
	public boolean canProvidePower()
    {
        return true;
    }

    @Override
	public int isProvidingWeakPower(IBlockAccess blockAccess, int x, int y, int z, int side)
    {
        TileEntity tile = blockAccess.getTileEntity(x, y, z);
        if(tile == null || !(tile instanceof TileDetector))
        {
        	return 0;
        }
        return ((TileDetector)tile).redstoneOutput;
    }
    
    @Override
	public int isProvidingStrongPower(IBlockAccess blockAccess, int x, int y, int z, int side)
    {
        TileEntity tile = blockAccess.getTileEntity(x, y, z);
        if(tile == null || !(tile instanceof TileDetector))
        {
        	return 0;
        }
        return ((TileDetector)tile).redstoneOutput;
    }

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.iron_block.getIcon(side, meta);
	}

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {}

	@Override
	public void openGUI(EntityPlayer player, World world, int x, int y, int z)
	{
		player.openGui(Craftmistry.instance, IntLib.GUI_DETECTOR_ID, world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileDetector();
	}

}
