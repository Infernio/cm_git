package com.infernio.Craftmistry.common.block;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.HashUtils;

public class BlockTrigger
{
	private final Block trigger;
	private final int triggerMeta;
	private final int hash;

	public BlockTrigger(Block block, int meta)
	{
		this.trigger = block;
		this.triggerMeta = meta;
		this.hash = HashUtils.hashItemStack(new ItemStack(this.trigger, 1, this.triggerMeta));
	}

	@Override
	public boolean equals(Object other)
	{
		if(!(other instanceof BlockTrigger))
		{
			return false;
		}
		return Block.isEqualTo(((BlockTrigger)other).trigger, this.trigger) && ((BlockTrigger)other).triggerMeta == this.triggerMeta;
	}

	@Override
	public int hashCode()
	{
		return this.hash;
	}
}
