package com.infernio.Craftmistry.common.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileMineralExtractor;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;

public class BlockMineralExtractor extends BlockContainer {

	private Random rand = new Random();

	public BlockMineralExtractor()
	{
		super(Material.rock);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setHardness(3.5F);
		this.setBlockName(StringLib.BLOCK_MINERAL_EXTRACTOR_NAME);
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.iron_block.getIcon(side, meta);
	}

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int face, float par7, float par8, float par9)
    {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile == null || !(tile instanceof TileMineralExtractor))
		{
			return false;
		}
		TileMineralExtractor extractor = (TileMineralExtractor)tile;
		ItemStack current = player.getCurrentEquippedItem();
		if(player.isSneaking() && current != null)
		{
			return false;
		}
		if(player.isSneaking() && current == null)
		{
			player.openGui(Craftmistry.instance, IntLib.GUI_MINERAL_EXTRACTOR_ID, world, x, y, z);
			return true;
		}
		if(current == null)
		{
			player.openGui(Craftmistry.instance, IntLib.GUI_MINERAL_EXTRACTOR_ID, world, x, y, z);
			return true;
		}
		FluidStack fluid = FluidContainerRegistry.getFluidForFilledItem(current);
		if(fluid != null)
		{
			if(world.isRemote)
			{
				return true;
			}
			if(extractor.fill(ForgeDirection.DOWN, fluid, true) > 0)
			{
				ItemStack container = current.getItem().getContainerItem(current);
				if(container != null)
				{
					player.inventory.setInventorySlotContents(player.inventory.currentItem, container.copy());
				}
				return true;
			}
		}
		player.openGui(Craftmistry.instance, IntLib.GUI_MINERAL_EXTRACTOR_ID, world, x, y, z);
		return true;
    }

	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileMineralExtractor)
		{
			if(stack.hasDisplayName())
			{
				((TileMineralExtractor)tile).setInventoryName(stack.getDisplayName());
			}
	    }
    }

    @Override
    @Deprecated
    public boolean hasTileEntity()
    {
    	return true;
    }

    @Override
    public boolean hasTileEntity(int meta)
    {
    	return true;
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {}

	@Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta)
    {
		TileMineralExtractor tile = (TileMineralExtractor)world.getTileEntity(x, y, z);
        if(tile != null)
        {
            for(int i = 0; i < tile.getSizeInventory(); ++i)
            {
                ItemStack stack = tile.getStackInSlot(i);
                if(stack != null)
                {
                    float f = this.rand.nextFloat() * 0.8F + 0.1F;
                    float f1 = this.rand.nextFloat() * 0.8F + 0.1F;
                    EntityItem item;
                    for(float f2 = this.rand.nextFloat() * 0.8F + 0.1F; stack.stackSize > 0; world.spawnEntityInWorld(item))
                    {
                        int k1 = this.rand.nextInt(21) + 10;
                        if(k1 > stack.stackSize)
                        {
                            k1 = stack.stackSize;
                        }
                        stack.stackSize -= k1;
                        item = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(stack.getItem(), k1, stack.getItemDamage()));
                        float f3 = 0.05F;
                        item.motionX = (double)((float)this.rand.nextGaussian() * f3);
                        item.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
                        item.motionZ = (double)((float)this.rand.nextGaussian() * f3);
                        if(stack.hasTagCompound())
                        {
                            item.getEntityItem().setTagCompound((NBTTagCompound)stack.getTagCompound().copy());
                        }
                    }
                }
            }
            world.func_147453_f(x, y, z, block);
        }
        super.breakBlock(world, x, y, z, block, meta);
    }

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileMineralExtractor();
	}
}
