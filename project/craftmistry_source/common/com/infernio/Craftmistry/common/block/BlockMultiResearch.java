package com.infernio.Craftmistry.common.block;

import java.util.ArrayList;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.tile.IResearchTile;
import com.infernio.Craftmistry.common.tile.TileComputer;
import com.infernio.Craftmistry.common.tile.TileEnergyStorage;
import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;
import com.infernio.Craftmistry.common.tile.TileProductionFacility;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.block.BlockMetaTile;
import com.infernio.InfernoCore.tile.IRotatable;

public class BlockMultiResearch extends BlockMetaTile {

	public BlockMultiResearch()
	{
		super(Material.rock);
		this.setHardness(3.5F);
		this.setBlockName(StringLib.BLOCK_RESEARCH_MULTI_NAME);
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.iron_block.getIcon(side, meta);
	}

	@Override
    public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z)
    {
		return true;
    }

	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int meta) 
	{
		if(!world.isRemote)
		{
			TileEntity tile = world.getTileEntity(x, y, z);
			if(tile instanceof TileResearchPanel)
			{
				TileResearchPanel panel = (TileResearchPanel)tile;
				if(panel.getNetwork() != null)
				{
					panel.getNetwork().cleanReferences();
				}
			}
			if(tile instanceof IResearchTile)
			{
				IResearchTile tile2 = (IResearchTile)tile;
				if(tile2.getNetwork() != null)
				{
					tile2.getNetwork().clear();
					tile2.getNetwork().build();
					tile2.setResearchNetwork(null);
				}
			}
		}
		world.setBlockToAir(x, y, z);
	}

	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
		super.onBlockPlacedBy(world, x, y, z, entity, stack);
		int facing = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		if(facing == 1)
		{
			facing = 3;
		}
		else if(facing == 3)
		{
			facing = 1;
		}
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof IRotatable)
		{
			IRotatable rotatable = (IRotatable)tile;
			if(rotatable.acceptsRotation())
			{
				rotatable.rotate(facing);
			}
		}
    }

    @Override
    public boolean renderAsNormalBlock()
    {
    	return false;
    }
    
    @Override
    public int getRenderType()
    {
    	return IntLib.RESEARCH_MODEL_ID;
    }

    @Override
    public boolean isOpaqueCube()
    {
    	return false;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int face, float par7, float par8, float par9)
    {
    	if(world.isRemote)
    	{
    		return true;
    	}
    	TileEntity tile = world.getTileEntity(x, y, z);
    	if(tile == null)
		{
			return false;
		}
    	if(tile instanceof IResearchTile)
    	{
    		ResearchNetwork network = ((IResearchTile)tile).getNetwork();
    		if(network != null)
    		{
	    		network.setWorld(world);
	    		network.clear();
	    		network.build();
	    		if(!network.isMultiBlockComplete())
	    		{
	    			if(!world.isRemote)
	    			{
		    			ArrayList<String> missing = network.getMissingBlocks();
		    			player.addChatMessage(new ChatComponentText(StatCollector.translateToLocal("craftmistry.research.missingParts")));
		    			for(String s : missing)
		    			{
		    				player.addChatMessage(new ChatComponentText("- " + StatCollector.translateToLocal("craftmistry.research.part." + s)));
		    			}
	    			}
	    			return true;
	    		}
	    		if(player.isSneaking() && player.getCurrentEquippedItem() != null)
	    		{
	    			return false;
	    		}
	    		if(player.isSneaking() && player.getCurrentEquippedItem() == null)
	    		{
	    			this.openGUI(player, world, x, y, z);
	    			return true;
	    		}
	    		if(player.getCurrentEquippedItem() == null)
	    		{
	    			this.openGUI(player, world, x, y, z);
	    			return true;
	    		}
	    		this.openGUI(player, world, x, y, z);
	    		return true;
    		}
    		else
    		{
	    		return false;
	    	}
    	}
		return false;
    }
 
	@Override
	public void openGUI(EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof TileResearchPanel)
		{
			player.openGui(Craftmistry.instance, IntLib.GUI_PANEL_ID, world, x, y, z);
		}
		else if(tile instanceof TileMaterialCompartment)
		{
			player.openGui(Craftmistry.instance, IntLib.GUI_MATERIAL_COMPARTMENT_ID, world, x, y, z);
		}
		else if(tile instanceof TileEnergyStorage)
		{
			//TODO GUI
		}
	}

	@Override
	public void registerBlockIcons(IIconRegister register) {}

	@Override
	public TileEntity createTileEntity(World world, int meta)
	{
		switch(meta)
		{
		case 0: return new TileResearchPanel();
		case 1: return new TileComputer();
		case 2: return new TileMaterialCompartment();
		case 3: return new TileEnergyStorage();
		case 4: return new TileProductionFacility();
		//TODO This is temporary, plasmatic stuff needs its own block.
		//case 5: return new TilePlasmaticBuilder();
		//case 6: return new TilePlasmaticLaser();
		default: return null;
		}
	}
}
