package com.infernio.Craftmistry.common.block;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.block.predefined.BlockCM;
import com.infernio.Craftmistry.common.tile.TileManaSpring;
import com.infernio.Craftmistry.core.lib.StringLib;

public class BlockManaSpring extends BlockCM {

	public BlockManaSpring()
	{
		super(Material.glass);
		this.setStepSound(soundTypeMetal);
		this.setHardness(1.25F);
		this.setLightLevel(1.0F);
		this.setBlockName(StringLib.BLOCK_MANA_SPRING_NAME);
	}

	@Override
	public boolean canSilkHarvest(World world, EntityPlayer player, int x, int y, int z, int meta)
	{
		return true;
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.glowstone.getIcon(side, meta);
	}

	@Override
    public float getEnchantPowerBonus(World world, int x, int y, int z)
    {
        return 15.0F;
    }

	@Override
	public void dropBlockAsItemWithChance(World world, int x, int y, int z, int meta, float chance, int fortune)
    {
        super.dropBlockAsItemWithChance(world, x, y, z, meta, chance, fortune);
        this.dropXpOnBlockBreak(world, x, y, z, 1000);
    }

    @Override
	public int quantityDroppedWithBonus(int fortune, Random rand)
    {
        return MathHelper.clamp_int(this.quantityDropped(rand) + rand.nextInt(fortune + 1), 1, 4);
    }

    @Override
	public int quantityDropped(Random rand)
    {
        return 2 + rand.nextInt(3);
    }

    @Override
	public Item getItemDropped(int meta, Random rand, int fortune)
    {
        return Items.glowstone_dust;
    }

	@Override
	@Deprecated
	public boolean hasTileEntity()
	{
		return true;
	}

	@Override
	public boolean hasTileEntity(int meta)
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(World world, int meta)
	{
		return new TileManaSpring();
	}

	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {}

}
