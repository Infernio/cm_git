package com.infernio.Craftmistry.common.block;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileThermalRegulationChamber;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.math.MathUtils;
import com.infernio.InfernoCore.multiblock.BlockMultiblock;
import com.infernio.InfernoCore.multiblock.MultiblockPart;
import com.infernio.InfernoCore.world.WorldUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockThermalRC extends BlockMultiblock {

	@SideOnly(Side.CLIENT)
	private IIcon iconIncomplete;
	@SideOnly(Side.CLIENT)
	private IIcon iconCompleteSide;
	@SideOnly(Side.CLIENT)
	private IIcon iconCompleteMiddle;

	public BlockThermalRC()
	{
		super(ModBlocks.thermalRC, Material.iron);
		this.setBlockName(StringLib.BLOCK_MULTI_BLOCK_NAME);
		this.setHardness(3.5F);
		this.setTickRandomly(true);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setStepSound(soundTypeMetal);
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		if(side == 3)
		{
			return iconCompleteMiddle;
		}
		return iconCompleteSide;
	}

	@Override
	public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(!part.clientCompleted)
			{
				return this.iconIncomplete;
			}
			boolean top = WorldUtils.tileFits(world, x, y + 1, z, part);
			boolean bottom = WorldUtils.tileFits(world, x, y - 1, z, part);
			boolean side1 = WorldUtils.tileFits(world, x + 1, y, z, part);
			boolean side2 = WorldUtils.tileFits(world, x - 1, y, z, part);
			boolean side3 = WorldUtils.tileFits(world, x, y, z + 1, part);
			boolean side4 = WorldUtils.tileFits(world, x, y, z - 1, part);
			if(top && bottom && side1 && side2 && side3 && side4)
			{
				return this.iconCompleteSide;
			}
			if(top && bottom && side1 && side2 && side3)
			{
				return this.iconCompleteMiddle;
			}
			if(top && bottom && side1 && side2 && side4)
			{
				return this.iconCompleteMiddle;
			}
			if(top && bottom && side1 && side4 && side3)
			{
				return this.iconCompleteMiddle;
			}
			if(top && bottom && side2 && side4 && side3)
			{
				return this.iconCompleteMiddle;
			}
		}
		return this.iconCompleteSide;
	}

    @Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
    	super.onBlockPlacedBy(world, x, y, z, entity, stack);
    	if(world.getBlock(x, y, z) != null)
    	{
    		TileEntity tile = world.getTileEntity(x, y, z);
    		if(tile instanceof TileThermalRegulationChamber)
    		{
    			TileThermalRegulationChamber chamber = (TileThermalRegulationChamber)tile;
    			chamber.temperature = 298;
    			chamber.targetTemperature = 298;
    		}
    	}
    }

	@SideOnly(Side.CLIENT)
    @Override
	public void randomDisplayTick(World world, int x, int y, int z, Random rand)
    {
    	TileEntity tile = world.getTileEntity(x, y, z);
    	if(tile instanceof TileThermalRegulationChamber)
    	{
    		TileThermalRegulationChamber chamber = (TileThermalRegulationChamber)tile;
    		if(!chamber.clientCompleted || MathUtils.RNG.nextInt(3) == 2)
    		{
    			return;
    		}
			boolean top = WorldUtils.tileFits(world, x, y + 1, z, chamber);
			boolean bottom = WorldUtils.tileFits(world, x, y - 1, z, chamber);
			boolean side1 = WorldUtils.tileFits(world, x + 1, y, z, chamber);
			boolean side2 = WorldUtils.tileFits(world, x - 1, y, z, chamber);
			boolean side3 = WorldUtils.tileFits(world, x, y, z + 1, chamber);
			boolean side4 = WorldUtils.tileFits(world, x, y, z - 1, chamber);
			float offset = MathUtils.RNG.nextFloat() * 0.6F - 0.3F;
			double yOffset = 0.3D + MathUtils.RNG.nextFloat() * 6.0F / 16.0F;
			if(top && bottom && side1 && side2 && side3)
			{
				world.spawnParticle("flame", x + 0.5D + offset, y + yOffset, z - 0.05F, 0.0D, 0.0D, 0.0D);
			}
			if(top && bottom && side1 && side2 && side4)
			{
				world.spawnParticle("flame", x + 0.5D + offset, y + yOffset, z + 1.0D + 0.05F, 0.0D, 0.0D, 0.0D);
			}
			if(top && bottom && side1 && side4 && side3)
			{
				world.spawnParticle("flame", x - 0.05D, y + yOffset, z + 0.5D + offset, 0.0D, 0.0D, 0.0D);
			}
			if(top && bottom && side2 && side4 && side3)
			{
				world.spawnParticle("flame", x + 1.05D, y + yOffset, z + 0.5D + offset, 0.0D, 0.0D, 0.0D);
			}
    	}
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister)
    {
    	this.iconIncomplete = iconRegister.registerIcon(StringLib.MODID + ":multiblock/ThermalRC_incomplete");
    	this.iconCompleteSide = iconRegister.registerIcon(StringLib.MODID + ":multiblock/ThermalRC_complete_side");
    	this.iconCompleteMiddle = iconRegister.registerIcon(StringLib.MODID + ":multiblock/ThermalRC_complete_middle");
    }

	@Override
	public void openGUI(EntityPlayer player, World world, int x, int y, int z)
	{
		player.openGui(Craftmistry.instance, IntLib.GUI_THERMAL_REGULATION_CHAMBER_ID, world, x, y, z);
	}

	@Override
	public TileEntity createTileEntity(World world, int meta)
	{
		this.block = ModBlocks.thermalRC;
		return new TileThermalRegulationChamber();
	}
}