package com.infernio.Craftmistry.common.block;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileTestTubeRack;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.block.BlockTile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTestTubeRack extends BlockTile {

	public BlockTestTubeRack()
	{
		super(Material.wood);
		this.setBlockName(StringLib.BLOCK_TEST_TUBE_RACK_NAME);
		this.setHardness(2.5F);
		this.setStepSound(soundTypeWood);
		this.setCreativeTab(Craftmistry.cmTab);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.planks.getIcon(side, meta);
	}

	@Override
    public boolean isOpaqueCube()
    {
        return false;
    }
	
	@Override
    public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z)
    {
		return true;
    }
	
	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int meta) 
	{
		if(!world.isRemote && world.getGameRules().getGameRuleBooleanValue("doTileDrops"))
		{
			ItemStack stack = new ItemStack(this);
			stack.setTagCompound(new NBTTagCompound());
			TileEntity tile = world.getTileEntity(x, y, z);
			if(tile instanceof TileTestTubeRack)
			{
				TileTestTubeRack tile2 = (TileTestTubeRack)tile;
				for(int i = 0; i < tile2.getSizeInventory(); ++i)
				{
					if(tile2.hasSlotTestTube(i))
					{
						int dmg = tile2.getStackInSlot(i).getItemDamage();
						stack.getTagCompound().setInteger("testTube_" + i, dmg + 1);
						stack.getTagCompound().setString("testTubeState_" + i, Item.itemRegistry.getNameForObject(((TileTestTubeRack)tile).getStackInSlot(i).getItem()));
					}
				}
			}
			world.spawnEntityInWorld(new EntityItem(world, x, y, z, stack));
			world.setBlockToAir(x, y, z);
		}
		world.setBlockToAir(x, y, z);
	}
	
	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z)
    {
		ItemStack stack = new ItemStack(this);
		stack.setTagCompound(new NBTTagCompound());
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof TileTestTubeRack)
		{
			TileTestTubeRack tile2 = (TileTestTubeRack)tile;
			for(int i = 0; i < tile2.getSizeInventory(); ++i)
			{
				if(tile2.hasSlotTestTube(i))
				{
					int dmg = tile2.getStackInSlot(i).getItemDamage();
					stack.getTagCompound().setInteger("testTube_" + i, dmg + 1);
					stack.getTagCompound().setString("testTubeState_" + i, Item.itemRegistry.getNameForObject(((TileTestTubeRack)tile).getStackInSlot(i).getItem()));
				}
			}
		}
		return stack;
    }
	
	@Override
	public Item getItemDropped(int id, Random rand, int meta)
	{
		return null;
	}
	
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
	
	@Override
	public int getRenderType()
	{
		return IntLib.TEST_TUBE_RACK_MODEL_ID;
	}
	
    @Override
    @Deprecated
    public boolean hasTileEntity()
    {
    	return true;
    }
    
    @Override
    public boolean hasTileEntity(int meta)
    {
    	return true;
    }

	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
		int facing = MathHelper.floor_double((double)(entity.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;
		if(facing == 0)
		{
			facing = 2;
		}
		else if(facing == 2)
		{
			facing = 0;
		}
		world.setBlockMetadataWithNotify(x, y, z, facing, 0);
		super.onBlockPlacedBy(world, x, y, z, entity, stack);
    }
	
	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {}

	@Override
	public void openGUI(EntityPlayer player, World world, int x, int y, int z)
	{
		player.openGui(Craftmistry.instance, IntLib.GUI_TEST_TUBE_RACK_ID, world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileTestTubeRack();
	}
}
