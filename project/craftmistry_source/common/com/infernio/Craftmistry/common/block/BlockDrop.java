package com.infernio.Craftmistry.common.block;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class BlockDrop {

	private Block replacement;
	private int replacementMeta;
	private ItemStack[] droppedItems;
	private int[] chances;

	public BlockDrop(Block replacement, int meta, ItemStack[] drops, int[] chances)
	{
		this.replacement = replacement;
		this.replacementMeta = meta;
		this.droppedItems = drops;
		this.chances = chances;
	}

	public Block getBlock()
	{
		return this.replacement;
	}

	public int getMeta()
	{
		return this.replacementMeta;
	}

	public ItemStack[] getDrops()
	{
		return this.droppedItems;
	}

	public int[] getChances()
	{
		return this.chances;
	}

	public void drop(World world, int x, int y, int z)
	{
		if(!world.isRemote)
		{
			world.setBlock(x, y, z, this.replacement, this.replacementMeta, 2);
			for(int i = 0; i < this.droppedItems.length; ++i)
			{
				if(this.droppedItems[i] != null)
				{
					if(world.rand.nextInt(100) <= this.chances[i])
					{
						world.spawnEntityInWorld(new EntityItem(world, x, y + 0.5D, z, this.droppedItems[i].copy()));
					}
				}
			}
		}
	}

}
