package com.infernio.Craftmistry.common.block;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.oredict.ShapedOreRecipe;

import com.infernio.Craftmistry.common.item.ItemCraftingComponent;
import com.infernio.Craftmistry.common.item.ItemCraftmistryOre;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.item.testtube.ItemTestTubeRack;
import com.infernio.Craftmistry.common.tile.TileLoader;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.common.registry.GameRegistry;

public class ModBlocks {

	public static Block testTubeRack;
	public static Block craftmistryOre;
	public static Block infuser;
	public static Block analyzer;
	public static Block airFilter;
	public static Block destabilizer;
	public static Block compressor;
	public static Block cleaner;
	public static Block stabilizer;
	public static Block reactor;
	public static Block breaker;
	public static Block collector;
	public static Block detector;
	public static Block mineralExtractor;
	public static Block fusionReactor;
	public static Block research;
	public static Block craftingComponent;
	public static Block manaSpring;
	public static Block thermalRC;
	public static Block electrolyser;
	public static Block forester;

	public static void init()
	{
		testTubeRack = new BlockTestTubeRack();
		craftmistryOre = new BlockCraftmistryOre();
		analyzer = new BlockAnalyzer();
		infuser = new BlockInfuser();
		airFilter = new BlockAirFilter();
		destabilizer = new BlockDestabilizer();
		compressor = new BlockCompressor();
		cleaner = new BlockCleaner();
		stabilizer = new BlockStabilizer();
		mineralExtractor = new BlockMineralExtractor();
		reactor = new BlockReactor();
		breaker = new BlockBreaker();
		collector = new BlockCollector();
		detector = new BlockDetector();
		research = new BlockMultiResearch();
		thermalRC = new BlockThermalRC();
		craftingComponent = new BlockMachineFrame();
		manaSpring = new BlockManaSpring();
		electrolyser = new BlockElectrolyser();
		forester = new BlockForester();

		GameRegistry.registerBlock(testTubeRack, ItemTestTubeRack.class, StringLib.BLOCK_TEST_TUBE_RACK_SANE_NAME);
		GameRegistry.registerBlock(craftmistryOre, ItemCraftmistryOre.class, StringLib.BLOCK_CRAFTMISTRY_ORE_SANE_NAME);
		GameRegistry.registerBlock(infuser, StringLib.BLOCK_INFUSER_SANE_NAME);
		GameRegistry.registerBlock(analyzer, StringLib.BLOCK_ANALYZER_SANE_NAME);
		GameRegistry.registerBlock(airFilter, StringLib.BLOCK_AIR_FILTER_SANE_NAME);
		GameRegistry.registerBlock(destabilizer, StringLib.BLOCK_DESTABILIZER_SANE_NAME);
		GameRegistry.registerBlock(compressor, StringLib.BLOCK_COMPRESSOR_SANE_NAME);
		GameRegistry.registerBlock(cleaner, StringLib.BLOCK_CLEANER_SANE_NAME);
		GameRegistry.registerBlock(stabilizer, StringLib.BLOCK_STABILIZER_SANE_NAME);
		GameRegistry.registerBlock(mineralExtractor, StringLib.BLOCK_MINERAL_EXTRACTOR_SANE_NAME);
		GameRegistry.registerBlock(reactor, StringLib.BLOCK_REACTOR_SANE_NAME);
		GameRegistry.registerBlock(breaker, StringLib.BLOCK_BREAKER_SANE_NAME);
		GameRegistry.registerBlock(collector, StringLib.BLOCK_COLLECTOR_SANE_NAME);
		GameRegistry.registerBlock(detector, StringLib.BLOCK_DETECTOR_SANE_NAME);
		GameRegistry.registerBlock(research, StringLib.BLOCK_RESEARCH_MULTI_SANE_NAME);
		GameRegistry.registerBlock(manaSpring, StringLib.BLOCK_MANA_SPRING_SANE_NAME);
		GameRegistry.registerBlock(thermalRC, StringLib.BLOCK_MULTI_BLOCK_SANE_NAME);
		GameRegistry.registerBlock(craftingComponent, ItemCraftingComponent.class, StringLib.BLOCK_CRAFTING_COMPONENT_SANE_NAME);
		GameRegistry.registerBlock(electrolyser, StringLib.BLOCK_ELECTROLYSER_SANE_NAME);
		GameRegistry.registerBlock(forester, StringLib.BLOCK_FORESTER_SANE_NAME);

		TileLoader.init();

		addRecipes();
	}

	private static void addRecipes()
	{
		//Crafting components
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(craftingComponent, 1, 0), new Object[]{
			"ICI","PIP","PPP",'P',new ItemStack(ModItems.multiItem, 1, 11),'I',Blocks.iron_bars,'C',new ItemStack(ModItems.multiItem, 1, 17)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(craftingComponent, 1, 1), new Object[]{
			"IPI","PIP","PPP",'P',new ItemStack(ModItems.multiItem, 1, 11),'I',Blocks.iron_bars
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(craftingComponent, 1, 2), new Object[]{
			"IPI","PIP","PPP",'P',new ItemStack(ModItems.multiItem, 1, 12),'I',Blocks.iron_bars
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(craftingComponent, 1, 3), new Object[]{
			"IPI","PIP","PPP",'P',new ItemStack(ModItems.multiItem, 1, 13),'I',Blocks.iron_bars
		}));

		//Machines etc.
		ItemStack ttRack = new ItemStack(testTubeRack);
		ttRack.setTagCompound(new NBTTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(ttRack, new Object[]{
			"SSS","SSS","PPP",'S',Items.stick,'P',"plankWood"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(analyzer), new Object[]{
			"GCG","IBI","TIT",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'T',"ingotTin",'I',Blocks.iron_bars,'G',Blocks.glass
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(infuser), new Object[]{
			"ICI","ABA","NSN",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'N',"ingotCopper",'I',Blocks.iron_bars,'A',"ingotAluminium",'S',"ingotSilver"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(airFilter), new Object[]{
			"ICI","ABA","IAI",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'I',Items.iron_ingot,'A',"ingotAluminium"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(destabilizer), new Object[]{
			"SCS","SBS","AIA",'I',new ItemStack(ModItems.multiItem, 1, 17),'B',new ItemStack(craftingComponent, 1, 2),'C',new ItemStack(ModItems.multiItem, 1, 15),'A',"ingotCopper",'S',new ItemStack(ModItems.testTubeCompound, 1, 5)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(compressor), new Object[]{
			"ACA","ABA","GSG",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'S',"ingotSilver",'G',Blocks.glass,'A',"ingotAluminium"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(cleaner), new Object[]{
			"ICI","NBN","INI",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'N',Items.iron_ingot,'I',"ingotCopper"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(stabilizer), new Object[]{
			"SCS","SBS","AIA",'I',new ItemStack(ModItems.multiItem, 1, 17),'B',new ItemStack(craftingComponent, 1, 2),'C',new ItemStack(ModItems.multiItem, 1, 15),'A',"ingotAluminium",'S',new ItemStack(ModItems.testTubeCompound, 1, 5)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(reactor), new Object[]{
			"ACA","IBI","AFA",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'A',"ingotAluminium",'I',Blocks.iron_block,'F',Blocks.iron_bars
		}));
		//FIXME Instead of ender pearls, use wireless crafting components
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(breaker), new Object[]{
			"PCP","DBD","PEP",'E',Items.ender_pearl,'C',new ItemStack(ModItems.multiItem, 1, 15),'B',new ItemStack(craftingComponent, 1, 2),'P',Items.iron_pickaxe,'D',new ItemStack(ModItems.multiItem, 1, 20)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(collector), new Object[]{
			"ECE","NBN","EWE",'W',Blocks.chest,'N',Items.ender_eye,'E',Items.ender_pearl,'B',new ItemStack(craftingComponent, 1, 2),'C',new ItemStack(ModItems.multiItem, 1, 15)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(detector), new Object[]{
			"SCS","GBG","III",'C',new ItemStack(ModItems.multiItem, 1, 15),'B',new ItemStack(craftingComponent, 1, 2),'S',"ingotSilver",'G',Blocks.glass,'I',Items.iron_ingot
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(thermalRC), new Object[]{
			"TCT","SBS","TDT",'T',"ingotTin",'S',new ItemStack(ModItems.testTubeCompound, 1, 5),'D',new ItemStack(ModItems.multiItem, 1, 20),'B',new ItemStack(craftingComponent, 1, 0),'C',new ItemStack(ModItems.multiItem, 1, 15)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(mineralExtractor), new Object[]{
			"GCG","ABA","AUA",'A',"ingotAluminium",'G',Blocks.glass,'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'U',Items.bucket
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(electrolyser), new Object[]{
			"UCU","GBG","TIT",'C',new ItemStack(ModItems.multiItem, 1, 14),'B',new ItemStack(craftingComponent, 1, 0),'U',Items.water_bucket,'G',Blocks.glass,'T',"ingotTin",'I',Items.iron_ingot
		}));

		//Smelting
		FurnaceRecipes.smelting().func_151394_a(new ItemStack(craftmistryOre, 1, 1), new ItemStack(ModItems.ingot, 1, 0), 0.5F);
		FurnaceRecipes.smelting().func_151394_a(new ItemStack(craftmistryOre, 1, 2), new ItemStack(ModItems.ingot, 1, 1), 0.5F);
		FurnaceRecipes.smelting().func_151394_a(new ItemStack(craftmistryOre, 1, 3), new ItemStack(ModItems.ingot, 1, 2), 0.5F);
		FurnaceRecipes.smelting().func_151394_a(new ItemStack(craftmistryOre, 1, 4), new ItemStack(ModItems.ingot, 1, 3), 0.5F);
	}

}
