package com.infernio.Craftmistry.common.block;

import java.util.List;
import java.util.Locale;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import com.infernio.Craftmistry.common.block.predefined.BlockCM;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockCraftmistryOre extends BlockCM {

	@SideOnly(Side.CLIENT)
	private IIcon[] icons;

	public BlockCraftmistryOre()
	{
		super(Material.rock);
		this.setBlockName(StringLib.BLOCK_CRAFTMISTRY_ORE_NAME);
		this.setHardness(3.0F);
		this.setResistance(5.0F);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < EnumOre.values().length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		if(this.icons == null)
		{
			return null;
		}
		return icons[meta];
	}
	
	@Override
	public int damageDropped(int meta)
	{
		return meta;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		int length = EnumOre.values().length;
		icons = new IIcon[length];
		for(int i = 0; i < length; ++i)
		{
			icons[i] = iconRegister.registerIcon(StringLib.MODID.toLowerCase(Locale.ENGLISH) + ":" + StringLib.BLOCK_CRAFTMISTRY_ORE_NAME + "." + EnumOre.values()[i].name);
		}
	}

	public static enum EnumOre {

		SULFUR("sulfur"),
		COPPER("copper"),
		TIN("tin"),
		SILVER("silver"),
		ALUMINIUM("aluminium"),
		COBALT("cobalt"),
		SALT("salt"),
		BERYL("beryl"),
		FLUORITE("fluorite");

		public final String name;

		private EnumOre(String name)
		{
			this.name = name;
		}
	}

}
