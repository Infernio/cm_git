package com.infernio.Craftmistry.common.block;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileStabilizer;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.block.BlockTile;

public class BlockStabilizer extends BlockTile {

	public BlockStabilizer()
	{
		super(Material.rock);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setHardness(3.5F);
		this.setBlockName(StringLib.BLOCK_STABILIZER_NAME);
	}

	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileStabilizer)
		{
			((TileStabilizer)tile).amplifier = 1;
	    }
    }

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.iron_block.getIcon(side, meta);
	}

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {}

	@Override
	public void openGUI(EntityPlayer player, World world, int x, int y, int z)
	{
		player.openGui(Craftmistry.instance, IntLib.GUI_STABILIZER_ID, world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileStabilizer();
	}

}
