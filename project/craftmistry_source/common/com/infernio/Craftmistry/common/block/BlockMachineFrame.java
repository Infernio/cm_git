package com.infernio.Craftmistry.common.block;

import java.util.List;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import com.infernio.Craftmistry.common.block.predefined.BlockCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class BlockMachineFrame extends BlockCM {

	public static final String[] names = new String[]
	{
		"blockCMChemicalMachineFrame",
		"blockCMBasicMachineFrame",
		"blockCMHardenedMachineFrame",
		"blockCMAdvancedMachineFrame"
	};

	private IIcon[] icons;

	public BlockMachineFrame()
	{
		super(Material.rock);
		this.setBlockName(StringLib.BLOCK_CRAFTING_COMPONENT_NAME);
		this.setHardness(3.5F);
		this.setResistance(10.0F);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List list)
    {
    	for(int meta = 0; meta < names.length; ++meta)
    	{
    		list.add(new ItemStack(item, 1, meta));
    	}
    }

	@Override
	public int damageDropped(int meta)
	{
		return meta;
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return this.icons[meta];
	}

	@Override
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		icons = new IIcon[names.length];
		for(int i = 0; i < names.length; ++i)
		{
			icons[i] = iconRegister.registerIcon(StringLib.MODID + ":" + names[i]);
		}
	}

}
