package com.infernio.Craftmistry.common.block;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileInfuser;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.block.BlockTile;

public class BlockInfuser extends BlockTile {

	public BlockInfuser()
	{
		super(Material.rock);
		this.setBlockName(StringLib.BLOCK_INFUSER_NAME);
		this.setHardness(3.5F);
		this.setCreativeTab(Craftmistry.cmTab);
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.iron_block.getIcon(side, meta);
	}

    @Override
    public void registerBlockIcons(IIconRegister iconRegister) {}

	@Override
	public void openGUI(EntityPlayer player, World world, int x, int y, int z)
	{
		player.openGui(Craftmistry.instance, IntLib.GUI_INFUSER_ID, world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileInfuser();
	}
}
