package com.infernio.Craftmistry.common.block.predefined;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockCM extends Block {
	
	protected Random rand = new Random();

	public BlockCM(Material material)
	{
		super(material);
		this.setCreativeTab(Craftmistry.cmTab);
	}
	
	@Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) 
	{
        this.blockIcon = iconRegister.registerIcon(StringLib.MODID + ":" + this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1));
    }

}
