package com.infernio.Craftmistry.common.research;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.CraftmistryAPI;
import com.infernio.Craftmistry.api.research.EnumProjectSize;
import com.infernio.Craftmistry.api.research.IResearchRegistry;
import com.infernio.Craftmistry.api.research.tech.Technology;
import com.infernio.InfernoCore.math.ArrayUtils;
import com.infernio.InfernoCore.math.Vector2D;

//TODO Completely rewrite this. Instead of one integer, use a class with equals and hashCode.
public class ResearchRegistry implements IResearchRegistry {

	private static ResearchRegistry instance = new ResearchRegistry();

	private HashMap<Integer, Integer> primaryWeights = new HashMap<Integer, Integer>();
	private HashMap<Integer, Integer> secondaryWeights = new HashMap<Integer, Integer>();
	private HashMap<Vector2D, ItemStack> products = new HashMap<Vector2D, ItemStack>();
	private HashMap<Vector2D, Integer> requirements = new HashMap<Vector2D, Integer>();
	private HashMap<Vector2D, Integer> techRequirements = new HashMap<Vector2D, Integer>();
	private HashMap<Vector2D, Technology> techInfo = new HashMap<Vector2D, Technology>();
	private HashMap<String, Class<? extends Technology>> techClasses = new HashMap<String, Class<? extends Technology>>();
	private Random rand = new Random();

	private ResearchRegistry()
	{
		CraftmistryAPI.Research.registry = this;
	}

	public static void registerItem(ItemStack stack, int weight1, int weight2)
	{
		instance.registerWeights(stack, weight1, weight2);
	}

	public static void registerProduct(ItemStack stack, EnumProjectSize minimum, int weight1, int weight2)
	{
		instance.addProduct(stack, minimum, weight1, weight2);
	}

	public static void registerTech(Technology tech, EnumProjectSize minimum, int weight1, int weight2)
	{
		instance.registerTechnology(tech, minimum, weight1, weight2);
	}

	public static int getPrimary(ItemStack stack)
	{
		return instance.calculatePrimary(stack);
	}

	public static int getSecondary(ItemStack stack)
	{
		return instance.calculateSecondary(stack);
	}

	public static ItemStack findProduct(int x, int y, EnumProjectSize size)
	{
		return instance.getProductFor(x, y, size);
	}

	public static Technology findTech(int x, int y, EnumProjectSize size)
	{
		return instance.getTechnologyFor(x, y, size);
	}

	public static Class<? extends Technology> getClassFromString(String classID)
	{
		return instance.findClassFromString(classID);
	}

	@Override
	public int calculatePrimary(ItemStack stack)
	{
		if(stack == null || stack.getItem() == null)
		{
			return 0;
		}
		int key = this.calculateKey(stack);
		Integer ret = this.primaryWeights.get(Integer.valueOf(key));
		if(ret != null)
		{
			return ret.intValue();
		}
		return 0;
	}

	@Override
	public int calculateSecondary(ItemStack stack)
	{
		if(stack == null || stack.getItem() == null)
		{
			return 0;
		}
		int key = this.calculateKey(stack);
		Integer ret = this.secondaryWeights.get(Integer.valueOf(key));
		if(ret != null)
		{
			return ret.intValue();
		}
		return 0;
	}

	private int calculateKey(ItemStack stack)
	{
		int ret = 0;
		ret += 31 * Item.getIdFromItem(stack.getItem());
		ret += 31 * stack.getItemDamage();
		ret += 31 * stack.stackSize;
		if(stack.stackTagCompound != null)
		{
			ret += 31 * stack.stackTagCompound.hashCode();
		}
		return ret;
	}

	@Override
	public void registerWeights(ItemStack stack, int weight1, int weight2)
	{
		if(stack != null && stack.getItem() != null)
		{
			int key = this.calculateKey(stack);
			if(!this.primaryWeights.containsKey(key) && !this.secondaryWeights.containsKey(key))
			{
				this.primaryWeights.put(key, Integer.valueOf(weight1));
				this.secondaryWeights.put(key, Integer.valueOf(weight2));
			}
		}
	}

	@Override
	public void addProduct(ItemStack stack, EnumProjectSize minimum, int weight1, int weight2)
	{
		Vector2D key = new Vector2D(weight1, weight2);
		if(!products.containsKey(key) && !requirements.containsKey(key))
		{
			products.put(key, stack);
			requirements.put(key, Integer.valueOf(minimum.ordinal()));
		}
	}

	@Override
	public void registerTechnology(Technology tech, EnumProjectSize minimum, int weight1, int weight2)
	{
		Vector2D key = new Vector2D(weight1, weight2);
		if(!techInfo.containsKey(key) && !techRequirements.containsKey(key))
		{
			techInfo.put(key, tech);
			techRequirements.put(key, Integer.valueOf(minimum.ordinal()));
			if(!techClasses.containsKey(tech.classID))
			{
				techClasses.put(tech.classID, tech.techClass);
			}
		}
	}

	@Override
	public ItemStack getProductFor(int x, int y, EnumProjectSize size)
	{
		Vector2D key = new Vector2D(x, y);
		int projectSize = size.ordinal();
		Integer required = this.requirements.get(key);
		if(required != null && required.intValue() <= projectSize)
		{
			return this.products.get(key);
		}
		boolean[] tried = new boolean[14];
		Arrays.fill(tried, false);
		while(ArrayUtils.contains(tried, false))
		{
			int random = this.rand.nextInt(14);
			if(random > 6)
			{
				if(!tried[random])
				{
					tried[random] = true;
					random -= 10;
					Vector2D newKey = new Vector2D(key.x, key.y + random);
					required = this.requirements.get(newKey);
					if(required != null && required.intValue() <= projectSize)
					{
						if(this.rand.nextInt(5) != 1)
						{
							return this.products.get(newKey);
						}
					}
					continue;
				}
				else
				{
					continue;
				}
			}
			else
			{
				if(!tried[random])
				{
					tried[random] = true;
					random -= 3;
					Vector2D newKey = new Vector2D(key.x + random, key.y);
					required = this.requirements.get(newKey);
					if(required != null && required.intValue() <= projectSize)
					{
						if(this.rand.nextInt(5) != 1)
						{
							return this.products.get(newKey);
						}
					}
					continue;
				}
				else
				{
					continue;
				}
			}
		}
		return null;
	}

	@Override
	public Technology getTechnologyFor(int x, int y, EnumProjectSize size)
	{
		Vector2D key = new Vector2D(x, y);
		int projectSize = size.ordinal();
		Integer required = this.techRequirements.get(key);
		if(required != null && required.intValue() <= projectSize)
		{
			return this.techInfo.get(key);
		}
		boolean[] tried = new boolean[14];
		Arrays.fill(tried, false);
		while(ArrayUtils.contains(tried, false))
		{
			int random = this.rand.nextInt(14);
			if(random > 6)
			{
				if(!tried[random])
				{
					tried[random] = true;
					random -= 10;
					Vector2D newKey = new Vector2D(key.x, key.y + random);
					required = this.techRequirements.get(newKey);
					if(required != null && required.intValue() <= projectSize)
					{
						if(this.rand.nextInt(5) != 1)
						{
							return this.techInfo.get(newKey);
						}
					}
					continue;
				}
				else
				{
					continue;
				}
			}
			else
			{
				if(!tried[random])
				{
					tried[random] = true;
					random -= 3;
					Vector2D newKey = new Vector2D(key.x + random, key.y);
					required = this.techRequirements.get(newKey);
					if(required != null && required.intValue() <= projectSize)
					{
						if(this.rand.nextInt(5) != 1)
						{
							return this.techInfo.get(newKey);
						}
					}
					continue;
				}
				else
				{
					continue;
				}
			}
		}
		return null;
	}

	@Override
	public Class<? extends Technology> findClassFromString(String classID)
	{
		Class<? extends Technology> ret = this.techClasses.get(classID);
		if(ret == null)
		{
			return Technology.class;
		}
		return ret;
	}

}
