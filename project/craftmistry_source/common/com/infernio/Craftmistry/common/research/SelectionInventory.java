package com.infernio.Craftmistry.common.research;

import java.util.Arrays;
import java.util.List;

import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.util.NBTLib;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class SelectionInventory implements IInventory {

	private ItemStack[] inv;

	public SelectionInventory(int size)
	{
		this.inv = new ItemStack[size];
	}

	public void increaseSize(int amount)
	{
		ItemStack[] newInv = new ItemStack[this.inv.length + amount];
		Arrays.fill(newInv, null);
		System.arraycopy(this.inv, 0, newInv, 0, this.inv.length);
		this.inv = newInv;
	}

	public List<ItemStack> asList()
	{
		return Arrays.asList(this.inv);
	}

	@Override
	public int getSizeInventory()
	{
		return this.inv.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return this.inv[slot];
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount)
	{
		ItemStack stack = getStackInSlot(slot);
		if (stack != null) 
		{
			if(stack.stackSize <= amount) 
			{
				this.setInventorySlotContents(slot, null);
			}
			else
			{
				stack = stack.splitStack(amount);
				if(stack.stackSize <= 0)
				{
					this.setInventorySlotContents(slot, null);
				}
			}
		}
		return stack;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		ItemStack stack = getStackInSlot(slot);
    	if(stack != null)
    	{
    		setInventorySlotContents(slot, null);
    	}
    	return stack;
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack)
	{
		this.inv[slot] = stack;
	}

	@Override
	public String getInventoryName()
	{
		return StringLib.CONTAINER_SELECTION_INVENTORY_NAME;
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return false;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer entityplayer)
	{
		return true;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack stack)
	{
		return true;
	}

	public void writeToNBT(NBTTagCompound tag)
	{
		NBTTagList list = new NBTTagList();
		for(int i = 0; i < inv.length; i++)
		{
			ItemStack stack = inv[i];
			if(stack != null)
			{
				NBTTagCompound slot = new NBTTagCompound();
				slot.setByte("Slot", (byte)i);
				stack.writeToNBT(slot);
				list.appendTag(slot);
			}
		}
		tag.setTag("Inventory", list);
	}

	public void readFromNBT(NBTTagCompound tag)
	{
		NBTTagList list = tag.getTagList("Inventory", NBTLib.NBT_COMPOUND);
		for(int i = 0; i < list.tagCount(); i++)
		{
			NBTTagCompound slot = list.getCompoundTagAt(i);
			byte b = slot.getByte("Slot");
			if(b >= 0 && b < inv.length)
			{
				inv[b] = ItemStack.loadItemStackFromNBT(slot);
			}
		}
	}

	@Override
	public void markDirty() {}
}