package com.infernio.Craftmistry.common.research;

import com.infernio.Craftmistry.api.research.EnumProjectSize;
import com.infernio.Craftmistry.common.research.tech.TechnologyInfo;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class ResearchProduct {

	private TechnologyInfo tech;
	private int x;
	private int y;
	private ItemStack product;
	private EnumProjectSize size;

	public ResearchProduct() {}

	public ResearchProduct(int first, int second, EnumProjectSize projectSize)
	{
		this.x = first;
		this.y = second;
		this.size = projectSize;
		this.product = ResearchRegistry.findProduct(this.x, this.y, projectSize);
		this.tech = new TechnologyInfo();
		this.tech.setTech(ResearchRegistry.findTech(this.x, this.y, projectSize));
	}

	public ItemStack getProduct()
	{
		return this.product;
	}

	public EnumProjectSize getSize()
	{
		return this.size;
	}

	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setInteger("XWeight", x);
		tag.setInteger("YWeight", y);
		if(this.product != null)
		{
			tag.setTag("Product", new NBTTagCompound());
			this.product.writeToNBT(tag.getCompoundTag("Product"));
		}
		if(this.tech != null)
		{
			tag.setTag("TechInfo", new NBTTagCompound());
			this.tech.writeToNBT(tag.getCompoundTag("TechInfo"));
		}
	}

	public void readFromNBT(NBTTagCompound tag)
	{
		this.x = tag.getInteger("XWeight");
		this.y = tag.getInteger("YWeight");
		this.product = null;
		if(tag.hasKey("Product"))
		{
			this.product = ItemStack.loadItemStackFromNBT(tag.getCompoundTag("Product"));
		}
		if(tag.hasKey("TechInfo"))
		{
			this.tech = new TechnologyInfo();
			this.tech.readFromNBT(tag.getCompoundTag("TechInfo"));
		}
	}

}
