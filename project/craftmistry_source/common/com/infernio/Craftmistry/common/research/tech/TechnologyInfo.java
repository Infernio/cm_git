package com.infernio.Craftmistry.common.research.tech;

import java.lang.reflect.Constructor;

import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.api.research.tech.Technology;
import com.infernio.Craftmistry.common.research.ResearchRegistry;
import com.infernio.Craftmistry.core.logging.CMLogger;

public class TechnologyInfo {

	private Technology tech;

	public Technology getTech()
	{
		return this.tech;
	}

	public void setTech(Technology tech)
	{
		this.tech = tech;
	}

	public void writeToNBT(NBTTagCompound tag)
	{
		if(this.tech != null)
		{
			tag.setTag("Requirements", new NBTTagCompound());
			this.saveRequirements(this.tech.requirements, tag.getCompoundTag("Requirements"));
			tag.setString("Name", this.tech.name);
			tag.setByte("Level", (byte)this.tech.level);
			tag.setString("ClassID", this.tech.classID);
		}
	}

	public void readFromNBT(NBTTagCompound tag)
	{
		String name = tag.getString("Name");
		int level = tag.getInteger("Level");
		String classID = tag.getString("ClassID");
		Class<? extends Technology> clazz = ResearchRegistry.getClassFromString(classID);
		Technology[] requirements = this.loadRequirements(tag.getCompoundTag("Requirements"));
		try
		{
			Constructor<? extends Technology> ctr = clazz.getConstructor(String.class, int.class, String.class, Class.class, Technology[].class);
			this.tech = ctr.newInstance(name, level, classID, clazz, requirements);
		}
		catch(Throwable e)
		{
			CMLogger.warning("Failed to restore tech information from NBT! This is a severe bug.");
			e.printStackTrace();
			//TODO Is this required?
			this.tech = new Technology(name, level, classID, clazz, requirements);
		}
	}

	private void saveRequirements(Technology[] requirements, NBTTagCompound tag)
	{
		for(int i = 0; i < requirements.length; ++i)
		{
			Technology requirement = requirements[i];
			if(requirement != null)
			{
				NBTTagCompound tag2 = new NBTTagCompound();
				tag2.setString("Name", requirement.name);
				tag2.setByte("Level", (byte)requirement.level);
				tag2.setString("ClassID", requirement.classID);
				if(requirement.requirements != null)
				{
					tag2.setByte("RequirementsLength", (byte)requirement.requirements.length);
					tag2.setTag("Requirements", new NBTTagCompound());
					this.saveRequirements(requirement.requirements, tag2.getCompoundTag("Requirements"));
				}
				tag.setTag("Requirement_" + i, tag2);
			}
		}
	}

	private Technology[] loadRequirements(NBTTagCompound tag)
	{
		int length = tag.getByte("RequirementsLength");
		Technology[] ret = new Technology[length];
		for(int i = 0; i < length; ++i)
		{
			NBTTagCompound requirement = tag.getCompoundTag("Requirement_" + i);
			String name = requirement.getString("Name");
			int level = requirement.getByte("Level");
			String classID = requirement.getString("ClassID");
			Technology[] requirements = null;
			if(requirement.hasKey("Requirements"))
			{
				requirements = this.loadRequirements(requirement.getCompoundTag("Requirements"));
			}
			Class<? extends Technology> clazz = ResearchRegistry.getClassFromString(classID);
			try
			{
				Constructor<? extends Technology> ctr = clazz.getConstructor(String.class, int.class, String.class, Class.class, Technology[].class);
				ret[i] = ctr.newInstance(name, level, classID, clazz, requirements);
			}
			catch(Throwable e)
			{
				CMLogger.warning("Failed to restore tech information from NBT! This is a severe bug.");
				e.printStackTrace();
				//TODO Is this required?
				ret[i] = new Technology(name, level, classID, clazz, requirements);
			}
		}
		return ret;
	}

}
