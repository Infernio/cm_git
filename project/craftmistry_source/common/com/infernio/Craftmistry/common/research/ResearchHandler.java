package com.infernio.Craftmistry.common.research;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.CraftmistryAPI;
import com.infernio.Craftmistry.api.research.EnumProjectSize;
import com.infernio.Craftmistry.api.research.tech.ITechnologyList;
import com.infernio.Craftmistry.api.research.tech.Technology;
import com.infernio.Craftmistry.common.research.tech.TechTest;

public class ResearchHandler implements ITechnologyList {

	private static List<Technology> techList = new ArrayList<Technology>();
	private static HashMap<String, Technology> techMap = new HashMap<String, Technology>();

	private static ResearchHandler instance = new ResearchHandler();

	private ResearchHandler() {}

	public static void init()
	{
		CraftmistryAPI.Research.techList = instance;

		//TODO Register default research here

		//Internal way
		ResearchRegistry.registerItem(new ItemStack(Blocks.stone), 2, 4);
		ResearchRegistry.registerItem(new ItemStack(Blocks.cobblestone), 4, 2);
		//Stone + Cobblestone -> Mossy Cobblestone
		ResearchRegistry.registerProduct(new ItemStack(Blocks.mossy_cobblestone), EnumProjectSize.SMALL, 3, 3);
		//Also, there is a chance that you can get Test Tech Lvl 1 from that
		Technology testTech1 = new TechTest("Test Tech Lvl 1", 1);
		instance.addTechnology(testTech1);
		ResearchRegistry.registerTech(testTech1, EnumProjectSize.SMALL, 3, 4);

		//API way
		CraftmistryAPI.Research.registry.registerWeights(new ItemStack(Items.diamond), 105, 35);
		CraftmistryAPI.Research.registry.registerWeights(new ItemStack(Items.apple), 95, 35);
		CraftmistryAPI.Research.registry.registerWeights(new ItemStack(Items.arrow), 100, 25);
		//Diamond + Apple + Arrow -> Diamond Axe
		CraftmistryAPI.Research.registry.addProduct(new ItemStack(Items.diamond_axe), EnumProjectSize.MEDIUM, 100, 32);
		//Also, you receive Test Tech Lvl 2 for that
		Technology testTech2 = new TechTest("Test Tech Lvl 2", 2, testTech1);
		instance.addTechnology(testTech2);
		CraftmistryAPI.Research.registry.registerTechnology(testTech2, EnumProjectSize.MEDIUM, 100, 32);
	}

	@Override
	public List<Technology> getTechnologies()
	{
		return techList;
	}

	@Override
	public Technology getTechnology(String tech)
	{
		return techMap.get(tech);
	}

	@Override
	public void addTechnology(Technology tech)
	{
		if(!techList.contains(tech) && !techMap.containsKey(tech))
		{
			techList.add(tech);
			techMap.put(tech.name, tech);
		}
	}

}
