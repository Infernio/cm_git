package com.infernio.Craftmistry.common.research;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.tile.IResearchTile;
import com.infernio.Craftmistry.common.tile.TileComputer;
import com.infernio.Craftmistry.common.tile.TileEnergyStorage;
import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;
import com.infernio.Craftmistry.common.tile.TileProductionFacility;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;

public class ResearchNetwork {

	private HashMap<String, IResearchTile> network;
	private World world;
	private int x;
	private int y;
	private int z;

	public ResearchNetwork(World world, int x, int y, int z)
	{
		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.initMap();
	}

	public void setWorld(World world)
	{
		this.world = world;
	}

	public void setCoordinates(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void build()
	{
		ArrayList<IResearchTile> discovered = new ArrayList<IResearchTile>();
		TileEntity tile = this.world.getTileEntity(x + 1, y, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x - 1, y, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y + 1, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y - 1, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y, z + 1);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y, z - 1);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		for(IResearchTile tile2 : discovered)
		{
			TileEntity tile3 = (TileEntity)tile2;
			this.explore(tile3.xCoord, tile3.yCoord, tile3.zCoord);
		}
	}

	public void clear()
	{
		TileResearchPanel panel = this.getPanel();
		this.network.clear();
		this.initMap();
		this.addTile(panel);
	}

	public void cleanReferences()
	{
		if(this.hasPanel())
		{
			this.getPanel().setResearchNetwork(null);
		}
		if(this.hasComputer())
		{
			this.getComputer().setResearchNetwork(null);
		}
		if(this.hasMaterialCompartment())
		{
			this.getMaterialCompartment().setResearchNetwork(null);
		}
		if(this.hasEnergyStorage())
		{
			this.getEnergyStorage().setResearchNetwork(null);
		}
		if(this.hasProductionFacility())
		{
			this.getProductionFacility().setResearchNetwork(null);
		}
	}

	public void refresh()
	{
		if(this.hasPanel())
		{
			this.getPanel().setResearchNetwork(this);
		}
		if(this.hasComputer())
		{
			this.getComputer().setResearchNetwork(this);
		}
		if(this.hasMaterialCompartment())
		{
			this.getMaterialCompartment().setResearchNetwork(this);
		}
		if(this.hasEnergyStorage())
		{
			this.getEnergyStorage().setResearchNetwork(this);
		}
		if(this.hasProductionFacility())
		{
			this.getProductionFacility().setResearchNetwork(this);
		}
	}

	public void addTile(IResearchTile tile)
	{
		if(tile != null)
		{
			String type = tile.getType();
			if(this.isValid(type))
			{
				tile.setResearchNetwork(this);
				this.network.put(type, tile);
			}
		}
	}

	public void removeTile(IResearchTile tile)
	{
		if(tile != null)
		{
			String type = tile.getType();
			if(this.isValid(type))
			{
				this.network.put(type, null);
			}
		}
	}

	public void removeTile(String type)
	{
		if(this.isValid(type))
		{
			this.network.put(type, null);
		}
	}

	public IResearchTile get(String type)
	{
		if(this.isValid(type))
		{
			return this.network.get(type);
		}
		return null;
	}

	public boolean has(String type)
	{
		if(this.isValid(type))
		{
			return this.network.get(type) != null;
		}
		return false;
	}

	public TileResearchPanel getPanel()
	{
		return (TileResearchPanel)this.network.get("Panel");
	}

	public TileComputer getComputer()
	{
		return (TileComputer)this.network.get("Computer");
	}

	public TileMaterialCompartment getMaterialCompartment()
	{
		return (TileMaterialCompartment)this.network.get("MaterialCompartment");
	}

	public TileEnergyStorage getEnergyStorage()
	{
		return (TileEnergyStorage)this.network.get("EnergyStorage");
	}

	public TileProductionFacility getProductionFacility()
	{
		return (TileProductionFacility)this.network.get("ProductionFacility");
	}

	public boolean hasPanel()
	{
		return this.has("Panel");
	}

	public boolean hasComputer()
	{
		return this.has("Computer");
	}

	public boolean hasMaterialCompartment()
	{
		return this.has("MaterialCompartment");
	}

	public boolean hasEnergyStorage()
	{
		return this.has("EnergyStorage");
	}

	public boolean hasProductionFacility()
	{
		return this.has("ProductionFacility");
	}

	public boolean isMultiBlockComplete()
	{
		if(!this.hasPanel() || !this.hasComputer() || !this.hasEnergyStorage() || !this.hasMaterialCompartment())
		{
			return false;
		}
		return true;
	}

	public ArrayList<String> getMissingBlocks()
	{
		ArrayList<String> ret = new ArrayList<String>();
		if(!this.hasPanel())
		{
			ret.add("researchPanel");
		}
		if(!this.hasComputer())
		{
			ret.add("computer");
		}
		if(!this.hasEnergyStorage())
		{
			ret.add("energyStorage");
		}
		if(!this.hasMaterialCompartment())
		{
			ret.add("materialCompartment");
		}
		return ret;
	}


	public void broadcastResearchFinished(ResearchProduct product)
	{
		if(this.hasComputer())
		{
			this.getComputer().onProjectFinished(product);
		}
		if(this.hasEnergyStorage())
		{
			this.getEnergyStorage().onProjectFinished(product);
		}
		if(this.hasMaterialCompartment())
		{
			this.getMaterialCompartment().onProjectFinished(product);
		}
		if(this.hasProductionFacility())
		{
			this.getProductionFacility().onProjectFinished(product);
		}
	}

	private void initMap()
	{
		this.network = new HashMap<String, IResearchTile>(6);
		this.network.put("Panel", null);
		this.network.put("Computer", null);
		this.network.put("MaterialCompartment", null);
		this.network.put("EnergyStorage", null);
		this.network.put("ProductionFacility", null);
	}

	private void explore(int x, int y, int z)
	{
		ArrayList<IResearchTile> discovered = new ArrayList<IResearchTile>();
		TileEntity tile = this.world.getTileEntity(x + 1, y, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x - 1, y, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y + 1, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y - 1, z);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y, z + 1);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		tile = this.world.getTileEntity(x, y, z - 1);
		if(tile != null && tile instanceof IResearchTile)
		{
			String type = ((IResearchTile)tile).getType();
			if(this.isValid(type) && !this.has(type))
			{
				ResearchNetwork network = ((IResearchTile)tile).getNetwork();
				if(network == null || this.equals(network))
				{
					discovered.add((IResearchTile)tile);
					this.addTile((IResearchTile)tile);
				}
			}
		}
		for(IResearchTile tile2 : discovered)
		{
			TileEntity tile3 = (TileEntity)tile2;
			this.explore(tile3.xCoord, tile3.yCoord, tile3.zCoord);
		}
	}

	private boolean isValid(String type)
	{
		return type.equals("Panel") || type.equals("Computer") || type.equals("MaterialCompartment") || type.equals("EnergyStorage") || type.equals("ProductionFacility") ? true : false;
	}

}
