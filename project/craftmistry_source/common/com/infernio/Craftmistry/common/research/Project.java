package com.infernio.Craftmistry.common.research;

import net.minecraft.nbt.NBTTagCompound;

public class Project {

	private ProjectInfo storedInfo;
	private String name;
	private long progress;
	private long progressFinished;
	private boolean initialized;

	public Project(String projectName)
	{
		this.initialized = false;
		this.name = projectName;
		this.progress = 0;
	}

	public void setInfo(ProjectInfo info)
	{
		this.storedInfo = info;
	}

	public void create()
	{
		this.progressFinished = this.storedInfo.getProgressFinished();
	}

	public ResearchProduct getProduct()
	{
		return this.storedInfo.createProduct();
	}

	public long getProgress()
	{
		return this.progress;
	}

	public void increaseProgress()
	{
		++this.progress;
	}

	public long getProgressFinished()
	{
		return this.progressFinished;
	}

	public boolean isInitialized()
	{
		return this.initialized;
	}

	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setLong("Progress", progress);
		tag.setLong("ProgressFinished", progressFinished);
		tag.setString("ProjectName", this.name);
		tag.setTag("Info", new NBTTagCompound());
		this.storedInfo.writeToNBT(tag.getCompoundTag("Info"));
	}

	public void readFromNBT(NBTTagCompound tag)
	{
		this.initialized = true;
		this.progress = tag.getLong("Progress");
		this.progressFinished = tag.getLong("ProgressFinished");
		this.name = tag.getString("ProjectName");
		this.storedInfo.readFromNBT(tag.getCompoundTag("Info"));
	}

}
