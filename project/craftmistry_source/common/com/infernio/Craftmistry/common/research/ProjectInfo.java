package com.infernio.Craftmistry.common.research;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.MathHelper;

import com.infernio.Craftmistry.api.research.EnumProjectSize;
import com.infernio.InfernoCore.math.Vector2D;
import com.infernio.InfernoCore.util.NBTLib;

public class ProjectInfo {

	private EnumProjectSize projectSize;
	private List<ItemStack> items;

	public ProjectInfo()
	{
		this(new ArrayList<ItemStack>(), EnumProjectSize.UNKNOWN);
	}

	public ProjectInfo(List<ItemStack> input, EnumProjectSize projectSize)
	{
		this.items = input;
		this.projectSize = projectSize;
	}

	public ProjectInfo(ItemStack[] input, EnumProjectSize projectSize)
	{
		this(Arrays.asList(input), projectSize);
	}

	public long getProgressFinished()
	{
		return this.projectSize.progressFinished;
	}

	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setByte("ProjectSize", (byte)MathHelper.clamp_int(projectSize.ordinal(), 0, EnumProjectSize.values().length - 1));
		tag.setShort("ItemLength", (short)this.items.size());
		NBTTagList list = new NBTTagList();
		for(int i = 0; i < items.size(); ++i)
		{
			if(this.items.get(i) != null)
			{
				NBTTagCompound tag2 = new NBTTagCompound();
				tag2.setByte("Slot", (byte)i);
				this.items.get(i).writeToNBT(tag2);
				list.appendTag(tag2);
			}
		}
		tag.setTag("ResearchItems", list);
	}

	public void readFromNBT(NBTTagCompound tag)
	{
		this.projectSize = EnumProjectSize.values()[MathHelper.clamp_int(tag.getByte("ProjectSize"), 0, EnumProjectSize.values().length - 1)];
		NBTTagList list = tag.getTagList("ResearchItems", NBTLib.NBT_COMPOUND);
		if(tag.hasKey("ItemLength"))
		{
			this.items = new ArrayList<ItemStack>(tag.getShort("ItemLength"));
		}
		else
		{
			this.items = new ArrayList<ItemStack>();
		}
		for(int i = 0; i < list.tagCount(); ++i)
        {
            NBTTagCompound tag2 = list.getCompoundTagAt(i);
            byte slot = tag2.getByte("Slot");
            if(slot >= 0)
            {
                this.items.add(slot, ItemStack.loadItemStackFromNBT(tag2));
            }
        }
	}

	public ResearchProduct createProduct()
	{
		Vector2D weight = this.calculateWeight();
		return new ResearchProduct(weight.x, weight.y, projectSize);
	}

	private Vector2D calculateWeight()
	{
		int iteration = 0;
		double x = 0;
		double y = 0;
		for(ItemStack stack : this.items)
		{
			if(stack != null && stack.getItem() != null)
			{
				int x1 = ResearchRegistry.getPrimary(stack);
				int y1 = ResearchRegistry.getSecondary(stack);
				if(x1 > 0 || y1 > 0)
				{
					x += x1;
					y += y1;
					++iteration;
				}
			}
			
		}
		int x2 = (int)(x / (double)iteration);
		int y2 = (int)(y / (double)iteration);
		return new Vector2D(x2, y2);
	}

}
