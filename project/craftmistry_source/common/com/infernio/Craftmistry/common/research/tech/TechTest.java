package com.infernio.Craftmistry.common.research.tech;

import com.infernio.Craftmistry.api.research.PanelProperties;
import com.infernio.Craftmistry.api.research.tech.Technology;

import net.minecraft.world.World;

public class TechTest extends Technology {

	public TechTest(String name, int level, Technology... requirements)
	{
		super(name, level, requirements);
	}

	public TechTest(String name, int level, String classID, Class<? extends Technology> clazz, Technology... requirements)
	{
		super(name, level, classID, clazz, requirements);
	}

	@Override
	public void apply(World world, PanelProperties panel)
	{
		panel.powerConsumption -= 1;
	}

}
