package com.infernio.Craftmistry.common.research;

import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.api.research.EnumProjectSize;
import com.infernio.Craftmistry.common.tile.IResearchTile;

public class ProjectContainer {

	public Project[] projects;
	private final IResearchTile researchManager;

	public ProjectContainer(IResearchTile owner, int max)
	{
		this.projects = new Project[max];
		this.researchManager = owner;
	}

	public void researchTick()
	{
		for(int i = 0; i < projects.length; ++i)
		{
			Project project = projects[i];
			if(project != null && project.isInitialized())
			{
				project.increaseProgress();
				long progress = project.getProgress();
				long progressFinished = project.getProgressFinished();
				if(progress >= progressFinished)
				{
					this.projects[i] = null;
					this.researchManager.onProjectFinished(project.getProduct());
				}
			}
		}
	}

	public boolean canStartProject()
	{
		for(Project project : projects)
		{
			if(project == null)
			{
				return true;
			}
		}
		return false;
	}

	public void startProject(List<ItemStack> list, EnumProjectSize size, String name)
	{
		this.startProject(new ProjectInfo(list, size), name);
	}

	public void startProject(ProjectInfo info, String name)
	{
		Project project = new Project(name);
		project.setInfo(info);
		project.create();
		this.storeProject(project);
	}

	public void saveProjects(NBTTagCompound tag)
	{
		for(int i = 0; i < projects.length; ++i)
		{
			Project project = projects[i];
			if(project != null)
			{
				tag.setTag("project_" + i, new NBTTagCompound());
				project.writeToNBT(tag.getCompoundTag("project_" + i));
			}
		}
	}

	public void loadProjects(NBTTagCompound tag)
	{
		for(int i = 0; i < projects.length; ++i)
		{
			if(tag.hasKey("project_" + i))
			{
				Project project = new Project("");
				project.readFromNBT(tag.getCompoundTag("project_" + i));
				projects[i] = project;
			}
		}
	}

	public boolean shouldTick()
	{
		for(Project project : this.projects)
		{
			if(project != null)
			{
				return true;
			}
		}
		return false;
	}

	public int getTickCost(int powerConsumption)
	{
		int ret = 0;
		for(Project project : this.projects)
		{
			if(project != null)
			{
				ret += powerConsumption;
			}
		}
		return ret;
	}

	private void storeProject(Project proj)
	{
		for(int i = 0; i < projects.length; ++i)
		{
			Project project = this.projects[i];
			if(project == null)
			{
				this.projects[i] = proj;
			}
		}
	}

}
