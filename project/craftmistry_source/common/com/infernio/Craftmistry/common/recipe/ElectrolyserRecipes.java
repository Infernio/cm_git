package com.infernio.Craftmistry.common.recipe;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.HashUtils;
import com.infernio.InfernoCore.util.ToStringUtils;

public class ElectrolyserRecipes {

	private static final ElectrolyserRecipes instance = new ElectrolyserRecipes();

	private final HashMap<ItemPair, ItemStack> recipeMap = new HashMap<ItemPair, ItemStack>();

    public static final ElectrolyserRecipes electrolysing()
    {
        return instance;
    }

    private ElectrolyserRecipes() {}

    public Map<ItemPair, ItemStack> getRecipeList()
    {
        return this.recipeMap;
    }

    public void addRecipe(Item item1, int meta1, Item item2, int meta2, ItemStack result)
    {
    	ItemPair key = new ItemPair(item1, meta1, item2, meta2);
		if(!this.recipeMap.containsKey(key))
		{
    		this.recipeMap.put(key, result);
    		this.recipeMap.put(new ItemPair(item2, meta2, item1, meta1), result);
    	}
    }

    public ItemStack getResult(ItemStack stack1, ItemStack stack2)
    {
    	ItemPair key = new ItemPair(stack1.getItem(), stack1.getItemDamage(), stack2.getItem(), stack2.getItemDamage());
    	if(this.recipeMap.containsKey(key))
    	{
    		return this.recipeMap.get(key);
    	}
    	return null;
    }

	public int getNumberRecipes()
	{
		return this.recipeMap.size();
	}

	public static final class ItemPair
	{
		public final Item item1;
		public final int meta1;
		public final Item item2;
		public final int meta2;
		private final int hash;

		public ItemPair(Item item1, int meta1, Item item2, int meta2)
		{
			this.item1 = item1;
			this.meta1 = meta1;
			this.item2 = item2;
			this.meta2 = meta2;
			this.hash = this.computeHash();
		}

		private int computeHash()
		{
			int hash1 = HashUtils.hashItemStack(new ItemStack(this.item1, 1, this.meta1));
			int hash2 = HashUtils.hashItemStack(new ItemStack(this.item2, 1, this.meta2));
			return HashUtils.combineHashes(hash1, hash2);
		}

		@Override
		public boolean equals(Object other)
		{
			if(other instanceof ItemPair)
			{
				ItemPair pair = (ItemPair)other;
				return pair.item1 == this.item1 && pair.meta1 == this.meta1 && pair.item2 == this.item2 && pair.meta2 == this.meta2;
			}
			return false;
		}

		@Override
		public int hashCode()
		{
			return this.hash;
		}

		@Override
		public String toString()
		{
			return ToStringUtils.reportDeclaredFields(this.getClass(), this);
		}
	}
}
