package com.infernio.Craftmistry.common.recipe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.recipe.IForesterRegistry;
import com.infernio.Craftmistry.api.tile.ITreeHandler;
import com.infernio.InfernoCore.util.ItemUtils;

public class ForesterRegistry implements IForesterRegistry
{
	private static final ForesterRegistry instance = new ForesterRegistry();

	private ForesterRegistry() {}

	public static final ForesterRegistry getInstance()
	{
		return instance;
	}

	//That's right, this is a Tree Map. (sorry)
	private final HashMap<String, Tree> treeMap = new HashMap<String, Tree>();
	private final ArrayList<String> trees = new ArrayList<String>();
	private final HashMap<String, ITreeHandler> handlerMap = new HashMap<String, ITreeHandler>();

	@Override
	public void registerTree(String name, ItemStack sapling, ItemStack leaves, ItemStack wood)
	{
		this.registerTree(name, new ItemStack[]{sapling}, new ItemStack[]{leaves}, wood);
	}

	@Override
	public void registerTree(String name, ItemStack[] saplings, ItemStack[] leaves, ItemStack... wood)
	{
		if(!this.treeMap.containsKey(name))
		{
			this.treeMap.put(name, new Tree(saplings, leaves, wood));
			this.trees.add(name);
		}
		else
		{
			Tree tree = this.treeMap.get(name);
			if(tree != null)
			{
				for(ItemStack stack : saplings)
				{
					if(!tree.hasSapling(stack))
					{
						tree.addSapling(stack);
					}
				}
				for(ItemStack stack : leaves)
				{
					if(!tree.hasLeaves(stack))
					{
						tree.addLeaves(stack);
					}
				}
				for(ItemStack stack : wood)
				{
					if(!tree.hasWood(stack))
					{
						tree.addWood(stack);
					}
				}
			}
			else
			{
				this.treeMap.put(name, new Tree(saplings, leaves, wood));
				this.trees.add(name);
			}
		}
	}

	@Override
	public void registerSaplings(String name, ItemStack... saplings)
	{
		if(!this.treeMap.containsKey(name))
		{
			this.treeMap.put(name, new Tree(saplings, new ItemStack[]{}, new ItemStack[]{}));
			this.trees.add(name);
		}
		else
		{
			Tree tree = this.treeMap.get(name);
			if(tree != null)
			{
				for(ItemStack stack : saplings)
				{
					if(!tree.hasSapling(stack))
					{
						tree.addSapling(stack);
					}
				}
			}
			else
			{
				this.treeMap.put(name, new Tree(saplings, new ItemStack[]{}, new ItemStack[]{}));
				this.trees.add(name);
			}
		}
	}

	@Override
	public void registerLeaves(String name, ItemStack... leaves) 
	{
		if(!this.treeMap.containsKey(name))
		{
			this.treeMap.put(name, new Tree(new ItemStack[]{}, leaves, new ItemStack[]{}));
			this.trees.add(name);
		}
		else
		{
			Tree tree = this.treeMap.get(name);
			if(tree != null)
			{
				for(ItemStack stack : leaves)
				{
					if(!tree.hasLeaves(stack))
					{
						tree.addLeaves(stack);
					}
				}
			}
			else
			{
				this.treeMap.put(name, new Tree(new ItemStack[]{}, leaves, new ItemStack[]{}));
				this.trees.add(name);
			}
		}
	}

	@Override
	public void registerWood(String name, ItemStack... wood)
	{
		if(!this.treeMap.containsKey(name))
		{
			this.treeMap.put(name, new Tree(new ItemStack[]{}, new ItemStack[]{}, wood));
			this.trees.add(name);
		}
		else
		{
			Tree tree = this.treeMap.get(name);
			if(tree != null)
			{
				for(ItemStack stack : wood)
				{
					if(!tree.hasWood(stack))
					{
						tree.addWood(stack);
					}
				}
			}
			else
			{
				this.treeMap.put(name, new Tree(new ItemStack[]{}, new ItemStack[]{}, wood));
				this.trees.add(name);
			}
		}
	}

	@Override
	public void registerTreeHandler(String tree, ITreeHandler handler)
	{
		if(!this.handlerMap.containsKey(tree))
		{
			this.handlerMap.put(tree, handler);
		}
	}

	@Override
	public List<ItemStack> getSaplings(String name)
	{
		Tree ret = this.treeMap.get(name);
		if(ret != null)
		{
			return ret.saplings;
		}
		return null;
	}

	@Override
	public List<ItemStack> getLeaves(String name)
	{
		Tree ret = this.treeMap.get(name);
		if(ret != null)
		{
			return ret.leaves;
		}
		return null;
	}

	@Override
	public List<ItemStack> getWood(String name)
	{
		Tree ret = this.treeMap.get(name);
		if(ret != null)
		{
			return ret.wood;
		}
		return null;
	}

	public ITreeHandler getHandler(String tree)
	{
		return this.handlerMap.get(tree);
	}

	public List<String> getRegisteredTrees()
	{
		return this.trees;
	}

	private static final class Tree
	{
		public final List<ItemStack> saplings;
		public final List<ItemStack> leaves;
		public final List<ItemStack> wood;

		public Tree(ItemStack[] saplings, ItemStack[] leaves, ItemStack[] wood)
		{
			this(Arrays.asList(saplings), Arrays.asList(leaves), Arrays.asList(wood));
		}

		public Tree(List<ItemStack> saplings, List<ItemStack> leaves, List<ItemStack> wood)
		{
			this.saplings = saplings;
			this.leaves = leaves;
			this.wood = wood;
		}

		public void addSapling(ItemStack sapling)
		{
			this.saplings.add(sapling);
		}

		public void addLeaves(ItemStack leaves)
		{
			this.leaves.add(leaves);
		}

		public void addWood(ItemStack wood)
		{
			this.wood.add(wood);
		}

		public boolean hasSapling(ItemStack sapling)
		{
			for(ItemStack stack : this.saplings)
			{
				if(ItemUtils.areItemStacksEqualNBT(stack, sapling, false))
				{
					return true;
				}
			}
			return false;
		}

		public boolean hasLeaves(ItemStack leaves)
		{
			for(ItemStack stack : this.leaves)
			{
				if(ItemUtils.areItemStacksEqualNBT(stack, leaves, false))
				{
					return true;
				}
			}
			return false;
		}

		public boolean hasWood(ItemStack wood)
		{
			for(ItemStack stack : this.wood)
			{
				if(ItemUtils.areItemStacksEqualNBT(stack, wood, false))
				{
					return true;
				}
			}
			return false;
		}
	}
}
