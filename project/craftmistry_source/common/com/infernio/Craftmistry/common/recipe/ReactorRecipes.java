package com.infernio.Craftmistry.common.recipe;

import java.util.HashMap;

import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.recipe.ItemCollection;

public class ReactorRecipes {

	private static final ReactorRecipes instance = new ReactorRecipes();

	private final HashMap<ItemCollection, ItemStack> recipeMap = new HashMap<ItemCollection, ItemStack>();
	private final HashMap<ItemCollection, Integer> energyMap = new HashMap<ItemCollection, Integer>();

	private ReactorRecipes() {}

	public static ReactorRecipes reactions()
	{
		return instance;
	}

	public void addRecipe(ItemStack result, ItemStack... input)
	{
		this.addRecipe(20000, result, input);
	}

	public void addRecipe(int energy, ItemStack result, ItemStack... input)
	{
		ItemCollection key = new ItemCollection(input);
		if(!this.recipeMap.containsKey(key))
		{
			this.recipeMap.put(key, result);
			this.energyMap.put(key, energy);
		}
	}

	public int getEnergyRequired(ItemStack... input)
	{
		if(input != null && input.length > 0)
		{
			Integer ret = this.energyMap.get(new ItemCollection(input));
			return ret == null ? 0 : ret;
		}
		return 0;
	}

	public int getEnergyRequired(ItemCollection input)
	{
		if(input != null && input.getItems().length > 0)
		{
			Integer ret = this.energyMap.get(input);
			return ret == null ? 0 : ret;
		}
		return 0;
	}

	public ItemStack getResult(ItemStack... input)
	{
		if(input != null && input.length > 0)
		{
			return this.recipeMap.get(new ItemCollection(input));
		}
		return null;
	}

	public ItemStack getResult(ItemCollection input)
	{
		if(input != null && input.getItems().length > 0)
		{
			return this.recipeMap.get(input);
		}
		return null;
	}

	public HashMap<ItemCollection, ItemStack> getRecipeList()
	{
		return this.recipeMap;
	}

}