package com.infernio.Craftmistry.common.recipe;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class CompressorRecipes
{
    private static final CompressorRecipes instance = new CompressorRecipes();

    private final HashMap<Item, HashMap<Integer, ItemStack>> recipeMap = new HashMap<Item, HashMap<Integer, ItemStack>>();
    private final HashMap<Item, HashMap<Integer, Integer>> energyMap = new HashMap<Item, HashMap<Integer, Integer>>();
    private final HashMap<Item, HashMap<Integer, Integer>> stackSizeMap = new HashMap<Item, HashMap<Integer, Integer>>();

    public static final CompressorRecipes compressing()
    {
        return instance;
    }
    
    private CompressorRecipes() {}

    public void addRecipe(Item item, int amount, int meta, ItemStack result, int energyRequired)
    {
    	if(this.recipeMap.containsKey(item))
    	{
    		HashMap<Integer, ItemStack> recipes = this.recipeMap.get(item);
    		if(recipes == null)
    		{
    			recipes = new HashMap<Integer, ItemStack>();
    			this.recipeMap.put(item, recipes);
    		}
			recipes.put(meta, result);
    	}
    	else
    	{
    		HashMap<Integer, ItemStack> recipes = new HashMap<Integer, ItemStack>();
    		recipes.put(meta, result);
    		this.recipeMap.put(item, recipes);
    	}
    	if(this.energyMap.containsKey(item))
    	{
    		HashMap<Integer, Integer> energy = this.energyMap.get(item);
    		if(energy == null)
    		{
    			energy = new HashMap<Integer, Integer>();
    			this.energyMap.put(item, energy);
    		}
			energy.put(meta, energyRequired);
    	}
    	else
    	{
    		HashMap<Integer, Integer> energy = new HashMap<Integer, Integer>();
    		energy.put(meta, energyRequired);
    		this.energyMap.put(item, energy);
    	}
    	if(this.stackSizeMap.containsKey(item))
    	{
    		HashMap<Integer, Integer> stackSizes = this.stackSizeMap.get(item);
    		if(stackSizes == null)
    		{
    			stackSizes = new HashMap<Integer, Integer>();
    			this.stackSizeMap.put(item, stackSizes);
    		}
			stackSizes.put(meta, amount);
    	}
    	else
    	{
    		HashMap<Integer, Integer> stackSizes = new HashMap<Integer, Integer>();
    		stackSizes.put(meta, amount);
    		this.stackSizeMap.put(item, stackSizes);
    	}
    }

    public void addRecipe(Item item, int meta, ItemStack result)
    {
    	addRecipe(item, 1, meta, result, 7500);
    }

	public void addRecipe(Item item , int amount, int meta, ItemStack result)
	{
		addRecipe(item, amount, meta, result, 7500);
	}

    public ItemStack getResult(ItemStack stack)
    {
    	if(stack != null && stack.stackSize >= this.stackSizeMap.get(stack.getItem()).get(stack.getItemDamage()))
    	{
	        ItemStack ret = this.recipeMap.get(stack.getItem()).get(stack.getItemDamage());
	        return ret;
	    }
    	return null;
    }

    public int getStackSizeToRemove(ItemStack stack)
    {
    	if(stack != null)
    	{
    		return this.stackSizeMap.get(stack.getItem()).get(stack.getItemDamage());
    	}
    	return 0;
    }

    public int getEnergyRequired(ItemStack stack)
    {
    	if(stack != null)
    	{
    		return this.energyMap.get(stack.getItem()).get(stack.getItemDamage());
	    }
    	return 0;
    }

    public boolean isRegistered(ItemStack stack)
    {
    	if(stack != null)
    	{
    		return this.recipeMap.containsKey(stack.getItem()) && this.recipeMap.get(stack.getItem()).containsKey(stack.getItemDamage());
    	}
    	return false;
    }

	public Map<Item, HashMap<Integer, ItemStack>> getRecipeList() 
	{
		return this.recipeMap;
	}
}
