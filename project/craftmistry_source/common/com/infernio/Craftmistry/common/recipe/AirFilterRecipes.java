package com.infernio.Craftmistry.common.recipe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.ItemStack;

public class AirFilterRecipes {

	private static final AirFilterRecipes instance = new AirFilterRecipes();

	private final ArrayList<String> registered = new ArrayList<String>(9);
	private final HashMap<String, ItemStack> results = new HashMap<String, ItemStack>();
	private final HashMap<String, Integer> durations = new HashMap<String, Integer>();
	private final HashMap<String, Integer> energy =  new HashMap<String, Integer>();

	private AirFilterRecipes() {}

	public static AirFilterRecipes recipes()
	{
		return instance;
	}

	public void registerRecipe(String elementName, int duration, int energy, ItemStack result)
	{
		this.registered.add(elementName);
		this.results.put(elementName, result);
		this.durations.put(elementName, Integer.valueOf(duration));
		this.energy.put(elementName, Integer.valueOf(energy));
	}

	public int getRegistrationSize()
	{
		return this.registered.size();
	}

	public String convert(int saveID)
	{
		return this.registered.get(saveID);
	}

	public int getDuration(String element)
	{
		Integer ret = this.durations.get(element);
		if(ret != null)
		{
			return ret.intValue();
		}
		return 0;
	}

	public int getEnergy(String element)
	{
		Integer ret = this.energy.get(element);
		if(ret != null)
		{
			return ret.intValue();
		}
		return 0;
	}

	public ItemStack getResult(String element)
	{
		return this.results.get(element);
	}

	public Map<String, ItemStack> getRecipeList() 
	{
		return this.results;
	}

}
