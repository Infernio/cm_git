package com.infernio.Craftmistry.common.recipe;

import static com.infernio.Craftmistry.common.recipe.AirFilterRecipes.recipes;
import static com.infernio.Craftmistry.common.recipe.CompressorRecipes.compressing;
import static com.infernio.Craftmistry.common.recipe.DeconstructorRecipes.deconstructions;
import static com.infernio.Craftmistry.common.recipe.ElectrolyserRecipes.electrolysing;
import static com.infernio.Craftmistry.common.recipe.ForesterRegistry.getInstance;
import static com.infernio.Craftmistry.common.recipe.InfuserRecipes.infusing;
import static com.infernio.Craftmistry.common.recipe.MineralExtractorRecipes.extractions;
import static com.infernio.Craftmistry.common.recipe.ReactorRecipes.reactions;
import net.minecraft.block.BlockSapling;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.CraftmistryAPI;
import com.infernio.Craftmistry.common.detector.DetectorModes;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.logging.CMLogger;

public class RecipeCreator {

	private static Item solid;
	//private static Item liquid;
	private static Item gas;
	//private static Item plasma;
	private static Item compound;
	private static Item rawMineral;
	private static Item ingot;
	private static Item multi;

	public static void createRecipes()
	{
		CMLogger.info("Testing Craftmistry installation... Step 2 / 4 (Recipes)");
		CraftmistryAPI.Recipes.foresterRegistry = ForesterRegistry.getInstance();
		solid = ModItems.testTubeSolid;
		//liquid = ModItems.testTubeLiquid
		gas = ModItems.testTubeGas;
		//plasma = ModItems.testTubePlasma
		compound = ModItems.testTubeCompound;
		rawMineral = ModItems.rawMineral;
		ingot = ModItems.ingot;
		multi = ModItems.multiItem;
		CraftmistryAPI.Recipes.addInfuserRecipe(rawMineral, 0, new ItemStack(solid, 1, 16));
		infusing().addRecipe(rawMineral, 1, new ItemStack(solid, 1, 29));
		infusing().addRecipe(rawMineral, 2, new ItemStack(solid, 1, 50));
		infusing().addRecipe(rawMineral, 3, new ItemStack(solid, 1, 47));
		infusing().addRecipe(rawMineral, 4, new ItemStack(solid, 1, 13));
		infusing().addRecipe(rawMineral, 5, new ItemStack(solid, 1, 27));
		infusing().addRecipe(rawMineral, 6, new ItemStack(solid, 1, 26));
		infusing().addRecipe(rawMineral, 7, new ItemStack(solid, 1, 6));
		infusing().addRecipe(rawMineral, 8, new ItemStack(solid, 1, 79));
		infusing().addRecipe(rawMineral, 9, new ItemStack(solid, 1, 11));
		infusing().addRecipe(rawMineral, 10, new ItemStack(solid, 1, 19));
		infusing().addRecipe(rawMineral, 11, new ItemStack(solid, 1, 4));
		infusing().addRecipe(multi, 5, new ItemStack(solid, 1, 20));
		infusing().addRecipe(multi, 7, new ItemStack(solid, 1, 16));
		CraftmistryAPI.Recipes.addCompressorRecipe(solid, 32, 6, new ItemStack(Items.diamond), 75000);
		compressing().addRecipe(rawMineral, 1, 1, new ItemStack(ingot, 1, 0));
		compressing().addRecipe(rawMineral, 1, 2, new ItemStack(ingot, 1, 1));
		compressing().addRecipe(rawMineral, 1, 3, new ItemStack(ingot, 1, 2));
		compressing().addRecipe(rawMineral, 1, 4, new ItemStack(ingot, 1, 3));
		compressing().addRecipe(rawMineral, 1, 6, new ItemStack(Items.iron_ingot, 1, 0));
		compressing().addRecipe(rawMineral, 1, 7, new ItemStack(Items.coal, 1, 0));
		compressing().addRecipe(rawMineral, 1, 8, new ItemStack(Items.gold_ingot, 1, 0));
		compressing().addRecipe(rawMineral, 1, 9, new ItemStack(rawMineral, 1, 10));
		compressing().addRecipe(Items.diamond, 1, 0, new ItemStack(multi, 9, 20));
		CraftmistryAPI.Recipes.addAirFilterRecipe("Nitrogen", 100, 100, new ItemStack(ModItems.testTubeGas, 1, 7));
		recipes().registerRecipe("Oxygen", 200, 100, new ItemStack(ModItems.testTubeGas, 1, 8));
		recipes().registerRecipe("Hydrogen", 500, 250, new ItemStack(ModItems.testTubeGas, 1, 1));
		recipes().registerRecipe("Neon", 800, 400, new ItemStack(ModItems.testTubeGas, 1, 10));
		recipes().registerRecipe("Argon", 1800, 900, new ItemStack(ModItems.testTubeGas, 1, 18));
		recipes().registerRecipe("Helium", 2000, 1000, new ItemStack(ModItems.testTubeGas, 1, 2));
		recipes().registerRecipe("Chlorine", 2500, 1250, new ItemStack(ModItems.testTubeGas, 1, 17));
		recipes().registerRecipe("Krypton", 2500, 1250, new ItemStack(ModItems.testTubeGas, 1, 36));
		recipes().registerRecipe("Xenon", 4500, 1500, new ItemStack(ModItems.testTubeGas, 1, 54));
		recipes().registerRecipe("Iodine", 7000, 1750, new ItemStack(ModItems.testTubeGas, 1, 53));
		recipes().registerRecipe("Radon", 13500, 2250, new ItemStack(ModItems.testTubeGas, 1, 86));
		CraftmistryAPI.Recipes.addReactorRecipe(new ItemStack(compound, 1, 2), new ItemStack(gas, 2, 1), new ItemStack(gas, 1, 8));
		addInternalReactorRecipe(new ItemStack(compound, 1, 0), new ItemStack(gas, 2, 1), new ItemStack(solid, 1, 16), new ItemStack(gas, 4, 8));
		addInternalReactorRecipe(new ItemStack(compound, 1, 1), new ItemStack(solid, 3, 20), new ItemStack(gas, 8, 8), new ItemStack(solid, 2, 15));
		addInternalReactorRecipe(new ItemStack(compound, 1, 3), new ItemStack(solid, 1, 19), new ItemStack(gas, 1, 7), new ItemStack(gas, 3, 8));
		addInternalReactorRecipe(new ItemStack(compound, 1, 4), new ItemStack(solid, 1, 11), new ItemStack(gas, 1, 17));
		addInternalReactorRecipe(new ItemStack(ModItems.superPhosphate), new ItemStack(compound, 1, 0), new ItemStack(compound, 1, 1));
		addInternalReactorRecipe(new ItemStack(ModItems.buckets, 1, 0), new ItemStack(Items.bucket, 1, 0), new ItemStack(compound, 10, 0));
		addInternalReactorRecipe(new ItemStack(Blocks.obsidian), new ItemStack(Items.lava_bucket, 1, 0), new ItemStack(Items.water_bucket, 1, 0));
		addInternalReactorRecipe(new ItemStack(Items.water_bucket), new ItemStack(Items.bucket, 1, 0), new ItemStack(compound, 10, 2));
		addInternalReactorRecipe(new ItemStack(Items.gunpowder), new ItemStack(compound, 1, 3), new ItemStack(solid, 1, 6), new ItemStack(solid, 1, 16));
		addInternalReactorRecipe(new ItemStack(compound, 1, 5), new ItemStack(solid, 3, 26), new ItemStack(solid, 1, 6));
		addInternalReactorRecipe(new ItemStack(compound, 1, 6), new ItemStack(solid, 1, 19), new ItemStack(gas, 1, 17));
		CraftmistryAPI.Recipes.addMineralExtractorRecipe(new ItemStack(multi, 1, 5), 60);
		extractions().addRecipe(new ItemStack(rawMineral, 1, 9), 45);
		extractions().addRecipe(new ItemStack(multi, 1, 21), 30);
		extractions().addRecipe(new ItemStack(rawMineral, 1, 12), 20);
		CraftmistryAPI.Recipes.addElectrolyserRecipe(multi, 21, compound, 6, new ItemStack(solid, 1, 3));
		electrolysing().addRecipe(rawMineral, 12, compound, 0, new ItemStack(solid, 1, 5));
		electrolysing().addRecipe(rawMineral, 13, solid, 28, new ItemStack(gas, 1, 9));
		DetectorModes.init();
		getInstance().registerTree(BlockSapling.field_149882_a[0], new ItemStack(Blocks.sapling, 1, 0), new ItemStack(Blocks.leaves, 1, 0), new ItemStack(Blocks.log, 1, 0));
		getInstance().registerTree(BlockSapling.field_149882_a[1], new ItemStack(Blocks.sapling, 1, 1), new ItemStack(Blocks.leaves, 1, 1), new ItemStack(Blocks.log, 1, 1));
		getInstance().registerTree(BlockSapling.field_149882_a[2], new ItemStack(Blocks.sapling, 1, 2), new ItemStack(Blocks.leaves, 1, 2), new ItemStack(Blocks.log, 1, 2));
		getInstance().registerTree(BlockSapling.field_149882_a[3], new ItemStack(Blocks.sapling, 1, 3), new ItemStack(Blocks.leaves, 1, 3), new ItemStack(Blocks.log, 1, 3));
		getInstance().registerTree(BlockSapling.field_149882_a[4], new ItemStack(Blocks.sapling, 1, 4), new ItemStack(Blocks.leaves2, 1, 0), new ItemStack(Blocks.log2, 1, 0));
		getInstance().registerTree(BlockSapling.field_149882_a[5], new ItemStack(Blocks.sapling, 1, 5), new ItemStack(Blocks.leaves2, 1, 1), new ItemStack(Blocks.log2, 1, 1));
		VanillaTreeHandler vanilla = new VanillaTreeHandler();
		getInstance().registerTreeHandler(BlockSapling.field_149882_a[0], vanilla);
		getInstance().registerTreeHandler(BlockSapling.field_149882_a[1], vanilla);
		getInstance().registerTreeHandler(BlockSapling.field_149882_a[2], vanilla);
		getInstance().registerTreeHandler(BlockSapling.field_149882_a[3], vanilla);
		getInstance().registerTreeHandler(BlockSapling.field_149882_a[4], vanilla);
		getInstance().registerTreeHandler(BlockSapling.field_149882_a[5], vanilla);
	}

	public static void addCompressorRecipe(Item item, int meta, ItemStack result)
	{
		compressing().addRecipe(item, meta, result);
	}

	public static void addCompressorRecipe(Item item, int amount, int meta, ItemStack result)
	{
		compressing().addRecipe(item, amount, meta, result);
	}

	public static void addCompressorRecipe(Item item, int amount, int meta, ItemStack result, int energyRequired)
	{
		compressing().addRecipe(item, amount, meta, result, energyRequired);
	}

	public static void addInfuserRecipe(Item item, int meta, ItemStack result)
	{
		infusing().addRecipe(item, meta, result);
	}

	public static void addInfuserRecipe(Item item, int meta, ItemStack result, int energyRequired)
	{
		infusing().addRecipe(item, meta, result, energyRequired);
	}

	public static void addReactorRecipe(ItemStack result, ItemStack[] input)
	{
		reactions().addRecipe(result, input);
		deconstructions().addRecipe(result.getItem(), result.getItemDamage(), input);
	}

	public static void addReactorRecipe(int energyRequired, ItemStack result, ItemStack[] input)
	{
		reactions().addRecipe(energyRequired, result, input);
		deconstructions().addRecipe(energyRequired, result.getItem(), result.getItemDamage(), input);
	}

	static void addInternalReactorRecipe(ItemStack result, ItemStack... input)
	{
		reactions().addRecipe(result, input);
		deconstructions().addRecipe(result.getItem(), result.getItemDamage(), input);
	}

	static void addInternalReactorRecipe(int energyRequired, ItemStack result, ItemStack... input)
	{
		reactions().addRecipe(energyRequired, result, input);
		deconstructions().addRecipe(energyRequired, result.getItem(), result.getItemDamage(), input);
	}

	public static void addOneWayReactorRecipe(ItemStack result, ItemStack[] input)
	{
		reactions().addRecipe(result, input);
	}

	public static void addOneWayReactorRecipe(int energyRequired, ItemStack result, ItemStack[] input)
	{
		reactions().addRecipe(energyRequired, result, input);
	}

	public static void addDeconstructorRecipe(Item item, int meta, ItemStack[] result)
	{
		deconstructions().addRecipe(item, meta, result);
	}

	public static void addDeconstructorRecipe(Item item, int meta, ItemStack[] result, int energyRequired)
	{
		deconstructions().addRecipe(energyRequired, item, meta, result);
	}

	public static void addAirFilterRecipe(String element, int duration, int energyRequired, ItemStack result)
	{
		recipes().registerRecipe(element, duration, energyRequired, result);
	}

	public static void addMineralExtractorRecipe(ItemStack result, int chance)
	{
		extractions().addRecipe(result, chance);
	}

	public static void addElectrolyserRecipe(Item item1, int meta1, Item item2, int meta2, ItemStack result)
	{
		electrolysing().addRecipe(item1, meta1, item2, meta2, result);
	}

}
