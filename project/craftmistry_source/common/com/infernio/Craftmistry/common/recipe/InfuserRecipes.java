package com.infernio.Craftmistry.common.recipe;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class InfuserRecipes
{
    private static final InfuserRecipes instance = new InfuserRecipes();

    private final HashMap<Item, HashMap<Integer, ItemStack>> recipeMap = new HashMap<Item, HashMap<Integer, ItemStack>>();
    private final HashMap<Item, HashMap<Integer, Integer>> energyMap = new HashMap<Item, HashMap<Integer, Integer>>();

    public static final InfuserRecipes infusing()
    {
        return instance;
    }

    private InfuserRecipes() {}

    public Map<Item, HashMap<Integer, ItemStack>> getRecipeList()
    {
        return this.recipeMap;
    }

    public void addRecipe(Item item, int meta, ItemStack result, int energyRequired)
    {
    	if(this.recipeMap.containsKey(item))
    	{
    		HashMap<Integer, ItemStack> recipes = this.recipeMap.get(item);
    		if(recipes == null)
    		{
    			recipes = new HashMap<Integer, ItemStack>();
    			this.recipeMap.put(item, recipes);
    		}
			recipes.put(meta, result);
    	}
    	else
    	{
    		HashMap<Integer, ItemStack> recipes = new HashMap<Integer, ItemStack>();
    		recipes.put(meta, result);
    		this.recipeMap.put(item, recipes);
    	}
    	if(this.energyMap.containsKey(item))
    	{
    		HashMap<Integer, Integer> energy = this.energyMap.get(item);
    		if(energy == null)
    		{
    			energy = new HashMap<Integer, Integer>();
    			this.energyMap.put(item, energy);
    		}
			energy.put(meta, energyRequired);
    	}
    	else
    	{
    		HashMap<Integer, Integer> energy = new HashMap<Integer, Integer>();
    		energy.put(meta, energyRequired);
    		this.energyMap.put(item, energy);
    	}
    }

	public void addRecipe(Item item, int meta, ItemStack result)
	{
		this.addRecipe(item, meta, result, 600);
	}

    public ItemStack getResult(ItemStack stack)
    {
    	if(stack != null && stack.getItem() != null)
    	{
    		Item item = stack.getItem();
    		if(this.recipeMap.containsKey(item))
    		{
    			HashMap<Integer, ItemStack> recipes = this.recipeMap.get(item);
    			if(recipes != null)
    			{
    				return recipes.get(stack.getItemDamage());
    			}
    		}
    	}
    	return null;
    }

    public int getEnergyRequired(ItemStack stack)
    {
    	if(stack != null && stack.getItem() != null)
    	{
    		Item item = stack.getItem();
    		if(this.energyMap.containsKey(item))
    		{
    			HashMap<Integer, Integer> energy = this.energyMap.get(item);
    			if(energy != null)
    			{
    				return energy.get(stack.getItemDamage());
    			}
    		}
    	}
    	return 0;
    }

	public int getNumberRecipes()
	{
		return this.recipeMap.size();
	}

}
