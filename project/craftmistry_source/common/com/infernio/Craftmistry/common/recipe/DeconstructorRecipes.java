package com.infernio.Craftmistry.common.recipe;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

//TODO Recipe framework is done, machine and NEI plugin must be created
public class DeconstructorRecipes {

	private static final DeconstructorRecipes instance = new DeconstructorRecipes();

	private final HashMap<Item, HashMap<Integer, ItemStack[]>> recipeMap = new HashMap<Item, HashMap<Integer, ItemStack[]>>();
	private final HashMap<Item, HashMap<Integer, Integer>> energyMap = new HashMap<Item, HashMap<Integer, Integer>>();

	private DeconstructorRecipes() {}

	public static DeconstructorRecipes deconstructions()
	{
		return instance;
	}

	public void addRecipe(Item item, int meta, ItemStack[] result)
	{
		this.addRecipe(20000, item, meta, result);
	}

	public void addRecipe(int energyRequired, Item item, int meta, ItemStack[] result)
	{
		HashMap<Integer, ItemStack[]> map;
		if(!this.recipeMap.containsKey(item))
		{
			map = new HashMap<Integer, ItemStack[]>();
			map.put(meta, result);
			this.recipeMap.put(item, map);
		}
		else
		{
			map = this.recipeMap.get(item);
			if(!map.containsKey(meta))
			{
				map.put(meta, result);
			}
		}
		HashMap<Integer, Integer> map1;
		if(!this.energyMap.containsKey(item))
		{
			map1 = new HashMap<Integer, Integer>();
			map1.put(meta, energyRequired);
			this.energyMap.put(item, map1);
		}
		else
		{
			map1 = this.energyMap.get(item);
			if(!map1.containsKey(meta))
			{
				map1.put(meta, energyRequired);
			}
		}
	}

	public ItemStack[] getResult(ItemStack stack)
	{
		if(stack != null && stack.getItem() != null && this.recipeMap.containsKey(stack.getItem()))
		{
			HashMap<Integer, ItemStack[]> metaMap = this.recipeMap.get(stack.getItem());
			return metaMap.get(stack.getItemDamage());
		}
		return null;
	}

	public int getEnergyRequired(ItemStack stack)
	{
		if(stack != null && stack.getItem() != null && this.energyMap.containsKey(stack.getItem()))
		{
			HashMap<Integer, Integer> metaMap = this.energyMap.get(stack.getItem());
			int meta = stack.getItemDamage();
			if(metaMap.containsKey(meta))
			{
				Integer ret = metaMap.get(meta);
				if(ret != null)
				{
					return ret.intValue();
				}
			}
		}
		return 0;
	}

	public int getNumberRecipes()
	{
		return this.recipeMap.size();
	}

	public Map<Item, HashMap<Integer, ItemStack[]>> getRecipeList()
	{
		return this.recipeMap;
	}

}
