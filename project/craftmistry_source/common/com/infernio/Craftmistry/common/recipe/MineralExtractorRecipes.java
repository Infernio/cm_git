package com.infernio.Craftmistry.common.recipe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.recipe.ItemCollection;
import com.infernio.Craftmistry.common.tile.TileMineralExtractor;

public class MineralExtractorRecipes {

	private static final MineralExtractorRecipes instance = new MineralExtractorRecipes();

	private final ArrayList<ItemStack> recipes = new ArrayList<ItemStack>();
	private final HashMap<ItemCollection, Integer> chances = new HashMap<ItemCollection, Integer>();
	private final Random rand = new Random();

	private MineralExtractorRecipes() {}

	public static MineralExtractorRecipes extractions()
	{
		return instance;
	}

	public void addRecipe(ItemStack result, int chance)
	{
		this.recipes.add(result);
		ItemCollection key = new ItemCollection(result);
		this.chances.put(key, chance);
	}

	public ItemStack[] generateResults()
	{
		if(this.recipes.isEmpty())
		{
			return new ItemStack[]{};
		}
		int amount = this.rand.nextInt(TileMineralExtractor.INVENTORY_SIZE + 1);
		ItemStack[] ret = new ItemStack[amount];
		if(amount > 0)
		{
			for(int i = 0; i < amount; ++i)
			{
				ItemStack stack = this.recipes.get(this.rand.nextInt(this.recipes.size()));
				int chance = this.rand.nextInt(100);
				List<Integer> key = Arrays.asList(Item.getIdFromItem(stack.getItem()), stack.stackSize, stack.getItemDamage());
				if(key != null)
				{
					Integer requirement = this.chances.get(key);
					if(requirement != null)
					{
						while(chance > requirement.intValue())
						{
							stack = this.recipes.get(this.rand.nextInt(this.recipes.size()));
							chance = this.rand.nextInt(100);
							key = Arrays.asList(Item.getIdFromItem(stack.getItem()), stack.stackSize, stack.getItemDamage());
							requirement = this.chances.get(key);
						}
						ret[i] = stack.copy();
					}
				}
			}
		}
		return ret;
	}

	public int getChance(ItemStack stack)
	{
		if(stack != null)
		{
			Integer ret = this.chances.get(new ItemCollection(stack));
			if(ret != null)
			{
				return ret.intValue();
			}
		}
		return 0;
	}

	public ArrayList<ItemStack> getRecipes()
	{
		return this.recipes;
	}

}
