package com.infernio.Craftmistry.common.recipe;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockNewLog;
import net.minecraft.block.BlockOldLog;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.ITreeHandler;
import com.infernio.InfernoCore.util.ItemUtils;

public class VanillaTreeHandler implements ITreeHandler
{
	@Override
	public boolean isSaplingValid(String tree, World world, int x, int y, int z)
	{
		int meta = world.getBlockMetadata(x, y, z) & 7;
		List<ItemStack> saplings = ForesterRegistry.getInstance().getSaplings(tree);
		for(ItemStack sapling : saplings)
		{
			ItemStack stack = new ItemStack(world.getBlock(x, y, z), 1, meta);
			if(ItemUtils.areItemStacksEqual(sapling, stack, false))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isWoodValid(String tree, World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
		if(Block.isEqualTo(block, Blocks.log))
		{
			return this.isOldWoodValid(tree, world, x, y, z);
		}
		else if(Block.isEqualTo(block, Blocks.log2))
		{
			return this.isNewWoodValid(tree, world, x, y, z);
		}
		else
		{
			return false;
		}
	}

	private boolean isOldWoodValid(String tree, World world, int x, int y, int z)
	{
		int meta = world.getBlockMetadata(x, y, z) % BlockOldLog.field_150168_M.length;
		List<ItemStack> wood = ForesterRegistry.getInstance().getWood(tree);
		for(ItemStack stack : wood)
		{
			ItemStack stack1 = new ItemStack(Blocks.log, 1, meta);
			if(ItemUtils.areItemStacksEqual(stack, stack1, false))
			{
				return true;
			}
		}
		return false;
	}

	private boolean isNewWoodValid(String tree, World world, int x, int y, int z)
	{
		int meta = world.getBlockMetadata(x, y, z) % BlockNewLog.field_150169_M.length;
		List<ItemStack> wood = ForesterRegistry.getInstance().getWood(tree);
		for(ItemStack stack : wood)
		{
			ItemStack stack1 = new ItemStack(Blocks.log2, 1, meta);
			if(ItemUtils.areItemStacksEqual(stack, stack1, false))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isLeafValid(String tree, World world, int x, int y, int z)
	{
		int meta = world.getBlockMetadata(x, y, z) & 3;
		List<ItemStack> leaves = ForesterRegistry.getInstance().getLeaves(tree);
		for(ItemStack leaf : leaves)
		{
			ItemStack stack = new ItemStack(world.getBlock(x, y, z), 1, meta);
			if(ItemUtils.areItemStacksEqual(leaf, stack, false))
			{
				return true;
			}
		}
		return false;
	}
}
