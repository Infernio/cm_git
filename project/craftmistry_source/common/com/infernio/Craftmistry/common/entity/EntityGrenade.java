package com.infernio.Craftmistry.common.entity;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityGrenade extends EntityThrowable {

	public EnumGrenade grenade;
	public static final String NBT_GRENADE_TYPE = "GrenadeType";

	public EntityGrenade(World world)
    {
        super(world);
    }

    public EntityGrenade(World world, EntityLivingBase entity)
    {
        super(world, entity);
    }

    public EntityGrenade(World world, double x, double y, double z)
    {
        super(world, x, y, z);
    }
    
    @Override
	protected void onImpact(MovingObjectPosition mop)
    {
    	if(!this.worldObj.isRemote)
    	{
    		boolean destroyBlocks = this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing");
    		ExplosionCustom explosion = new ExplosionCustom(this.worldObj, this, this.posX, this.posY, this.posZ, 4.0F, this.grenade);
            explosion.isSmoking = destroyBlocks;
            explosion.doExplosionA();
            explosion.doExplosionB();
        	explosion.doExplosionC();
        }
    	this.setDead();
    }

    @Override
	public void writeEntityToNBT(NBTTagCompound tag)
    {
    	super.writeEntityToNBT(tag);
    	tag.setByte(NBT_GRENADE_TYPE, (byte)this.grenade.value);
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound tag)
    {
    	super.readEntityFromNBT(tag);
    	byte type = tag.getByte(NBT_GRENADE_TYPE);
    	if(type >= EnumGrenade.values().length)
    	{
    		this.grenade = EnumGrenade.NORMAL;
    	}
    	else
    	{
    		this.grenade = EnumGrenade.values()[type];
    	}
    }

    public static enum EnumGrenade {

    	NORMAL(false, "conventional", 0), WHITE_PHOSPHORUS(true, "whitePhosphorus", 1, new PotionEffect(Potion.poison.id, 500, 2), new PotionEffect(Potion.confusion.id, 200, 0)), FLAMMABLE(true, "flammable", 2);

    	public final boolean flaming;
    	public final PotionEffect[] effects;
    	public final String name;
    	public final int value;

    	private EnumGrenade(boolean setsFlames, String name, int value, PotionEffect... effects)
    	{
    		this.flaming = setsFlames;
    		this.name = name;
    		this.value = value;
    		this.effects = effects;
    	}

    }

}
