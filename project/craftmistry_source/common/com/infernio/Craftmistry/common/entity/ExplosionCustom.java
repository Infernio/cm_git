package com.infernio.Craftmistry.common.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.enchantment.EnchantmentProtection;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.entity.EntityGrenade.EnumGrenade;
import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.Packets;
import com.infernio.InfernoCore.network.message.MessageSpawnParticle;

public class ExplosionCustom
{
    public boolean isSmoking = true;
    private int radius = 16;
    private Random explosionRNG = new Random();
    private World worldObj;
    public double explosionX;
    public double explosionY;
    public double explosionZ;
    public Entity exploder;
    public float explosionSize;
    public boolean isFlaming;
    private PotionEffect[] effects;
	public List<ChunkPosition> affectedBlockPositions = new ArrayList<ChunkPosition>();
	private Map<EntityPlayer, Vec3> playerCache = new HashMap<EntityPlayer, Vec3>();

    public ExplosionCustom(World world, Entity exploder, double x, double y, double z, float size, EnumGrenade grenade)
    {
        this.worldObj = world;
        this.exploder = exploder;
        this.explosionSize = size;
        this.explosionX = x;
        this.explosionY = y;
        this.explosionZ = z;
        this.isFlaming = grenade.flaming;
        this.effects = grenade.effects;
    }

    public ExplosionCustom(World world, Entity exploder, double x, double y, double z, float size, boolean isFlaming)
    {
        this.worldObj = world;
        this.exploder = exploder;
        this.explosionSize = size;
        this.explosionX = x;
        this.explosionY = y;
        this.explosionZ = z;
        this.isFlaming = isFlaming;
    }

    @SuppressWarnings("rawtypes")
	public void doExplosionA()
    {
    	Explosion ex = new Explosion(this.worldObj, this.exploder, this.explosionX, this.explosionY, this.explosionZ, this.explosionSize);
        float size = this.explosionSize;
        HashSet<ChunkPosition> positions = new HashSet<ChunkPosition>();
        int x;
        int y;
        int z;
        double expl_x;
        double expl_y;
        double expl_z;
        for(x = 0; x < this.radius; ++x)
        {
            for(y = 0; y < this.radius; ++y)
            {
                for(z = 0; z < this.radius; ++z)
                {
                    if(x == 0 || x == this.radius - 1 || y == 0 || y == this.radius - 1 || z == 0 || z == this.radius - 1)
                    {
                        double x1 = (double)((float)x / ((float)this.radius - 1.0F) * 2.0F - 1.0F);
                        double x2 = (double)((float)y / ((float)this.radius - 1.0F) * 2.0F - 1.0F);
                        double x3 = (double)((float)z / ((float)this.radius - 1.0F) * 2.0F - 1.0F);
                        double sqrt = Math.sqrt(x1 * x1 + x2 * x2 + x3 * x3);
                        x1 /= sqrt;
                        x2 /= sqrt;
                        x3 /= sqrt;
                        float newSize = this.explosionSize * (0.7F + this.worldObj.rand.nextFloat() * 0.6F);
                        expl_x = this.explosionX;
                        expl_y = this.explosionY;
                        expl_z = this.explosionZ;
                        for(float tempSize = 0.3F; newSize > 0.0F; newSize -= tempSize * 0.75F)
                        {
                            int floor_x = MathHelper.floor_double(expl_x);
                            int floor_y = MathHelper.floor_double(expl_y);
                            int floor_z = MathHelper.floor_double(expl_z);
                            Block block = this.worldObj.getBlock(floor_x, floor_y, floor_z);
                            if(block.getMaterial() != Material.air)
                            {
                                float resistance = this.exploder != null ? this.exploder.func_145772_a(ex, this.worldObj, floor_x, floor_y, floor_z, block) : block.getExplosionResistance(this.exploder, worldObj, floor_x, floor_y, floor_z, explosionX, explosionY, explosionZ);
                                newSize -= (resistance + 0.3F) * tempSize;
                            }
                            if(newSize > 0.0F && (this.exploder == null || this.exploder.func_145774_a(ex, this.worldObj, floor_x, floor_y, floor_z, block, newSize)))
                            {
                                positions.add(new ChunkPosition(floor_x, floor_y, floor_z));
                            }
                            expl_x += x1 * (double)tempSize;
                            expl_y += x2 * (double)tempSize;
                            expl_z += x3 * (double)tempSize;
                        }
                    }
                }
            }
        }
        this.affectedBlockPositions.addAll(positions);
        this.explosionSize *= 2.0F;
        x = MathHelper.floor_double(this.explosionX - (double)this.explosionSize - 1.0D);
        y = MathHelper.floor_double(this.explosionX + (double)this.explosionSize + 1.0D);
        z = MathHelper.floor_double(this.explosionY - (double)this.explosionSize - 1.0D);
        int x2 = MathHelper.floor_double(this.explosionY + (double)this.explosionSize + 1.0D);
        int y2 = MathHelper.floor_double(this.explosionZ - (double)this.explosionSize - 1.0D);
        int z2 = MathHelper.floor_double(this.explosionZ + (double)this.explosionSize + 1.0D);
        List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this.exploder, AxisAlignedBB.getBoundingBox((double)x, (double)z, (double)y2, (double)y, (double)x2, (double)z2));
        Vec3 vec3 = this.worldObj.getWorldVec3Pool().getVecFromPool(this.explosionX, this.explosionY, this.explosionZ);
        for(int i = 0; i < list.size(); ++i)
        {
            Entity entity = (Entity)list.get(i);
            double distance = entity.getDistance(this.explosionX, this.explosionY, this.explosionZ) / (double)this.explosionSize;
            if(distance <= 1.0D)
            {
                expl_x = entity.posX - this.explosionX;
                expl_y = entity.posY + (double)entity.getEyeHeight() - this.explosionY;
                expl_z = entity.posZ - this.explosionZ;
                double expl_total = (double)MathHelper.sqrt_double(expl_x * expl_x + expl_y * expl_y + expl_z * expl_z);
                if(expl_total != 0.0D)
                {
                    expl_x /= expl_total;
                    expl_y /= expl_total;
                    expl_z /= expl_total;
                    double density = (double)this.worldObj.getBlockDensity(vec3, entity.boundingBox);
                    double scaledDensity = (1.0D - distance) * density;
                    entity.attackEntityFrom(DamageSource.setExplosionSource(ex), (float)((int)((scaledDensity * scaledDensity + scaledDensity) / 2.0D * 8.0D * (double)this.explosionSize + 1.0D)));
                    if(entity instanceof EntityLivingBase && this.effects != null && this.effects.length > 0)
                    {
                    	for(PotionEffect effect : this.effects)
                    	{
                    		((EntityLivingBase)entity).addPotionEffect(new PotionEffect(effect));
                    	}
                    }
                    double protection = EnchantmentProtection.func_92092_a(entity, scaledDensity);
                    entity.motionX += expl_x * protection;
                    entity.motionY += expl_y * protection;
                    entity.motionZ += expl_z * protection;
                    if(entity instanceof EntityPlayer)
                    {
                        this.playerCache.put((EntityPlayer)entity, this.worldObj.getWorldVec3Pool().getVecFromPool(expl_x * scaledDensity, expl_y * scaledDensity, expl_z * scaledDensity));
                    }
                }
            }
        }
        this.explosionSize = size;
    }

    @SuppressWarnings("rawtypes")
	public void doExplosionB()
    {
    	Explosion ex = new Explosion(this.worldObj, this.exploder, this.explosionX, this.explosionY, this.explosionZ, this.explosionSize);
    	Iterator iterator;
        ChunkPosition pos;
        int x;
        int y;
        int z;
        Block block;
        if(this.isSmoking)
        {
            iterator = this.affectedBlockPositions.iterator();
            while(iterator.hasNext())
            {
                pos = (ChunkPosition)iterator.next();
                x = pos.chunkPosX;
                y = pos.chunkPosY;
                z = pos.chunkPosZ;
                block = this.worldObj.getBlock(x, y, z);
                if(block.getMaterial() != Material.air)
                {
                    if(block.canDropFromExplosion(ex))
                    {
                        block.dropBlockAsItemWithChance(this.worldObj, x, y, z, this.worldObj.getBlockMetadata(x, y, z), 1.0F / this.explosionSize, 0);
                    }
                    block.onBlockExploded(this.worldObj, x, y, z, ex);
                }
            }
        }
        if(this.isFlaming)
        {
            iterator = this.affectedBlockPositions.iterator();
            while(iterator.hasNext())
            {
                pos = (ChunkPosition)iterator.next();
                x = pos.chunkPosX;
                y = pos.chunkPosY;
                z = pos.chunkPosZ;
                block = this.worldObj.getBlock(x, y, z);
                Block block1 = this.worldObj.getBlock(x, y - 1, z);
                if (block.getMaterial() == Material.air && block1.func_149730_j() && this.explosionRNG.nextInt(3) == 0)
                {
                    this.worldObj.setBlock(x, y, z, Blocks.fire);
                }
            }
        }
    }

    @SuppressWarnings("rawtypes")
	public void doExplosionC()
    {
    	this.worldObj.playSoundEffect(this.explosionX, this.explosionY, this.explosionZ, "random.explode", 4.0F, (1.0F + (this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat()) * 0.2F) * 0.7F);
    	if (this.explosionSize >= 2.0F && this.isSmoking)
        {
    		PacketHelper.sendToAllAround(new MessageSpawnParticle(this.explosionX, this.explosionY, this.explosionZ, 1.0D, 0.0D, 0.0D, "hugeexplosion"), this.worldObj.provider.dimensionId, (int)this.explosionX, (int)this.explosionY, (int)this.explosionZ, Packets.PACKET_SPAWN_PARTICLE_RANGE);
        }
        else
        {
    		PacketHelper.sendToAllAround(new MessageSpawnParticle(this.explosionX, this.explosionY, this.explosionZ, 1.0D, 0.0D, 0.0D, "largeexplode"), this.worldObj.provider.dimensionId, (int)this.explosionX, (int)this.explosionY, (int)this.explosionZ, Packets.PACKET_SPAWN_PARTICLE_RANGE);
        }
        Iterator iterator;
        ChunkPosition chunkposition;
        int i;
        int j;
        int k;
        if(this.isSmoking)
        {
            iterator = this.affectedBlockPositions.iterator();
            while(iterator.hasNext())
            {
                chunkposition = (ChunkPosition)iterator.next();
                i = chunkposition.chunkPosX;
                j = chunkposition.chunkPosY;
                k = chunkposition.chunkPosZ;
		        double d0 = (double)((float)i + this.worldObj.rand.nextFloat());
		        double d1 = (double)((float)j + this.worldObj.rand.nextFloat());
		        double d2 = (double)((float)k + this.worldObj.rand.nextFloat());
		        double d3 = d0 - this.explosionX;
		        double d4 = d1 - this.explosionY;
		        double d5 = d2 - this.explosionZ;
		        double d6 = (double)MathHelper.sqrt_double(d3 * d3 + d4 * d4 + d5 * d5);
		        d3 /= d6;
		        d4 /= d6;
		        d5 /= d6;
		        double d7 = 0.5D / (d6 / (double)this.explosionSize + 0.1D);
		        d7 *= (double)(this.worldObj.rand.nextFloat() * this.worldObj.rand.nextFloat() + 0.3F);
		        d3 *= d7;
		        d4 *= d7;
		        d5 *= d7;
		        double particleX = (d0 + this.explosionX * 1.0D) / 2.0D;
		        double particleY = (d1 + this.explosionY * 1.0D) / 2.0D;
		        double particleZ = (d2 + this.explosionZ * 1.0D) / 2.0D;
		        PacketHelper.sendToAllAround(new MessageSpawnParticle(particleX, particleY, particleZ, d3, d4, d5, "explode"), this.worldObj.provider.dimensionId, (int)particleX, (int)particleY, (int)particleZ, Packets.PACKET_SPAWN_PARTICLE_RANGE);
		        PacketHelper.sendToAllAround(new MessageSpawnParticle(d0, d1, d2, d3, d4, d5, "smoke"), this.worldObj.provider.dimensionId, (int)d0, (int)d1, (int)d2, Packets.PACKET_SPAWN_PARTICLE_RANGE);
		        if(this.isFlaming)
		        {
			        PacketHelper.sendToAllAround(new MessageSpawnParticle(d0, d1, d2, d3, d4, d5, "flame"), this.worldObj.provider.dimensionId, (int)d0, (int)d1, (int)d2, Packets.PACKET_SPAWN_PARTICLE_RANGE);
			    }
            }
        }
    }

}
