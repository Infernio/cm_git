package com.infernio.Craftmistry.common.entity;

import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.InfernoCore.world.WorldUtils;

public class EntityAlkaliItem extends EntityItem {

	public EntityAlkaliItem(World world)
	{
		super(world);
	}

	public EntityAlkaliItem(World world, double x, double y, double z, ItemStack stack)
	{
		super(world, x, y, z, stack);
	}

	@Override
    public void onUpdate()
    {
    	super.onUpdate();
    	Material material = WorldUtils.getMaterial(this.worldObj, (int)this.posX, (int)this.posY, (int)this.posZ);
    	if(material == Material.water && !this.worldObj.isRemote)
    	{
    		ExplosionCustom explosion = new ExplosionCustom(this.worldObj, this, (int)this.posX, (int)this.posY + 1, (int)this.posZ, 0.25F * this.getEntityItem().stackSize, true);
    		System.out.println(explosion.explosionSize);
    		explosion.isSmoking = false;
    		explosion.doExplosionA();
    		explosion.doExplosionB();
    		explosion.doExplosionC();
    		this.setDead();
    	}
    }

}
