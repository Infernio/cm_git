package com.infernio.Craftmistry.common.entity;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.common.registry.EntityRegistry;

public class EntityHandler {
	
	public static final int DEFAULT_TRACKING_RANGE = 64;
	public static final int DEFAULT_UPDATE_FREQUENCY = 1;

	public static void init()
	{
		EntityRegistry.registerModEntity(EntityGrenade.class, StringLib.ENTITY_GRENADE_NAME, 0, Craftmistry.instance, DEFAULT_TRACKING_RANGE, DEFAULT_UPDATE_FREQUENCY, true);
		EntityRegistry.registerModEntity(EntityAlkaliItem.class, StringLib.ENTITY_ALKALI_ITEM_NAME, 1, Craftmistry.instance, DEFAULT_TRACKING_RANGE, DEFAULT_UPDATE_FREQUENCY, true);
	}

}
