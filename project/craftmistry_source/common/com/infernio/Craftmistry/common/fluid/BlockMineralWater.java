package com.infernio.Craftmistry.common.fluid;

import java.util.Locale;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.client.fx.EntityCustomDropParticleFX;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.world.WorldUtils;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockMineralWater extends BlockFluidClassic {

	private IIcon[] icons;

	public BlockMineralWater(Fluid fluid, Material material)
	{
		super(fluid, material);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setBlockName(StringLib.BLOCK_MINERAL_WATER_NAME);
	}

    @Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
    {
    	if(!world.isRemote)
    	{
	    	if(entity instanceof EntityPlayer && !((EntityPlayer)entity).isPotionActive(Potion.regeneration.id))
	    	{
	    		((EntityPlayer)entity).addPotionEffect(new PotionEffect(Potion.regeneration.id, 25, 1));
	    	}
    	}
    }

	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random rand)
	{
		super.randomDisplayTick(world, x, y, z, rand);
		if(rand.nextInt(14) == 0 && World.doesBlockHaveSolidTopSurface(world, x, y - 1, z) && !WorldUtils.getMaterial(world, x, y - 2, z).blocksMovement())
		{
			double particleX = (double)((float)x + rand.nextFloat());
			double particleY = (double)y - 1.05D;
			double particleZ = (double)((float)z + rand.nextFloat());
			EntityCustomDropParticleFX fx = new EntityCustomDropParticleFX(world, particleX, particleY, particleZ, 0.321F, 0.741F, 0.964F);
			FMLClientHandler.instance().getClient().effectRenderer.addEffect(fx);
		}
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return side != 0 && side != 1 ? this.icons[1] : this.icons[0];
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.icons = new IIcon[]{iconRegister.registerIcon(StringLib.MODID + ":fluids/" + fluidName + "_still"), iconRegister.registerIcon(StringLib.MODID.toLowerCase(Locale.ENGLISH) + ":fluids/" + fluidName + "_flow")};
	}

}
