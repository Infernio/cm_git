package com.infernio.Craftmistry.common.fluid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.block.material.Material;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.common.registry.GameRegistry;

public class Fluids {

	public static BlockSulfuricAcid sulfuricAcidBlock;
	public static Fluid sulfuricAcid;
	public static BlockMineralWater mineralWaterBlock;
	public static Fluid mineralWater;
	public static Fluid[] elementsLiquid = new Fluid[256];
	public static Fluid[] elementsGaseous = new Fluid[256];

	public static void init()
	{
		//Create fluids
		sulfuricAcid = new Fluid(StringLib.FLUID_SULFURIC_ACID_NAME).setBlock(sulfuricAcidBlock).setViscosity(1250);
		mineralWater = new Fluid(StringLib.FLUID_MINERAL_WATER_NAME).setBlock(mineralWaterBlock);
		if(!FluidRegistry.registerFluid(sulfuricAcid))
		{
			sulfuricAcid = FluidRegistry.getFluid(StringLib.FLUID_SULFURIC_ACID_NAME);
		}
		if(!FluidRegistry.registerFluid(mineralWater))
		{
			mineralWater = FluidRegistry.getFluid(StringLib.FLUID_MINERAL_WATER_NAME);
		}

		//Create element liquids / gases
		ArrayList<Integer> elements = Elements.getValidElements();
		for(int i = 0; i < elements.size(); ++i)
		{
			Integer integer = elements.get(i);
			if(integer != null)
			{
				int atomNumber = integer.intValue();
				if(atomNumber == 0)
				{
					continue;
				}
				List<EnumMatterState> states = Arrays.asList(Elements.getPossibleStates(atomNumber));
				String elementName = Elements.getNormalName(atomNumber);
				boolean radioactive = Elements.isElementRadioactive(atomNumber);
				if(states.contains(EnumMatterState.LIQUID))
				{
					String name = "Liquid" + elementName;
					Fluid liquid = new Fluid(name);
					if(radioactive)
					{
						liquid.setLuminosity(5);
					}
					if(!FluidRegistry.registerFluid(liquid))
					{
						liquid = FluidRegistry.getFluid(name);
					}
					FluidContainerRegistry.registerFluidContainer(new FluidStack(liquid, IntLib.TEST_TUBE_VOLUME), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeSolid, 1, 0));
					elementsLiquid[atomNumber] = liquid;
				}
				if(states.contains(EnumMatterState.GAS))
				{
					String name = "Gas" + elementName;
					Fluid gas = new Fluid(name).setGaseous(true).setDensity(-50);
					if(radioactive)
					{
						gas.setLuminosity(5);
					}
					if(!FluidRegistry.registerFluid(gas))
					{
						gas = FluidRegistry.getFluid(name);
					}
					FluidContainerRegistry.registerFluidContainer(new FluidStack(gas, IntLib.TEST_TUBE_VOLUME), new ItemStack(ModItems.testTubeGas, 1, atomNumber), new ItemStack(ModItems.testTubeSolid, 1, 0));
					elementsGaseous[atomNumber] = gas;
				}
			}
		}

		//Create block implementation
		sulfuricAcidBlock = new BlockSulfuricAcid(sulfuricAcid, Material.water);
		sulfuricAcid.setBlock(sulfuricAcidBlock);
		GameRegistry.registerBlock(sulfuricAcidBlock, ItemBlock.class, StringLib.BLOCK_SULFURIC_ACID_SANE_NAME);
		mineralWaterBlock = new BlockMineralWater(mineralWater, Material.water);
		mineralWater.setBlock(mineralWaterBlock);
		GameRegistry.registerBlock(mineralWaterBlock, ItemBlock.class, StringLib.BLOCK_MINERAL_WATER_SANE_NAME);

		//Register container items
		FluidContainerRegistry.registerFluidContainer(new FluidStack(sulfuricAcid, FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(ModItems.buckets, 1, 0), new ItemStack(Items.bucket, 1, 0));
		FluidContainerRegistry.registerFluidContainer(new FluidStack(mineralWater, FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(ModItems.buckets, 1, 1), new ItemStack(Items.bucket, 1, 0));
		FluidContainerRegistry.registerFluidContainer(new FluidStack(sulfuricAcid, IntLib.TEST_TUBE_VOLUME), new ItemStack(ModItems.testTubeCompound, 1, 0), new ItemStack(ModItems.testTubeSolid, 1, 0));
		FluidContainerRegistry.registerFluidContainer(new FluidStack(FluidRegistry.WATER, IntLib.TEST_TUBE_VOLUME), new ItemStack(ModItems.testTubeCompound, 1, 2), new ItemStack(ModItems.testTubeSolid, 1, 0));
	}

}
