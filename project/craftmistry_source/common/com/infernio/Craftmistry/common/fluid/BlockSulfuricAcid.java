package com.infernio.Craftmistry.common.fluid;

import java.util.Locale;
import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.client.fx.EntityCustomDropParticleFX;
import com.infernio.Craftmistry.common.damage.CustomDS;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.InfernoCore;
import com.infernio.InfernoCore.world.WorldUtils;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockSulfuricAcid extends BlockFluidClassic {

	private IIcon[] icons;

	public BlockSulfuricAcid(Fluid fluid, Material material)
	{
		super(fluid, material);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setBlockName(StringLib.BLOCK_SULFURIC_ACID_NAME);
	}

    @Override
	public void onEntityCollidedWithBlock(World world, int x, int y, int z, Entity entity)
    {
    	if(entity instanceof EntityLivingBase)
    	{
    		entity.attackEntityFrom(CustomDS.sulfuricAcid, 2);
    		return;
	    }
    	if(entity instanceof EntityItem)
    	{
    		for(int i = 0; i < 9; ++i)
    		{
    			world.spawnParticle("smoke", entity.posX - 0.3D + Math.random(), entity.posY - 0.3D + Math.random(), entity.posZ - 0.3D + Math.random(), 0.0D, 0.0D, 0.0D);
    		}
			world.playSound(x, y, z, "random.fizz", InfernoCore.proxy.getSoundVolume(SoundCategory.AMBIENT), 1.0F, false);
    		entity.attackEntityFrom(CustomDS.sulfuricAcid, 2);
    	}
    }

	@Override
	public boolean canDisplace(IBlockAccess world, int x, int y, int z)
	{
		Material material = WorldUtils.getMaterial(world, x, y, z);
		if(material.isLiquid())
		{
			return false;
		}
		if(material == Material.leaves || material == Material.cactus || material == Material.vine || material == Material.gourd || material == Material.plants)
		{
			return true;
		}
		return super.canDisplace(world, x, y, z);
	}

	@Override
	public boolean displaceIfPossible(World world, int x, int y, int z)
	{
		Material material = WorldUtils.getMaterial(world, x, y, z);
		if(material.isLiquid())
		{
			return false;
		}
		if(material == Material.leaves || material == Material.cactus || material == Material.vine || material == Material.gourd || material == Material.plants)
		{
			world.getBlock(x, y, z).dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
			return true;
		}
		return super.displaceIfPossible(world, x, y, z);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random rand)
	{
		super.randomDisplayTick(world, x, y, z, rand);
		if(rand.nextInt(14) == 0 && World.doesBlockHaveSolidTopSurface(world, x, y - 1, z) && !WorldUtils.getMaterial(world, x, y, z).blocksMovement())
		{
			double particleX = (double)((float)x + rand.nextFloat());
			double particleY = (double)y - 1.05D;
			double particleZ = (double)((float)z + rand.nextFloat());
			EntityCustomDropParticleFX fx = new EntityCustomDropParticleFX(world, particleX, particleY, particleZ, 0.956F, 0.862F, 0.192F);
			FMLClientHandler.instance().getClient().effectRenderer.addEffect(fx);
		}
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		return side != 0 && side != 1 ? this.icons[1] : this.icons[0];
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.icons = new IIcon[]{iconRegister.registerIcon(StringLib.MODID + ":fluids/" + fluidName + "_still"), iconRegister.registerIcon(StringLib.MODID.toLowerCase(Locale.ENGLISH) + ":fluids/" + fluidName + "_flow")};
	}

}
