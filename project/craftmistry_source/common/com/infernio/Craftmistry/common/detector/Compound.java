package com.infernio.Craftmistry.common.detector;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.item.ModItems;

class Compound implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isCompound";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(ModItems.testTubeCompound);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return stack.getItem() == ModItems.testTubeCompound;
	}

}
