package com.infernio.Craftmistry.common.detector;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;

class Fuel implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isFuel";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(Items.coal);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return TileEntityFurnace.isItemFuel(stack);
	}

}