package com.infernio.Craftmistry.common.detector;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.item.ModItems;

class Gas implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isGas";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(ModItems.testTubeGas, 1, 1);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return stack.getItem() == ModItems.testTubeGas && stack.getItemDamage() > 0;
	}

}
