package com.infernio.Craftmistry.common.detector;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.recipe.CompressorRecipes;

class Compressing implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.compressing";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(ModBlocks.compressor);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return CompressorRecipes.compressing().isRegistered(stack);
	}

}