package com.infernio.Craftmistry.common.detector;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;

class PotionIngredient implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.potionIngredient";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(Items.potionitem);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return stack.getItem() != null && stack.getItem().isPotionIngredient(stack);
	}

}