package com.infernio.Craftmistry.common.detector;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.item.ModItems;

public class WeatherThundering implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return false;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isThundering";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(ModItems.multiItem, 1, 2);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return world.isThundering();
	}

}
