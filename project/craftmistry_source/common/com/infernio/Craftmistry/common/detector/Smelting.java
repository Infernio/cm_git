package com.infernio.Craftmistry.common.detector;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;

class Smelting implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.smelting";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(Blocks.furnace);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return FurnaceRecipes.smelting().getSmeltingResult(stack) != null;
	}

}