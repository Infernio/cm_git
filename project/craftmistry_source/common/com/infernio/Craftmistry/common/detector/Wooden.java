package com.infernio.Craftmistry.common.detector;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;

class Wooden implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isWooden";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(Blocks.planks);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		if(stack.getItem() instanceof ItemBlock)
		{
			Block block = Block.getBlockFromItem(stack.getItem());
			if(block.getMaterial() == Material.wood)
			{
				return true;
			}
			return false;
		}
		if(stack.getItem() instanceof ItemTool)
		{
			if(((ItemTool)stack.getItem()).getToolMaterialName().equals("WOOD"))
			{
				return true;
			}
			return false;
		}
		Item item = stack.getItem();
		if(item instanceof ItemSword)
		{
			if(((ItemSword)item).getToolMaterialName().equals("WOOD"))
			{
				return true;
			}
			return false;
		}
		if(item instanceof ItemHoe)
		{
			if(((ItemHoe)item).getToolMaterialName().equals("WOOD"))
			{
				return true;
			}
			return false;
		}
		return false;
	}

}