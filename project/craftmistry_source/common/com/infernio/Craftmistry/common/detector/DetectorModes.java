package com.infernio.Craftmistry.common.detector;

import java.util.ArrayList;

import com.infernio.Craftmistry.api.tile.IDetectorMode;

public class DetectorModes {

	private static DetectorModes instance = new DetectorModes();

	private ArrayList<IDetectorMode> modes = new ArrayList<IDetectorMode>();

	private DetectorModes() {}

	public static DetectorModes detections()
	{
		return instance;
	}

	public void registerMode(IDetectorMode mode)
	{
		this.modes.add(mode);
	}

	public IDetectorMode getMode(int id)
	{
		return this.modes.get(id);
	}

	public int getNumberRegisteredModes()
	{
		return this.modes.size();
	}

	public ArrayList<IDetectorMode> getModes()
	{
		return this.modes;
	}

	public static void init()
	{
		instance.registerMode(new Fuel());
		instance.registerMode(new Smelting());
		instance.registerMode(new PotionIngredient());
		instance.registerMode(new Infusing());
		instance.registerMode(new Compressing());
		instance.registerMode(new Food());
		instance.registerMode(new Spoiled());
		instance.registerMode(new Wooden());
		instance.registerMode(new ElementStorage());
		instance.registerMode(new StoresElement());
		instance.registerMode(new Solid());
		instance.registerMode(new Liquid());
		instance.registerMode(new Gas());
		instance.registerMode(new Plasma());
		instance.registerMode(new Compound());
		instance.registerMode(new Radioactive());
		instance.registerMode(new WeatherClear());
		instance.registerMode(new WeatherRaining());
		instance.registerMode(new WeatherThundering());
		instance.registerMode(new TimeDay());
		instance.registerMode(new TimeNight());
	}

}
