package com.infernio.Craftmistry.common.detector;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.item.ItemSpoiledFood;

public class Spoiled implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isSpoiled";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(Items.rotten_flesh);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return stack.getItem() instanceof ItemSpoiledFood || stack.getItem() == Items.rotten_flesh;
	}

}
