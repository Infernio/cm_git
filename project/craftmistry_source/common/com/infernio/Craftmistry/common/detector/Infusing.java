package com.infernio.Craftmistry.common.detector;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.recipe.InfuserRecipes;

class Infusing implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.infusing";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(ModBlocks.infuser);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return InfuserRecipes.infusing().getResult(stack) != null;
	}

}