package com.infernio.Craftmistry.common.detector;

import net.minecraft.init.Items;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.tile.IDetectorMode;

public class Food implements IDetectorMode {

	@Override
	public boolean requiresItem()
	{
		return true;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile.blockCMDetector.mode.isFood";
	}

	@Override
	public ItemStack getDisplayStack()
	{
		return new ItemStack(Items.cooked_porkchop);
	}

	@Override
	public boolean detect(ItemStack stack, World world)
	{
		return stack.getItem() instanceof ItemFood;
	}

}
