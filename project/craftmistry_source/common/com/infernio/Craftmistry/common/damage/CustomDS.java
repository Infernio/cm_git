package com.infernio.Craftmistry.common.damage;

import net.minecraft.util.DamageSource;

public class CustomDS {

	public static DamageSource noOxygen;
	public static DamageSource radiation;
	public static DamageSource sulfuricAcid;
	public static DamageSource corrosionElement;
	public static DamageSource corrosionCompound;

	public static void init()
	{
		noOxygen = new DamageSourceNoArmor("noOxygen");
		radiation = new DamageSourceNoArmor("radiation");
		sulfuricAcid = new DamageSourceNoArmor("sulfurAcid");
		corrosionElement = new NormalDamageSource("corrosionElement");
		corrosionCompound = new NormalDamageSource("corrosionCompound");
	}

}
