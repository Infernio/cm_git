package com.infernio.Craftmistry.common.damage;

import net.minecraft.util.DamageSource;

public class DamageSourceNoArmor extends DamageSource {

	public DamageSourceNoArmor(String name)
	{
		super(name);
		this.setDamageBypassesArmor();
	}

}
