package com.infernio.Craftmistry.common.damage;

import net.minecraft.util.DamageSource;

public class FireDamageSource extends DamageSource {

	public FireDamageSource(String name)
	{
		super(name);
		this.setFireDamage();
	}

}
