package com.infernio.Craftmistry.common.tile;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.FakePlayerFactory;

import com.infernio.Craftmistry.api.tile.ITreeHandler;
import com.infernio.Craftmistry.api.tile.LeafEvent.CanHarvestLeafEvent;
import com.infernio.Craftmistry.api.tile.LeafEvent.HarvestLeafEvent;
import com.infernio.Craftmistry.api.tile.SaplingEvent.CanPlantSaplingEvent;
import com.infernio.Craftmistry.api.tile.SaplingEvent.PlantSaplingEvent;
import com.infernio.Craftmistry.api.tile.WoodEvent.CanHarvestWoodEvent;
import com.infernio.Craftmistry.api.tile.WoodEvent.HarvestWoodEvent;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.recipe.ForesterRegistry;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageForesterBoneMeal;
import com.infernio.InfernoCore.math.MathUtils;
import com.infernio.InfernoCore.math.Vector;
import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.Packets;
import com.infernio.InfernoCore.network.message.MessagePlayAuxSFX;
import com.infernio.InfernoCore.tile.TileMachineInventory;
import com.infernio.InfernoCore.util.ItemUtils;
import com.infernio.InfernoCore.util.TemporaryInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileForester extends TileMachineInventory
{
	private static final int INVENTORY_SIZE = 17;
	private static final int STANDARD_RANGE = 25;
	private static final int RANGE_PER_UPGRADE = 10;
	private static final int ENERGY_FULL = 25000;
	private static final int SLEEP_TIME = 20;
	public static final int MAXIMUM_UPGRADES = 5;

    private int range;
    private int sleepTicks;
    private boolean shouldSleep;
    private ForesterTask currentTask;
    private ArrayList<TreeNode> nodes;
    private EntityPlayer player;
    private TreeHarvester treeHarvester;

    @SideOnly(Side.CLIENT)
    public int treesMaintained;
    @SideOnly(Side.CLIENT)
    public int task;

    public TileForester()
    {
    	super(INVENTORY_SIZE);
    	this.range = STANDARD_RANGE;
    	this.nodes = new ArrayList<TreeNode>();    	
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.range = tag.getByte("Range");
        int nodes = tag.getInteger("Nodes");
        for(int i = 0; i < nodes; ++i)
        {
        	if(tag.hasKey("Node" + i))
        	{
        		TreeNode node = new TreeNode();
        		node.readFromNBT(tag.getCompoundTag("Node" + i));
        		this.nodes.add(node);
        	}
        }
        if(tag.hasKey("TreeHarvester"))
        {
        	this.treeHarvester = new TreeHarvester("", new ArrayList<Vector>());
        	this.treeHarvester.readFromNBT(tag.getCompoundTag("TreeHarvester"));
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setByte("Range", (byte)this.range);
        int nodes = 0;
        for(TreeNode node : this.nodes)
        {
        	if(node != null)
        	{
        		NBTTagCompound tag1 = new NBTTagCompound();
        		node.writeToNBT(tag1);
        		tag.setTag("Node" + nodes, tag1);
        		++nodes;
        	}
        }
        if(this.treeHarvester != null)
        {
        	NBTTagCompound tag1 = new NBTTagCompound();
        	this.treeHarvester.writeToNBT(tag1);
        	tag.setTag("TreeHarvester", tag1);
        }
        else
        {
        	if(tag.hasKey("TreeHarvester"))
        	{
        		tag.removeTag("TreeHarvester");
        	}
        }
        tag.setInteger("Nodes", nodes);
    }

    @Override
    public void onUpdate()
    {
    	if(this.player == null && this.worldObj instanceof WorldServer)
    	{
    		this.player = FakePlayerFactory.getMinecraft((WorldServer)this.worldObj);
    	}
		this.checkUpgrades();
		if(this.shouldSleep && this.sleepTicks < SLEEP_TIME)
		{
	    	++this.sleepTicks;
		}
		else
		{
			this.sleepTicks = 0;
			if(this.currentTask == null)
			{
				ForesterTask task = this.getNextTask();
		    	if(task != null && task != ForesterTask.SLEEP)
		    	{
		    		this.shouldSleep = false;
		    		this.currentTask = task;
		    		this.executeTask();
		    	}
		    	else
		    	{
		    		this.shouldSleep = true;
		    	}
			}
			else
			{
				this.executeTask();
			}
		}
    }

    private void executeTask()
    {
    	this.checkNodeRange();
    	int energyRequired = this.currentTask.energyRequired;
    	if(this.energy >= energyRequired)
    	{
    		switch(this.currentTask)
    		{
    		case PLANT:
    			this.executePlant();
    			break;
    		case HARVEST:
    			this.executeHarvest();
    			break;
    		case BONE_MEAL:
    			this.executeBoneMeal();
    			break;
			case SLEEP:
			default:
				break;
    		}
    	}
    }

    private void executePlant()
    {
    	for(TreeNode node : this.nodes)
    	{
    		if(this.tryPlantSapling(node))
    		{
    			this.energy -= ForesterTask.PLANT.energyRequired;
    			return;
    		}
    	}
    	this.currentTask = null;
    	this.shouldSleep = false;
    }

    private boolean tryPlantSapling(TreeNode node)
    {
    	if(!this.saplingAt(node))
		{
    		int slot = this.findSaplingSlot(node.tree);
    		if(slot != -1)
    		{
    			ItemStack sapling = this.inv[slot];
    			if(this.canPlantSaplingAt(sapling, node))
    			{
	    			boolean ret = this.plantSapling(sapling, node);
	    			if(ret)
	    			{
	    				this.decrStackSize(slot, 1);
	    			}
	    			return ret;
    			}
    		}
		}
    	return false;
    }

    private boolean canPlantSaplingAt(ItemStack sapling, TreeNode node)
    {
    	CanPlantSaplingEvent event = new CanPlantSaplingEvent(sapling, this.worldObj, node.x, node.y, node.z);
    	if(MinecraftForge.EVENT_BUS.post(event))
    	{
    		return false;
    	}
    	Block block = this.worldObj.getBlock(node.x, node.y - 1, node.z);
    	Block block1 = this.worldObj.getBlock(node.x, node.y, node.z);
    	boolean b1 = block != null && !block.isAir(this.worldObj, node.x, node.y - 1, node.z) && block.isNormalCube(this.worldObj, node.x, node.y - 1, node.z) && block.isOpaqueCube();
    	boolean b2 = block1 == null || block1.isAir(this.worldObj, node.x, node.y, node.z);
    	return b1 && b2;
    }

    private boolean plantSapling(ItemStack sapling, TreeNode node)
    {
    	PlantSaplingEvent event = new PlantSaplingEvent(sapling, this.worldObj, node.x, node.y, node.z);
    	if(MinecraftForge.EVENT_BUS.post(event) || event.planted)
    	{
    		return event.planted;
    	}
    	Block block = Block.getBlockFromItem(sapling.getItem());
    	int meta = sapling.getItemDamage();
    	if(!this.worldObj.setBlock(node.x, node.y, node.z, block, meta, 3))
    	{
    		return false;
    	}
		PacketHelper.sendToAllAround(new MessagePlayAuxSFX(node.x, node.y, node.z, 2001, Block.getIdFromBlock(block) + (meta << 12)), worldObj.provider.dimensionId, node.x, node.y, node.z, Packets.PACKET_PLAY_AUX_SFX_RANGE);
    	TileEntity tile = null;
    	if(block instanceof ITileEntityProvider)
    	{
    		tile = ((ITileEntityProvider)block).createNewTileEntity(this.worldObj, meta);
    	}
    	else if(block.hasTileEntity(meta))
    	{
    		tile = block.createTileEntity(this.worldObj, meta);
    	}
		if(tile != null)
		{
			if(sapling.getTagCompound() != null)
			{
				tile.readFromNBT(sapling.getTagCompound());
			}
    		this.worldObj.setTileEntity(node.x, node.y, node.z, tile);
		}
		return true;
    }

    private int findSaplingSlot(String tree)
    {
    	List<ItemStack> saplings = ForesterRegistry.getInstance().getSaplings(tree);
    	if(saplings != null && !saplings.isEmpty())
    	{
    		for(ItemStack sapling : saplings)
    		{
    			if(sapling != null)
    			{
    				ArrayList<Integer> slots = ItemUtils.findItemSlots(this, sapling.getItem(), sapling.getItemDamage());
    				if(!slots.isEmpty())
    				{
    					for(Integer slot : slots)
    					{
    						if(slot != null)
    						{
    							return slot.intValue();
    						}
    					}
    				}
    			}
    		}
    	}
    	return -1;
    }

    private void executeHarvest()
    {
    	if(this.treeHarvester == null)
    	{
    		for(TreeNode node : this.nodes)
    		{
    			if(woodAt(node) || this.leafAt(node))
    			{
    				ArrayList<Vector> list = new ArrayList<Vector>(1);
    				list.add(new Vector(node.x, node.y, node.z));
    				this.treeHarvester = new TreeHarvester(node.tree, list);
    				break;
    			}
    		}
    	}
    	if(!this.treeHarvester.isDone())
    	{
    		boolean harvested = this.treeHarvester.harvest();
    		if(harvested)
    		{
    			this.energy -= ForesterTask.HARVEST.energyRequired;
    		}
    		else
    		{
    			this.treeHarvester = null;
    			this.currentTask = null;
    			this.shouldSleep = false;
    		}
    		return;
    	}
    	this.treeHarvester = null;
    	this.currentTask = null;
    	this.shouldSleep = false;
    }

    private void executeBoneMeal()
    {
    	List<Integer> slots = ItemUtils.findItemSlots(this, Items.dye, 15);
    	if(!slots.isEmpty())
    	{
    		Integer slot = slots.get(0);
    		if(slot != null)
    		{
    			for(TreeNode node : this.nodes)
    			{
    				if(this.saplingAt(node))
    				{
    					ItemDye.applyBonemeal(this.getStackInSlot(slot), this.worldObj, node.x, node.y, node.z, this.player);
    					PacketHandler.sendToAllAround(new MessageForesterBoneMeal(node.x, node.y, node.z), this.worldObj.provider.dimensionId, node.x, node.y, node.z, Packets.PACKET_SPAWN_PARTICLE_RANGE);
    					if(this.getStackInSlot(slot).stackSize <= 0)
    					{
    						this.setInventorySlotContents(slot, null);
    					}
    					this.energy -= ForesterTask.BONE_MEAL.energyRequired;
    					this.currentTask = null;
    					this.shouldSleep = true;
    					return;
    				}
    			}
    		}
    	}
    	this.currentTask = null;
    	this.shouldSleep = false;
    }

    private ForesterTask getNextTask()
    {
    	this.checkNodeRange();
    	ForesterTask ret = this.checkSaplings();
    	if(ret != null)
    	{
    		return ret;
    	}
    	ret = this.checkHarvest();
    	if(ret != null)
    	{
    		return ret;
    	}
    	ret = this.checkBoneMeal();
    	if(ret != null)
    	{
    		return ret;
    	}
    	return ForesterTask.SLEEP;
    }

    private ForesterTask checkSaplings()
    {
    	for(TreeNode node : this.nodes)
    	{
    		if(!this.saplingAt(node))
    		{
    			List<ItemStack> saplings = ForesterRegistry.getInstance().getSaplings(node.tree);
    			if(saplings != null && !saplings.isEmpty())
    			{
    				for(ItemStack sapling : saplings)
    				{
    					if(this.canPlantSaplingAt(sapling, node))
    					{
    						return ForesterTask.PLANT;
    					}
    				}
    			}
    		}
    	}
    	return null;
    }

    private boolean saplingAt(TreeNode node)
    {
    	return this.saplingAt(node.tree, node.x, node.y, node.z);
    }

    private boolean saplingAt(String tree, int x, int y, int z)
    {
    	Block block = this.worldObj.getBlock(x, y, z);
    	if(block == null || block.isAir(this.worldObj, x, y, z))
    	{
    		return false;
    	}
    	else
    	{
    		ITreeHandler handler = ForesterRegistry.getInstance().getHandler(tree);
    		if(handler != null)
    		{
    			return handler.isSaplingValid(tree, this.worldObj, x, y, z);
    		}
    		List<ItemStack> saplings = ForesterRegistry.getInstance().getSaplings(tree);
    		if(saplings != null && !saplings.isEmpty())
    		{
	    		ItemStack sapling = new ItemStack(block, 1, this.worldObj.getBlockMetadata(x, y, z));
	    		for(ItemStack stack : saplings)
	    		{
	    			if(ItemUtils.areItemStacksEqual(stack, sapling, false))
	    			{
	    				return true;
	    			}
	    		}
    		}
    		return false;
    	}
    }

    private ForesterTask checkHarvest()
    {
    	for(TreeNode node : this.nodes)
    	{
    		if(this.woodAt(node) || this.leafAt(node))
    		{
    			return ForesterTask.HARVEST;
    		}
    	}
    	return null;
    }

    private boolean woodAt(TreeNode node)
    {
    	return this.woodAt(node.tree, node.x, node.y, node.z);
    }

    private boolean woodAt(String tree, int x, int y, int z)
    {
    	Block block = this.worldObj.getBlock(x, y, z);
    	if(block == null || block.isAir(this.worldObj, x, y, z))
    	{
    		return false;
    	}
    	else
    	{
    		ITreeHandler handler = ForesterRegistry.getInstance().getHandler(tree);
    		if(handler != null)
    		{
    			return handler.isWoodValid(tree, this.worldObj, x, y, z);
    		}
    		List<ItemStack> wood = ForesterRegistry.getInstance().getWood(tree);
    		if(wood != null && !wood.isEmpty())
    		{
	    		ItemStack stack = new ItemStack(block, 1, this.worldObj.getBlockMetadata(x, y, z));
	    		for(ItemStack stack1 : wood)
	    		{
	    			if(ItemUtils.areItemStacksEqual(stack, stack1, false))
	    			{
	    				return true;
	    			}
	    		}
    		}
	    	return false;
    	}
    }

	private boolean leafAt(TreeNode node)
    {
    	return this.leafAt(node.tree, node.x, node.y, node.z);
    }

    private boolean leafAt(String tree, int x, int y, int z)
    {
    	Block block = this.worldObj.getBlock(x, y, z);
    	if(block == null || block.isAir(this.worldObj, x, y, z))
    	{
    		return false;
    	}
    	else
    	{
    		ITreeHandler handler = ForesterRegistry.getInstance().getHandler(tree);
    		if(handler != null)
    		{
    			return handler.isLeafValid(tree, this.worldObj, x, y, z);
    		}
    		List<ItemStack> leaves = ForesterRegistry.getInstance().getLeaves(tree);
    		if(leaves != null && !leaves.isEmpty())
    		{
	    		ItemStack stack = new ItemStack(block, 1, this.worldObj.getBlockMetadata(x, y, z));
	    		for(ItemStack leaf : leaves)
	    		{
	    			if(ItemUtils.areItemStacksEqual(stack, leaf, false))
	    			{
	    				return true;
	    			}
	    		}
    		}
    		return false;
    	}
    }

    private ForesterTask checkBoneMeal()
    {
    	ArrayList<ItemStack> boneMeal = ItemUtils.findItems(this, Items.dye, 15);
    	if(!boneMeal.isEmpty())
    	{
	    	for(TreeNode node : this.nodes)
	    	{
	    		if(this.saplingAt(node))
	    		{
	    			return ForesterTask.BONE_MEAL;
	    		}
	    	}
    	}
    	return null;
    }

    private void checkNodeRange()
    {
    	ArrayList<TreeNode> nodesToRemove = new ArrayList<TreeNode>();
    	for(TreeNode node : this.nodes)
    	{
    		if(MathUtils.distance(this.xCoord, this.yCoord, this.zCoord, node.x, node.y, node.z) > this.range)
    		{
    			nodesToRemove.add(node);
    		}
    	}
    	if(!nodesToRemove.isEmpty())
    	{
    		this.nodes.removeAll(nodesToRemove);
    	}
    }

	private void checkUpgrades()
    {
		ItemStack stack = this.inv[16];
		if(stack != null)
		{
			if(stack.getItem() != ModItems.upgrade)
			{
				if(!this.worldObj.isRemote)
				{
					this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.xCoord + Math.random(), this.yCoord + Math.random(), this.zCoord + Math.random(), stack.copy()));
				}
				this.inv[16] = null;
				if(this.range != STANDARD_RANGE)
				{
					this.range = STANDARD_RANGE;
					this.checkNodeRange();
				}
				return;
			}
			if(stack.stackSize > MAXIMUM_UPGRADES)
			{
				int over = stack.stackSize - MAXIMUM_UPGRADES;
				this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.xCoord + Math.random(), this.yCoord + 1 + Math.random(), this.zCoord + Math.random(), new ItemStack(stack.getItem(), over, stack.getItemDamage())));
				stack.stackSize = MAXIMUM_UPGRADES;
			}
			int newRange = STANDARD_RANGE + (stack.stackSize * RANGE_PER_UPGRADE);
			if(this.range != newRange)
			{
				this.range = newRange;
				this.checkNodeRange();
			}
		}
		else
		{
			if(this.range != STANDARD_RANGE)
			{
				this.range = STANDARD_RANGE;
				this.checkNodeRange();
			}
		}
	}

	public void addNode(String tree, int x, int y, int z)
	{
		TreeNode newNode = new TreeNode(tree, x, y, z);
		for(TreeNode node : this.nodes)
		{
			if(node.equals(newNode))
			{
				return;
			}
		}
		this.clearNodesOnXZ(x, z);
		this.nodes.add(newNode);
	}

	private void clearNodesOnXZ(int x, int z)
	{
		ArrayList<TreeNode> nodesToRemove = new ArrayList<TreeNode>();
		for(TreeNode node : this.nodes)
		{
			if(node.x == x && node.z == z)
			{
				nodesToRemove.add(node);
			}
		}
		if(!nodesToRemove.isEmpty())
		{
			this.nodes.removeAll(nodesToRemove);
		}
	}

	public int getNumberNodes()
	{
		return this.nodes.size();
	}

	public ForesterTask getCurrentTask()
	{
		return this.currentTask;
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 32;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_FORESTER_NAME;
	}

	public static enum ForesterTask
	{
		PLANT(30),
		HARVEST(15),
		BONE_MEAL(10),
		SLEEP(0);

		public final int energyRequired;

		private ForesterTask(int energy)
		{
			this.energyRequired = energy;
		}
	}

	private static final class TreeNode
	{
		public int x;
		public int y;
		public int z;
		public String tree;

		public TreeNode()
		{
			this("", 0, 0, 0);
		}

		public TreeNode(String tree, int x, int y, int z)
		{
			this.tree = tree;
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public void readFromNBT(NBTTagCompound tag)
		{
			this.tree = tag.getString("TreeName");
			this.x = tag.getInteger("X");
			this.y = tag.getInteger("Y");
			this.z = tag.getInteger("Z");
		}

		public void writeToNBT(NBTTagCompound tag)
		{
			tag.setString("TreeName", this.tree);
			tag.setInteger("X", this.x);
			tag.setInteger("Y", this.y);
			tag.setInteger("Z", this.z);
		}

		@Override
		public boolean equals(Object other)
		{
			if(!(other instanceof TreeNode))
			{
				return false;
			}
			TreeNode node = (TreeNode)other;
			return node.tree.equals(this.tree) && node.x == this.x && node.y == this.y && node.z == this.z;
		}
	}

	private class TreeHarvester
	{
		public String tree;
		public List<Vector> targetBlocks;
		public List<Vector> nextTargetBlocks;

		public TreeHarvester(String tree, List<Vector> targetBlocks)
		{
			this.tree = tree;
			this.targetBlocks = targetBlocks;
			this.nextTargetBlocks = new ArrayList<Vector>(targetBlocks.size());
		}

		public boolean harvest()
		{
			if(!this.isDone())
			{
				if(this.targetBlocks.isEmpty())
				{
					this.targetBlocks = this.nextTargetBlocks;
					this.nextTargetBlocks = new ArrayList<Vector>();
				}
				Vector vec = this.targetBlocks.get(0);
				if(woodAt(this.tree, vec.x, vec.y, vec.z) && this.canHarvestWood(vec))
				{
					if(this.harvestWood(vec))
					{
						this.targetBlocks.remove(0);
						return true;
					}
				}
				else if(leafAt(this.tree, vec.x, vec.y, vec.z) && this.canHarvestLeaf(vec))
				{
					if(this.harvestLeaf(vec))
					{
						this.targetBlocks.remove(0);
						return true;
					}
				}
				else
				{
					this.targetBlocks.remove(0);
				}
			}
			return false;
		}

		private boolean canHarvestWood(Vector vec)
		{
			CanHarvestWoodEvent event = new CanHarvestWoodEvent(this.tree, worldObj, vec.x, vec.y, vec.z);
			return !MinecraftForge.EVENT_BUS.post(event);
		}

		private boolean canHarvestLeaf(Vector vec)
		{
			CanHarvestLeafEvent event = new CanHarvestLeafEvent(this.tree, worldObj, vec.x, vec.y, vec.z);
			return !MinecraftForge.EVENT_BUS.post(event);
		}

		private boolean harvestWood(Vector vec)
		{
			TemporaryInventory forester = TemporaryInventory.wrap(0, INVENTORY_SIZE - 1, inv);
			int x = vec.x;
			int y = vec.y;
			int z = vec.z;
			HarvestWoodEvent event = new HarvestWoodEvent(this.tree, worldObj, forester, x, y, z);
			if(MinecraftForge.EVENT_BUS.post(event) || event.harvested)
			{
				return event.harvested;
			}
			int meta = worldObj.getBlockMetadata(x, y, z);
			Block block = worldObj.getBlock(x, y, z);
			block.onBlockPreDestroy(worldObj, x, y, z, meta);
			block.onBlockDestroyedByPlayer(worldObj, x, y, z, meta);
			boolean ret = worldObj.setBlockToAir(x, y, z);
			if(ret)
			{
				ArrayList<ItemStack> drops = block.getDrops(worldObj, x, y, z, meta, 0);
				for(ItemStack stack : drops)
				{
					ItemStack remaining = ItemUtils.addItemStack(stack, forester);
					if(remaining != null && remaining.stackSize > 0)
					{
						ItemUtils.dropItem(worldObj, x, y, z, remaining);
					}
				}
				PacketHelper.sendToAllAround(new MessagePlayAuxSFX(x, y, z, 2001, Block.getIdFromBlock(block) + (meta << 12)), worldObj.provider.dimensionId, x, y, z, Packets.PACKET_PLAY_AUX_SFX_RANGE);
				this.addNextTargets(x, y, z);
			}
			return ret;
		}

		private boolean harvestLeaf(Vector vec)
		{
			TemporaryInventory forester = TemporaryInventory.wrap(0, INVENTORY_SIZE - 1, inv);
			int x = vec.x;
			int y = vec.y;
			int z = vec.z;
			HarvestLeafEvent event = new HarvestLeafEvent(this.tree, worldObj, forester, x, y, z);
			if(MinecraftForge.EVENT_BUS.post(event) || event.harvested)
			{
				return event.harvested;
			}
			int meta = worldObj.getBlockMetadata(x, y, z);
			Block block = worldObj.getBlock(x, y, z);
			block.onBlockPreDestroy(worldObj, x, y, z, meta);
			block.onBlockDestroyedByPlayer(worldObj, x, y, z, meta);
			boolean ret = worldObj.setBlockToAir(x, y, z);
			if(ret)
			{
				ArrayList<ItemStack> drops = block.getDrops(worldObj, x, y, z, meta, 0);
				for(ItemStack stack : drops)
				{
					ItemStack remaining = ItemUtils.addItemStack(stack, forester);
					if(remaining != null && remaining.stackSize > 0)
					{
						ItemUtils.dropItem(worldObj, x, y, z, remaining);
					}
				}
				PacketHelper.sendToAllAround(new MessagePlayAuxSFX(x, y, z, 2001, Block.getIdFromBlock(block) + (meta << 12)), worldObj.provider.dimensionId, x, y, z, Packets.PACKET_PLAY_AUX_SFX_RANGE);
				this.addNextTargets(x, y, z);
			}
			return ret;
		}

		private void addNextTargets(int x, int y, int z)
		{
			this.addTarget(x - 1, y, z);
			this.addTarget(x + 1, y, z);
			this.addTarget(x, y, z - 1);
			this.addTarget(x, y, z + 1);
			this.addTarget(x, y - 1, z);
			this.addTarget(x, y + 1, z);
		}

		private void addTarget(int x, int y, int z)
		{
			if(woodAt(this.tree, x, y, z) || leafAt(this.tree, x, y, z))
			{
				Vector vec = new Vector(x, y, z);
				for(Vector vec1 : this.nextTargetBlocks)
				{
					if(vec1.equals(vec))
					{
						return;
					}
				}
				this.nextTargetBlocks.add(vec);
			}
		}

		public boolean isDone()
		{
			return this.targetBlocks.isEmpty() && this.nextTargetBlocks.isEmpty();
		}

		public void writeToNBT(NBTTagCompound tag)
		{
			tag.setString("Tree", this.tree);
			tag.setInteger("TargetBlocksLength", this.targetBlocks.size());
			for(int i = 0; i < this.targetBlocks.size(); ++i)
			{
				Vector vec = this.targetBlocks.get(i);
				NBTTagCompound tag1 = new NBTTagCompound();
				tag1.setInteger("X", vec.x);
				tag1.setInteger("Y", vec.y);
				tag1.setInteger("Z", vec.z);
				tag.setTag("TargetBlock" + i, tag1);
			}
			tag.setInteger("NextTargetBlocksLength", this.nextTargetBlocks.size());
			for(int i = 0; i < this.nextTargetBlocks.size(); ++i)
			{
				Vector vec = this.nextTargetBlocks.get(i);
				NBTTagCompound tag1 = new NBTTagCompound();
				tag1.setInteger("X", vec.x);
				tag1.setInteger("Y", vec.y);
				tag1.setInteger("Z", vec.z);
				tag.setTag("NextTargetBlock" + i, tag1);
			}
		}

		public void readFromNBT(NBTTagCompound tag)
		{
			this.tree = tag.getString("Tree");
			int length = tag.getInteger("TargetBlocksLength");
			this.targetBlocks = new ArrayList<Vector>(length);
			for(int i = 0; i < length; ++i)
			{
				NBTTagCompound tag1 = tag.getCompoundTag("TargetBlock" + i);
				Vector vec = new Vector(tag1.getInteger("X"), tag1.getInteger("Y"), tag1.getInteger("Z"));
				this.targetBlocks.set(i, vec);
			}
			length = tag.getInteger("NextTargetBlocksLength");
			this.nextTargetBlocks = new ArrayList<Vector>(length);
			for(int i = 0; i < length; ++i)
			{
				NBTTagCompound tag1 = tag.getCompoundTag("NextTargetBlock" + i);
				Vector vec = new Vector(tag1.getInteger("X"), tag1.getInteger("Y"), tag1.getInteger("Z"));
				this.nextTargetBlocks.set(i, vec);
			}
		}
	}
}
