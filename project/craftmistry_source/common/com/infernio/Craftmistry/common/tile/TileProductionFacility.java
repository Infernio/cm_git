package com.infernio.Craftmistry.common.tile;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.research.ResearchProduct;

import net.minecraft.tileentity.TileEntity;

public class TileProductionFacility extends TileEntity implements IResearchTile {

	private ResearchNetwork network;

	@Override
	public void onProjectFinished(ResearchProduct product) {}

	@Override
	public String getType()
	{
		return "ProductionFacility";
	}

	@Override
	public void setResearchNetwork(ResearchNetwork network)
	{
		this.network = network;
	}

	@Override
	public ResearchNetwork getNetwork()
	{
		return this.network;
	}
}
