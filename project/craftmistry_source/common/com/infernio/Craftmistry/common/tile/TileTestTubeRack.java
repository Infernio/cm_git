package com.infernio.Craftmistry.common.tile;

import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileBasicInventory;

public class TileTestTubeRack extends TileBasicInventory {

	public static final int INVENTORY_LENGTH = 6;

	public TileTestTubeRack()
	{
		super(INVENTORY_LENGTH);
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_TEST_TUBE_RACK_NAME;
	}

	public boolean hasSlotTestTube(int slot)
	{
		return ItemHelper.isTestTube(this.getStackInSlot(slot));
	}
}
