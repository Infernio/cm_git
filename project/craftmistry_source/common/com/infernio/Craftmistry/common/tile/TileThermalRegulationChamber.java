package com.infernio.Craftmistry.common.tile;

import ic2.api.energy.event.EnergyTileLoadEvent;
import ic2.api.energy.event.EnergyTileUnloadEvent;
import ic2.api.energy.tile.IEnergySink;

import java.util.Arrays;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ForgeDirection;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerHandler;
import buildcraft.api.power.PowerHandler.PowerReceiver;
import buildcraft.api.transport.IPipeConnection;
import buildcraft.api.transport.IPipeTile.PipeType;

import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.InfernoCore.multiblock.MultiblockInventoryPart;
import com.infernio.InfernoCore.multiblock.MultiblockSize;

import cpw.mods.fml.common.FMLCommonHandler;

public class TileThermalRegulationChamber extends MultiblockInventoryPart implements IEnergySink, IPowerReceptor, IPipeConnection {

	private static final MultiblockSize size = new MultiblockSize(3, 3, 3);
	public static final int ENERGY_FULL = 20000;
	public static final int ENERGY_CHANGE_TEMPERATURE = 25;
	public static final int ENERGY_CHANGE_STATE = 500;
	private static final int INVENTORY_SIZE = 1;
	private boolean firstTick;
	public int energy;
	private boolean modifiersSet;
	private float upwardsTemperatureModifier;
	private float downwardsTemperatureModifier;
	public int temperature;
	public int targetTemperature;
	private int counter;
	private int stateChangeCooldown;
	public boolean hasTargetTemperature;
	private PowerHandler handler;

	public TileThermalRegulationChamber()
	{
		super(INVENTORY_SIZE);
		this.firstTick = true;
		this.modifiersSet = false;
		this.handler = new PowerHandler(this, PowerHandler.Type.MACHINE);
		this.handler.configure(0.0F, 50.0F, 0.0F, ENERGY_FULL / 5.0F);
		this.handler.configurePowerPerdition(1, 1);
	}

    @Override
    public void onChunkUnload()
    {
    	super.onChunkUnload();
    	if(!this.worldObj.isRemote)
    	{
	    	MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
	    }
    }
    
    @Override
    public void invalidate()
    {
    	super.invalidate();
    	if(!this.worldObj.isRemote)
    	{
	    	MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
	    }
    }

    @Override
    public void updateEntity()
    {
    	if(this.worldObj.isRemote)
    	{
    		return;
    	}
    	super.updateEntity();
    	if(this.firstTick)
    	{
	    	if(!this.worldObj.isRemote)
	    	{
	    		MinecraftForge.EVENT_BUS.post(new EnergyTileLoadEvent(this));
	    	}
	    	this.firstTick = false;
    	}
    	this.fillBuffer();
    	if(this.isMaster)
    	{
    		if(!this.modifiersSet)
    		{
	    		float temperature = this.worldObj.getBiomeGenForCoords(xCoord, zCoord).temperature;
	    		if(temperature > 1.0F)
	    		{
	    			this.downwardsTemperatureModifier = MathHelper.clamp_float(temperature + 1.0F, 0.0F, 1.0F);
	    			this.upwardsTemperatureModifier = MathHelper.clamp_float(temperature - 1.0F, 0.0F, 1.0F);
	    		}
	    		else if(temperature < 0.1F)
	    		{
	    			this.downwardsTemperatureModifier = MathHelper.clamp_float(temperature + 1.0F, 0.0F, 1.0F);
	    			this.upwardsTemperatureModifier = MathHelper.clamp_float(temperature - 1.0F, 0.0F, 1.0F);
	    		}
	    		else
	    		{
	    			this.downwardsTemperatureModifier = MathHelper.clamp_float(temperature, 0.0F, 1.0F);
	    			this.upwardsTemperatureModifier = MathHelper.clamp_float(temperature, 0.0F, 1.0F);
	    		}
	    		this.modifiersSet = true;
    		}
    		if(this.inv[0] != null)
    		{
    			if(this.stateChangeCooldown < 20)
    			{
    				++this.stateChangeCooldown;
    			}
    			else
    			{
    				this.stateChangeCooldown = 0;
		    		this.changeState();
	    		}
    		}
    		if(this.hasTargetTemperature && this.temperature < this.targetTemperature)
    		{
    			if(this.counter < 5 - (int)(this.downwardsTemperatureModifier * 2))
    			{
    				++this.counter;
    				return;
    			}
    			if(this.energy >= ENERGY_CHANGE_TEMPERATURE)
    			{
	    			this.counter = 0;
	    			this.energy -= ENERGY_CHANGE_TEMPERATURE;
	    			++this.temperature;
	    			this.changeState();
    			}
    			return;
    		}
    		else if(this.hasTargetTemperature && this.temperature > this.targetTemperature)
    		{
    			if(this.counter < 5 - (int)(this.upwardsTemperatureModifier * 2))
    			{
    				++this.counter;
    				return;
    			}
    			if(this.energy >= ENERGY_CHANGE_TEMPERATURE)
    			{
	    			this.counter = 0;
	    			this.energy -= ENERGY_CHANGE_TEMPERATURE;
	    			--this.temperature;
	    			this.changeState();
    			}
	    		return;
    		}
    	}
    }

    private void changeState()
    {
		ItemStack stack = this.inv[0];
		if(ItemHelper.isTestTubeAndFilled(stack) && stack.getItem() != ModItems.testTubeCompound && stack.getItem() != ModItems.testTubePlasma)
		{
			int atomNumber = stack.getItemDamage();
			List<EnumMatterState> states = Arrays.asList(Elements.getPossibleStates(atomNumber));
			int boilingPoint = Elements.getBoilingPoint(atomNumber);
			if(states.contains(EnumMatterState.GAS) && stack.getItem() != ModItems.testTubeGas && this.temperature > boilingPoint && this.energy >= ENERGY_CHANGE_STATE)
			{
				this.energy -= ENERGY_CHANGE_STATE;
				this.inv[0] = new ItemStack(ModItems.testTubeGas, stack.stackSize, atomNumber);
				return;
			}
			int meltingPoint = Elements.getMeltingPoint(atomNumber);
			if(states.contains(EnumMatterState.LIQUID) && stack.getItem() != ModItems.testTubeLiquid && this.temperature > meltingPoint && this.temperature < boilingPoint && this.energy >= ENERGY_CHANGE_STATE)
			{
				this.energy -= ENERGY_CHANGE_STATE;
				this.inv[0] = new ItemStack(ModItems.testTubeLiquid, stack.stackSize, atomNumber);
				return;
			}
			if(states.contains(EnumMatterState.SOLID) && stack.getItem() != ModItems.testTubeSolid && this.temperature < meltingPoint && this.temperature < boilingPoint && this.energy >= ENERGY_CHANGE_STATE)
			{
				this.energy -= ENERGY_CHANGE_STATE;
				this.inv[0] = new ItemStack(ModItems.testTubeSolid, stack.stackSize, atomNumber);
				return;
			}
		}
	}

	private void fillBuffer()
    {
    	if(this.master != null)
    	{
    		((TileThermalRegulationChamber)this.master).doFillBuffer(this.handler);
    	}
    }

    private void doFillBuffer(PowerHandler handler)
    {
    	if(this.isMaster && this.energy < ENERGY_FULL)
    	{
	     	double energyRequired = handler.useEnergy(0.0F, 25.0F, true);
	     	if(energyRequired > 0.0F)
	     	{
		    	int converted = (int)(energyRequired * 5);
				int newAmount = this.energy + converted;
				if(newAmount >= ENERGY_FULL)
				{
					this.energy = ENERGY_FULL;
				}
				else
				{
					this.energy += converted;
			    }
	     	}
    	}
    }

    public int getTemperature()
    {
    	return this.temperature;
    }

    @Override
	public String getID()
	{
		return "ThermalRC";
	}

	@Override
	public MultiblockSize getSize()
	{
		return size;
	}

	@Override
	protected void readMBFromNBT(NBTTagCompound tag)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			return;
		}
		super.readMBFromNBT(tag);
		this.handler.readFromNBT(tag, "BCPower");
		this.temperature = tag.getShort("Temperature");
		this.targetTemperature = tag.getShort("TargetTemperature");
		this.hasTargetTemperature = tag.getBoolean("HasTargetTemperature");
	}

	@Override
	protected void writeMBToNBT(NBTTagCompound tag)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			return;
		}
		super.writeMBToNBT(tag);
		this.handler.writeToNBT(tag, "BCPower");
		tag.setShort("Temperature", (short)this.temperature);
		tag.setShort("TargetTemperature", (short)this.targetTemperature);
		tag.setBoolean("HasTargetTemperature", this.hasTargetTemperature);
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack stack, int side)
	{
		return ItemHelper.isTestTubeAndFilled(stack);
	}

	public int getDemandedEnergy()
	{
		if(this.isMaster && this.energy < ENERGY_FULL)
		{
			return 512;
		}
		return 0;
	}

	public int doInjectEnergyUnits(Direction directionFrom, int amount)
	{
		if(this.isMaster)
		{
			if(this.energy < ENERGY_FULL)
			{
				int newAmount = (int)(this.energy + amount);
				if(newAmount >= ENERGY_FULL)
				{
					this.energy = ENERGY_FULL;
					return newAmount - ENERGY_FULL;
				}
				this.energy += amount;
				return 0;
			}
			return amount;
		}
		return 0;
	}

	@Override
	public boolean acceptsEnergyFrom(TileEntity emitter, Direction direction)
	{
		return true;//this.multiblock == null ? false : this.multiblock.isComplete();
	}

	@Override
	public int demandsEnergy()
	{
		if(this.master == null)
		{
			return 0;
		}
		return (((TileThermalRegulationChamber)this.master).getDemandedEnergy());
	}

	@Override
	public int injectEnergy(Direction directionFrom, int amount)
	{
		if(this.master == null)
		{
			return 0;
		}
		return ((TileThermalRegulationChamber)this.master).doInjectEnergyUnits(directionFrom, amount);
	}

	@Override
	public int getMaxSafeInput()
	{
		return 512;
	}

	@Override
	public PowerReceiver getPowerReceiver(ForgeDirection side)
	{
		return this.handler.getPowerReceiver();
	}

	@Override
	public void doWork(PowerHandler workProvider) {}

	@Override
	public World getWorld()
	{
		return this.worldObj;
	}

	@Override
	public ConnectOverride overridePipeConnection(PipeType type, ForgeDirection with)
	{
		if(this.multiblock == null || !this.multiblock.isComplete())
		{
			return ConnectOverride.DISCONNECT;
		}
		return type.equals(PipeType.POWER) || type.equals(PipeType.ITEM) ? ConnectOverride.CONNECT : ConnectOverride.DISCONNECT;
	}

	@Override
	public boolean isAddedToEnergyNet()
	{
		return !this.firstTick;
	}

}
