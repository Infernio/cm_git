package com.infernio.Craftmistry.common.tile;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.research.ResearchProduct;

public interface IResearchTile {

	void onProjectFinished(ResearchProduct product);

	String getType();

	void setResearchNetwork(ResearchNetwork network);

	ResearchNetwork getNetwork();

}
