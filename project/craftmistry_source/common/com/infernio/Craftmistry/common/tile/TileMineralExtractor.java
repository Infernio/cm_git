package com.infernio.Craftmistry.common.tile;

import ic2.api.energy.event.EnergyTileLoadEvent;
import ic2.api.energy.event.EnergyTileUnloadEvent;
import ic2.api.energy.tile.IEnergySink;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerHandler;
import buildcraft.api.power.PowerHandler.PowerReceiver;

import com.infernio.Craftmistry.common.fluid.Fluids;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.recipe.MineralExtractorRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileSingleFluid;
import com.infernio.InfernoCore.util.NBTLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileMineralExtractor extends TileSingleFluid implements IInventory, IEnergySink, IPowerReceptor
{
	public static final int ENERGY_FULL = 1000;
	public static final int ENERGY_PER_TICK = 20;
	public static final int PROGRESS_FINISHED = 80;
	public static final int INVENTORY_SIZE = 6;
	public static final FluidStack MINERAL_WATER_REFERENCE = new FluidStack(Fluids.mineralWater, 0);

    private ItemStack[] inv;
    private boolean firstTick;
    public int progress;
    public int energy;
    private PowerHandler handler;
    private String customName;
    private boolean disabled = false;

    public TileMineralExtractor()
    {
    	super(MINERAL_WATER_REFERENCE, FluidContainerRegistry.BUCKET_VOLUME * 10);
    	this.inv = new ItemStack[INVENTORY_SIZE];
    	this.firstTick = true;
    	this.handler = new PowerHandler(this, PowerHandler.Type.MACHINE);
    	this.handler.configure(0.0F, 50.0F, 0.0F, ENERGY_FULL / 5.0F);
    	this.handler.configurePowerPerdition(1, 1);
    }

    @Override
    public int getSizeInventory()
    {
        return this.inv.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return this.inv[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount)
    {
		ItemStack stack = getStackInSlot(slot);
		if (stack != null) 
		{
			if(stack.stackSize <= amount) 
			{
				this.setInventorySlotContents(slot, null);
			}
			else
			{
				stack = stack.splitStack(amount);
				if(stack.stackSize <= 0)
				{
					this.setInventorySlotContents(slot, null);
				}
			}
		}
		return stack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot)
    {
    	ItemStack stack = getStackInSlot(slot);
    	if(stack != null)
    	{
    		setInventorySlotContents(slot, null);
    	}
    	return stack;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        this.inv[slot] = stack;
        if (stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }
    }

    @Override
    public String getInventoryName()
    {
        return this.hasCustomInventoryName() ? this.customName : StringLib.CONTAINER_MINERAL_EXTRACTOR_NAME;
    }

    @Override
    public boolean hasCustomInventoryName()
    {
        return this.customName != null && !this.customName.isEmpty();
    }

    public void setInventoryName(String to)
    {
        this.customName = to;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
        this.energy = tag.getShort("Energy");
        this.handler.readFromNBT(tag, "BCPower");
        this.handler.configure(0.0F, 50.0F, 0.0F, ENERGY_FULL / 5.0F);
        this.handler.configurePowerPerdition(1, 1);
        NBTTagList nbttaglist = tag.getTagList("Items", NBTLib.NBT_COMPOUND);
        this.inv = new ItemStack[this.getSizeInventory()];
        for(int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound tag2 = nbttaglist.getCompoundTagAt(i);
            byte b = tag2.getByte("Slot");

            if(b >= 0 && b < this.inv.length)
            {
                this.inv[b] = ItemStack.loadItemStackFromNBT(tag2);
            }
        }
        if(tag.hasKey("CustomName"))
        {
            this.customName = tag.getString("CustomName");
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
        tag.setShort("Energy", (short)this.energy);
        this.handler.writeToNBT(tag, "BCPower");
        NBTTagList tagList = new NBTTagList();
        for(int i = 0; i < this.inv.length; ++i)
        {
            if(this.inv[i] != null)
            {
                NBTTagCompound tag2 = new NBTTagCompound();
                tag2.setByte("Slot", (byte)i);
                this.inv[i].writeToNBT(tag2);
                tagList.appendTag(tag2);
            }
        }
        tag.setTag("Items", tagList);
        if(this.hasCustomInventoryName())
        {
            tag.setString("CustomName", this.customName);
        }
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 64;
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int par1)
    {
        return this.progress * par1 / PROGRESS_FINISHED;
    }

    @Override
    public void onChunkUnload()
    {
    	super.onChunkUnload();
    	if(!this.worldObj.isRemote)
    	{
	    	MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
	    }
    }

    @Override
    public void invalidate()
    {
    	super.invalidate();
    	if(!this.worldObj.isRemote)
    	{
	    	MinecraftForge.EVENT_BUS.post(new EnergyTileUnloadEvent(this));
	    }
    }

    @Override
    public void updateEntity()
    {
    	if(firstTick)
    	{
	    	if(!this.worldObj.isRemote)
	    	{
	    		MinecraftForge.EVENT_BUS.post(new EnergyTileLoadEvent(this));
	    	}
	    	firstTick = false;
    	}
		this.checkRedstone();
		if(!this.disabled)
		{
			this.fillBuffer();
	        if(!this.worldObj.isRemote)
	        {
	        	if(this.canExtract() && this.energy >= ENERGY_PER_TICK)
	        	{
	        		this.energy -= ENERGY_PER_TICK;
		        	if(this.progress < PROGRESS_FINISHED)
		        	{
		        		++this.progress;
		        		return;
		        	}
		        	this.progress = 0;
		        	this.extract();
	        	}
	        }
		}
    }

    private void checkRedstone()
    {
		if(this.worldObj.isBlockProvidingPowerTo(xCoord, yCoord, zCoord, 1) > 0)
		{
			this.disabled = true;
			return;
		}
		if(this.worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
		{
			this.disabled = true;
			return;
		}
		this.disabled = false;
	}

    private void fillBuffer()
    {
    	if(this.energy < ENERGY_FULL)
		{
	     	double energyRequired = handler.useEnergy(0.0F, 25.0F, true);
	    	int converted = (int)(energyRequired * 5);
			int newAmount = this.energy + converted;
			if(newAmount >= ENERGY_FULL)
			{
				this.energy = ENERGY_FULL;
			}
			else
			{
				this.energy += converted;
		    }
		}
    }

    private boolean canExtract()
    {
    	for(int i = 0; i < this.inv.length; ++i)
    	{
    		if(this.inv[i] != null)
    		{
    			return false;
    		}
    	}
    	return this.getFluidAmount() >= FluidContainerRegistry.BUCKET_VOLUME;
    }

    private void extract()
    {
    	ItemStack[] results = MineralExtractorRecipes.extractions().generateResults();
    	for(int i = 0; i < results.length; ++i)
    	{
    		this.inv[i] = results[i];
    	}
    	this.decreaseFluidAmount(FluidContainerRegistry.BUCKET_VOLUME);
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
    {
        return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : par1EntityPlayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }
    
    @Override
    public void openInventory() {}

    @Override
    public void closeInventory() {}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return slot == 0 ? isValidTestTube(stack) : false;
    }

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && stack.getItem() == ModItems.testTubeSolid && stack.getItemDamage() == 0;
	}

	@Override
	public boolean acceptsEnergyFrom(TileEntity emitter, Direction direction)
	{
		return true;
	}

	@Override
	public int demandsEnergy()
	{
		if(this.energy < ENERGY_FULL)
		{
			return 32;
		}
		return 0;
	}

	@Override
	public int injectEnergy(Direction directionFrom, int amount)
	{
		if(this.energy < ENERGY_FULL)
		{
			int newAmount = (int)(this.energy + amount);
			if(newAmount >= ENERGY_FULL)
			{
				this.energy = ENERGY_FULL;
				return newAmount - ENERGY_FULL;
			}
			this.energy += amount;
			return 0;
		}
		return amount;
	}

	@Override
	public int getMaxSafeInput()
	{
		return 32;
	}

	@Override
	public boolean isAddedToEnergyNet()
	{
		return !this.firstTick;
	}

	@Override
	public PowerReceiver getPowerReceiver(ForgeDirection side)
	{
		return this.handler.getPowerReceiver();
	}

	@Override
	public void doWork(PowerHandler workProvider) {}

	@Override
	public World getWorld()
	{
		return this.worldObj;
	}
}
