package com.infernio.Craftmistry.common.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.detector.DetectorModes;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

public class TileDetector extends TileMachineInventory
{
	public static final int INVENTORY_SIZE = 1;
    public static final int ENERGY_FULL = 1000;
    public static final int SETTINGS = DetectorModes.detections().getNumberRegisteredModes();

    public int setting;
    public int redstoneOutput;
    public int redstoneConfig;

    public TileDetector()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.setting = MathHelper.clamp_int(tag.getByte("Setting"), 0, SETTINGS);
        this.redstoneConfig = tag.getByte("RedstonePower");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setByte("Setting", (byte)this.setting);
        tag.setByte("RedstonePower", (byte)this.redstoneConfig);
    }

    @Override
    public void onUpdate()
    {
    	if(this.shouldDetect())
    	{
	    	if(this.energy >= 1)
	    	{
	    		this.energy -= 1;
	    		this.detect();
	    	}
    	}
    	else
    	{
    		this.setOutput(0);
    	}
    }

    private boolean shouldDetect()
    {
    	return DetectorModes.detections().getMode(this.setting).requiresItem() ? this.inv[0] != null : true;
    }

    private void detect()
    {
    	IDetectorMode mode = DetectorModes.detections().getMode(this.setting);
		if(mode.requiresItem())
		{
			ItemStack stack = this.inv[0];
			if(stack != null)
			{
				if(mode.detect(stack, this.worldObj))
				{
					this.setOutput(this.redstoneConfig);
				}
				else
				{
					this.setOutput(0);
				}
			}
			else
			{
				this.setOutput(0);
			}
		}
		else
		{
			if(mode.detect(this.inv[0], this.worldObj))
			{
				this.setOutput(this.redstoneConfig);
			}
			else
			{
				this.setOutput(0);
			}
		}
	}
    
    private void setOutput(int to)
    {
    	this.redstoneOutput = to;
		this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, ModBlocks.detector);
    }

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && ItemHelper.isTestTube(stack) && stack.getItemDamage() == 0 && stack.stackSize >= 1;
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return false;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 32;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_DETECTOR_NAME;
	}
}
