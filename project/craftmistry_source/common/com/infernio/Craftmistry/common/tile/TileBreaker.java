package com.infernio.Craftmistry.common.tile;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.FakePlayerFactory;

import com.infernio.Craftmistry.common.item.IMachineUpgrade;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.Packets;
import com.infernio.InfernoCore.network.message.MessagePlayAuxSFX;
import com.infernio.InfernoCore.tile.TileMachineInventory;
import com.infernio.InfernoCore.util.ItemUtils;

public class TileBreaker extends TileMachineInventory implements ISidedInventory {

	public static final int INVENTORY_SIZE = 4;
    public static final int ENERGY_FULL = 5000;
    public static final int ENERGY_BREAK_BLOCK = 100;
    public static final int ENERGY_CLEAN_MINING_AREA = 20;
    public static final int MAX_COOLDOWN = 20;
    public static final ArrayList<String> blacklist = new ArrayList<String>();

    private int range = 1;
    private int counter;
    private int targetX;
    private int targetY;
    private int targetZ;
    private int lastX;
    private int lastY;
    private int lastZ;
    private MiningAreaController controller;
    private boolean hasOutput;
    private IInventory output;
    private boolean targetFound;
    private boolean shouldSleep;
    private int updateCooldown;
    private EntityPlayer breakingPlayer;

    public TileBreaker()
    {
    	super(INVENTORY_SIZE);
    	this.controller = new MiningAreaController(0, 0, 0);
    	this.counter = 0;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        NBTTagCompound area = tag.getCompoundTag("MiningArea");
        this.targetX = area.getInteger("TargetX");
        this.targetY = area.getInteger("TargetY");
        this.targetZ = area.getInteger("TargetZ");
        this.lastX = area.getInteger("LastX");
        this.lastY = area.getInteger("LastY");
        this.lastZ = area.getInteger("LastZ");
        this.range = tag.getByte("Range");
        this.counter = tag.getByte("Counter");
        this.updateCooldown = tag.getByte("Cooldown");
        this.shouldSleep = tag.getBoolean("NoTargetAvailable");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Energy", (short)this.energy);
        tag.setTag("MiningArea", new NBTTagCompound());
        NBTTagCompound area = tag.getCompoundTag("MiningArea");
        area.setInteger("TargetX", this.targetX);
        area.setInteger("TargetY", this.targetY);
        area.setInteger("TargetZ", this.targetZ);
        area.setInteger("LastX", this.lastX);
        area.setInteger("LastY", this.lastY);
        area.setInteger("LastZ", this.lastZ);
        tag.setByte("Range", (byte)this.range);
        tag.setByte("Counter", (byte)this.counter);
        tag.setByte("Cooldown", (byte)this.updateCooldown);
        tag.setBoolean("NoTargetAvailable", this.shouldSleep);
    }

    @Override
    public void onUpdate()
    {
    	this.checkUpgrades();
    	if(this.shouldSleep)
    	{
    		if(this.updateCooldown < MAX_COOLDOWN)
    		{
    			++this.updateCooldown;
    			return;
    		}
    		this.updateCooldown = 0;
    	}
    	if(this.counter < 3)
    	{
    		++this.counter;
    	}
    	else
    	{
    		this.counter = 0;
	    	int toolSlot = this.getHighestEfficiencySlot();
	    	if(toolSlot == -1)
	    	{
	    		return;
	    	}
	    	this.updateArea();
	    	this.updateOutput();
	    	this.findTarget();
	    	boolean sleep = true;
        	if(this.targetFound && this.energy >= ENERGY_BREAK_BLOCK)
        	{
        		this.energy -= ENERGY_BREAK_BLOCK;
        		this.targetFound = false;
        		sleep = false;
        		this.breakBlock(toolSlot);
        	}
	    	this.controller.scanArea(worldObj);
	    	if(!this.controller.itemsInArea())
	    	{
	    		this.shouldSleep = sleep;
	    	}
	    	else if(this.hasOutput && this.energy >= ENERGY_CLEAN_MINING_AREA)
	    	{
	    		this.shouldSleep = false;
	    		this.energy -= ENERGY_CLEAN_MINING_AREA;
	    		this.collectItem();
	    	}
    	}
    }

    private void updateOutput()
    {
    	TileEntity tile = worldObj.getTileEntity(xCoord + 1, yCoord, zCoord);
    	if((tile instanceof IInventory || tile instanceof ISidedInventory) && ItemUtils.findEmptySlot((IInventory)tile) != -1)
    	{
    		this.hasOutput = true;
    		this.output = (IInventory)tile;
    		return;
    	}
    	tile = worldObj.getTileEntity(xCoord - 1, yCoord, zCoord);
    	if((tile instanceof IInventory || tile instanceof ISidedInventory) && ItemUtils.findEmptySlot((IInventory)tile) != -1)
    	{
    		this.hasOutput = true;
    		this.output = (IInventory)tile;
    		return;
    	}
    	tile = worldObj.getTileEntity(xCoord, yCoord + 1, zCoord);
    	if((tile instanceof IInventory || tile instanceof ISidedInventory) && ItemUtils.findEmptySlot((IInventory)tile) != -1)
    	{
    		this.hasOutput = true;
    		this.output = (IInventory)tile;
    		return;
    	}
    	tile = worldObj.getTileEntity(xCoord, yCoord - 1, zCoord);
    	if((tile instanceof IInventory || tile instanceof ISidedInventory) && ItemUtils.findEmptySlot((IInventory)tile) != -1)
    	{
    		this.hasOutput = true;
    		this.output = (IInventory)tile;
    		return;
    	}
    	tile = worldObj.getTileEntity(xCoord, yCoord, zCoord + 1);
    	if((tile instanceof IInventory || tile instanceof ISidedInventory) && ItemUtils.findEmptySlot((IInventory)tile) != -1)
    	{
    		this.hasOutput = true;
    		this.output = (IInventory)tile;
    		return;
    	}
    	tile = worldObj.getTileEntity(xCoord, yCoord, zCoord - 1);
    	if((tile instanceof IInventory || tile instanceof ISidedInventory) && ItemUtils.findEmptySlot((IInventory)tile) != -1)
    	{
    		this.hasOutput = true;
    		this.output = (IInventory)tile;
    		return;
    	}
    	this.output = null;
    	this.hasOutput = false;
    }

    private void checkUpgrades()
    {
		if(this.inv[3] == null || this.inv[3].getItem() == null)
		{
			if(this.range != 1)
			{
				this.lastX = -1;
				this.lastY = this.yCoord + 1;
				this.lastZ = -1;
			}
			this.range = 1;
			return;
		}
		if(this.inv[3].getItem() != ModItems.upgrade)
		{
			this.worldObj.spawnEntityInWorld(new EntityItem(worldObj, xCoord + Math.random(), yCoord + Math.random(), zCoord + Math.random(), this.inv[3].copy()));
			this.inv[3] = null;
			return;
		}
		if(this.inv[3].stackSize > 6)
		{
			int over = this.inv[3].stackSize - 6;
			this.worldObj.spawnEntityInWorld(new EntityItem(worldObj, xCoord + Math.random(), yCoord + 1 + Math.random(), zCoord + Math.random(), new ItemStack(this.inv[3].getItem(), over, this.inv[3].getItemDamage())));
			this.inv[3].stackSize = 6;
		}
		int newStackSize = 1 + this.inv[3].stackSize;
		if(this.range != newStackSize)
		{
			this.lastX = -newStackSize;
			this.lastY = this.yCoord + newStackSize;
			this.lastZ = -newStackSize;
		}
		this.range = newStackSize;
    }

    private void updateArea()
    {
    	this.controller.setAreaBoundaries(this.xCoord - this.range, 0, this.zCoord - this.range, this.xCoord + this.range, this.yCoord + this.range, this.zCoord + this.range);
    }

    private void collectItem()
    {
    	EntityItem item = this.controller.findItem(worldObj);
    	if(item != null)
    	{
	    	ItemStack stack = item.getEntityItem();
	    	if(stack != null && stack.getItem() != null)
	    	{
	    		ItemStack added = ItemUtils.addItemStack(stack, this.output);
	    		if(added != null && added.stackSize > 0)
				{
	    			this.worldObj.spawnEntityInWorld(new EntityItem(worldObj, xCoord, yCoord + 1, zCoord, added));
				}
	    	}
	    	item.setDead();
	    }
    	this.worldObj.updateEntity(item);
    }

	private void findTarget()
	{
		for(int z = this.lastZ; z <= this.range; ++z)
		{
			for(int x = this.lastX; x <= this.range; ++x)
			{
				yLoop: for(int y = this.lastY; y > 0; --y)
				{
					Block block = this.worldObj.getBlock(this.xCoord + x, y, this.zCoord + z);
					if(block != null && !block.isAir(this.worldObj, this.xCoord + x, y, this.zCoord + z) && !block.getMaterial().isLiquid())
					{
						for(int i = 0; i < blacklist.size(); ++i)
						{
							if(Block.blockRegistry.getNameForObject(block).equals(blacklist.get(i)))
							{
								continue yLoop;
							}
						}
						this.targetX = this.xCoord + x;
						this.targetY = y;
						this.targetZ = this.zCoord + z;
						this.lastX = x;
						this.lastY = y;
						this.lastZ = z;
						this.targetFound = true;
						this.shouldSleep = false;
						return;
					}
				}
				this.lastY = this.yCoord + this.range;
			}
			this.lastX = -this.range;
		}
		this.targetX = 0;
		this.targetY = 0;
		this.targetZ = 0;
		this.lastX = -range;
		this.lastY = this.yCoord + this.range;
		this.lastZ = -range;
		this.targetFound = false;
		this.shouldSleep = true;
	}

	private int getHighestEfficiencySlot()
	{
		float efficiency = 0.0F;
		int efficientSlot = -1;
		for(int slot = 0; slot < this.inv.length - 1; ++slot)
		{
			ItemStack stack = this.inv[slot];
			if(stack != null && stack.getItem() instanceof ItemTool)
			{
				float f = stack.getItem().getDigSpeed(stack, this.worldObj.getBlock(this.targetX, this.targetY, this.targetZ), 0);
				if(f > efficiency)
				{
					efficiency = f;
					efficientSlot = slot;
				}
			}
		}
		return efficientSlot;
	}

	private void breakBlock(int slot)
    {
		if(slot != -1 && this.inv[slot] != null && this.inv[slot].getItem() != null)
		{
			ItemStack stack = this.inv[slot];
			if(stack.getItem().onBlockStartBreak(stack, this.targetX, this.targetY, this.targetZ, this.breakingPlayer))
			{
				this.energy += 25;
				return;
			}
			if(this.breakingPlayer == null)
			{
				this.breakingPlayer = FakePlayerFactory.getMinecraft((WorldServer)this.worldObj);
			}
			Block block = this.worldObj.getBlock(this.targetX, this.targetY, this.targetZ);
			boolean shouldDrop;
			if(ConfigSettings.RESPECT_NO_TILE_DROPS)
			{
				shouldDrop = !this.worldObj.getGameRules().getGameRuleBooleanValue("noTileDrops");
			}
			else
			{
				shouldDrop = true;
			}
			if(block != null && shouldDrop)
			{
				if(EnchantmentHelper.getEnchantmentLevel(Enchantment.silkTouch.effectId, stack) >= 1)
				{
					if(block.canSilkHarvest(this.worldObj, this.breakingPlayer, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ)))
					{
						ItemStack drop;
						if(Item.getItemFromBlock(block) != null && Item.getItemFromBlock(block).getHasSubtypes())
						{
							drop = new ItemStack(block, 1, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ));
						}
						else
						{
							drop = new ItemStack(block, 1, 0);
						}
						if(this.hasOutput)
						{
							ItemStack added = ItemUtils.addItemStack(drop, this.output);
							if(added != null && added.stackSize > 0)
							{
								this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.targetX, this.targetY, this.targetZ, added));
							}
						}
						else
						{
							this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.targetX, this.targetY, this.targetZ, drop));
						}
					}
					else
					{
						if(this.hasOutput)
						{
							ArrayList<ItemStack> dropped = block.getDrops(this.worldObj, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ), 0);
							for(ItemStack drop : dropped)
							{
								ItemStack added = ItemUtils.addItemStack(drop, this.output);
								if(added != null && added.stackSize > 0)
								{
									this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.targetX, this.targetY, this.targetZ, added));
								}
							}
						}
						else
						{
							block.dropBlockAsItem(this.worldObj, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ), 0);
						}
					}
				}
				else
				{
					if(stack.getItem().getDigSpeed(stack, block, 0) > 1.0F)
					{
						if(this.hasOutput)
						{
							ArrayList<ItemStack> dropped = block.getDrops(this.worldObj, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ), EnchantmentHelper.getEnchantmentLevel(Enchantment.fortune.effectId, stack));
							for(ItemStack drop : dropped)
							{
								ItemStack added = ItemUtils.addItemStack(drop, this.output);
								if(added != null && added.stackSize > 0)
								{
									this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.targetX, this.targetY, this.targetZ, added));
								}
							}
						}
						else
						{
							block.dropBlockAsItem(this.worldObj, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ), EnchantmentHelper.getEnchantmentLevel(Enchantment.fortune.effectId, stack));
						}
					}
					else
					{
						if(this.hasOutput)
						{
							ArrayList<ItemStack> dropped = block.getDrops(this.worldObj, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ), 0);
							for(ItemStack drop : dropped)
							{
								ItemStack added = ItemUtils.addItemStack(drop, this.output);
								if(added != null && added.stackSize > 0)
								{
									this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.targetX, this.targetY, this.targetZ, added));
								}
							}
						}
						else
						{
							block.dropBlockAsItem(this.worldObj, this.targetX, this.targetY, this.targetZ, this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ), 0);
						}
					}
				}
				stack.damageItem(1, this.breakingPlayer);
				PacketHelper.sendToAllAround(new MessagePlayAuxSFX(this.targetX, this.targetY, this.targetZ, 2001, Block.getIdFromBlock(block) + (this.worldObj.getBlockMetadata(this.targetX, this.targetY, this.targetZ) << 12)), this.worldObj.provider.dimensionId, this.targetX, this.targetY, this.targetZ, Packets.PACKET_PLAY_AUX_SFX_RANGE);
				this.worldObj.setBlockToAir(this.targetX, this.targetY, this.targetZ);
			}
			this.checkInventory();
		}
    }

    private void checkInventory()
    {
		for(int i = 0; i < this.inv.length - 1; ++i)
		{
			ItemStack stack = this.inv[i];
			if(stack != null && stack.getItem() != null && stack.getItem().isItemTool(stack))
			{
				if(stack.getItemDamage() > stack.getMaxDamage() || stack.stackSize <= 0)
				{
					this.inv[i] = null;
				}
			}
		}
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		if(side == 0 || side == 1)
		{
			return new int[]{3};
		}
		return new int[]{0, 1, 2};
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack stack, int side)
	{
		if(slot == 3)
		{
			return stack != null && stack.getItem() instanceof IMachineUpgrade;
		}
		return stack != null && stack.getItem() instanceof ItemTool;
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack stack, int side)
	{
		return true;
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 128;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_BREAKER_NAME;
	}

	private class MiningAreaController {

		private AxisAlignedBB areaBB;
		private boolean itemsExist;

		public MiningAreaController(int maxX, int maxY, int maxZ)
		{
			this(-maxX, 0, -maxZ, maxX, maxY, maxZ);
		}

		public MiningAreaController(int minX, int minY, int minZ, int maxX, int maxY, int maxZ)
		{
			areaBB = AxisAlignedBB.getBoundingBox(minX, minY, minZ, maxX, maxY, maxZ);
		}

		public void setAreaBoundaries(int minX, int minY, int minZ, int maxX, int maxY, int maxZ)
		{
			this.areaBB.setBounds(minX, minY, minZ, maxX, maxY, maxZ);
		}

		@SuppressWarnings("rawtypes")
		public void scanArea(World world)
		{
			List entites = world.getEntitiesWithinAABB(EntityItem.class, this.areaBB);
			if(entites != null && !entites.isEmpty())
			{
				this.itemsExist = true;
			}
			else
			{
				this.itemsExist = false;
			}
		}

		public boolean itemsInArea()
		{
			return this.itemsExist;
		}

		@SuppressWarnings("rawtypes")
		public EntityItem findItem(World world)
		{
			List entites = world.getEntitiesWithinAABB(EntityItem.class, this.areaBB);
			if(entites != null && !entites.isEmpty())
			{
				return (EntityItem)entites.get(0);
			}
			return null;
		}

	}
}
