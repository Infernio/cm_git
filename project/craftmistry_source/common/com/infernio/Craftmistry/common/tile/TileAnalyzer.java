package com.infernio.Craftmistry.common.tile;

import net.minecraft.inventory.IInventory;

import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileBasicInventory;

public class TileAnalyzer extends TileBasicInventory implements IInventory {

	public static final int INVENTORY_SIZE = 1;

	public int currentPage;
	
	public TileAnalyzer()
	{
		super(INVENTORY_SIZE);
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 1;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_ANALYZER_NAME;
	}
}
