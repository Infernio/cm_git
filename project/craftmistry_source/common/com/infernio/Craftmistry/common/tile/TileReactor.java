package com.infernio.Craftmistry.common.tile;

import java.util.ArrayList;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.api.recipe.ItemCollection;
import com.infernio.Craftmistry.common.recipe.ReactorRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileReactor extends TileMachineInventory
{
    private static final int PROGRESS_FINISHED = 200;
    private static final int ENERGY_FULL = 20000;
    private static final int INVENTORY_SIZE = 10;
    private static final int OUTPUT_SLOT = 9;
    private static final int CHECK_TICKS = 10;

    private int ticksSinceCheck;
    private boolean cachedCanReact;
    private int cachedEnergyRequired;
    private ItemStack cachedResult;
    public int progress;

    public TileReactor()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return slot < OUTPUT_SLOT;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int scale)
    {
        return this.progress * scale / PROGRESS_FINISHED;
    }

    @Override
    public void onUpdate()
    {
        if(this.canReact())
        {
        	if(this.energy >= this.cachedEnergyRequired && this.progress < PROGRESS_FINISHED)
        	{
        		this.energy -= this.cachedEnergyRequired;
        		++this.progress;
        		return;
        	}
            if(this.progress == PROGRESS_FINISHED)
            {
                this.progress = 0;
                this.react();
            }
        }
        else
        {
            this.progress = 0;
        }
    }

    @Override
    public void markDirty()
    {
    	super.markDirty();
    	this.ticksSinceCheck = CHECK_TICKS;
    }

	private boolean canReact()
    {
		if(this.ticksSinceCheck < CHECK_TICKS)
		{
			++this.ticksSinceCheck;
			return this.cachedCanReact;
		}
		else
		{
			return this.checkNow();
		}
    }

	private boolean checkNow()
	{
		this.ticksSinceCheck = 0;
		ItemStack[] inv = this.getValidInventory();
		if(inv.length > 0)
		{
			ItemCollection input = new ItemCollection(inv);
			ItemStack result = ReactorRecipes.reactions().getResult(input);
			if(this.checkStack(result))
			{
				this.cachedEnergyRequired = ReactorRecipes.reactions().getEnergyRequired(input) / PROGRESS_FINISHED;
				this.cachedResult = result;
				this.cachedCanReact = this.checkStack(result);
				return true;
			}
			else
			{
				this.progress = 0;
				this.cachedCanReact = false;
				return false;
			}
		}
		else
		{
			this.progress = 0;
			this.cachedCanReact = false;
			return false;
		}
	}

	private boolean checkStack(ItemStack stack)
	{
		if(stack == null) return false;
	    if(this.inv[OUTPUT_SLOT] == null) return true;
	    if(!this.inv[OUTPUT_SLOT].isItemEqual(stack)) return false;
	    int result = this.inv[OUTPUT_SLOT].stackSize + stack.stackSize;
	    return (result <= getInventoryStackLimit() && result <= stack.getMaxStackSize());
	}

	private void react()
    {
		if(this.checkNow())
		{
	        for(int i = 0; i < INVENTORY_SIZE - 1; ++i)
	        {
	            this.inv[i] = null;
	        }
	        if(this.inv[OUTPUT_SLOT] == null)
	        {
	        	this.inv[OUTPUT_SLOT] = this.cachedResult.copy();
	        }
	        else if(this.inv[OUTPUT_SLOT].isItemEqual(this.cachedResult))
	        {
	        	this.inv[OUTPUT_SLOT].stackSize += this.cachedResult.stackSize;
	        }
		}
    }

	private ItemStack[] getValidInventory()
	{
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		for(int i = 0; i < INVENTORY_SIZE - 1; ++i)
		{
			ItemStack stack = this.inv[i];
			if(stack != null)
			{
				ret.add(stack);
			}
		}
		return ret.toArray(new ItemStack[ret.size()]);
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 512;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_REACTOR_NAME;
	}
}
