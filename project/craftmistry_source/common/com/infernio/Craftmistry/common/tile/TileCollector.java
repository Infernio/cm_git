package com.infernio.Craftmistry.common.tile;

import java.util.List;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.Packets;
import com.infernio.InfernoCore.network.message.MessageSpawnParticle;
import com.infernio.InfernoCore.tile.TileMachineInventory;
import com.infernio.InfernoCore.util.ItemUtils;

//TODO Make ISided so that pipes don't pull out upgrades :P
public class TileCollector extends TileMachineInventory
{
	private static final int INVENTORY_SIZE = 28;
	private static final int STANDARD_RANGE = 6;
    private static final int ENERGY_FULL = 1000;
    private static final int ENERGY_REQUIRED = 25;
	public static final int MAXIMUM_UPGRADES = 16;

    private int range;
    private AxisAlignedBB itemPickupAABB;

    public TileCollector()
    {
    	super(INVENTORY_SIZE);
    	this.range = STANDARD_RANGE;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.range = tag.getByte("Range");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setByte("Range", (byte)this.range);
    }

    @Override
    public void onUpdate()
    {
		this.checkUpgrades();
    	this.updateAABB();
        if(this.canCollect())
        {
	    	if(this.energy >= ENERGY_REQUIRED)
	    	{
	    		this.energy -= ENERGY_REQUIRED;
	    		this.collectItem();
	    		return;
	    	}
        }
    }

	private void updateAABB()
    {
    	if(this.itemPickupAABB == null)
    	{
    		this.itemPickupAABB = AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord + 1, yCoord + 1, zCoord + 1);
    	}
    	this.itemPickupAABB.setBounds(xCoord - range, yCoord - range, zCoord - range, xCoord + range, yCoord + range, zCoord + range);
    }

    private boolean canCollect()
    {
    	return !this.worldObj.getEntitiesWithinAABB(EntityItem.class, itemPickupAABB).isEmpty();
	}

	private void checkUpgrades()
    {
		ItemStack stack = this.inv[27];
		if(stack != null)
		{
			if(stack.getItem() != ModItems.upgrade)
			{
				if(!this.worldObj.isRemote)
				{
					this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.xCoord + Math.random(), this.yCoord + Math.random(), this.zCoord + Math.random(), stack.copy()));
				}
				this.inv[27] = null;
				this.range = STANDARD_RANGE;
				return;
			}
			if(stack.stackSize > MAXIMUM_UPGRADES)
			{
				int over = stack.stackSize - MAXIMUM_UPGRADES;
				this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, this.xCoord + Math.random(), this.yCoord + 1 + Math.random(), this.zCoord + Math.random(), new ItemStack(stack.getItem(), over, stack.getItemDamage())));
				stack.stackSize = MAXIMUM_UPGRADES;
			}
			this.range = STANDARD_RANGE + stack.stackSize;
		}
		else
		{
			this.range = STANDARD_RANGE;
		}
	}

	@SuppressWarnings("rawtypes")
	private void collectItem()
    {
		List items = this.worldObj.getEntitiesWithinAABB(EntityItem.class, this.itemPickupAABB);
		EntityItem item = (EntityItem)items.get(this.worldObj.rand.nextInt(items.size()));
		if(item != null && !item.isDead)
		{
			ItemStack stack = ItemUtils.addItemStack(item.getEntityItem(), this);
			if(stack != null && stack.stackSize > 0)
			{
				this.worldObj.spawnEntityInWorld(new EntityItem(this.worldObj, item.posX, item.posY, item.posZ, stack));
			}
			for(int i = 0; i < 10; ++i)
			{
				double pX = item.posX + Math.random();
				double pY = item.posY + Math.random();
				double pZ = item.posZ + Math.random();
				PacketHelper.sendToAllAround(new MessageSpawnParticle(pX, pY, pZ, Math.random(), Math.random(), Math.random(), "portal"), this.worldObj.provider.dimensionId, (int)pX, (int)pY, (int)pZ, i);
			}
			for(int i = 0; i < 10; ++i)
			{
				double pX = item.posX - Math.random();
				double pY = item.posY - Math.random();
				double pZ = item.posZ - Math.random();
				PacketHelper.sendToAllAround(new MessageSpawnParticle(pX, pY, pZ, Math.random(), Math.random(), Math.random(), "portal"), this.worldObj.provider.dimensionId, (int)pX, (int)pY, (int)pZ, Packets.PACKET_SPAWN_PARTICLE_RANGE);
			}
			this.worldObj.playSoundAtEntity(item, "random.pop", 0.2F, ((this.worldObj.rand.nextFloat() - this.worldObj.rand.nextFloat()) * 0.7F + 1.0F) * 2.0F);
			item.setDead();
			this.worldObj.updateEntity(item);
		}
    }

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 32;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_COLLECTOR_NAME;
	}
}
