package com.infernio.Craftmistry.common.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

public class TileDestabilizer extends TileMachineInventory
{
	public static final int INVENTORY_SIZE = 1;
    public static final int ENERGY_FULL = 1000;

    public int amplifier;

    public TileDestabilizer()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.amplifier = tag.getShort("Amplifier");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Amplifier", (short)this.amplifier);
    }

    @Override
    public int getInventoryStackLimit()
    {
        return 1;
    }

    @Override
    public void onUpdate()
    {
        if(isValidTestTube(this.inv[0]))
        {
    		for(int i = 0; i < amplifier + 1; ++i)
    		{
            	if(this.energy >= 2)
            	{
            		this.energy -= 2;
        			this.destabilize();
        		}
    		}
        }
    }

	public void destabilize()
    {
		ItemStack stack = this.inv[0];
        if(isValidTestTube(stack))
        {
            if(stack != null)
            {
            	int atomNumber = stack.getItemDamage();
            	if(Elements.isElementRadioactive(atomNumber))
            	{
            		NBTTagCompound tag = stack.stackTagCompound;
            		if(tag == null)
            		{
            			tag = new NBTTagCompound();
            			stack.setTagCompound(tag);
            		}
    				if(!tag.hasKey("HalfLife"))
    				{
    					tag.setInteger("HalfLife", Elements.getHalfLife(atomNumber));
    				}
    				int halfLife = tag.getInteger("HalfLife");
    				if(halfLife > 1)
    				{
						tag.setInteger("HalfLife", halfLife - 1);
						return;
    				}
    				if(halfLife <= 1)
    				{
    					ItemStack decay = Elements.getDecayProduct(atomNumber, stack);
    					inv[0] = decay;
    					return;
    				}
            	}
            }
        }
    }

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && ItemHelper.isTestTube(stack) && stack.getItemDamage() > 0 && Elements.isElementRadioactive(stack.getItemDamage());
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 128;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_DESTABILIZER_NAME;
	}
}
