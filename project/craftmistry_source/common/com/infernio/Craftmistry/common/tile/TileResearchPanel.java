package com.infernio.Craftmistry.common.tile;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.api.research.PanelProperties;
import com.infernio.Craftmistry.common.research.Project;
import com.infernio.Craftmistry.common.research.ProjectContainer;
import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.research.ResearchProduct;
import com.infernio.Craftmistry.common.research.SelectionInventory;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageUpdateClientResearch;
import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.message.MessageFacing;
import com.infernio.InfernoCore.tile.IRotatable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileResearchPanel extends TileEntity implements IResearchTile, IRotatable {

	private static final int MAX_PROJECTS = 3;

	private ResearchNetwork network;
	//TODO Move this to TileComputer, it'll store research information.
	public ArrayList<ResearchProduct> products;
	private ProjectContainer projectManager;
	private int counter;
	private boolean firstTick;
	public PanelProperties properties;
	public SelectionInventory selectedItems;

	@SideOnly(Side.CLIENT)
	public int materialCompartmentX;
	@SideOnly(Side.CLIENT)
	public int materialCompartmentY;
	@SideOnly(Side.CLIENT)
	public int materialCompartmentZ;
	private int facing;

	public TileResearchPanel()
	{
		this.projectManager = new ProjectContainer(this, MAX_PROJECTS);
		this.products = new ArrayList<ResearchProduct>();
		this.selectedItems = new SelectionInventory(0);
		this.properties = new PanelProperties();
		this.counter = 0;
		this.firstTick = true;
	}

	private void updateNetwork()
	{
		this.network.setWorld(worldObj);
		this.network.setCoordinates(xCoord, yCoord, zCoord);
		this.network.clear();
		this.network.build();
		PacketHelper.sendToDimension(new MessageFacing(this.xCoord, this.yCoord, this.zCoord, (byte)this.facing), this.worldObj.provider.dimensionId);
		if(this.network.hasMaterialCompartment())
		{
			TileMaterialCompartment tile = this.network.getMaterialCompartment();
			PacketHandler.sendToDimension(new MessageUpdateClientResearch(this.xCoord, this.yCoord, this.zCoord, new int[]{tile.xCoord, tile.yCoord, tile.zCoord}), this.worldObj.provider.dimensionId);
		}
	}

	private void collapseResearchList()
	{
		Project[] list = this.projectManager.projects;
		for(int i = 0; i < list.length; ++i)
		{
			if(list[i] == null)
			{
				innerLoop: for(int e = i; e < list.length; ++e)
				{
					if(list[e] != null)
					{
						Project project = list[e];
						list[i] = project;
						list[e] = null;
						break innerLoop;
					}
				}
			}
		}
		this.projectManager.projects = list;
	}

	@Override
	public void updateEntity()
	{
		if(this.worldObj.isRemote)
		{
			if(this.firstTick)
			{
				if(this.selectedItems.getSizeInventory() < 3)
				{
					this.properties.selectionSize = 3;
					this.selectedItems.increaseSize(3 - this.selectedItems.getSizeInventory());
				}
				this.firstTick = false;
			}
			return;
		}
		if(this.firstTick)
		{
			this.network = new ResearchNetwork(this.worldObj, this.xCoord, this.yCoord, this.zCoord);
			this.network.addTile(this);
			this.updateNetwork();
			if(this.selectedItems.getSizeInventory() < 3)
			{
				this.selectedItems.increaseSize(3 - this.selectedItems.getSizeInventory());
				this.properties.selectionSize = 3;
			}
			PacketHelper.sendToDimension(new MessageFacing(this.xCoord, this.yCoord, this.zCoord, (byte)this.facing), this.worldObj.provider.dimensionId);
			this.firstTick = false;
			//TODO Remove this, testing only
			//this.projectManager.startProject(Arrays.asList(new ItemStack(Block.stone), new ItemStack(Block.cobblestone)), EnumProjectSize.SMALL, "Test");
		}
		this.network.refresh();
		if(this.network.hasEnergyStorage())
		{
			TileEnergyStorage supply = this.network.getEnergyStorage();
			if(this.projectManager.shouldTick())
			{
				int usage = this.projectManager.getTickCost(this.properties.powerConsumption);
				if(supply.canUse(usage))
				{
					supply.useEnergy(usage);
					this.projectManager.researchTick();
				}
			}
		}
		if(this.counter < 50)
		{
			++this.counter;
			return;
		}
		this.counter = 0;
		this.updateNetwork();
	}

	public void checkSelectionSize()
	{
		int size = this.selectedItems.getSizeInventory();
		if(this.properties.selectionSize > size)
		{
			this.selectedItems.increaseSize(this.properties.selectionSize - size);
		}
	}

	public List<ItemStack> getSelectedItems()
	{
		return this.selectedItems.asList();
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		tag.setTag("Research", new NBTTagCompound());
		this.projectManager.saveProjects(tag.getCompoundTag("Research"));
		tag.setTag("Properties", new NBTTagCompound());
		this.properties.writeToNBT(tag.getCompoundTag("Properties"));
		tag.setTag("SelectedItems", new NBTTagCompound());
		this.selectedItems.writeToNBT(tag.getCompoundTag("SelectedItems"));
		tag.setShort("UpdateCounter", (short)this.counter);
		tag.setShort("Facing", (short)this.facing);
		for(int i = 0; i < this.products.size(); ++i)
		{
			if(products.get(i) != null)
			{
				tag.setTag("product_" + i, new NBTTagCompound());
				products.get(i).writeToNBT(tag.getCompoundTag("product_" + i));
			}
		}
		tag.setShort("NumberProducts", (short)this.products.size());
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.projectManager.loadProjects(tag.getCompoundTag("Research"));
		this.properties.readFromNBT(tag.getCompoundTag("Properties"));
		if(this.properties.selectionSize < 3)
		{
			this.properties.selectionSize = 3;
		}
		this.selectedItems = new SelectionInventory(this.properties.selectionSize);
		this.selectedItems.readFromNBT(tag.getCompoundTag("SelectedItems"));
		this.counter = tag.getShort("UpdateCounter");
		this.facing = tag.getShort("Facing");
		for(int i = 0; i < tag.getShort("NumberProducts"); ++i)
		{
			if(tag.hasKey("product_" + i))
			{
				products.add(i, new ResearchProduct());
				products.get(i).readFromNBT(tag.getCompoundTag("product_" + i));
			}
		}
	}

	@Override
	public void onProjectFinished(ResearchProduct product)
	{
		this.products.add(product);
		this.updateNetwork();
		this.network.broadcastResearchFinished(product);
		this.collapseResearchList();
	}

	@Override
	public String getType()
	{
		return "Panel";
	}

	@Override
	public void setResearchNetwork(ResearchNetwork network)
	{
		this.network = network;
	}

	@Override
	public ResearchNetwork getNetwork()
	{
		return this.network;
	}

	@Override
	public boolean acceptsRotation()
	{
		return true;
	}

	@Override
	public void rotate(int facing)
	{
		this.facing = facing;
	}

	@Override
	public int getFacing()
	{
		return this.facing;
	}

}
