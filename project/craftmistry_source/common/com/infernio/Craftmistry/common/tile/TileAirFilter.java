package com.infernio.Craftmistry.common.tile;

import java.util.Iterator;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;

import com.infernio.Craftmistry.common.damage.CustomDS;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.recipe.AirFilterRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileAirFilter extends TileMachineInventory implements ISidedInventory
{
    public static final int ENERGY_FULL = 1000;
    public static final int INVENTORY_SIZE = 2;

    public int progress;
    public int mode;
    private int counter;

    public TileAirFilter()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public String getDefaultInvName()
    {
    	return StringLib.CONTAINER_AIR_FILTER_NAME;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
        this.mode = tag.getShort("Mode");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
        tag.setShort("Mode", (short)this.mode);
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int par1)
    {
        return this.progress * par1 / AirFilterRecipes.recipes().getDuration(AirFilterRecipes.recipes().convert(this.mode));
    }

    @Override
    public void onUpdate()
    {
        if(this.canFilter())
        {
        	int duration = AirFilterRecipes.recipes().getDuration(AirFilterRecipes.recipes().convert(this.mode));
        	int energyRequired = AirFilterRecipes.recipes().getEnergy(AirFilterRecipes.recipes().convert(this.mode)) / duration;
        	if(this.energy >= energyRequired && this.progress < duration)
        	{
        		if(this.mode == 1)
        		{
        			if(this.counter < 100)
        			{
        				++this.counter;
        			}
        			else
        			{
        				this.counter = 0;
        				if(!worldObj.isRemote)
        				{
        					this.suffocateEntities();
        				}
        			}
        		}
        		this.energy -= energyRequired;
        		++this.progress;
        		return;
        	}
            if(this.progress == duration)
            {
                this.progress = 0;
                this.filter();
            }
        }
        else
        {
            this.progress = 0;
        }
    }

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

    @SuppressWarnings("rawtypes")
	private void suffocateEntities()
    {
    	AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord + 1, yCoord + 1, zCoord + 1).expand(10, 10, 10);
    	List list = worldObj.getEntitiesWithinAABB(EntityLivingBase.class, aabb);
    	Iterator iterator = list.iterator();
    	Entity entity;
    	while(iterator.hasNext())
    	{
    		entity = (Entity)iterator.next();
    		if(!hasOxygen(entity))
    		{
    			entity.attackEntityFrom(CustomDS.noOxygen, 2);
    		}
    	}
    }
    
    private boolean hasOxygen(Entity entity)
    {
    	if(entity instanceof EntityPlayer)
    	{
    		EntityPlayer player = (EntityPlayer)entity;
    		ItemStack mask = player.inventory.getStackInSlot(39);
    		if(mask != null && mask.getItem() == ModItems.oxygenMask)
    		{
    			NBTTagCompound tag = mask.stackTagCompound;
    			if(tag == null)
    			{
    				tag = new NBTTagCompound();
    				mask.setTagCompound(tag);
    				tag.setInteger("Oxygen", 100);
    			}
    			int oxygen = tag.getInteger("Oxygen");
    			if(oxygen <= 0)
    			{
    				player.inventory.setInventorySlotContents(39, null);
    				return false;
    			}
    			tag.setInteger("Oxygen", oxygen - 1);
    			return true;
    		}
    	}
    	return false;
    }

    private boolean canFilter()
    {
    	if(!hasTestTube())
    	{
    		return false;
    	}
        if(this.inv[1] == null) return true;
        ItemStack stack = AirFilterRecipes.recipes().getResult(AirFilterRecipes.recipes().convert(this.mode));
        if(stack != null)
        {
        	stack = stack.copy();
        }
        if(!this.inv[1].isItemEqual(stack)) return false;
        int result = inv[1].stackSize + stack.stackSize;
        return (result <= getInventoryStackLimit() && result <= stack.getMaxStackSize());
    }
    
    private boolean hasTestTube()
    {
		return isValidTestTube(inv[0]);
	}

	public void filter()
    {
        if(this.canFilter())
        {
            if(this.inv[0] == null)
            {
            	return;
            }
            else
            {
            	this.decrStackSize(0, 1);
            }
            ItemStack stack = AirFilterRecipes.recipes().getResult(AirFilterRecipes.recipes().convert(this.mode));
            if(stack != null)
            {
            	stack = stack.copy();
            }
            if(this.inv[1] == null)
            {
                this.inv[1] = stack;
            }
            else if(this.inv[1].isItemEqual(stack))
            {
                inv[1].stackSize += stack.stackSize;
            }
        }
    }

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack stack)
	{
		return slot == 0 ? isValidTestTube(stack) : false;
	}

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && stack.getItem() == ModItems.testTubeSolid && stack.getItemDamage() == 0;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		if(side == 0 || side == 1)
		{
			return new int[]{0};
		}
		return new int[]{1};
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack stack, int side)
	{
		if(slot == 0)
		{
			return isValidTestTube(stack);
		}
		return false;
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack stack, int side)
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 32;
	}
}
