package com.infernio.Craftmistry.common.tile;

import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileManaSpring extends TileEntity {

	private int delay = 0;
	private int maxDelay = 0;

	@Override
	public void updateEntity()
	{
		if(!this.worldObj.isRemote)
		{
			if(this.maxDelay == 0)
			{
				this.maxDelay = this.worldObj.rand.nextInt(150);
			}
			if(this.delay < this.maxDelay)
			{
				++this.delay;
				return;
			}
			else if(this.delay >= this.maxDelay)
			{
				this.delay = 0;
				this.maxDelay = this.worldObj.rand.nextInt(150);
				EntityPlayer player = this.worldObj.getClosestPlayer(xCoord, yCoord, zCoord, 16);
				if(player != null)
				{
					EntityXPOrb xpOrb = new EntityXPOrb(this.worldObj, this.xCoord, this.yCoord + 1.0F + Math.random(), this.zCoord, this.worldObj.rand.nextInt(8));
					xpOrb.xpOrbAge = 5960;
					xpOrb.motionX = 0.0D;
					xpOrb.motionY = 0.0D;
					xpOrb.motionZ = 0.0D;
					this.worldObj.spawnEntityInWorld(xpOrb);
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		tag.setShort("Delay", (short)this.delay);
		tag.setShort("MaxDelay", (short)this.maxDelay);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.delay = tag.getShort("Delay");
		this.maxDelay = tag.getShort("MaxDelay");
	}

}
