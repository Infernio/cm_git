package com.infernio.Craftmistry.common.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileCleaner extends TileMachineInventory
{
	private static final int INVENTORY_SIZE = 2;
    public static final int PROGRESS_FINISHED = 100;
    public static final int ENERGY_FULL = 1000;
    private static final ItemStack refStack = new ItemStack(ModItems.testTubeSolid, 1, 0);

    public int progress;
    
    public TileCleaner()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int scale)
    {
        return this.progress * scale / PROGRESS_FINISHED;
    }

    @Override
    public void onUpdate()
    {
        if(this.canClean())
        {
        	if(this.energy >= 1 && this.progress < PROGRESS_FINISHED)
        	{
        		this.energy -= 1;
        		++this.progress;
        		return;
        	}
            if(this.progress == PROGRESS_FINISHED)
            {
                this.progress = 0;
                this.clean();
            }
        }
        else
        {
            this.progress = 0;
        }
    }

    private boolean canClean()
    {
        if (this.inv[0] == null)
        {
            return false;
        }
        else
        {
        	if(isValidTestTube(this.inv[0]))
        	{
	            if(this.inv[1] == null) return true;
	            if(!this.inv[1].isItemEqual(refStack)) return false;
	            int result = inv[1].stackSize + refStack.stackSize;
	            return (result <= getInventoryStackLimit() && result <= refStack.getMaxStackSize());
	        }
        	return false;
        }
    }

	public void clean()
    {
        if(this.canClean())
        {
        	this.decrStackSize(0, 1);
            if(this.inv[1] == null)
            {
                this.inv[1] = refStack.copy();
            }
            else if(this.inv[1].isItemEqual(refStack))
            {
                inv[1].stackSize += refStack.stackSize;
            }
        }
    }

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && ItemHelper.isTestTube(stack) && stack.getItemDamage() > 0 && stack.stackSize >= 1;
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 32;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_CLEANER_NAME;
	}
}
