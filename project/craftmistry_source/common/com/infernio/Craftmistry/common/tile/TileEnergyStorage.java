
package com.infernio.Craftmistry.common.tile;

import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.research.ResearchProduct;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileElectric;

public class TileEnergyStorage extends TileElectric implements IResearchTile
{
	public static final int ENERGY_FULL = 1000000;

	private ResearchNetwork network;

	@Override
	public void updateEntity()
	{
		super.updateEntity();
		this.fillBuffer();
	}

	@Override
	public void onProjectFinished(ResearchProduct product) {}

	@Override
	public String getType()
	{
		return "EnergyStorage";
	}

	@Override
	public void setResearchNetwork(ResearchNetwork network)
	{
		this.network = network;
	}

	@Override
	public ResearchNetwork getNetwork()
	{
		return this.network;
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setInteger("Energy", this.energy); //In this case, int must be used becaused of the silly amount of EU this thing can store. Otherwise, use short.
        this.getBuildCraftHandler().writeToNBT(tag, "BCPower");
		tag.setString("id", StringLib.ENERGY_STORAGE_ID);
        tag.setInteger("x", this.xCoord);
        tag.setInteger("y", this.yCoord);
        tag.setInteger("z", this.zCoord);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		this.energy = tag.getInteger("Energy");
		this.getBuildCraftHandler().readFromNBT(tag, "BCPower");
        this.getBuildCraftHandler().configure(0.0F, 50.0F, 0.0F, this.getMaxEnergy() / 5.0F);
        this.getBuildCraftHandler().configurePowerPerdition(1, 1);
		this.xCoord = tag.getInteger("x");
		this.yCoord = tag.getInteger("y");
		this.zCoord = tag.getInteger("z");
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 512;
	}
}
