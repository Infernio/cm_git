
package com.infernio.Craftmistry.common.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.recipe.CompressorRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileCompressor extends TileMachineInventory
{
	public static final int INVENTORY_SIZE = 2;
    public static final int PROGRESS_FINISHED = 1250;
    public static final int ENERGY_FULL = 2000;

    public int progress;

    public TileCompressor()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int par1)
    {
        return this.progress * par1 / PROGRESS_FINISHED;
    }

    @Override
    public void onUpdate()
    {
        if(this.canCompress())
        {
        	int energyRequired = CompressorRecipes.compressing().getEnergyRequired(inv[0]) / PROGRESS_FINISHED;
        	if(this.energy >= energyRequired && this.progress < PROGRESS_FINISHED)
        	{
        		this.energy -= energyRequired;
        		++this.progress;
        		return;
        	}
            if(this.progress == PROGRESS_FINISHED)
            {
                this.progress = 0;
                this.compress();
            }
        }
        else
        {
            this.progress = 0;
        }
    }

    private boolean canCompress()
    {
        if(this.inv[0] == null)
        {
            return false;
        }
        else
        {
        	if(!CompressorRecipes.compressing().isRegistered(this.inv[0]))
        	{
        		return false;
        	}
            ItemStack itemstack = CompressorRecipes.compressing().getResult(inv[0]);
            if (itemstack == null) return false;
            if (this.inv[1] == null) return true;
            if (!this.inv[1].isItemEqual(itemstack)) return false;
            int result = inv[1].stackSize + itemstack.stackSize;
            return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
        }
    }
    
	public void compress()
    {
        if(this.canCompress())
        {
            ItemStack stack = CompressorRecipes.compressing().getResult(inv[0]);
            if(this.inv[1] == null)
            {
                this.inv[1] = stack.copy();
            }
            else if(this.inv[1].isItemEqual(stack))
            {
                inv[1].stackSize += stack.stackSize;
            }
            this.decrStackSize(0, CompressorRecipes.compressing().getStackSizeToRemove(inv[0]));
        }
    }

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 128;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_COMPRESSOR_NAME;
	}
}
