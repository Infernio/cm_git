package com.infernio.Craftmistry.common.tile;

import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.research.ResearchProduct;

public class TileComputer extends TileEntity implements IResearchTile {

	private ResearchNetwork network;

	@Override
	public void onProjectFinished(ResearchProduct product) {}

	@Override
	public String getType()
	{
		return "Computer";
	}

	@Override
	public void setResearchNetwork(ResearchNetwork network)
	{
		this.network = network;
	}

	@Override
	public ResearchNetwork getNetwork()
	{
		return this.network;
	}

}
