package com.infernio.Craftmistry.common.tile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.api.plasmatic.BuildNode;
import com.infernio.Craftmistry.api.plasmatic.BuilderRegistry;
import com.infernio.Craftmistry.api.plasmatic.INodeBuilder;
import com.infernio.Craftmistry.common.plasmatic.NodePlaceBlock;
import com.infernio.Craftmistry.core.logging.CMLogger;
import com.infernio.InfernoCore.collect.DoubledIterator;
import com.infernio.InfernoCore.math.MathUtils;
import com.infernio.InfernoCore.math.Vector;

public class TilePlasmaticBuilder extends TileEntity {

	private List<TilePlasmaticLaser> lasers;
	private BuildInformation currentBuildInformation;
	private Cache cache;
	private int cooldown;
	private int id;
	private boolean firstTick;

	public TilePlasmaticBuilder()
	{
		this.firstTick = true;
		this.lasers = new ArrayList<TilePlasmaticLaser>();
		this.cache = new Cache(10);
	}

	public void linkLaser(TilePlasmaticLaser laser)
	{
		if(laser != null && !this.lasers.contains(laser) && laser.getID() == this.id && MathUtils.distance(laser.xCoord, laser.yCoord, laser.zCoord, this.xCoord, this.yCoord, this.zCoord) <= 32)
		{
			this.lasers.add(laser);
		}
	}

	@Override
	public void updateEntity()
	{
		if(this.firstTick)
		{
			//TODO Temporary, for testing
			TileEntity tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord + 4, this.zCoord);
			if(tile instanceof TilePlasmaticLaser)
			{
				this.linkLaser((TilePlasmaticLaser)tile);
			}
			tile = this.worldObj.getTileEntity(this.xCoord + 2, this.yCoord + 4, this.zCoord);
			if(tile instanceof TilePlasmaticLaser)
			{
				this.linkLaser((TilePlasmaticLaser)tile);
			}
			tile = this.worldObj.getTileEntity(this.xCoord - 2, this.yCoord + 4, this.zCoord);
			if(tile instanceof TilePlasmaticLaser)
			{
				this.linkLaser((TilePlasmaticLaser)tile);
			}
			tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord + 4, this.zCoord + 2);
			if(tile instanceof TilePlasmaticLaser)
			{
				this.linkLaser((TilePlasmaticLaser)tile);
			}
			tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord + 4, this.zCoord - 2);
			if(tile instanceof TilePlasmaticLaser)
			{
				this.linkLaser((TilePlasmaticLaser)tile);
			}
			TreeMap<Vector, BuildNode> nodes = new TreeMap<Vector, BuildNode>();
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.stone, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.grass, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.dirt, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.cobblestone, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.planks, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.sapling, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.bedrock, 0));
			nodes.put(new Vector(this.xCoord + 2, this.yCoord, this.zCoord), new NodePlaceBlock(Blocks.flowing_water, 0));
			this.currentBuildInformation = new BuildInformation(nodes, 6);
			this.firstTick = false;
		}
		if(this.cooldown < 40)
		{
			++this.cooldown;
			return;
		}
		this.cooldown = 0;
		this.broadcastBuildInfo();
	}

	public void cacheRecent(boolean delete)
	{
		this.cache.cache(this.currentBuildInformation);
		if(delete)
		{
			this.currentBuildInformation = null;
		}
	}

	private void broadcastBuildInfo()
	{
		for(TilePlasmaticLaser laser : this.lasers)
		{
			laser.acceptInformation(this.currentBuildInformation);
		}
	}

	public static class BuildInformation {

		private TreeMap<Vector, BuildNode> nodes = new TreeMap<Vector, BuildNode>();
		private DoubledIterator<Vector, BuildNode> iterator;
		private boolean repeated;
		private int loops;

		public BuildInformation(TreeMap<Vector, BuildNode> nodes)
		{
			this.repeated = false;
			this.loops = -1;
			this.nodes = nodes;
			this.iterator = new DoubledIterator<Vector, BuildNode>(new ArrayList<Vector>(this.nodes.keySet()), new ArrayList<BuildNode>(this.nodes.values()), this.repeated, this.loops);
		}

		public BuildInformation(TreeMap<Vector, BuildNode> nodes, boolean repeated)
		{
			this.repeated = repeated;
			this.loops = -1;
			this.nodes = nodes;
			this.iterator = new DoubledIterator<Vector, BuildNode>(new ArrayList<Vector>(this.nodes.keySet()), new ArrayList<BuildNode>(this.nodes.values()), this.repeated, this.loops);
		}

		public BuildInformation(TreeMap<Vector, BuildNode> nodes, int targetLoops)
		{
			this.repeated = false;
			this.loops = targetLoops;
			this.nodes = nodes;
			this.iterator = new DoubledIterator<Vector, BuildNode>(new ArrayList<Vector>(this.nodes.keySet()), new ArrayList<BuildNode>(this.nodes.values()), this.repeated, this.loops);
		}

		public BuildInformation(TreeMap<Vector, BuildNode> nodes, boolean repeated, int targetLoops)
		{
			this.repeated = repeated;
			this.loops = targetLoops;
			this.nodes = nodes;
			this.iterator = new DoubledIterator<Vector, BuildNode>(new ArrayList<Vector>(this.nodes.keySet()), new ArrayList<BuildNode>(this.nodes.values()), this.repeated, this.loops);
		}

		public void writeToNBT(NBTTagCompound tag)
		{
			int slot = 0;
			for(Entry<Vector, BuildNode> entry : this.nodes.entrySet())
			{
				Vector vec = entry.getKey();
				BuildNode node = entry.getValue();
				NBTTagCompound tag2 = new NBTTagCompound();
				tag2.setInteger("VectorX", vec.x);
				tag2.setInteger("VectorY", vec.y);
				tag2.setInteger("VectorZ", vec.z);
				tag2.setString("Identifier", node.getIdentifier());
				node.writeToNBT(tag2);
				tag.setTag("Node_" + slot, tag2);
				++slot;
			}
			NBTTagCompound tag3 = new NBTTagCompound();
			this.iterator.writeToNBT(tag3);
			tag.setTag("NodeIterator", tag3);
			tag.setInteger("NodeCount", slot);
			tag.setBoolean("Repeated", this.repeated);
			tag.setInteger("Loops", this.loops);
		}

		public void readFromNBT(NBTTagCompound tag)
		{
			int count = tag.getInteger("NodeCount");
			for(int i = 0; i < count; ++i)
			{
				if(tag.hasKey("Node_" + i))
				{
					NBTTagCompound nodeTag = tag.getCompoundTag("Node_" + i);
					String id = nodeTag.getString("Identifier");
					INodeBuilder builder = BuilderRegistry.getBuilder(id);
					if(builder != null)
					{
						BuildNode node = builder.buildNode(id);
						node.readFromNBT(nodeTag);
						Vector vec = new Vector(nodeTag.getInteger("VectorX"), nodeTag.getInteger("VectorY"), nodeTag.getInteger("VectorZ"));
						this.nodes.put(vec, node);
					}
				}
			}
			this.repeated = tag.getBoolean("Repeated");
			this.loops = tag.getInteger("Loops");
			this.iterator = new DoubledIterator<Vector, BuildNode>(new ArrayList<Vector>(this.nodes.keySet()), new ArrayList<BuildNode>(this.nodes.values()), this.repeated, this.loops);
			this.iterator.readFromNBT(tag.getCompoundTag("NodeIterator"));
		}

		public DoubledIterator<Vector, BuildNode> getIterator()
		{
			return this.iterator;
		}
	}

	public static class Cache {

		private BuildInformation[] cache;
		private int[] usage;
		private int cacheSize;
		private Random rand;

		public Cache(int size)
		{
			this.cache = new BuildInformation[size];
			this.usage = new int[size];
			this.cacheSize = size;
			this.rand = new Random();
		}

		public void cache(BuildInformation info)
		{
			for(int i = this.cacheSize - 1; i >= 0; --i)
			{
				BuildInformation info2 = this.cache[i];
				if(info2 == null)
				{
					this.cache[i] = info;
					return;
				}
			}
			int lowestUsage = Integer.MAX_VALUE;
			int slot = -1;
			for(int i = 0; i < this.cacheSize; ++i)
			{
				int usage = this.usage[i];
				if(usage < lowestUsage)
				{
					lowestUsage = usage;
					slot = i;
				}
			}
			if(slot != -1)
			{
				this.cache[slot] = info;
				return;
			}
			this.cache[this.rand.nextInt(this.cacheSize)] = info;
		}

		public BuildInformation get(int index)
		{
			if(index >= this.cacheSize)
			{
				CMLogger.warning("Attemped to receive cached build information for an invalid index.");
				return null;
			}
			if(this.cache[index] != null)
			{
				++this.usage[index];
				return this.cache[index];
			}
			return null;
		}

		public void writeToNBT(NBTTagCompound tag)
		{
			tag.setInteger("CacheSize", this.cacheSize);
			int saveSlot = 0;
			for(BuildInformation info : this.cache)
			{
				if(info != null)
				{
					tag.setTag("info_" + saveSlot, new NBTTagCompound());
					info.writeToNBT(tag.getCompoundTag("info_" + saveSlot));
					++saveSlot;
				}
			}
		}

		public void readFromNBT(NBTTagCompound tag)
		{
			this.cacheSize = tag.getInteger("CacheSize");
			int slot = 0;
			for(int i = 0; i < this.cacheSize; ++i)
			{
				if(tag.hasKey("info_" + i))
				{
					BuildInformation info = new BuildInformation(new TreeMap<Vector, BuildNode>());
					info.readFromNBT(tag.getCompoundTag("info_" + i));
					this.cache[slot] = info;
					++slot;
				}
			}
		}
	}

}
