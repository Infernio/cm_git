package com.infernio.Craftmistry.common.tile;

import net.minecraft.inventory.IInventory;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.research.ResearchProduct;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileBasicInventory;

public class TileMaterialCompartment extends TileBasicInventory implements IResearchTile, IInventory {

	public static final int INVENTORY_SIZE = 54;

	private ResearchNetwork network;

	public TileMaterialCompartment()
	{
		super(INVENTORY_SIZE);
	}

	@Override
	public void onProjectFinished(ResearchProduct product) {}

	@Override
	public String getType()
	{
		return "MaterialCompartment";
	}

	@Override
	public void setResearchNetwork(ResearchNetwork network)
	{
		this.network = network;
	}

	@Override
	public ResearchNetwork getNetwork()
	{
		return this.network;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_MATERIAL_COMPARTMENT_NAME;
	}

}
