package com.infernio.Craftmistry.common.tile;

import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.common.registry.GameRegistry;

public class TileLoader {

	public static void init()
	{
		GameRegistry.registerTileEntity(TileTestTubeRack.class, StringLib.TEST_TUBE_RACK_ID);
		GameRegistry.registerTileEntity(TileAnalyzer.class, StringLib.ANALYZER_ID);
		GameRegistry.registerTileEntity(TileInfuser.class, StringLib.INFUSER_ID);
		GameRegistry.registerTileEntity(TileAirFilter.class, StringLib.AIR_FILTER_ID);
		GameRegistry.registerTileEntity(TileDestabilizer.class, StringLib.DESTABILIZER_ID);
		GameRegistry.registerTileEntity(TileCompressor.class, StringLib.COMPRESSOR_ID);
		GameRegistry.registerTileEntity(TileCleaner.class, StringLib.CLEANER_ID);
		GameRegistry.registerTileEntity(TileStabilizer.class, StringLib.STABILIZER_ID);
		GameRegistry.registerTileEntity(TileMineralExtractor.class, StringLib.MINERAL_EXTRACTOR_ID);
		GameRegistry.registerTileEntity(TileReactor.class, StringLib.REACTOR_ID);
		GameRegistry.registerTileEntity(TileBreaker.class, StringLib.BREAKER_ID);
		GameRegistry.registerTileEntity(TileCollector.class, StringLib.COLLECTOR_ID);
		GameRegistry.registerTileEntity(TileDetector.class, StringLib.DETECTOR_ID);
		GameRegistry.registerTileEntity(TileResearchPanel.class, StringLib.RESEARCH_PANEL_ID);
		GameRegistry.registerTileEntity(TileComputer.class, StringLib.COMPUTER_ID);
		GameRegistry.registerTileEntity(TileMaterialCompartment.class, StringLib.MATERIAL_COMPARTMENT_ID);
		GameRegistry.registerTileEntity(TileEnergyStorage.class, StringLib.ENERGY_STORAGE_ID);
		GameRegistry.registerTileEntity(TileProductionFacility.class, StringLib.PRODUCTION_FACILITY_ID);
		GameRegistry.registerTileEntity(TileManaSpring.class, StringLib.MANA_SPRING_ID);
		GameRegistry.registerTileEntity(TileThermalRegulationChamber.class, StringLib.THERMAL_REGULATION_CHAMBER_ID);
		GameRegistry.registerTileEntity(TilePlasmaticBuilder.class, StringLib.PLASMATIC_BUILDER_ID);
		GameRegistry.registerTileEntity(TilePlasmaticLaser.class, StringLib.PLASMATIC_LASER_ID);
		GameRegistry.registerTileEntity(TileElectrolyser.class, StringLib.ELECTROLYSER_ID);
		GameRegistry.registerTileEntity(TileForester.class, StringLib.FORESTER_ID);
	}

}
