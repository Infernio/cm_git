package com.infernio.Craftmistry.common.tile;

import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.api.plasmatic.BuildNode;
import com.infernio.Craftmistry.common.tile.TilePlasmaticBuilder.BuildInformation;
import com.infernio.InfernoCore.collect.DoubledIterator;
import com.infernio.InfernoCore.math.Vector;

public class TilePlasmaticLaser extends TileEntity {

	public static final int MAX_PROGRESS = 10;

	private BuildInformation information;
	private BuildNode currentNode;
	private Vector currentVector;
	private boolean finished;
	private int buildingProgress;
	private boolean inProgress;
	private int id;

	@Override
	public void updateEntity()
	{
		if(this.worldObj.isRemote || this.finished)
		{
			return;
		}
		if(this.information != null && !this.inProgress)
		{
			DoubledIterator<Vector, BuildNode> iterator = this.information.getIterator();
			if(!iterator.isFinished())
			{
				this.currentVector = iterator.nextFirst();
				this.currentNode = iterator.nextSecond();
				this.inProgress = true;
			}
			else
			{
				this.finished = true;
			}
		}
		if(this.inProgress)
		{
			if(this.buildingProgress < MAX_PROGRESS)
			{
				++this.buildingProgress;
				return;
			}
			this.buildingProgress = 0;
			this.inProgress = false;
			if(this.currentNode != null && this.currentVector != null)
			{
				this.currentNode.apply(this.worldObj, this.currentVector.x, this.currentVector.y, this.currentVector.z);
			}
		}
	}

	public void acceptInformation(BuildInformation info)
	{
		if(this.information == null)
		{
			this.information = info;
			return;
		}
		if(!this.information.equals(info))
		{
			this.currentNode = null;
			this.currentVector = null;
			this.finished = false;
			this.information = info;
		}
	}

	public int getID()
	{
		return this.id;
	}

}
