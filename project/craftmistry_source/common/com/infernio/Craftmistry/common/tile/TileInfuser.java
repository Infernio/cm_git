package com.infernio.Craftmistry.common.tile;

import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.recipe.InfuserRecipes;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileInfuser extends TileMachineInventory implements ISidedInventory
{
	public static final int INVENTORY_SIZE = 3;
    public static final int PROGRESS_FINISHED = 100;
    public static final int ENERGY_FULL = 1000;

    public int progress;

    public TileInfuser()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int par1)
    {
        return this.progress * par1 / PROGRESS_FINISHED;
    }

    @Override
    public void onUpdate()
    {
        if(this.canInfuse())
        {
        	int energyRequired = InfuserRecipes.infusing().getEnergyRequired(inv[0]) / PROGRESS_FINISHED;
        	if(this.energy >= energyRequired && this.progress < PROGRESS_FINISHED)
        	{
        		this.energy -= energyRequired;
        		++this.progress;
        		return;
        	}
            if(this.progress == PROGRESS_FINISHED)
            {
                this.progress = 0;
                this.infuse();
            }
        }
        else
        {
            this.progress = 0;
        }
    }

    private boolean canInfuse()
    {
        if (this.inv[0] == null)
        {
            return false;
        }
        else
        {
        	if(isValidTestTube(this.inv[1]))
        	{
	            ItemStack itemstack = InfuserRecipes.infusing().getResult(inv[0]);
	            if(itemstack == null) return false;
	            if(this.inv[2] == null) return true;
	            if(!this.inv[2].isItemEqual(itemstack)) return false;
	            int result = inv[2].stackSize + itemstack.stackSize;
	            return (result <= getInventoryStackLimit() && result <= itemstack.getMaxStackSize());
	        }
        	else
        	{
        		return false;
        	}
        }
    }

	public void infuse()
    {
        if(this.canInfuse())
        {
            ItemStack stack = InfuserRecipes.infusing().getResult(inv[0]);
            if(this.inv[1] == null)
            {
            	return;
            }
            else
            {
            	this.decrStackSize(1, 1);
            }
            if(this.inv[2] == null)
            {
                this.inv[2] = stack.copy();
            }
            else if(this.inv[2].isItemEqual(stack))
            {
                inv[2].stackSize += stack.stackSize;
            }
            this.decrStackSize(0, 1);
        }
    }

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && ItemHelper.isTestTube(stack) && stack.getItemDamage() == 0 && stack.stackSize >= 1;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		if(side == 0)
		{
			return new int[]{1};
		}
		if(side == 1)
		{
			return new int[]{0};
		}
		return new int[]{2};
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack stack, int side)
	{
		if(side == 0)
		{
			return isValidTestTube(stack);
		}
		if(side == 1)
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack stack, int side)
	{
		return true;
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 32;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_INFUSER_NAME;
	}
}
