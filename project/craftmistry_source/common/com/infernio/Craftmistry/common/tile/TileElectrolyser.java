package com.infernio.Craftmistry.common.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.recipe.ElectrolyserRecipes;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.tile.TileMachineInventory;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TileElectrolyser extends TileMachineInventory
{
	public static final int INVENTORY_SIZE = 3;
    public static final int PROGRESS_FINISHED = 200;
    public static final int ENERGY_REQUIRED = 10;
    public static final int ENERGY_FULL = 2000;
    private static final int CHECK_TICKS = 10;

    private int ticksSinceCheck;
    private boolean cachedCanElectrolyse;
    private ItemStack cachedResult;

    public int progress;
    @SideOnly(Side.CLIENT)
    public int bubbleProgress;
    @SideOnly(Side.CLIENT)
    public int bubbleCooldown;

    public TileElectrolyser()
    {
    	super(INVENTORY_SIZE);
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.progress = tag.getShort("Progress");
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
        super.writeToNBT(tag);
        tag.setShort("Progress", (short)this.progress);
    }

    @SideOnly(Side.CLIENT)
    public int getProgressScaled(int scale)
    {
        return this.progress * scale / PROGRESS_FINISHED;
    }

    @Override
    public void onUpdate()
    {
        if(this.canElectrolyse())
        {
        	if(this.energy >= ENERGY_REQUIRED && this.progress < PROGRESS_FINISHED)
        	{
        		this.energy -= ENERGY_REQUIRED;
        		++this.progress;
        		return;
        	}
            if(this.progress == PROGRESS_FINISHED)
            {
                this.progress = 0;
                this.electroyse();
            }
        }
    }

    private boolean canElectrolyse()
    {
		if(this.ticksSinceCheck < CHECK_TICKS)
		{
			++this.ticksSinceCheck;
			return this.cachedCanElectrolyse;
		}
		else
		{
			return this.checkNow();
		}
    }

    private boolean checkNow()
    {
		this.ticksSinceCheck = 0;
    	if(this.inv[0] == null || this.inv[1] == null)
        {
    		this.progress = 0;
    		this.cachedCanElectrolyse = false;
            return false;
        }
        else
        {
        	ItemStack result = ElectrolyserRecipes.electrolysing().getResult(this.inv[0], this.inv[1]);
        	if(this.checkStack(result))
        	{
				this.cachedResult = result;
				this.cachedCanElectrolyse = this.checkStack(this.cachedResult);
				return true;
        	}
        	else
        	{
        		this.progress = 0;
        		this.cachedCanElectrolyse = false;
                return false;
        	}
        }
    }

    private boolean checkStack(ItemStack stack)
    {
    	if(stack == null) return false;
        if(this.inv[2] == null) return true;
        if(!this.inv[2].isItemEqual(stack)) return false;
        int result = inv[2].stackSize + stack.stackSize;
        return (result <= getInventoryStackLimit() && result <= stack.getMaxStackSize());
    }

	private void electroyse()
    {
        if(this.canElectrolyse())
        {
            if(this.inv[2] == null)
            {
                this.inv[2] = this.cachedResult.copy();
            }
            else if(this.inv[2].isItemEqual(this.cachedResult))
            {
                inv[2].stackSize += this.cachedResult.stackSize;
            }
            this.decrStackSize(0, 1);
            this.decrStackSize(1, 1);
        }
    }

    public static boolean isValidTestTube(ItemStack stack)
    {
		return stack != null && ItemHelper.isTestTube(stack) && stack.getItemDamage() == 0 && stack.stackSize >= 1;
	}

	@Override
	public boolean isRedstoneControlled()
	{
		return true;
	}

	@Override
	public int getMaxEnergy()
	{
		return ENERGY_FULL;
	}

	@Override
	public int getEUScale()
	{
		return 128;
	}

	@Override
	public String getDefaultInvName()
	{
		return StringLib.CONTAINER_ELECTROLYSER_NAME;
	}
}
