package com.infernio.Craftmistry.common.worldgen;

import java.util.Random;

import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;

import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.InfernoCore.world.WorldGenCustomOre;

import cpw.mods.fml.common.IWorldGenerator;

public class WorldGenOres implements IWorldGenerator {

	private WorldGenCustomOre sulfurOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 0, 2, Blocks.stone);
	private WorldGenCustomOre copperOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 1, 6, Blocks.stone);
	private WorldGenCustomOre tinOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 2, 3, Blocks.stone);
	private WorldGenCustomOre silverOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 3, 2, Blocks.stone);
	private WorldGenCustomOre aluminiumOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 4, 3, Blocks.stone);
	private WorldGenCustomOre cobaltOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 5, 2, Blocks.stone);
	private WorldGenCustomOre haliteOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 6, 3, Blocks.stone);
	private WorldGenCustomOre berylOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 7, 4, Blocks.stone);
	private WorldGenCustomOre fluoriteOreGen = new WorldGenCustomOre(ModBlocks.craftmistryOre, 8, 3, Blocks.stone);

	@Override
	public void generate(Random random, int x, int z, World world, IChunkProvider generator, IChunkProvider provider)
	{
		switch(world.provider.dimensionId)
		{
			case -1: generateNether(world, random, x * 16, z * 16);
			case 0: generateSurface(world, random, x * 16, z * 16);
			case 1: generateEnd(world, random, x * 16, z * 16);
			default: generateSurface(world, random, x * 16, z * 16);
		}
	}

	private void generateSurface(World world, Random random, int x, int z)
	{
		for(int i = 0; i < 18; i++)
		{
			int xCoord = x + random.nextInt(16);
			int zCoord = z + random.nextInt(16);
			int yCoord = random.nextInt(25);
			sulfurOreGen.generate(world, random, xCoord, yCoord, zCoord);
		}
		if(!CompatHandler.copperAvailable)
		{
			for(int i = 0; i < 18; i++)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(40);
				copperOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.tinAvailable)
		{
			for(int i = 0; i < 15; i++)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(40);
				tinOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.silverAvailable)
		{
			for(int i = 0; i < 14; i++)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(25);
				silverOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.aluminiumAvailable)
		{
			for(int i = 0; i < 15; i++)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(30);
				aluminiumOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.copperAvailable)
		{
			for(int i = 0; i < 10; i++)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(25);
				cobaltOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.haliteAvailable)
		{
			for(int i = 0; i < 17; i++)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(50);
				haliteOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.berylAvailable)
		{
			for(int i = 0; i < 14; ++i)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(40);
				berylOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
		if(!CompatHandler.fluoriteAvailable)
		{
			for(int i = 0; i < 12; ++i)
			{
				int xCoord = x + random.nextInt(16);
				int zCoord = z + random.nextInt(16);
				int yCoord = random.nextInt(35);
				fluoriteOreGen.generate(world, random, xCoord, yCoord, zCoord);
			}
		}
	}

	private void generateNether(World world, Random random, int x, int z)
	{
		//Craftmistry's nether ore generation should go here
	}
	

	private void generateEnd(World world, Random random, int x, int z)
	{
		//Craftmistry's end ore generation should go here
	}

}
