package com.infernio.Craftmistry.common.worldgen;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntityMobSpawner;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.World;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.DungeonHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;

import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.InfernoCore.world.WorldUtils;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.registry.GameRegistry;

public class WorldGenerator {

	public static void init()
	{
		GameRegistry.registerWorldGenerator(new WorldGenOres(), 4); //arbitrary magic number 4 cuz y not br0
		if(ConfigSettings.GENERATE_MINERAL_WATER_LAKES)
		{
			GameRegistry.registerWorldGenerator(new WorldGenCustomLakes(), 1);
		}
		MinecraftForge.EVENT_BUS.register(new WorldGenerator());
	}

	@SubscribeEvent
	public void populate(PopulateChunkEvent.Post event)
	{
		if(event.world.isRemote)
		{
			return;
		}
		int x = event.chunkX << 4;
		int z = event.chunkZ << 4;
		int structure = event.rand.nextInt(3);
		switch(structure)
		{
		case 0:
		{
			if(ConfigSettings.GENERATE_MANA_SPRINGS)
			{
				this.generateManaSpring(event.world, x, z, event.rand);
			}
			break;
		}
		case 1:
		{
			if(ConfigSettings.GENERATE_OBSIDIAN_CIRCLES)
			{
				this.generateObsidianCircle(event.world, x, z, event.rand);
			}
			break;
		}
		case 2:
		{
			if(ConfigSettings.GENERATE_ROOTS)
			{
				this.generateRoots(event.world, x, z, event.rand);
			}
		}
		}
	}

	private void generateManaSpring(World world, int x, int z, Random rand)
	{
		if(rand.nextInt(800) > 2)
		{
			return;
		}
		x += rand.nextInt(16);
		z += rand.nextInt(16);
		int y = 30 + rand.nextInt(10);
		if(WorldUtils.canOreGenReplace(world, x, y, z, ModBlocks.manaSpring) && WorldUtils.canOreGenReplace(world, x, y - 1, z, Blocks.mob_spawner))
		{
			world.setBlock(x, y, z, ModBlocks.manaSpring);
			world.setBlock(x, y - 1, z, Blocks.mob_spawner);
			TileEntity tile = world.getTileEntity(x, y - 1, z);
			if(tile instanceof TileEntityMobSpawner)
			{
				TileEntityMobSpawner spawner = (TileEntityMobSpawner)tile;
				spawner.func_145881_a().setEntityName(DungeonHooks.getRandomDungeonMob(rand));
			}
			int height = (world.getHeight() / 4) + rand.nextInt(7);
			for(int y1 = y + 1; y1 < height; ++y1)
			{
				Block block = world.getBlock(x, y1, z);
				if(block != null)
				{
					if(WorldUtils.isAirBlock(world, x, y1, z) || WorldUtils.canOreGenReplace(world, x, y1, z, Blocks.obsidian))
					{
						world.setBlock(x, y1, z, Blocks.obsidian);
					}
				}
				else
				{
					world.setBlock(x, y1, z, Blocks.obsidian);
				}
			}
		}
	}

	private void generateObsidianCircle(World world, int x, int z, Random rand)
	{
		if(rand.nextInt(1000) > 2)
		{
			return;
		}
		x += rand.nextInt(16);
		z += rand.nextInt(16);
		int y = world.getHeight() / 3 + rand.nextInt(10);
		while(true)
		{
			if(WorldUtils.isAirBlock(world, x, y, z))
			{
				--y;
				continue;
			}
			else
			{
				Block block = world.getBlock(x, y, z);
				if(block != null && block.getMaterial().isSolid())
				{
					break;
				}
				else
				{
					--y;
					continue;
				}
			}
		}
		if(WorldUtils.isAirBlock(world, x, y - 1, z) || WorldUtils.canOreGenReplace(world, x, y - 1, z, Blocks.chest))
		{
			world.setBlock(x, y - 1, z, Blocks.chest);
			TileEntity tile = world.getTileEntity(x, y - 1, z);
			if(tile instanceof TileEntityChest)
			{
				TileEntityChest chest = (TileEntityChest)tile;
				if(rand.nextBoolean())
				{
					WeightedRandomChestContent.generateChestContents(rand, ChestGenHooks.getItems(ChestGenHooks.STRONGHOLD_CORRIDOR, rand), chest, ChestGenHooks.getCount(ChestGenHooks.STRONGHOLD_CORRIDOR, rand));
				}
				else
				{
					 if(rand.nextBoolean())
					 {
						 WeightedRandomChestContent.generateChestContents(rand, ChestGenHooks.getItems(ChestGenHooks.DUNGEON_CHEST, rand), chest, ChestGenHooks.getCount(ChestGenHooks.DUNGEON_CHEST, rand));
					 }
					 else
					 {
						 WeightedRandomChestContent.generateChestContents(rand, ChestGenHooks.getItems(ChestGenHooks.VILLAGE_BLACKSMITH, rand), chest, ChestGenHooks.getCount(ChestGenHooks.VILLAGE_BLACKSMITH, rand));
					 }
				}
			}
			if(WorldUtils.isAirBlock(world, x, y, z) || WorldUtils.canOreGenReplace(world, x, y, z, Blocks.obsidian))
			{
				world.setBlock(x, y, z, Blocks.obsidian);
			}
			if(WorldUtils.isAirBlock(world, x, y + 1, z) || WorldUtils.canOreGenReplace(world, x, y + 1, z, Blocks.obsidian))
			{
				world.setBlock(x, y + 1, z, Blocks.obsidian);
			}
			if(ConfigSettings.GENERATE_MANA_SPRINGS && rand.nextInt(100) < 5)
			{
				if(WorldUtils.isAirBlock(world, x, y + 2, z) || WorldUtils.canOreGenReplace(world, x, y + 2, z, ModBlocks.manaSpring))
				{
					world.setBlock(x, y + 2, z, ModBlocks.manaSpring);
				}
			}
			else
			{
				if(WorldUtils.isAirBlock(world, x, y + 2, z) || WorldUtils.canOreGenReplace(world, x, y + 2, z, Blocks.glowstone))
				{
					world.setBlock(x, y + 2, z, Blocks.glowstone);
				}
			}
			this.placePillar(world, x + 3, y, z, rand);
			this.placePillar(world, x - 3, y, z, rand);
			this.placePillar(world, x, y, z + 3, rand);
			this.placePillar(world, x, y, z - 3, rand);
			this.placePillar(world, x + 2, y, z + 2, rand);
			this.placePillar(world, x + 2, y, z - 2, rand);
			this.placePillar(world, x - 2, y, z + 2, rand);
			this.placePillar(world, x - 2, y, z - 2, rand);
		}
	}

	private void placePillar(World world, int x, int y, int z, Random rand)
	{
		if(WorldUtils.isAirBlock(world, x, y, z) || WorldUtils.canOreGenReplace(world, x, y, z, Blocks.obsidian))
		{
			world.setBlock(x, y, z, Blocks.obsidian);
		}
		if(rand.nextBoolean())
		{
			if(WorldUtils.isAirBlock(world, x, y + 1, z) || WorldUtils.canOreGenReplace(world, x, y + 1, z, Blocks.obsidian))
			{
				world.setBlock(x, y + 1, z, Blocks.obsidian);
			}
			if(rand.nextBoolean())
			{
				if(WorldUtils.isAirBlock(world, x, y + 2, z) || WorldUtils.canOreGenReplace(world, x, y + 2, z, Blocks.obsidian))
				{
					world.setBlock(x, y + 2, z, Blocks.obsidian);
				}
			}
		}
	}

	private void generateRoots(World world, int x, int z, Random rand)
	{
		if(rand.nextInt(25) > 2)
		{
			return;
		}
		x += rand.nextInt(16);
		z += rand.nextInt(16);
		int y = world.getHeight() / 3 + rand.nextInt(10);
		while(true)
		{
			if(WorldUtils.isAirBlock(world, x, y, z))
			{
				--y;
				continue;
			}
			else
			{
				Block block = world.getBlock(x, y, z);
				if(block != null && block.getMaterial().isSolid())
				{
					break;
				}
				else
				{
					--y;
					continue;
				}
			}
		}
		for(int i = 0; i < rand.nextInt(4); ++i)
		{
	    	int blocks = 0;
	    	int maxBlocks = 15;
	    	while(true)
	    	{
	    		if(blocks >= maxBlocks)
	    		{
	    			break;
	    		}
	    		if(!WorldUtils.isAirBlock(world, x, y, z) && WorldUtils.canOreGenReplace(world, x, y, z, Blocks.log))
	    		{
	    			world.setBlock(x, y, z, Blocks.log, 0, 2);
	    		}
	    		++blocks;
	        	switch(rand.nextInt(6))
	    		{
	    		case 0:
	    			++x;
	    			break;
	    		case 1:
	    			++y;
	    			break;
	    		case 2:
	    			++z;
	    			break;
	    		case 3:
	    			--x;
	    			break;
	    		case 4:
	    			--y;
	    			break;
	    		case 5:
	    			--z;
	    			break;
	    		default:
	    			break;
	    		}
	    	}
		}
	}

}
