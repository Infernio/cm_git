package com.infernio.Craftmistry.common.worldgen;

import java.util.Random;

import net.minecraft.village.Village;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenLakes;
import net.minecraftforge.event.terraingen.PopulateChunkEvent.Populate.EventType;
import net.minecraftforge.event.terraingen.TerrainGen;

import com.infernio.Craftmistry.common.fluid.Fluids;

import cpw.mods.fml.common.IWorldGenerator;

public class WorldGenCustomLakes implements IWorldGenerator {

	private WorldGenLakes mineralLakeGen = new WorldGenLakes(Fluids.mineralWaterBlock);

	@Override
	public void generate(Random random, int x, int z, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		switch(world.provider.dimensionId)
		{
			case -1: generateNether(world, random, x * 16, z * 16); break;
			case 0: generateSurface(world, random, x * 16, z * 16, chunkProvider); break;
			case 1: generateEnd(world, random, x * 16, z * 16); break;
			default: generateSurface(world, random, x * 16, z * 16, chunkProvider);
		}
	}

	private void generateSurface(World world, Random rand, int x, int z, IChunkProvider provider)
	{
        int y = rand.nextInt(128);
        BiomeGenBase biome = world.getBiomeGenForCoords(x + 16, z + 16);
        Village village = world.villageCollectionObj != null ? world.villageCollectionObj.findNearestVillage(x, y, z, 16) : null;
        //rand.nextInt(16) == 0 is chance
        if(biome != BiomeGenBase.desert && biome != BiomeGenBase.desertHills && rand.nextInt(16) == 0 && TerrainGen.populate(provider, world, rand, x, z, village != null, EventType.LAKE))
        {
        	int x1 = x + rand.nextInt(16) + 8;
            int z1 = z + rand.nextInt(16) + 8;
            this.mineralLakeGen.generate(world, rand, x1, y, z1);
        }
	}

	private void generateNether(World world, Random random, int x, int z)
	{
		//Craftmistry's nether lake generation should go here
	}
	

	private void generateEnd(World world, Random random, int x, int z)
	{
		//Craftmistry's end lake generation should go here
	}

}
