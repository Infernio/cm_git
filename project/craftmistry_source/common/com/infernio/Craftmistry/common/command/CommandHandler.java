package com.infernio.Craftmistry.common.command;

import cpw.mods.fml.common.event.FMLServerStartingEvent;

public class CommandHandler {
	
	public static void initCommands(FMLServerStartingEvent event) 
	{
		//event.registerServerCommand(new CommandMinescript());
		event.registerServerCommand(new CommandPocketDimension());
	}

}
