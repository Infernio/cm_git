package com.infernio.Craftmistry.common.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;

import com.infernio.Craftmistry.common.item.ItemPocketDimension;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.InfernoCore.gui.Tooltip;
import com.infernio.InfernoCore.util.ItemUtils;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.GameRegistry.UniqueIdentifier;

public class CommandPocketDimension extends CommandBase {

	@Override
	public int getRequiredPermissionLevel()
    {
        return 0;
    }

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender sender)
	{
		return true;
	}

	@Override
	public String getCommandName()
	{
		return "pocketdimension";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "commands.pocketdimension.usage";
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args)
	{
		if(args.length == 0)
		{
			throw new WrongUsageException("commands.pocketdimension.usage");
		}
		EntityPlayer player = getCommandSenderAsPlayer(sender);
		if(args.length == 1)
		{
			String arg = args[0];
			if(arg.equalsIgnoreCase("help") || arg.equalsIgnoreCase("h"))
			{
				this.processHelp(player);
				return;
			}
			if(arg.equalsIgnoreCase("clear") || arg.equalsIgnoreCase("c"))
			{
				this.processClear(player);
				return;
			}
			if(arg.equalsIgnoreCase("empty") || arg.equalsIgnoreCase("e"))
			{
				this.processEmpty(player);
				return;
			}
			if(arg.equalsIgnoreCase("emptyAll") || arg.equalsIgnoreCase("ea"))
			{
				this.processEmptyAll(player);
				return;
			}
			player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.unknownCommand"));
		}
		else if((args.length == 2 || args.length == 3) && (args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("s")))
		{
			try
			{
				if(args.length == 2)
				{
					this.processSet(player, args[1], 0);
					return;
				}
				else
				{
					this.processSet(player, args[1], Integer.parseInt(args[2]));
					return;
				}
			}
			catch(NumberFormatException e)
			{
				player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.numberFormat"));
			}
		}
		else
		{
			player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.unknownCommand"));
		}
	}

	private void processHelp(EntityPlayer player)
	{
		Tooltip help = Tooltip.loadLocalizedTooltip("commands.pocketdimension.help");
		for(String s : help.tooltip)
		{
			player.addChatMessage(new ChatComponentText(s));
		}
	}

	private void processClear(EntityPlayer player)
	{
		ArrayList<ItemStack> pds = ItemPocketDimension.getPocketDimensionsFor(player);
		if(pds.isEmpty())
		{
			player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.noDimensions"));
			return;
		}
		for(ItemStack pd : pds)
		{
			String id = ItemPocketDimension.getStoredID(pd);
			int meta = ItemPocketDimension.getStoredMeta(pd);
			Item item = (Item)Item.itemRegistry.getObject(id);
			ArrayList<Integer> slots = ItemUtils.findItemSlots(player.inventory, item, meta);
			for(Integer slot : slots)
			{
				ItemStack stack = player.inventory.getStackInSlot(slot);
				if(stack.stackTagCompound == null)
				{
					int current = ItemPocketDimension.getStoredAmount(pd);
					int total = current + stack.stackSize;
					if(total > ItemPocketDimension.MAX_STORAGE_AMOUNT)
					{
						stack.stackSize = total - ItemPocketDimension.MAX_STORAGE_AMOUNT;
						ItemPocketDimension.setStoredAmount(pd, ItemPocketDimension.MAX_STORAGE_AMOUNT);
						break;
					}
					else
					{
						player.inventory.setInventorySlotContents(slot, null);
					}
					ItemPocketDimension.setStoredAmount(pd, total);
				}
			}
		}
	}

	private void processSet(EntityPlayer player, String id, int meta)
	{
		ItemStack pd = player.getCurrentEquippedItem();
		if(pd != null && pd.getItem() == ModItems.pocketDimension)
		{
			if(Item.itemRegistry.getObject(id) == null)
			{
				player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.invalidItem"));
				return;
			}
			UniqueIdentifier uid = GameRegistry.findUniqueIdentifierFor((Item)Item.itemRegistry.getObject(id));
			if(uid != null)
			{
				if(!id.startsWith(uid.modId))
				{
					id = uid.modId + ":" + id;
				}
				int amount = ItemPocketDimension.getStoredAmount(pd);
				String id1 = ItemPocketDimension.getStoredID(pd);
				int meta1 = ItemPocketDimension.getStoredMeta(pd);
				Item item = (Item)Item.itemRegistry.getObject(id1);
				if(item != null)
				{
					ItemUtils.dropStacks(player.worldObj, player.posX, player.posY, player.posZ, item, meta1, amount, item.getItemStackLimit(new ItemStack(item, 1, meta1)));
				}
				ItemPocketDimension.setStoredAmount(pd, ItemPocketDimension.DEFAULT_STORED_AMOUNT);
				ItemPocketDimension.setStoredID(pd, id);
				ItemPocketDimension.setStoredMeta(pd, meta);
				return;
			}
			else
			{
				player.addChatComponentMessage(new ChatComponentTranslation("commands.pocketdimension.invalidRegistry"));
				return;
			}
		}
		player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.requiresPD"));
	}

	private void processEmpty(EntityPlayer player)
	{
		ItemStack pd = player.getCurrentEquippedItem();
		if(pd != null && pd.getItem() == ModItems.pocketDimension)
		{
			int amount = ItemPocketDimension.getStoredAmount(pd);
			String id1 = ItemPocketDimension.getStoredID(pd);
			int meta1 = ItemPocketDimension.getStoredMeta(pd);
			Item item = (Item)Item.itemRegistry.getObject(id1);
			if(item != null)
			{
				ItemUtils.dropStacks(player.worldObj, player.posX, player.posY, player.posZ, item, meta1, amount, item.getItemStackLimit(new ItemStack(item, 1, meta1)));
			}
			ItemPocketDimension.setStoredAmount(pd, ItemPocketDimension.DEFAULT_STORED_AMOUNT);
			return;
		}
		player.addChatMessage(new ChatComponentTranslation("commands.pocketdimension.requiresPD"));
	}

	private void processEmptyAll(EntityPlayer player)
	{
		ArrayList<ItemStack> stacks = ItemPocketDimension.getPocketDimensionsFor(player);
		for(ItemStack pd : stacks)
		{
			int amount = ItemPocketDimension.getStoredAmount(pd);
			String id1 = ItemPocketDimension.getStoredID(pd);
			int meta1 = ItemPocketDimension.getStoredMeta(pd);
			Item item = (Item)Item.itemRegistry.getObject(id1);
			if(item != null)
			{
				ItemUtils.dropStacks(player.worldObj, player.posX, player.posY, player.posZ, item, meta1, amount, item.getItemStackLimit(new ItemStack(item, 1, meta1)));
			}
			ItemPocketDimension.setStoredAmount(pd, ItemPocketDimension.DEFAULT_STORED_AMOUNT);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getCommandAliases()
	{
		return Arrays.asList("pd", "pdim");
	}

}
