package com.infernio.Craftmistry.common.tick;

import com.infernio.Craftmistry.core.nei.NEIHandler;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

//At some point, return capes. Cape URL: http://i.imgur.com/8JKr5Nj.png
@SideOnly(Side.CLIENT)
public class ClientTickHandler {

	private boolean initialized = false;

	@SubscribeEvent
	public void handleTick(TickEvent.ClientTickEvent event)
	{
		if(!initialized && FMLClientHandler.instance().getClient().currentScreen == null)
		{
			NEIHandler.printNEIMessage();
			initialized = true;
		}
	}

}
