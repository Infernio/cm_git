package com.infernio.Craftmistry.common.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.block.BlockMachineFrame;

public class ItemCraftingComponent extends ItemBlock {

	public ItemCraftingComponent(Block block)
	{
		super(block);
		this.setHasSubtypes(true);
	}

    @Override
	public String getUnlocalizedName(ItemStack stack)
    {
        return super.getUnlocalizedName(stack) + "." + BlockMachineFrame.names[stack.getItemDamage()];
    }

	@Override
	public int getMetadata(int meta)
	{
		return meta;
	}
}
