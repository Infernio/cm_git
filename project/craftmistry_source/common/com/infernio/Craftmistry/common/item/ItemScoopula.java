package com.infernio.Craftmistry.common.item;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.achievement.AchievementHandler;
import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemScoopula extends ItemCM {

	public ItemScoopula()
	{
		this.setUnlocalizedName(StringLib.ITEM_SCOOPULA_NAME);
		this.setMaxDamage(9);
		this.setMaxStackSize(1);
		this.setFull3D();
	}

	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float par8, float par9, float par10)
    {
		if(ItemHelper.specialDropOn(world.getBlock(x, y, z), world.getBlockMetadata(x, y, z)))
        {
        	ItemHelper.doSpecialDrop(world, x, y, z);
        	player.addStat(AchievementHandler.theProperTool, 1);
        	if(!world.isRemote)
        	{
        		world.playAuxSFX(2001, x, y, z, Block.getIdFromBlock(world.getBlock(x, y, z)) + (world.getBlockMetadata(x, y, z) << 12));
        	}
        	if(!player.capabilities.isCreativeMode)
        	{
        		if(stack.getItemDamage() >= this.getMaxDamage())
        		{
        			player.renderBrokenItemStack(stack);
        			player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
        		}
        		else
        		{
	        		stack.setItemDamage(stack.getItemDamage() + 1);
	        	}
        	}
        	return true;
        }
        return false;
    }

}
