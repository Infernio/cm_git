package com.infernio.Craftmistry.common.item.testtube;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.chemistry.ThreatHandler;
import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTestTubeCompound extends ItemCM {

	private IIcon[] icons;

	public ItemTestTubeCompound()
	{
		this.setUnlocalizedName(StringLib.ITEM_TEST_TUBE_NAME);
		this.setHasSubtypes(true);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		ArrayList<Integer> compounds = Elements.getValidCompounds();
		for(int i = 0; i < compounds.size(); ++i)
		{
			list.add(new ItemStack(item, 1, compounds.get(i)));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		int meta = stack.getItemDamage();
		list.add(StatCollector.translateToLocal("compound." + Elements.getCompoundName(meta) + ".name"));
	}

	@Override
    public IIcon getIconFromDamage(int meta)
    {
		if(this.icons == null)
		{
			return null;
		}
        return icons[meta];
    }

	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int slot, boolean b)
	{
		if(entity instanceof EntityPlayer)
		{
			ThreatHandler.updateCompound(slot, (EntityPlayer)entity);
		}
	}

	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister)
	{
		ArrayList<Integer> compounds = Elements.getValidCompounds();
		icons = new IIcon[2048];
		for(int i = 0; i < compounds.size(); ++i)
		{
			String iconName = ItemTestTubeSolid.DEF_NAME + Elements.getCompoundTextureName(compounds.get(i));
			icons[i] = iconRegister.registerIcon(Elements.getCompoundModId(compounds.get(i)) + ":" + iconName);
		}
	}

}
