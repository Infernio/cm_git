package com.infernio.Craftmistry.common.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemMulti extends ItemCM {

	public static final String[] names = new String[]
	{
	"iconSun",
	"iconRain",
	"iconThunder",
	"iconMoon",
	"iconRadioactive",
	"itemCMRawSalt",
	"itemCMRefinedSalt",
	"itemCMRawSulfur",
	"itemCMBasicMicrochip",
	"itemCMCommercialMicrochip",
	"itemCMAdvancedMicrochip",
	"itemCMBasicPlate",
	"itemCMHardenedPlate",
	"itemCMAdvancedPlate",
	"itemCMBasicMachineController",
	"itemCMCommercialMachineController",
	"itemCMAdvancedMachineController",
	"itemCMChemicalInterface",
	"itemCMWirelessTransmitter",
	"itemCMWirelessReceiver",
	"itemCMDiamondDust",
	"itemCMLithiumSalts",
	"itemCMRedPearl"
	};

	private IIcon[] icons;

	public ItemMulti()
	{
		this.setHasSubtypes(true);
		this.setUnlocalizedName(StringLib.ITEM_MULTI_NAME);
	}

	@Override
	public IIcon getIconFromDamage(int meta)
	{
		if(this.icons == null)
		{
			return null;
		}
		return icons[MathHelper.clamp_int(meta, 0, icons.length - 1)];
	}

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
    {
    	for(int meta = 5; meta < names.length; ++meta)
    	{
    		list.add(new ItemStack(item, 1, meta));
    	}
    }

	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new IIcon[names.length];
		for(int i = 0; i < names.length; ++i)
		{
			icons[i] = iconRegister.registerIcon(StringLib.MODID + ":" + names[i]);
		}
	}

    @Override
	public String getUnlocalizedName(ItemStack stack)
    {
    	if(stack.getItemDamage() < 5 || stack.getItemDamage() >= names.length)
    	{
    		return "Something went wrong";
    	}
        return super.getUnlocalizedName(stack) + "." + names[stack.getItemDamage()];
    }

}
