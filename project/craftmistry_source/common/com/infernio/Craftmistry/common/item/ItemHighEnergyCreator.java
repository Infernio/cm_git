package com.infernio.Craftmistry.common.item;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerWorkbench;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.InfernoCore;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemHighEnergyCreator extends ItemCM implements IElectricItem {

	public static final int MAX_CHARGE = 1000000;

	public ItemHighEnergyCreator()
	{
		this.setUnlocalizedName(StringLib.ITEM_HIGH_ENERGY_CREATOR_NAME);
		this.setMaxStackSize(1);
		this.setMaxDamage(27);
		this.setFull3D();
		BlockDispenser.dispenseBehaviorRegistry.putObject(this, new BehaviorHighEnergyCreator());
	}

	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float exactX, float exactY, float exactZ)
    {
		NBTTagCompound tag = stack.stackTagCompound;
		if(tag == null)
		{
			tag = new NBTTagCompound();
			stack.setTagCompound(tag);
			stack.stackTagCompound.setInteger("charge", 0);
		}
		if(!CompatHandler.ic2Present || !ElectricItem.manager.canUse(stack, 1000))
		{
			return false;
		}
		if(player.isSneaking())
		{
	        if(side == 0)
	        {
	            --y;
	        }
	        if(side == 1)
	        {
	            ++y;
	        }
	        if(side == 2)
	        {
	            --z;
	        }
	        if(side == 3)
	        {
	            ++z;
	        }
	        if(side == 4)
	        {
	            --x;
	        }
	        if(side == 5)
	        {
	            ++x;
	        }
		}
		InventoryCrafting crafting = this.createCraftingInventory(world, player, x, y, z);
		int slot = 0;
		for(int i = -1; i < 2; ++i)
		{
			for(int e = -1; e < 2; ++e)
			{
				crafting.setInventorySlotContents(slot, this.getSlotContentFromWorld(world, x + i, y, z + e));
				++slot;
			}
		}
		ItemStack result = CraftingManager.getInstance().findMatchingRecipe(crafting, world);
		if(result != null)
		{
			this.craft(world, x, y, z);
			ElectricItem.manager.discharge(stack, 1000, 3, true, false);
			if(!world.isRemote)
			{
				world.spawnEntityInWorld(new EntityItem(world, x + 0.3D, y + 0.25D, z + 0.5D, result.copy()));
			}
		}
		else
		{
			if(world.isRemote)
			{
				this.spawnParticles(world, x, y, z);
			}
		}
		return true;
    }

	private InventoryCrafting createCraftingInventory(World world, EntityPlayer player, int x, int y, int z)
	{
		ContainerWorkbench container = new ContainerWorkbench(player.inventory, world, x, y, z);
		return new InventoryCrafting(container, 3, 3);
	}

	private ItemStack getSlotContentFromWorld(World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
		if(!block.isAir(world, x, y, z))
		{
			int meta = world.getBlockMetadata(x, y, z);
			return new ItemStack(block, 1, meta);
		}
		return null;
	}

	private void craft(World world, int x, int y, int z)
	{
		for(int i = -1; i < 2; ++i)
		{
			for(int e = -1; e < 2; ++e)
			{
				Block block = world.getBlock(x, y, z);
				if(block != null)
				{
					world.playSound(x + i, y, z + e, block.stepSound.getBreakSound(), InfernoCore.proxy.getSoundVolume(SoundCategory.BLOCKS), 1.0F, true);
				}
				world.setBlockToAir(x + i, y, z + e);
				if(world.isRemote)
				{
					for(int p = 0; p < 10; ++p)
					{
						world.spawnParticle("happyVillager", x + i + Math.random(), y + Math.random(), z + e + Math.random(), 0.0D, 0.0D, 0.0D);
					}
				}
			}
		}
	}

	private void spawnParticles(World world, int x, int y, int z)
	{
		world.playSound(x, y, z, "random.fizz", InfernoCore.proxy.getSoundVolume(SoundCategory.AMBIENT), 0.3F, true);
		if(world.isRemote)
		{
			for(int i = -1; i < 2; ++i)
			{
				for(int e = -1; e < 2; ++e)
				{
					for(int p = 0; p < 10; ++p)
					{
						world.spawnParticle("witchMagic", x + i + Math.random(), y + 0.25D + Math.random(), z + e + Math.random(), 0.0D, 0.0D, 0.0D);
					}
				}
			}
		}
	}

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
    @SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tab, List list)
    {
        ItemStack stack = new ItemStack(item, 1, 1);
        stack.setTagCompound(new NBTTagCompound());
        stack.stackTagCompound.setInteger("charge", MAX_CHARGE);
        list.add(stack);
        ItemStack stack2 = new ItemStack(item, 1, 26);
        stack2.setTagCompound(new NBTTagCompound());
        stack2.stackTagCompound.setInteger("charge", 0);
        list.add(stack2);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
    {
    	if(CompatHandler.ic2Present)
    	{
			NBTTagCompound tag = stack.stackTagCompound;
			if(tag == null)
			{
				tag = new NBTTagCompound();
				stack.setTagCompound(tag);
			}
	    	list.add("" + stack.stackTagCompound.getInteger("charge") + " EU / " + MAX_CHARGE + " EU");
	    }
    	else
    	{
	    	list.add(EnumChatFormatting.RED + "IndustrialCraft 2 is not loaded!");
	    }
    }

	@Override
	public boolean canProvideEnergy(ItemStack itemStack)
	{
		return false;
	}

	@Override
	public int getChargedItemId(ItemStack itemStack)
	{
		return Item.getIdFromItem(this);
	}

	@Override
	public int getEmptyItemId(ItemStack itemStack)
	{
		return Item.getIdFromItem(this);
	}

	@Override
	public int getMaxCharge(ItemStack itemStack)
	{
		return MAX_CHARGE;
	}

	@Override
	public int getTier(ItemStack itemStack)
	{
		return 0;
	}

	@Override
	public int getTransferLimit(ItemStack itemStack)
	{
		return 512;
	}

}
