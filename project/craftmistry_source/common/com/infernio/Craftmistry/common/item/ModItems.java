package com.infernio.Craftmistry.common.item;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;

import com.infernio.Craftmistry.api.CraftmistryAPI;
import com.infernio.Craftmistry.common.item.testtube.ItemTestTubeCompound;
import com.infernio.Craftmistry.common.item.testtube.ItemTestTubeGas;
import com.infernio.Craftmistry.common.item.testtube.ItemTestTubeLiquid;
import com.infernio.Craftmistry.common.item.testtube.ItemTestTubePlasma;
import com.infernio.Craftmistry.common.item.testtube.ItemTestTubeSolid;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.common.registry.GameRegistry;

public class ModItems {

	public static Item testTubeSolid;
	public static Item testTubeLiquid;
	public static Item testTubeGas;
	public static Item testTubePlasma;
	public static Item testTubeCompound;
	public static Item rawMineral;
	public static Item ingot;
	public static Item scoopula;
	public static Item oxygenMask;
	public static Item buckets;
	public static Item grenade;
	public static Item highEnergyCreator;
	public static Item superPhosphate;
	public static Item upgrade;
	public static Item multiItem;
	public static Item spoiledFood;
	public static Item novaShovel;
	public static Item novaPickaxe;
	public static Item novaAxe;
	public static Item novaShears;
	public static Item pocketDimension;
	public static Item treeController;

	public static void init()
	{
		testTubeSolid = new ItemTestTubeSolid();
		testTubeLiquid = new ItemTestTubeLiquid();
		testTubeGas = new ItemTestTubeGas();
		//TODO Add a usage for plasma
		testTubePlasma = new ItemTestTubePlasma();
		testTubeCompound = new ItemTestTubeCompound();
		rawMineral = new ItemRawMineral();
		ingot = new ItemCMIngot();
		scoopula = new ItemScoopula();
		oxygenMask = new ItemOxygenMask();
		buckets = new ItemCMBucket();
		superPhosphate = new ItemSuperPhosphate();
		grenade = new ItemGrenade();
		highEnergyCreator = new ItemHighEnergyCreator();
		upgrade = new ItemUpgrade();
		multiItem = new ItemMulti();
		spoiledFood = new ItemSpoiledFood();
		novaShovel = new ItemNovaShovel();
		novaPickaxe = new ItemNovaPickaxe();
		novaAxe = new ItemNovaAxe();
		novaShears = new ItemNovaShears();
		pocketDimension = new ItemPocketDimension();
		treeController = new ItemTreeController();

		GameRegistry.registerItem(testTubeSolid, StringLib.ITEM_TEST_TUBE_SANE_NAME + "_solid");
		GameRegistry.registerItem(testTubeLiquid, StringLib.ITEM_TEST_TUBE_SANE_NAME + "_liquid");
		GameRegistry.registerItem(testTubeGas, StringLib.ITEM_TEST_TUBE_SANE_NAME + "_gas");
		GameRegistry.registerItem(testTubePlasma, StringLib.ITEM_TEST_TUBE_SANE_NAME + "_plasma");
		GameRegistry.registerItem(testTubeCompound, StringLib.ITEM_TEST_TUBE_SANE_NAME + "_compound");
		GameRegistry.registerItem(rawMineral, StringLib.ITEM_RAW_MINERAL_SANE_NAME);
		GameRegistry.registerItem(ingot, StringLib.ITEM_CRAFTMISTRY_INGOT_SANE_NAME);
		GameRegistry.registerItem(scoopula, StringLib.ITEM_SCOOPULA_SANE_NAME);
		GameRegistry.registerItem(oxygenMask, StringLib.ITEM_OXYGEN_MASK_SANE_NAME);
		GameRegistry.registerItem(buckets, StringLib.ITEM_BUCKET_SANE_NAME);
		GameRegistry.registerItem(superPhosphate, StringLib.ITEM_SUPER_PHOSPHATE_SANE_NAME);
		GameRegistry.registerItem(grenade, StringLib.ITEM_GRENADE_SANE_NAME);
		GameRegistry.registerItem(highEnergyCreator, StringLib.ITEM_HIGH_ENERGY_CREATOR_SANE_NAME);
		GameRegistry.registerItem(upgrade, StringLib.ITEM_UPGRADE_SANE_NAME);
		GameRegistry.registerItem(multiItem, StringLib.ITEM_MULTI_SANE_NAME);
		GameRegistry.registerItem(spoiledFood, StringLib.ITEM_SPOILED_FOOD_SANE_NAME);
		GameRegistry.registerItem(novaShovel, StringLib.ITEM_NOVA_SHOVEL_SANE_NAME);
		GameRegistry.registerItem(novaPickaxe, StringLib.ITEM_NOVA_PICKAXE_SANE_NAME);
		GameRegistry.registerItem(novaAxe, StringLib.ITEM_NOVA_AXE_SANE_NAME);
		GameRegistry.registerItem(novaShears, StringLib.ITEM_NOVA_SHEARS_SANE_NAME);
		GameRegistry.registerItem(pocketDimension, StringLib.ITEM_POCKET_DIMENSION_SANE_NAME);
		GameRegistry.registerItem(treeController, StringLib.ITEM_TREE_CONTROLLER_SANE_NAME);

		setAPIValues();
	}

	public static void addRecipes()
	{
		CompatHandler.init();

		//Crafting components
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 8), new Object[]{
			"SRS","RGR","IRI",'R',Items.redstone,'S',Blocks.stone,'G',Items.gold_nugget,'I',"ingotIron"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 9), new Object[]{
			"RIR","GMG","RIR",'M',new ItemStack(multiItem, 1, 8),'I',"ingotIron",'G',Items.gold_ingot,'R',Items.redstone
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 10), new Object[]{
			"GDG","RMR","GDG",'M',new ItemStack(multiItem, 1, 9),'R',Items.redstone,'G',Items.gold_ingot,'D',new ItemStack(multiItem, 1, 20)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 11), new Object[]{
			"SIS","ISI","SIS",'S',Blocks.stone,'I',"ingotIron"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 12), new Object[]{
			"ISI","SPS","ISI",'S',new ItemStack(testTubeCompound, 1, 5),'I',"ingotIron",'P',new ItemStack(multiItem, 1, 11)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 13), new Object[]{
			"SDS","DPD","SDS",'S',new ItemStack(testTubeCompound, 1, 5),'D',new ItemStack(multiItem, 1, 20),'P',new ItemStack(multiItem, 1, 12)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 14), new Object[]{
			"LMB","ISI","MRM",'S',Blocks.stone,'I',"ingotIron",'R',Items.redstone,'L',Blocks.lever,'B',Blocks.stone_button,'M',new ItemStack(multiItem, 1, 8)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 15), new Object[]{
			"LMB","ISI","MRM",'S',Blocks.stone,'I',"ingotIron",'R',Blocks.redstone_block,'L',Blocks.lever,'B',Blocks.stone_button,'M',new ItemStack(multiItem, 1, 9)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 16), new Object[]{
			"LMB","ISI","MRM",'S',Blocks.stone,'I',Blocks.iron_block,'R',Blocks.redstone_block,'L',Blocks.lever,'B',Blocks.stone_button,'M',new ItemStack(multiItem, 1, 10)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 17), new Object[]{
			" T ","IPI"," I ",'T',testTubeSolid,'I',"ingotIron",'P',new ItemStack(multiItem, 1, 11)
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 22), new Object[]{
			"RRR","RPR","RRR",'P',Items.ender_pearl,'R',Items.redstone
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 18), new Object[]{
			"IPI","SLS","ISI",'P',Items.ender_pearl,'I',"ingotIron",'L',Blocks.lever,'S',Blocks.stone
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(multiItem, 1, 19), new Object[]{
			"IPI","SLS","ISI",'P',new ItemStack(multiItem, 1, 22),'I',"ingotIron",'L',Blocks.lever,'S',Blocks.stone
		}));

		//Items
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(testTubeSolid, 4, 0), new Object[]{
			"GWG","G G"," G ",'W',"plankWood",'G',Blocks.glass_pane
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(scoopula), new Object[]{
			"  I"," S ","   ",'S',Items.stick,'I',"ingotIron"
		}));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(scoopula), new Object[]{
			"   "," I ", "S  ",'S',Items.stick,'I',"ingotIron"
		}));
		GameRegistry.addRecipe(new ItemStack(oxygenMask), new Object[]{
			"SGS","TST","LLL",'L',Items.leather,'S',Items.string,'G',Blocks.glass_pane,'T',new ItemStack(testTubeGas, 1, 8)
		});
		GameRegistry.addShapelessRecipe(new ItemStack(oxygenMask), new Object[]{
			new ItemStack(oxygenMask, 1, 0), new ItemStack(testTubeGas, 1, 8), new ItemStack(testTubeGas, 1, 8)
		});
		//FIXME Recipe for range upgrade
	}

	private static void setAPIValues()
	{
		CraftmistryAPI.Chemistry.STATE_SOLID_ITEM = testTubeSolid;
		CraftmistryAPI.Chemistry.STATE_LIQUID_ITEM = testTubeLiquid;
		CraftmistryAPI.Chemistry.STATE_GAS_ITEM = testTubeGas;
		CraftmistryAPI.Chemistry.STATE_PLASMA_ITEM = testTubePlasma;
		CraftmistryAPI.Chemistry.COMPOUND_ITEM = testTubeCompound;
	}

}
