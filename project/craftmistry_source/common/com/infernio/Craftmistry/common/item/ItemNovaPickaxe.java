package com.infernio.Craftmistry.common.item;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemNovaPickaxe extends ItemNovaTool {

	public ItemNovaPickaxe()
	{
		super(new Block[]{Blocks.cobblestone, Blocks.double_stone_slab, Blocks.stone_slab, Blocks.stone, Blocks.sandstone, Blocks.mossy_cobblestone, Blocks.iron_ore, Blocks.iron_block, Blocks.coal_ore, Blocks.gold_block, Blocks.gold_ore, Blocks.diamond_ore, Blocks.diamond_block, Blocks.ice, Blocks.netherrack, Blocks.lapis_ore, Blocks.lapis_block, Blocks.redstone_ore, Blocks.lit_redstone_ore, Blocks.rail, Blocks.detector_rail, Blocks.golden_rail, Blocks.activator_rail, Blocks.obsidian, Blocks.emerald_ore, Blocks.emerald_block, Blocks.coal_block, Blocks.redstone_block});
		this.setUnlocalizedName(StringLib.ITEM_NOVA_PICKAXE_NAME);
	}

	@Override
	public boolean hasEffect(ItemStack stack)
	{
		return true;
	}

	@Override
	public EnumRarity getRarity(ItemStack stack)
	{
		return EnumRarity.rare;
	}

    @Override
	public boolean canHarvestBlock(Block block, ItemStack stack)
    {
    	return block.getMaterial() == Material.iron || block.getMaterial() == Material.rock || block.getMaterial() == Material.anvil ? true : super.canHarvestBlock(block, stack);
    }

    @Override
	public float getDigSpeed(ItemStack stack, Block block, int meta)
    {
    	return block.getMaterial() == Material.iron || block.getMaterial() == Material.rock || block.getMaterial() == Material.anvil ? 10.0F : super.getDigSpeed(stack, block, meta);
    }

}
