package com.infernio.Craftmistry.common.item;

import net.minecraft.block.BlockDispenser;
import net.minecraft.dispenser.BehaviorDefaultDispenseItem;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.FakePlayerFactory;

public final class BehaviorHighEnergyCreator extends BehaviorDefaultDispenseItem
{
    @Override
	protected ItemStack dispenseStack(IBlockSource blockSource, ItemStack stack)
    {
        EnumFacing facing = BlockDispenser.func_149937_b(blockSource.getBlockMetadata());
        World world = blockSource.getWorld();
        int x = blockSource.getXInt() + (facing.getFrontOffsetX() * 2);
        int y = blockSource.getYInt() + (facing.getFrontOffsetY() * 2);
        int z = blockSource.getZInt() + (facing.getFrontOffsetZ() * 2);
        ModItems.highEnergyCreator.onItemUse(stack, FakePlayerFactory.getMinecraft((WorldServer)world), world, x, y, z, 0, 0.0F, 0.0F, 0.0F);
        return stack;
    }

    @Override
	protected void playDispenseSound(IBlockSource blockSource)
    {
    	blockSource.getWorld().playAuxSFX(1000, blockSource.getXInt(), blockSource.getYInt(), blockSource.getZInt(), 0);
    }
}
