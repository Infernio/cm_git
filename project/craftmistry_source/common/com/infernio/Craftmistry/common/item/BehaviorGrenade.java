package com.infernio.Craftmistry.common.item;

import net.minecraft.block.BlockDispenser;
import net.minecraft.dispenser.BehaviorDefaultDispenseItem;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.dispenser.IPosition;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.entity.EntityGrenade;
import com.infernio.Craftmistry.common.entity.EntityGrenade.EnumGrenade;

public final class BehaviorGrenade extends BehaviorDefaultDispenseItem {

	@Override
	public ItemStack dispenseStack(IBlockSource block, ItemStack stack)
    {
        World world = block.getWorld();
        IPosition pos = BlockDispenser.func_149939_a(block);
        EnumFacing enumfacing = BlockDispenser.func_149937_b(block.getBlockMetadata());
        EntityGrenade grenade = new EntityGrenade(world, pos.getX(), pos.getY(), pos.getZ());
        grenade.setThrowableHeading((double)enumfacing.getFrontOffsetX(), (double)((float)enumfacing.getFrontOffsetY() + 0.1F), (double)enumfacing.getFrontOffsetZ(), 1.1F, 6.0F);
        EnumGrenade[] grenades = EntityGrenade.EnumGrenade.values();
        grenade.grenade = grenades[MathHelper.clamp_int(stack.getItemDamage(), 0, grenades.length)];
        world.spawnEntityInWorld(grenade);
        --stack.stackSize;
        return stack;
    }

	@Override
	protected void playDispenseSound(IBlockSource block)
    {
        block.getWorld().playAuxSFX(1002, block.getXInt(), block.getYInt(), block.getZInt(), 0);
    }

}
