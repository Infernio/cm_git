package com.infernio.Craftmistry.common.item;

import java.util.List;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemUpgrade extends ItemCM implements IMachineUpgrade {

	public static final String[] tooltips = new String[]{"Range"};
	public static final String[] advancedTooltips = new String[]{"Increases the range of a machine"};

	public ItemUpgrade()
	{
		this.setUnlocalizedName(StringLib.ITEM_UPGRADE_NAME);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		list.add(EnumChatFormatting.GOLD + tooltips[MathHelper.clamp_int(stack.getItemDamage(), 0, tooltips.length)]);
		list.add("");
		if(GuiScreen.isShiftKeyDown())
		{
			list.add(advancedTooltips[MathHelper.clamp_int(stack.getItemDamage(), 0, advancedTooltips.length)]);
		}
		else
		{
			list.add("Hold Shift for more information");
		}
	}

	@Override
	public boolean isValid(TileEntity machine, ItemStack stack)
	{
		return true;
	}

}
