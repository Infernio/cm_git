package com.infernio.Craftmistry.common.item;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemNovaShovel extends ItemNovaTool {

	public ItemNovaShovel()
	{
		super(new Block[]{Blocks.grass, Blocks.dirt, Blocks.sand, Blocks.gravel, Blocks.snow_layer, Blocks.snow, Blocks.clay, Blocks.farmland, Blocks.soul_sand, Blocks.mycelium});
		this.setUnlocalizedName(StringLib.ITEM_NOVA_SHOVEL_NAME);
	}

	@Override
	public boolean hasEffect(ItemStack stack)
	{
		return true;
	}

	@Override
	public EnumRarity getRarity(ItemStack stack)
	{
		return EnumRarity.rare;
	}

}
