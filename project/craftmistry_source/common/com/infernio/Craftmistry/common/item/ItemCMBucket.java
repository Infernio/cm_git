package com.infernio.Craftmistry.common.item;

import java.util.List;
import java.util.Locale;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.fluid.Fluids;
import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemCMBucket extends ItemCM {

	private IIcon[] icons;

	public static final String[] names = new String[]
	{
		"SulfuricAcid",
		"MineralWater"
	};

	public static final String[] iconNames = new String[]
	{
		"SulfuricAcid",
		"MineralWater"
	};

	public ItemCMBucket()
	{
		this.setHasSubtypes(true);
		this.setMaxStackSize(1);
		this.setContainerItem(Items.bucket);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setUnlocalizedName(StringLib.ITEM_BUCKET_NAME);
	}

    @Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float f, float f2, float f3)
    {
        if(side == 0)
        {
            --y;
        }
        if(side == 1)
        {
            ++y;
        }
        if(side == 2)
        {
            --z;
        }
        if(side == 3)
        {
            ++z;
        }
        if(side == 4)
        {
            --x;
        }
        if(side == 5)
        {
            ++x;
        }
        switch(stack.getItemDamage())
        {
        case 0: world.setBlock(x, y, z, Fluids.sulfuricAcidBlock, 0, 2); break;
        case 1: world.setBlock(x, y, z, Fluids.mineralWaterBlock, 0, 2); break;
        default: world.setBlock(x, y, z, Blocks.water); break;
        }
        if(!player.capabilities.isCreativeMode)
        {
        	player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(Items.bucket));
        }
        return true;
    }

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < names.length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
    public IIcon getIconFromDamage(int meta)
    {
		if(this.icons == null)
		{
			return null;
		}
        return icons[meta];
    }

	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new IIcon[iconNames.length];
		for(int i = 0; i < iconNames.length; ++i)
		{
			icons[i] = iconRegister.registerIcon(StringLib.MODID + ":" + this.getUnlocalizedName().substring(5) + iconNames[i].toLowerCase(Locale.ENGLISH));
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
    {
    	return super.getUnlocalizedName(stack) + "." + names[MathHelper.clamp_int(stack.getItemDamage(), 0, names.length - 1)];
    }

}
