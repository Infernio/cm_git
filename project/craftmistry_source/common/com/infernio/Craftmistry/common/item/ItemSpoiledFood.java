package com.infernio.Craftmistry.common.item;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemSpoiledFood extends ItemFood {

	public static final String NBT_FOOD_ID = "FoodID";
	public static final String NBT_FOOD_META = "FoodMeta";

	public ItemSpoiledFood()
	{
		super(0, 0.0F, false);
		this.setUnlocalizedName(StringLib.ITEM_SPOILED_FOOD_NAME);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setHasSubtypes(true);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	@SideOnly(Side.CLIENT)
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(Object id : Item.itemRegistry.getKeys())
		{
			String id1 = (String)id;
			Item item1 = (Item)Item.itemRegistry.getObject(id1);
			if(item1 instanceof ItemFood && item1 != this)
			{
				List<ItemStack> subStacks = new ArrayList<ItemStack>();
				item1.getSubItems(item1, item1.getCreativeTab(), subStacks);
				for(int i = 0; i < subStacks.size(); ++i)
				{
					ItemStack sub = subStacks.get(i);
					if(sub != null && sub.getItem() != null)
					{
						list.add(encode(item1, 1, sub.getItemDamage()));
					}
				}
			}
		}
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack)
	{
		if(stack != null && stack.getItem() != null && stack.stackTagCompound != null)
		{
			ItemStack decoded = decode(stack);
			if(decoded != null && decoded.getItem() != null)
			{
				return decoded.getItem().getItemStackDisplayName(decoded);
			}
		}
		return "ERROR";
	}

	@Override
	public EnumRarity getRarity(ItemStack stack)
	{
		if(stack != null && stack.getItem() != null && stack.stackTagCompound != null)
		{
			ItemStack decoded = decode(stack);
			if(decoded != null && decoded.getItem() != null)
			{
				return decoded.getItem().getRarity(decoded);
			}
		}
		return EnumRarity.common;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean hasEffect(ItemStack stack)
	{
		if(stack != null && stack.getItem() != null && stack.stackTagCompound != null)
		{
			ItemStack decoded = decode(stack);
			if(decoded != null && decoded.getItem() != null)
			{
				return decoded.getItem().hasEffect(decoded);
			}
		}
		return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		list.add(EnumChatFormatting.DARK_GRAY + "" + EnumChatFormatting.ITALIC + StatCollector.translateToLocal("item.itemCMSpoiledFood.tooltip"));
	}

    @Override
	public IIcon getIcon(ItemStack stack, int pass)
    {
    	return this.getIconIndex(stack);
    }

    @Override
	public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player)
    {
        --stack.stackSize;
        ItemStack stack1 = decode(stack);
        if(stack1 != null && stack1.getItem() instanceof ItemFood)
        {
        	ItemFood food = (ItemFood)stack1.getItem();
        	player.getFoodStats().addStats(food.func_150905_g(stack1) / 2, food.func_150906_h(stack1) / 2.0F);
        }
        world.playSoundAtEntity(player, "random.burp", 0.5F, world.rand.nextFloat() * 0.1F + 0.9F);
        if(!world.isRemote)
        {
        	player.addPotionEffect(new PotionEffect(Potion.poison.id, 600, world.rand.nextInt(3)));
        }
        return stack;
    }

	@Override
	@SideOnly(Side.CLIENT)
    public IIcon getIconIndex(ItemStack stack)
    {
		if(stack != null && stack.getItem() != null && stack.stackTagCompound != null)
		{
			ItemStack decoded = decode(stack);
			if(decoded != null && decoded.getItem() != null)
			{
				return decoded.getItem().getIconIndex(decoded);
			}
		}
		return null;
	}

	@Override
	public void registerIcons(IIconRegister iconRegister) {}

	public static ItemStack encode(Item item, int stackSize, int meta)
	{
		if(item != null && item instanceof ItemFood)
		{
			ItemStack ret = new ItemStack(ModItems.spoiledFood, stackSize, 0);
			NBTTagCompound tag = new NBTTagCompound();
			tag.setString(NBT_FOOD_ID, Item.itemRegistry.getNameForObject(item));
			tag.setShort(NBT_FOOD_META, (short)meta);
			ret.setTagCompound(tag);
			return ret;
		}
		return null;
	}

	public static ItemStack decode(ItemStack stack)
	{
		NBTTagCompound tag = stack.stackTagCompound;
		return new ItemStack((Item)Item.itemRegistry.getObject(tag.getString(NBT_FOOD_ID)), 1, tag.getShort(NBT_FOOD_META));
	}

}
