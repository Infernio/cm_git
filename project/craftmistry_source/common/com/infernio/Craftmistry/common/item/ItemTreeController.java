package com.infernio.Craftmistry.common.item;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.common.tile.TileForester;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemTreeController extends ItemCM
{
	public static final int EMTPY_TAG = Integer.MIN_VALUE;
	public static final String NBT_FORESTER_X = "ForesterX";
	public static final String NBT_FORESTER_Y = "ForesterY";
	public static final String NBT_FORESTER_Z = "ForesterZ";
	public static final String NBT_FORESTER_DIMENSION = "ForesterDimension";

	public ItemTreeController()
	{
		this.setUnlocalizedName(StringLib.ITEM_TREE_CONTROLLER_NAME);
		this.setMaxStackSize(1);
		this.setFull3D();
	}

	@Override
    public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float exactX, float exactY, float exactZ)
    {
		if(!world.isRemote)
		{
			TileEntity tile = world.getTileEntity(x, y, z);
			if(tile instanceof TileForester)
			{
				if(player.isSneaking())
				{
					if(getInteger(stack, NBT_FORESTER_DIMENSION) != world.provider.dimensionId || getInteger(stack, NBT_FORESTER_X) != x || getInteger(stack, NBT_FORESTER_Y) != y || getInteger(stack, NBT_FORESTER_Z) != z)
					{
						setInteger(stack, NBT_FORESTER_DIMENSION, world.provider.dimensionId);
						setInteger(stack, NBT_FORESTER_X, x);
						setInteger(stack, NBT_FORESTER_Y, y);
						setInteger(stack, NBT_FORESTER_Z, z);
						player.addChatMessage(new ChatComponentTranslation("item.itemCMTreeController.switched"));
					}
					else
					{
						player.addChatMessage(new ChatComponentTranslation("item.itemCMTreeController.isActiveForester"));
					}
				}
				else
				{
					player.addChatMessage(new ChatComponentTranslation("item.itemCMTreeController.sneak"));
				}
				return true;
			}
			
		}
		return true;
    }

	public static void checkNBT(ItemStack stack)
	{
		if(stack != null)
		{
			if(stack.stackTagCompound == null)
			{
				NBTTagCompound tag = new NBTTagCompound();
				tag.setInteger(NBT_FORESTER_DIMENSION, EMTPY_TAG);
				tag.setInteger(NBT_FORESTER_X, EMTPY_TAG);
				tag.setInteger(NBT_FORESTER_Y, EMTPY_TAG);
				tag.setInteger(NBT_FORESTER_Z, EMTPY_TAG);
				stack.stackTagCompound = tag;
				return;
			}
			NBTTagCompound tag = stack.stackTagCompound;
			if(!tag.hasKey(NBT_FORESTER_DIMENSION))
			{
				tag.setInteger(NBT_FORESTER_DIMENSION, EMTPY_TAG);
			}
			if(!tag.hasKey(NBT_FORESTER_X))
			{
				tag.setInteger(NBT_FORESTER_X, EMTPY_TAG);
			}
			if(!tag.hasKey(NBT_FORESTER_Y))
			{
				tag.setInteger(NBT_FORESTER_Y, EMTPY_TAG);
			}
			if(!tag.hasKey(NBT_FORESTER_Z))
			{
				tag.setInteger(NBT_FORESTER_Z, EMTPY_TAG);
			}
		}
	}

	public static boolean hasInteger(ItemStack stack, String key)
	{
		checkNBT(stack);
		return stack.stackTagCompound.hasKey(key) && stack.stackTagCompound.getInteger(key) != EMTPY_TAG;
	}

	public static void setInteger(ItemStack stack, String key, int value)
	{
		checkNBT(stack);
		NBTTagCompound tag = stack.stackTagCompound;
		if(tag.hasKey(key))
		{
			tag.removeTag(key);
		}
		tag.setInteger(key, value);
	}

	public static int getInteger(ItemStack stack, String key)
	{
		checkNBT(stack);
		return stack.stackTagCompound.getInteger(key);
	}
}
