package com.infernio.Craftmistry.common.item;

import ic2.api.item.ElectricItem;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraftforge.common.IShearable;

import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemNovaShears extends ItemNovaTool {

	public ItemNovaShears()
	{
		super(new Block[]{});
		this.setUnlocalizedName(StringLib.ITEM_NOVA_SHEARS_NAME);
	}

	@Override
	public boolean hasEffect(ItemStack stack)
	{
		return true;
	}

	@Override
	public EnumRarity getRarity(ItemStack stack)
	{
		return EnumRarity.rare;
	}

    @Override
	public boolean canHarvestBlock(Block block, ItemStack stack)
    {
        return Block.isEqualTo(block, Blocks.web) || Block.isEqualTo(block, Blocks.deadbush) || Block.isEqualTo(block, Blocks.wool) || Block.isEqualTo(block, Blocks.redstone_wire) || Block.isEqualTo(block, Blocks.tripwire) || block instanceof IShearable;
    }

	@Override
	public float getDigSpeed(ItemStack stack, Block block, int meta)
	{
		return Block.isEqualTo(block, Blocks.leaves) || Block.isEqualTo(block, Blocks.leaves2) || Block.isEqualTo(block, Blocks.deadbush) || Block.isEqualTo(block, Blocks.wool) || Block.isEqualTo(block, Blocks.web) || Block.isEqualTo(block, Blocks.tallgrass) || Block.isEqualTo(block, Blocks.vine) || Block.isEqualTo(block, Blocks.tripwire) || block instanceof IShearable ? 10.0F : 0.0F;
	}

    @Override
    public boolean itemInteractionForEntity(ItemStack stack, EntityPlayer player, EntityLivingBase entity)
    {
    	if(player.worldObj.isRemote || !CompatHandler.ic2Present || !ElectricItem.manager.canUse(stack, 100))
        {
            return false;
        }
        if(entity instanceof IShearable)
        {
            IShearable target = (IShearable)entity;
            if(target.isShearable(stack, entity.worldObj, (int)entity.posX, (int)entity.posY, (int)entity.posZ))
            {
                ArrayList<ItemStack> drops = target.onSheared(stack, entity.worldObj, (int)entity.posX, (int)entity.posY, (int)entity.posZ, EnchantmentHelper.getEnchantmentLevel(Enchantment.fortune.effectId, stack));
                Random rand = new Random();
                for(ItemStack drop : drops)
                {
                    EntityItem ent = entity.entityDropItem(drop, 1.0F);
                    ent.motionY += rand.nextFloat() * 0.05F;
                    ent.motionX += (rand.nextFloat() - rand.nextFloat()) * 0.1F;
                    ent.motionZ += (rand.nextFloat() - rand.nextFloat()) * 0.1F;
                }
                ElectricItem.manager.use(stack, 100, player);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onBlockStartBreak(ItemStack stack, int x, int y, int z, EntityPlayer player)
    {
        if(player.worldObj.isRemote || !CompatHandler.ic2Present || !ElectricItem.manager.canUse(stack, 100))
        {
            return false;
        }
        Block block = player.worldObj.getBlock(x, y, z);
        if(block instanceof IShearable)
        {
            IShearable target = (IShearable)block;
            if(target.isShearable(stack, player.worldObj, x, y, z))
            {
                ArrayList<ItemStack> drops = target.onSheared(stack, player.worldObj, x, y, z, EnchantmentHelper.getEnchantmentLevel(Enchantment.fortune.effectId, stack));
                Random rand = new Random();
                for(ItemStack drop : drops)
                {
                    float f = 0.7F;
                    double d  = (double)(rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    double d1 = (double)(rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    double d2 = (double)(rand.nextFloat() * f) + (double)(1.0F - f) * 0.5D;
                    EntityItem entityitem = new EntityItem(player.worldObj, (double)x + d, (double)y + d1, (double)z + d2, drop);
                    entityitem.delayBeforeCanPickup = 10;
                    player.worldObj.spawnEntityInWorld(entityitem);
                }
                ElectricItem.manager.use(stack, 100, player);
                player.addStat(StatList.mineBlockStatArray[Block.getIdFromBlock(block)], 1);
            }
        }
        return false;
    }
}
