package com.infernio.Craftmistry.common.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemRawMineral extends ItemCM {

	public static final String[] names = new String[]
	{
	"sulfur",
	"copper",
	"tin",
	"silver",
	"aluminium",
	"cobalt",
	"iron",
	"carbon",
	"gold",
	"halite",
	"compressedHalite",
	"beryl",
	"borax",
	"fluorite"
	};

	private IIcon[] icons;

	public ItemRawMineral()
	{
		this.setHasSubtypes(true);
		this.setUnlocalizedName(StringLib.ITEM_RAW_MINERAL_NAME);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < names.length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		list.add(StatCollector.translateToLocal("item.itemCMRawMineral." + names[stack.getItemDamage()] + ".name"));
	}

	@Override
    public IIcon getIconFromDamage(int meta)
    {
		if(this.icons == null)
		{
			return null;
		}
        return icons[MathHelper.clamp_int(meta, 0, icons.length - 1)];
    }

	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new IIcon[names.length];
		for(int i = 0; i < names.length; ++i)
		{
			icons[i] = iconRegister.registerIcon(StringLib.MODID + ":" + this.getUnlocalizedName().substring(5) + "." + names[i]);
		}
	}

}
