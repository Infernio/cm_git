package com.infernio.Craftmistry.common.item;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemNovaAxe extends ItemNovaTool {

	public ItemNovaAxe()
	{
		super(new Block[]{Blocks.planks, Blocks.bookshelf, Blocks.log, Blocks.log2, Blocks.chest, Blocks.pumpkin, Blocks.lit_pumpkin});
		this.setUnlocalizedName(StringLib.ITEM_NOVA_AXE_NAME);
	}

	@Override
	public boolean hasEffect(ItemStack stack)
	{
		return true;
	}

	@Override
	public EnumRarity getRarity(ItemStack stack)
	{
		return EnumRarity.rare;
	}

	@Override
	public float getDigSpeed(ItemStack stack, Block block, int meta)
	{
		return block.getMaterial() == Material.wood || block.getMaterial() == Material.plants || block.getMaterial() == Material.vine ? 10.0F : super.getDigSpeed(stack, block, meta);
	}

}
