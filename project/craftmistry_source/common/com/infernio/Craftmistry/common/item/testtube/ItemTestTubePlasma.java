package com.infernio.Craftmistry.common.item.testtube;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.chemistry.EnumElementType;
import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.common.chemistry.DecayHandler;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.chemistry.ThreatHandler;
import com.infernio.Craftmistry.common.entity.EntityAlkaliItem;
import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTestTubePlasma extends ItemCM {

	private IIcon[] icons;

	public ItemTestTubePlasma()
	{
		this.setHasSubtypes(true);
		this.setUnlocalizedName(StringLib.ITEM_TEST_TUBE_NAME);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		ArrayList<Integer> elements = Elements.getValidElements();
		for(int i = 0; i < elements.size(); ++i)
		{
			if(elements.get(i) > 0 && Arrays.asList(Elements.getPossibleStates(elements.get(i))).contains(EnumMatterState.PLASMA))
			{
				list.add(new ItemStack(item, 1, elements.get(i)));
			}
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		int meta = stack.getItemDamage();
		String name = Elements.getElementName(meta);
		list.add(StatCollector.translateToLocal("element." + name + ".name"));
		if(stack.getItemDamage() > 0)
		{
			list.add(StatCollector.translateToLocal("elements.symbol.name") + ": " + StatCollector.translateToLocal("element." + name + ".symbol"));
			list.add(StatCollector.translateToLocal("elements.state.testTubePlasma.name"));
		}
	}

	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int slot, boolean b)
	{
		if(entity instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)entity;
			int atomNumber = stack.getItemDamage();
			if(Elements.isElementRadioactive(atomNumber))
			{
				DecayHandler.decay(stack, player, world, atomNumber, stack, slot, itemRand);
			}
			if(atomNumber > 0)
			{
				ThreatHandler.updateElement(EnumMatterState.PLASMA, slot, player);
			}
		}
	}

	@Override
    public IIcon getIconFromDamage(int meta)
    {
		if(this.icons == null)
		{
			return null;
		}
        return icons[meta];
    }

	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) 
	{
		ArrayList<Integer> elements = Elements.getValidElements();
		icons = new IIcon[256];
		for(int i = 0; i < icons.length; i++)
		{
			if(i < elements.size())
			{
				int meta = elements.get(i);
				if(elements.get(i) > 0 && Arrays.asList(Elements.getPossibleStates(elements.get(i))).contains(EnumMatterState.PLASMA))
				{
					icons[meta] = iconRegister.registerIcon(Elements.getElementModId(meta) + ":" + ItemTestTubeSolid.DEF_NAME + Elements.getNormalName(meta));
				}
			}
		}
	}

    @Override
	public boolean hasCustomEntity(ItemStack stack)
    {
    	EnumElementType type = Elements.getType(stack.getItemDamage());
    	if(type != null && type.equals(EnumElementType.ALKALI_METAL) || type.equals(EnumElementType.ALKALINE_EARTH_METAL))
    	{
    		return true;
    	}
        return false;
    }

    @Override
	public Entity createEntity(World world, Entity location, ItemStack stack)
    {
    	EnumElementType type = Elements.getType(stack.getItemDamage());
    	if(type != null && type.equals(EnumElementType.ALKALI_METAL) || type.equals(EnumElementType.ALKALINE_EARTH_METAL))
    	{
    		EntityAlkaliItem ret = new EntityAlkaliItem(world, location.posX, location.posY, location.posZ, stack);
    		ret.motionX = location.motionX;
    		ret.motionY = location.motionY;
    		ret.motionZ = location.motionZ;
    		ret.delayBeforeCanPickup = 10;
    		return ret;
    	}
        return null;
    }

}
