package com.infernio.Craftmistry.common.item.testtube;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.tile.TileTestTubeRack;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemTestTubeRack extends ItemBlock {

	public ItemTestTubeRack(Block block)
	{
		super(block);
		this.setMaxStackSize(1);
		this.setCreativeTab(Craftmistry.cmTab);
	}
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
    @Override
    public void getSubItems(Item item, CreativeTabs tab, List list)
    {
        ItemStack stack = new ItemStack(item, 1, 0);
        stack.setTagCompound(new NBTTagCompound());
        list.add(stack);
    }
    
    @Override
    public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int metadata)
    {
    	if(!world.setBlock(x, y, z, this.field_150939_a, metadata, 3))
    	{
    		return false;
    	}
    	if(world.getBlock(x, y, z) == this.field_150939_a)
    	{
    		this.field_150939_a.onBlockPlacedBy(world, x, y, z, player, stack);
    		NBTTagCompound tag = stack.getTagCompound();
    		TileEntity tile = world.getTileEntity(x, y, z);
    		if(tile != null && tile instanceof TileTestTubeRack && stack.getTagCompound() != null)
    		{
    			TileTestTubeRack tile2 = (TileTestTubeRack)tile;
    			for(int i = 0; i < tile2.getSizeInventory(); ++i)
    			{
    				if(tag.getInteger("testTube_" + i) > 0)
    				{
    					tile2.setInventorySlotContents(i, new ItemStack((Item)Item.itemRegistry.getObject(tag.getString("testTubeState_" + i)), 1, tag.getInteger("testTube_" + i) - 1));
    				}
    			}
    		}
    		this.field_150939_a.onPostBlockPlaced(world, x, y, z, metadata);
    	}
    	return true;
    }

}
