package com.infernio.Craftmistry.common.item;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

import com.infernio.Craftmistry.api.chemistry.EnumThreat;
import com.infernio.Craftmistry.api.item.ISpecialInfo;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.math.ArrayUtils;

public class CompoundInformation implements ISpecialInfo {

	@Override
	public String[] getSpecialInfo(ItemStack stack, int page)
	{
		int meta = stack.getItemDamage();
		if(page == 0)
		{
			String[] ret = new String[2];
			ret[0] = StatCollector.translateToLocal("compounds.name.name") + ":";
			ret[1] = "%a" + StatCollector.translateToLocal("compound." + Elements.getCompoundName(meta) + ".name");
			return ret;
		}
		if(page == 1)
		{
			String formula = Elements.getCompoundFormula(meta);
			//TODO Break formula into pieces if it's too long
			//if(formula.length() > (Highest possible goes here))
			String[] ret = new String[2];
			ret[0] = StatCollector.translateToLocal("compounds.formula.name") + ":";
			ret[1] = "%a" + formula;
			return ret;
		}
		if(page == 2)
		{
			String[] ret = new String[4];
			ret[0] = StatCollector.translateToLocal("elements.testTubeHelp.line1");
			ret[1] = StatCollector.translateToLocal("elements.testTubeHelp.line2");
			ret[2] = StatCollector.translateToLocal("elements.testTubeHelp.line3");
			ret[3] = StatCollector.translateToLocal("elements.testTubeHelp.line4");
			return ret;
		}
		return new String[]{};
	}

	@Override
	public void addToScreen(ItemStack stack, FontRenderer fontRenderer, int x, int y, int page, int guiLeft, int guiTop, int guiHeight, int guiWidth)
	{
		if(GUIUtils.intersects(70, 10, x, y, 90, 65, guiLeft, guiTop))
		{
			EnumThreat[] threats = Elements.getCompoundThreats(stack.getItemDamage());
			if(threats != null)
			{
				EnumThreat threat = ArrayUtils.findObjectSkipNull(page - 3, threats);
				if(threat != null)
				{
					ArrayList<String> tooltip = new ArrayList<String>();
					tooltip.add(StatCollector.translateToLocal("elements.threats." + threat.getName() + ".name"));
					GUIUtils.drawHoveringText(tooltip, x, y, fontRenderer, guiHeight, guiWidth);
				}
			}
		}
	}

	@Override
	public boolean printDefaultInfo(ItemStack stack)
	{
		return false;
	}

	@Override
	public void addSpecialEffects(ItemStack stack, Minecraft mc, FontRenderer fontRenderer, RenderItem itemRenderer, int page, float zLevel, int guiLeft, int guiTop)
	{
		if(page > 2)
		{
			EnumThreat[] threats = Elements.getCompoundThreats(stack.getItemDamage());
			if(threats != null)
			{
				EnumThreat threat = ArrayUtils.findObjectSkipNull(page - 3, threats);
				if(threat != null)
				{
					int ordinal = threat.ordinal();
					mc.renderEngine.bindTexture(TextureMap.locationItemsTexture);
					if(ConfigSettings.FANCY_GRAPHICS)
					{
						GUIUtils.setZLevel(zLevel);
						GUIUtils.drawTexturedModelRectFromIcon(guiLeft + 84, guiTop + 10, EnumThreat.icons[ordinal], 64, 64);
					}
					else
					{
						GUIUtils.setZLevel(zLevel);
						GUIUtils.drawTexturedModelRectFromIcon(guiLeft + 101, guiTop + 28, EnumThreat.icons[ordinal], 32, 32);
					}
				}
			}
		}
	}

	@Override
	public int getNumberPages(ItemStack stack)
	{
		if(ItemHelper.isTestTube(stack))
		{
			EnumThreat[] threats = Elements.getCompoundThreats(stack.getItemDamage());
			if(threats != null)
			{
				return 2 + ArrayUtils.lengthExcludingNull(threats);
			}
		}
		return 2;
	}

}
