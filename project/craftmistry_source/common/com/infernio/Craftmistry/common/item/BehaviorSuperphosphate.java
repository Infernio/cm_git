package com.infernio.Craftmistry.common.item;

import net.minecraft.block.BlockDispenser;
import net.minecraft.dispenser.BehaviorDefaultDispenseItem;
import net.minecraft.dispenser.IBlockSource;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public final class BehaviorSuperphosphate extends BehaviorDefaultDispenseItem
{
    private boolean field_96461_b = true;

    @Override
	protected ItemStack dispenseStack(IBlockSource blockSource, ItemStack stack)
    {
        EnumFacing enumfacing = BlockDispenser.func_149937_b(blockSource.getBlockMetadata());
        World world = blockSource.getWorld();
        int x = blockSource.getXInt() + enumfacing.getFrontOffsetX();
        int y = blockSource.getYInt() + enumfacing.getFrontOffsetY();
        int z = blockSource.getZInt() + enumfacing.getFrontOffsetZ();
        if(ItemSuperPhosphate.apply(world, x, y, z))
        {
            --stack.stackSize;
            if(!world.isRemote)
            {
                world.playAuxSFX(2005, x, y, z, 0);
            }
        }
        else
        {
            this.field_96461_b = false;
        }
        return stack;
    }

    @Override
	protected void playDispenseSound(IBlockSource blockSource)
    {
        if (this.field_96461_b)
        {
            blockSource.getWorld().playAuxSFX(1000, blockSource.getXInt(), blockSource.getYInt(), blockSource.getZInt(), 0);
        }
        else
        {
            blockSource.getWorld().playAuxSFX(1001, blockSource.getXInt(), blockSource.getYInt(), blockSource.getZInt(), 0);
        }
    }
}
