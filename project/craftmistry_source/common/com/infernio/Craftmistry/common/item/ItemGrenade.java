package com.infernio.Craftmistry.common.item;

import java.util.List;

import net.minecraft.block.BlockDispenser;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.entity.EntityGrenade;
import com.infernio.Craftmistry.common.entity.EntityGrenade.EnumGrenade;
import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.InfernoCore;

public class ItemGrenade extends ItemCM {

	public static final int MAX_META = EnumGrenade.values().length;

	public ItemGrenade()
	{
		this.setHasSubtypes(true);
		this.setUnlocalizedName(StringLib.ITEM_GRENADE_NAME);
		BlockDispenser.dispenseBehaviorRegistry.putObject(this, new BehaviorGrenade());
	}

    @Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player)
    {
        if(!player.capabilities.isCreativeMode)
        {
            --stack.stackSize;
        }
        world.playSoundAtEntity(player, "random.bow", InfernoCore.proxy.getSoundVolume(SoundCategory.MASTER), 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
        if(!world.isRemote)
        {
        	EntityGrenade grenade = new EntityGrenade(world, player);
        	grenade.grenade = getTypeFromMeta(stack.getItemDamage());
        	world.spawnEntityInWorld(grenade);
        }
        return stack;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
    {
        for(int i = 0; i < MAX_META; ++i)
        {
        	list.add(new ItemStack(item, 1, i));
        }
    }

    @Override
    public String getUnlocalizedName(ItemStack stack)
    {
        return "item." + StringLib.ITEM_GRENADE_NAME + "." + getTypeFromMeta(stack.getItemDamage()).name;
    }

    private EnumGrenade getTypeFromMeta(int meta)
    {
    	return meta >= MAX_META ? EnumGrenade.NORMAL : EnumGrenade.values()[meta];
    }

}
