package com.infernio.Craftmistry.common.item;

import java.util.List;
import java.util.Locale;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemCMIngot extends ItemCM {

	public static final String[] names = new String[]
	{
		"Copper",
		"Tin",
		"Silver",
		"Aluminium"
	};

	private IIcon[] icons;

	public ItemCMIngot()
	{
		this.setHasSubtypes(true);
		this.setUnlocalizedName(StringLib.ITEM_CRAFTMISTRY_INGOT_NAME);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < names.length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
    public IIcon getIconFromDamage(int meta)
    {
		if(this.icons == null)
		{
			return null;
		}
        return icons[MathHelper.clamp_int(meta, 0, names.length - 1)];
    }

	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		icons = new IIcon[names.length];
		for(int i = 0; i < names.length; ++i)
		{
			icons[i] = iconRegister.registerIcon(StringLib.MODID + ":" + this.getUnlocalizedName().substring(5) + "." + names[i].toLowerCase(Locale.ENGLISH));
		}
	}

    @Override
	public String getUnlocalizedName(ItemStack stack)
    {
    	return super.getUnlocalizedName(stack) + "." + names[MathHelper.clamp_int(stack.getItemDamage(), 0, names.length - 1)].toLowerCase(Locale.ENGLISH);
    }

}
