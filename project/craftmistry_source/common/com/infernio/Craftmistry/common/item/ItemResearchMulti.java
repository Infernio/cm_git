package com.infernio.Craftmistry.common.item;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.block.ModBlocks;

public class ItemResearchMulti extends ItemBlock {

	public static final int MAX_META = 5;

	public ItemResearchMulti()
	{
		super(ModBlocks.research);
        this.setMaxDamage(0);
		this.setCreativeTab(Craftmistry.cmTab);
		this.setHasSubtypes(true);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(int meta = 0; meta < MAX_META; ++meta)
		{
			list.add(new ItemStack(item, 1, meta));
		}
	}

	@Override
	public int getMetadata(int meta)
	{
		return meta;
	}

}
