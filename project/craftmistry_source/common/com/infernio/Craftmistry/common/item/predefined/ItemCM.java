package com.infernio.Craftmistry.common.item.predefined;

import java.util.Locale;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemCM extends Item {

	public ItemCM()
	{
		this.setCreativeTab(Craftmistry.cmTab);
	}

	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) 
	{
        this.itemIcon = iconRegister.registerIcon(StringLib.MODID.toLowerCase(Locale.ENGLISH) + ":" + this.getUnlocalizedName().substring(5));
    }

}
