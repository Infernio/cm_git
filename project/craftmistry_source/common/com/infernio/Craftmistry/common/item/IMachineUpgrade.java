package com.infernio.Craftmistry.common.item;

import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public interface IMachineUpgrade {

	public boolean isValid(TileEntity machine, ItemStack stack);

}
