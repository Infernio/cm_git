package com.infernio.Craftmistry.common.item;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.gui.Tooltip;
import com.infernio.InfernoCore.key.IKeyTrigger;
import com.infernio.InfernoCore.key.KeyRegistry;
import com.infernio.InfernoCore.util.PlayerUtils;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemPocketDimension extends ItemCM implements IKeyTrigger {

	public static final String NBT_STORED_ID = "StoredID";
	public static final String NBT_STORED_META = "StoredMeta";
	public static final String NBT_STORED_AMOUNT = "StoredAmount";
	public static final String DEFAULT_STORED_ID = Block.blockRegistry.getNameForObject(Blocks.stone);
	public static final int DEFAULT_STORED_META = 0;
	public static final int DEFAULT_STORED_AMOUNT = 0;
	public static final int MAX_STORAGE_AMOUNT = 4096;
	public static final int ITEM_DAMAGE_SCALE = 64;

	public ItemPocketDimension()
	{
		this.setUnlocalizedName(StringLib.ITEM_POCKET_DIMENSION_NAME);
		this.setMaxDamage(scaleDamage(MAX_STORAGE_AMOUNT + 1));
		this.setMaxStackSize(1);
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			KeyRegistry.registerKeyTrigger(StringLib.KEY_POCKET_DIMENSION_GIVE_STACK_NAME, this);
			KeyRegistry.registerKeyTrigger(StringLib.KEY_POCKET_DIMENSION_CLEAR_STACK_NAME, this);
		}
		MinecraftForge.EVENT_BUS.register(this);
	}

	@SubscribeEvent
	public void onItemPickup(EntityItemPickupEvent event)
	{
		if(event.item != null && event.item.delayBeforeCanPickup == 0)
		{
			ItemStack stack = event.item.getEntityItem();
			if(stack != null && stack.getItem() != null && stack.stackTagCompound == null)
			{
				ItemStack pd = getPocketDimensionFor(event.entityPlayer, stack.getItem(), stack.getItemDamage());
				if(pd != null)
				{
					int current = getStoredAmount(pd);
					int total = current + stack.stackSize;
					if(total > MAX_STORAGE_AMOUNT)
					{
						stack.stackSize = total - MAX_STORAGE_AMOUNT;
						total = MAX_STORAGE_AMOUNT;
					}
					else
					{
						event.item.setDead();
						event.setCanceled(true);
					}
					setStoredAmount(pd, total);
				}
			}
		}
	}

	@Override
	public void onKeyPressed(String description, int keyCode, String category, EntityPlayer player)
	{
		if(player == null || player.inventory == null)
		{
			return;
		}
		if(keyCode == ConfigSettings.KEY_POCKET_DIMENSION_GIVE_STACK)
		{
			ItemStack pd = getFirstFilledPocketDimensionFor(player);
			if(pd != null)
			{
				int amount = getStoredAmount(pd);
				if(amount < 64)
				{
					ItemStack current = player.getCurrentEquippedItem();
					String id = getStoredID(pd);
					int meta = getStoredMeta(pd);
					Item item = (Item)Item.itemRegistry.getObject(id);
					if(current == null || !Item.itemRegistry.getNameForObject(current.getItem()).equals(id) || current.getItemDamage() != meta)
					{
						player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(item, amount, meta));
						if(current != null && current.getItem() != null)
						{
							PlayerUtils.giveItem(current.copy(), player.worldObj, player);
						}
						setStoredAmount(pd, DEFAULT_STORED_AMOUNT);
					}
					else if(Item.itemRegistry.getNameForObject(current.getItem()).equals(id) && current.getItemDamage() == meta && current.stackSize < current.getMaxStackSize())
					{
						int finalAmount = amount + current.stackSize;
						if(finalAmount <= current.getMaxStackSize())
						{
							player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(item, finalAmount, meta));
							setStoredAmount(pd, DEFAULT_STORED_AMOUNT);
						}
						else
						{
							player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(item, current.getMaxStackSize(), meta));
							setStoredAmount(pd, finalAmount - current.getMaxStackSize());
						}
					}
				}
				else
				{
					ItemStack current = player.getCurrentEquippedItem();
					String id = getStoredID(pd);
					int meta = getStoredMeta(pd);
					Item item = (Item)Item.itemRegistry.getObject(id);
					if(current == null || !Item.itemRegistry.getNameForObject(current.getItem()).equals(id) || current.getItemDamage() != meta)
					{
						int max = item == null ? 64 : item.getItemStackLimit(new ItemStack(item, 1, meta));
						player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(item, max, meta));
						if(current != null && current.getItem() != null)
						{
							PlayerUtils.giveItem(current.copy(), player.worldObj, player);
						}
						setStoredAmount(pd, amount - max);
					}
					else if(Item.itemRegistry.getNameForObject(current.getItem()).equals(id) && current.getItemDamage() == meta && current.stackSize < current.getMaxStackSize())
					{
						int finalAmount = amount + current.stackSize;
						if(finalAmount <= current.getMaxStackSize())
						{
							player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(item, finalAmount, meta));
							setStoredAmount(pd, amount - current.getMaxStackSize());
						}
						else
						{
							player.inventory.setInventorySlotContents(player.inventory.currentItem, new ItemStack(item, current.getMaxStackSize(), meta));
							setStoredAmount(pd, finalAmount - current.getMaxStackSize());
						}
					}
				}
			}
			else
			{
				player.addChatMessage(new ChatComponentText(Tooltip.resolveColors(StatCollector.translateToLocal("item.itemCMPocketDimension.noFilledPD"))));
			}
		}
		else if(keyCode == ConfigSettings.KEY_POCKET_DIMENSION_CLEAR_STACK)
		{
			ItemStack current = player.getCurrentEquippedItem();
			if(current != null && current.getItem() != null)
			{
				ItemStack pd = getPocketDimensionFor(player, current.getItem(), current.getItemDamage());
				if(pd != null)
				{
					int amount = getStoredAmount(pd);
					int total = amount + current.stackSize;
					if(total > MAX_STORAGE_AMOUNT)
					{
						current.stackSize = total - MAX_STORAGE_AMOUNT;
						total = MAX_STORAGE_AMOUNT;
					}
					else
					{
						player.inventory.setInventorySlotContents(player.inventory.currentItem, null);
					}
					setStoredAmount(pd, total);
				}
				else
				{
					player.addChatMessage(new ChatComponentText(Tooltip.resolveColors(StatCollector.translateToLocal("item.itemCMPocketDimension.noMatchingPD"))));
				}
			}
		}
	}

    @Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player)
    {
    	MovingObjectPosition mop = PlayerUtils.getFromPlayer(world, player, false);
    	if(mop != null)
    	{
    		int amount = getStoredAmount(stack);
    		if(amount > 0)
    		{
    			String id = getStoredID(stack);
    			Item item = (Item)Item.itemRegistry.getObject(id);
    			if(item != null)
    			{
	    			int meta = getStoredMeta(stack);
	    			ItemStack stack1 = new ItemStack(item, 1, meta);
	    			int finalAmount = MathHelper.clamp_int(amount, 1, item.getItemStackLimit(stack1));
	    			if(player.isSneaking())
	    			{
	    				finalAmount = 1;
	    			}
	    			if(!world.isRemote)
	    			{
	    				EntityItem item1 = new EntityItem(player.worldObj, mop.blockX + 0.5D, mop.blockY + 1, mop.blockZ + 0.5D, new ItemStack(item, finalAmount, meta));
	    				item1.onUpdate();
	    				item1.delayBeforeCanPickup = 10;
	    				item1.yOffset = 0;
	    				item1.motionX = 0.0D;
	    				item1.motionY = 0.0D;
	    				item1.motionZ = 0.0D;
	    				world.spawnEntityInWorld(item1);
	    			}
	    			setStoredAmount(stack, amount - finalAmount);
	    			player.swingItem();
    			}
    		}
    	}
    	else
    	{
    		int amount = getStoredAmount(stack);
    		if(amount > 0)
    		{
    			String id = getStoredID(stack);
    			Item item = (Item)Item.itemRegistry.getObject(id);
    			if(item != null)
    			{
	    			int meta = getStoredMeta(stack);
	    			ItemStack stack1 = new ItemStack(item, 1, meta);
	    			int finalAmount = MathHelper.clamp_int(amount, 1, item.getItemStackLimit(stack1));
	    			if(player.isSneaking())
	    			{
	    				finalAmount = 1;
	    			}
	    			PlayerUtils.giveItem(new ItemStack(item, finalAmount, meta), player.worldObj, player);
	    			setStoredAmount(stack, amount - finalAmount);
	    			player.swingItem();
    			}
    		}
    	}
    	return stack;
    }

	@Override
	public int getDamage(ItemStack stack)
	{
		if(!checkNBT(stack))
		{
			return DEFAULT_STORED_AMOUNT;
		}
		return scaleDamage(stack.stackTagCompound.getInteger(NBT_STORED_AMOUNT));
	}

	@Override
    public int getDisplayDamage(ItemStack stack)
    {
    	return this.getDamage(stack);
    }

	@SideOnly(Side.CLIENT)
	@Override
	public EnumRarity getRarity(ItemStack stack)
	{
		return EnumRarity.rare;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		String id = ItemPocketDimension.getStoredID(stack);
		int meta = ItemPocketDimension.getStoredMeta(stack);
		Item item = (Item)Item.itemRegistry.getObject(id);
		list.add(item.getItemStackDisplayName(new ItemStack(item, 1, meta)));
		list.add(StatCollector.translateToLocal("item.itemCMPocketDimension.metaStored") + " " + meta);
		list.add(StatCollector.translateToLocal("item.itemCMPocketDimension.amountStored") + " " + ItemPocketDimension.getStoredAmount(stack));
	}

	public static int scaleDamage(int damage)
	{
		return MAX_STORAGE_AMOUNT / ITEM_DAMAGE_SCALE - MathHelper.floor_double((double)damage / (double)ITEM_DAMAGE_SCALE);
	}

	public static boolean checkNBT(ItemStack pd)
	{
		NBTTagCompound tag = pd.stackTagCompound;
		boolean ret = true;
		if(tag != null)
		{
			if(!tag.hasKey(NBT_STORED_ID))
			{
				tag.setString(NBT_STORED_ID, DEFAULT_STORED_ID);
				ret = false;
			}
			if(!tag.hasKey(NBT_STORED_META))
			{
				tag.setInteger(NBT_STORED_META, DEFAULT_STORED_META);
				ret = false;
			}
			if(!tag.hasKey(NBT_STORED_AMOUNT))
			{
				tag.setInteger(NBT_STORED_AMOUNT, DEFAULT_STORED_AMOUNT);
				ret = false;
			}
		}
		else
		{
			ret = false;
			tag = new NBTTagCompound();
			pd.setTagCompound(tag);
			tag.setString(NBT_STORED_ID, DEFAULT_STORED_ID);
			tag.setInteger(NBT_STORED_META, DEFAULT_STORED_META);
			tag.setInteger(NBT_STORED_AMOUNT, DEFAULT_STORED_AMOUNT);
		}
		return ret;
	}

	public static String getStoredID(ItemStack pd)
	{
		if(!checkNBT(pd))
		{
			return DEFAULT_STORED_ID;
		}
		return pd.stackTagCompound.getString(NBT_STORED_ID);
	}

	public static int getStoredMeta(ItemStack pd)
	{
		if(!checkNBT(pd))
		{
			return DEFAULT_STORED_META;
		}
		return pd.stackTagCompound.getInteger(NBT_STORED_META);
	}

	public static int getStoredAmount(ItemStack pd)
	{
		if(!checkNBT(pd))
		{
			return DEFAULT_STORED_AMOUNT;
		}
		return pd.stackTagCompound.getInteger(NBT_STORED_AMOUNT);
	}

	public static void setStoredID(ItemStack pd, String id)
	{
		if(getStoredID(pd) != id)
		{
			pd.stackTagCompound.setString(NBT_STORED_ID, id);
		}
	}

	public static void setStoredMeta(ItemStack pd, int meta)
	{
		if(getStoredMeta(pd) != meta)
		{
			pd.stackTagCompound.setInteger(NBT_STORED_META, meta);
		}
	}

	public static void setStoredAmount(ItemStack pd, int amount)
	{
		if(checkNBT(pd))
		{
			pd.stackTagCompound.setInteger(NBT_STORED_AMOUNT, amount);
		}
	}

	public static ItemStack getPocketDimensionFor(EntityPlayer player, Item item, int meta)
	{
		for(int i = 0; i < player.inventory.getSizeInventory(); ++i)
		{
			ItemStack stack = player.inventory.getStackInSlot(i);
			if(stack != null && stack.getItem() != null && stack.getItem() == ModItems.pocketDimension && getStoredAmount(stack) < MAX_STORAGE_AMOUNT)
			{
				if(getStoredID(stack).equals(Item.itemRegistry.getNameForObject(item)) && meta == getStoredMeta(stack))
				{
					return stack;
				}
			}
		}
		return null;
	}

	public static ArrayList<ItemStack> getPocketDimensionsFor(EntityPlayer player)
	{
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		for(int i = 0; i < player.inventory.getSizeInventory(); ++i)
		{
			ItemStack stack = player.inventory.getStackInSlot(i);
			if(stack != null && stack.getItem() != null && stack.getItem() == ModItems.pocketDimension && getStoredAmount(stack) < MAX_STORAGE_AMOUNT)
			{
				ret.add(stack);
			}
		}
		return ret;
	}

	public static ItemStack getFirstPocketDimensionFor(EntityPlayer player)
	{
		for(int i = 0; i < player.inventory.getSizeInventory(); ++i)
		{
			ItemStack stack = player.inventory.getStackInSlot(i);
			if(stack != null && stack.getItem() != null && stack.getItem() == ModItems.pocketDimension && getStoredAmount(stack) < MAX_STORAGE_AMOUNT)
			{
				return stack;
			}
		}
		return null;
	}

	public static ItemStack getFirstFilledPocketDimensionFor(EntityPlayer player)
	{
		for(int i = 0; i < player.inventory.getSizeInventory(); ++i)
		{
			ItemStack stack = player.inventory.getStackInSlot(i);
			if(stack != null && stack.getItem() != null && stack.getItem() == ModItems.pocketDimension && getStoredAmount(stack) < MAX_STORAGE_AMOUNT && getStoredAmount(stack) > 0)
			{
				return stack;
			}
		}
		return null;
	}

}
