package com.infernio.Craftmistry.common.item;

import ic2.api.item.ElectricItem;
import ic2.api.item.IElectricItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.InfernoCore.math.Vector;

public class ItemNovaTool extends ItemCM implements IElectricItem {

	public static final int MAX_CHARGE = 1000000;
	public static final int ENERGY_DESTROY_BLOCK = 250;

	public List<Block> effectiveBlocks;
    private int attemps;
    private HashMap<Vector, Boolean> coords;

	public ItemNovaTool(Block[] effectiveBlocks)
	{
		this.effectiveBlocks = Arrays.asList(effectiveBlocks);
		this.setMaxDamage(27);
		this.setMaxStackSize(1);
		this.setFull3D();
	}

	@Override
    public boolean onBlockDestroyed(ItemStack stack, World world, Block block, int x, int y, int z, EntityLivingBase entity)
    {
		if(world.isRemote)// || !CompatHandler.ic2Present)
		{
			return false;
		}
		if(entity.isSneaking())
		{
			if(!ElectricItem.manager.canUse(stack, ENERGY_DESTROY_BLOCK))
			{
				return true;
			}
			ElectricItem.manager.use(stack, ENERGY_DESTROY_BLOCK, entity);
			return false;
		}
		if(stack.stackTagCompound == null)
		{
			stack.stackTagCompound = new NBTTagCompound();
			return true;
		}
		this.coords = new HashMap<Vector, Boolean>();
		this.attemps = 0;
		int max = 25;//MathHelper.clamp_int(stack.stackTagCompound.getInteger("charge") / ENERGY_DESTROY_BLOCK, 0, 25);
		if(max > 0)
		{
			if(block != null && this.getDigSpeed(stack, block, stack.getItemDamage()) == 10.0F)
			{
				int meta = world.getBlockMetadata(x, y, z);
				if(this.attemps >= max)
				{
					return false;
				}
				if(this.attemps < max)
				{
					if(Block.isEqualTo(world.getBlock(x + 1, y, z), block) && world.getBlockMetadata(x + 1, y, z) == meta)
					{
						Vector coord = new Vector(x + 1, y, z);
						if(!this.coords.containsKey(coord))
						{
							++this.attemps;
							this.coords.put(coord, Boolean.TRUE);
						}
					}
				}
				if(this.attemps < max)
				{
					if(Block.isEqualTo(world.getBlock(x - 1, y, z), block) && world.getBlockMetadata(x - 1, y, z) == meta)
					{
						Vector coord = new Vector(x - 1, y, z);
						if(!this.coords.containsKey(coord))
						{
							++this.attemps;
							this.coords.put(coord, Boolean.TRUE);
						}
					}
				}
				if(this.attemps < max)
				{
					if(Block.isEqualTo(world.getBlock(x, y + 1, z), block) && world.getBlockMetadata(x, y + 1, z) == meta)
					{
						Vector coord = new Vector(x, y + 1, z);
						if(!this.coords.containsKey(coord))
						{
							++this.attemps;
							this.coords.put(coord, Boolean.TRUE);
						}
					}
				}
				if(this.attemps < max)
				{
					if(Block.isEqualTo(world.getBlock(x, y - 1, z), block) && world.getBlockMetadata(x, y - 1, z) == meta)
					{
						Vector coord = new Vector(x, y - 1, z);
						if(!this.coords.containsKey(coord))
						{
							++this.attemps;
							this.coords.put(coord, Boolean.TRUE);
						}
					}
				}
				if(this.attemps < max)
				{
					if(Block.isEqualTo(world.getBlock(x, y, z + 1), block) && world.getBlockMetadata(x, y, z + 1) == meta)
					{
						Vector coord = new Vector(x, y, z + 1);
						if(!this.coords.containsKey(coord))
						{
							++this.attemps;
							this.coords.put(coord, Boolean.TRUE);
						}
					}
				}
				if(this.attemps < max)
				{
					if(Block.isEqualTo(world.getBlock(x, y, z - 1), block) && world.getBlockMetadata(x, y, z - 1) == meta)
					{
						Vector coord = new Vector(x, y, z - 1);
						if(!this.coords.containsKey(coord))
						{
							++this.attemps;
							this.coords.put(coord, Boolean.TRUE);
						}
					}
				}
				if(this.attemps < max)
				{
					List<Vector> entries = new ArrayList<Vector>();
					for(Entry<Vector, Boolean> entry : this.coords.entrySet())
					{
						entries.add(entry.getKey());
					}
					for(int i = 0; i < entries.size(); ++i)
					{
						Vector coord = entries.get(i);
						if(coord != null && this.attemps < max)
						{
							this.searchEnvironment(coord.x, coord.y, coord.z, world, max, block, meta);
						}
					}
				}
				for(Entry<Vector, Boolean> entry : this.coords.entrySet())
				{
					if(ElectricItem.manager.canUse(stack, ENERGY_DESTROY_BLOCK))
					{
						Vector coord = entry.getKey();
						ElectricItem.manager.use(stack, ENERGY_DESTROY_BLOCK, entity);
						block.dropBlockAsItem(world, coord.x, coord.y, coord.z, meta, EnchantmentHelper.getEnchantmentLevel(Enchantment.fortune.effectId, stack));
						world.setBlockToAir(coord.x, coord.y, coord.z);
					}
				}
			}
		}
		return false;
    }

	private void searchEnvironment(int x, int y, int z, World world, int max, Block block, int meta)
	{
		if(this.attemps >= max)
		{
			return;
		}
		if(this.attemps < max)
		{
			if(Block.isEqualTo(world.getBlock(x + 1, y, z), block) && world.getBlockMetadata(x + 1, y, z) == meta)
			{
				Vector coord = new Vector(x + 1, y, z);
				if(!this.coords.containsKey(coord))
				{
					++this.attemps;
					this.coords.put(coord, Boolean.FALSE);
				}
			}
		}
		if(this.attemps < max)
		{
			if(Block.isEqualTo(world.getBlock(x - 1, y, z), block) && world.getBlockMetadata(x - 1, y, z) == meta)
			{
				Vector coord = new Vector(x - 1, y, z);
				if(!this.coords.containsKey(coord))
				{
					++this.attemps;
					this.coords.put(coord, Boolean.FALSE);
				}
			}
		}
		if(this.attemps < max)
		{
			if(Block.isEqualTo(world.getBlock(x, y + 1, z), block) && world.getBlockMetadata(x, y + 1, z) == meta)
			{
				Vector coord = new Vector(x, y + 1, z);
				if(!this.coords.containsKey(coord))
				{
					++this.attemps;
					this.coords.put(coord, Boolean.FALSE);
				}
			}
		}
		if(this.attemps < max)
		{
			if(Block.isEqualTo(world.getBlock(x, y - 1, z), block) && world.getBlockMetadata(x, y - 1, z) == meta)
			{
				Vector coord = new Vector(x, y - 1, z);
				if(!this.coords.containsKey(coord))
				{
					++this.attemps;
					this.coords.put(coord, Boolean.FALSE);
				}
			}
		}
		if(this.attemps < max)
		{
			if(Block.isEqualTo(world.getBlock(x, y, z + 1), block) && world.getBlockMetadata(x, y, z + 1) == meta)
			{
				Vector coord = new Vector(x, y, z + 1);
				if(!this.coords.containsKey(coord))
				{
					++this.attemps;
					this.coords.put(coord, Boolean.FALSE);
				}
			}
		}
		if(this.attemps < max)
		{
			if(Block.isEqualTo(world.getBlock(x, y, z - 1), block) && world.getBlockMetadata(x, y, z - 1) == meta)
			{
				Vector coord = new Vector(x, y, z - 1);
				if(!this.coords.containsKey(coord))
				{
					++this.attemps;
					this.coords.put(coord, Boolean.FALSE);
				}
			}
		}
		if(this.attemps >= max)
		{
			return;
		}
		if(this.attemps < max)
		{
			List<Vector> entries = new ArrayList<Vector>();
			@SuppressWarnings("unchecked")
			HashMap<Vector, Boolean> newMap = (HashMap<Vector, Boolean>)this.coords.clone();
			for(Entry<Vector, Boolean> entry : this.coords.entrySet())
			{
				if(!entry.getValue().booleanValue())
				{
					entries.add(entry.getKey());
				}
				newMap.put(entry.getKey(), Boolean.TRUE);
			}
			this.coords.clear();
			this.coords.putAll(newMap);
			for(Vector coord : entries)
			{
				if(coord != null && this.attemps < max)
				{
					this.searchEnvironment(coord.x, coord.y, coord.z, world, max, block, meta);
				}
			}
		}
	}

    @Override
    public boolean canHarvestBlock(Block block, ItemStack stack)
    {
    	return this.effectiveBlocks.contains(block) ? true : false;
    }

    @Override
	public float getDigSpeed(ItemStack stack, Block block, int meta)
    {
        return this.effectiveBlocks.contains(block) ? 10.0F : super.getDigSpeed(stack, block, meta);
    }

	@Override
	public boolean canProvideEnergy(ItemStack stack)
	{
		return false;
	}

	@Override
	public int getChargedItemId(ItemStack stack)
	{
		return Item.getIdFromItem(this);
	}

	@Override
	public int getEmptyItemId(ItemStack stack)
	{
		return Item.getIdFromItem(this);
	}

	@Override
	public int getMaxCharge(ItemStack stack)
	{
		return MAX_CHARGE;
	}

	@Override
	public int getTier(ItemStack stack)
	{
		return 3;
	}

	@Override
	public int getTransferLimit(ItemStack stack)
	{
		return 4096;
	}

}
