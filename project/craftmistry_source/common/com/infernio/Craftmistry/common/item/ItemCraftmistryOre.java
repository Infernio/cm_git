package com.infernio.Craftmistry.common.item;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import com.infernio.Craftmistry.common.block.BlockCraftmistryOre;
import com.infernio.Craftmistry.common.block.ModBlocks;

public class ItemCraftmistryOre extends ItemBlock {
	
	public ItemCraftmistryOre(Block block)
	{
		super(block);
		this.setHasSubtypes(true);
	}

    @Override
    public IIcon getIconFromDamage(int meta)
    {
        return ModBlocks.craftmistryOre.getIcon(2, meta);
    }

    @Override
    public String getUnlocalizedName(ItemStack par1ItemStack)
    {
        int i = par1ItemStack.getItemDamage();
        if (i < 0 || i >= BlockCraftmistryOre.EnumOre.values().length)
        {
            i = 0;
        }
        return super.getUnlocalizedName() + "." + BlockCraftmistryOre.EnumOre.values()[i].name;
    }

	@Override
    public int getMetadata(int meta)
    {
        return meta;
    }
}
