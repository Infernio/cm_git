package com.infernio.Craftmistry.common.item;

import java.util.List;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraftforge.common.util.EnumHelper;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.InfernoCore;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemOxygenMask extends ItemArmor {

	private static ArmorMaterial armorM = EnumHelper.addArmorMaterial("oxygenMask", 5, new int[]{1, 3, 2, 1}, 15);

	public ItemOxygenMask()
	{
		super(armorM, InfernoCore.proxy.addArmor("oxygenMask"), 0);
		armorM.customCraftingMaterial = Items.leather;
		this.setUnlocalizedName(StringLib.ITEM_OXYGEN_MASK_NAME);
		this.setCreativeTab(Craftmistry.cmTab);
	}

	@Override
	public void onUpdate(ItemStack stack, World world, Entity entity, int i, boolean b)
	{
		if(entity instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)entity;
			NBTTagCompound tag = stack.stackTagCompound;
			if(tag == null)
			{
				tag = new NBTTagCompound();
				stack.setTagCompound(tag);
				stack.stackTagCompound.setInteger("Oxygen", 100);
			}
			int oxygen = tag.getInteger("Oxygen");
			if(oxygen <= 0)
			{
				player.inventory.setInventorySlotContents(39, null);
				return;
			}
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		NBTTagCompound tag = stack.stackTagCompound;
		if(tag == null)
		{
			tag = new NBTTagCompound();
			stack.setTagCompound(tag);
			stack.stackTagCompound.setInteger("Oxygen", 100);
		}
		int oxygen = tag.getInteger("Oxygen");
		if(oxygen <= 0)
		{
			player.inventory.setInventorySlotContents(39, null);
			return;
		}
		list.add("Oxygen left: " + oxygen);
		if(oxygen <= 10)
		{
			list.add(EnumChatFormatting.RED + "Low Oxygen!");
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		ItemStack stack =  new ItemStack(item, 1, 0);
		stack.setTagCompound(new NBTTagCompound());
		stack.stackTagCompound.setInteger("Oxygen", 100);
		list.add(stack);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type)
	{
		return StringLib.OXYGEN_MASK_TEXTURE;
	}
	
	@Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) 
	{
        this.itemIcon = iconRegister.registerIcon(StringLib.MODID + ":" + this.getUnlocalizedName().substring(this.getUnlocalizedName().indexOf(".") + 1));
    }

}
