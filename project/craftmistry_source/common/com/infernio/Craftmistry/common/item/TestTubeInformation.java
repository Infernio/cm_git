package com.infernio.Craftmistry.common.item;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

import com.infernio.Craftmistry.api.chemistry.EnumElementType;
import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.api.chemistry.EnumThreat;
import com.infernio.Craftmistry.api.item.ISpecialInfo;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.math.ArrayUtils;
import com.infernio.InfernoCore.math.Measurements;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class TestTubeInformation implements ISpecialInfo {

	@Override
	@SideOnly(Side.CLIENT)
	public String[] getSpecialInfo(ItemStack stack, int page)
	{
		int atomNumber = stack.getItemDamage();
		if(atomNumber > 0)
		{
			if(page == 0)
			{
				String name = Elements.getElementName(atomNumber);
				String[] ret = new String[4];
				ret[0] = StatCollector.translateToLocal("element." + name + ".name");
				ret[1] = StatCollector.translateToLocal("elements.symbol.name") + ": " + StatCollector.translateToLocal("element." + name + ".symbol");
				ret[2] = "%a" + StatCollector.translateToLocal("elements.atomicNumber.name") + ": ";
				ret[3] = "" + stack.getItemDamage();
				return ret;
			}
			if(page == 1)
			{
				EnumElementType type = Elements.getType(atomNumber);
				if(type != null)
				{
					String[] information = type.getUnformattedName().split("%s");
					String[] ret = new String[information.length + 3];
					ret[0] = StatCollector.translateToLocal("elements.elementType.name") + ":";
					for(int i = 0; i < information.length; ++i)
					{
						ret[i + 1] = StatCollector.translateToLocal("elements.elementType." + information[i] + ".name");
					}
					ret[information.length + 1] = "%a" + StatCollector.translateToLocal("elements.radioactive.name") + ":";
					ret[information.length + 2] = "" + Elements.isElementRadioactive(atomNumber);
					return ret;
				}
				return new String[]{StatCollector.translateToLocal("elements.elementType.name") + ":", StatCollector.translateToLocal("elements.elementType.unknown.name"), "%a" + StatCollector.translateToLocal("elements.radioactive.name") + ":", "" + Elements.isElementRadioactive(atomNumber)};
			}
			if(page == 2)
			{
				EnumMatterState[] states = Elements.getPossibleStates(atomNumber);
				String[] ret = new String[states.length + 1];
				ret[0] = StatCollector.translateToLocal("elements.possibleStates.name") + ":";
				for(int i = 0; i < states.length; ++i)
				{
					ret[i + 1] = StatCollector.translateToLocal("elements.state." + states[i].unlocalizedName + ".name");
				}
				return ret;
			}
			if(page == 3)
			{
				String[] ret = new String[4];
				ret[0] = StatCollector.translateToLocal("elements.halfLife.name") + ":";
				boolean radioactive = Elements.isElementRadioactive(atomNumber);
				if(radioactive && stack.stackTagCompound != null && stack.stackTagCompound.hasKey("HalfLife"))
				{
					ret[1] = stack.getTagCompound().getInteger("HalfLife") + "t";
				}
				else
				{
					ret[1] = "-";
				}
				ret[2] = "%a" + StatCollector.translateToLocal("elements.decayProduct.name") + ":";
				if(!radioactive)
				{
					ret[3] = "-";
				}
				else
				{
					ret[3] = "" + StatCollector.translateToLocal("element." + Elements.getElementName(Elements.getDecayMeta(atomNumber)) + ".name");
				}
				return ret;
			}
			if(page == 4)
			{
				String[] ret = new String[4];
				ret[0] = StatCollector.translateToLocal("elements.meltingPoint.name");
				int temperature = Elements.getMeltingPoint(atomNumber);
				//�
				ret[1] = "%a" + temperature + "�K";
				ret[2] = "" + Measurements.kelvinToCelsius(temperature) + "�C";
				ret[3] = "" + Measurements.celsiusToFahrenheit(Measurements.kelvinToCelsius(temperature)) + "�F";
				return ret;
			}
			if(page == 5)
			{
				String[] ret = new String[4];
				ret[0] = StatCollector.translateToLocal("elements.boilingPoint.name");
				int temperature = Elements.getBoilingPoint(atomNumber);
				ret[1] = "%a" + temperature + "�K";
				ret[2] = "" + Measurements.kelvinToCelsius(temperature) + "�C";
				ret[3] = "" + Measurements.celsiusToFahrenheit(Measurements.kelvinToCelsius(temperature)) + "�F";
				return ret;
			}
			if(page == 6)
			{
				String[] ret = new String[4];
				ret[0] = StatCollector.translateToLocal("elements.testTubeHelp.line1");
				ret[1] = StatCollector.translateToLocal("elements.testTubeHelp.line2");
				ret[2] = StatCollector.translateToLocal("elements.testTubeHelp.line3");
				ret[3] = StatCollector.translateToLocal("elements.testTubeHelp.line4");
				return ret;
			}
		}
		return new String[]{};
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean printDefaultInfo(ItemStack stack)
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addToScreen(ItemStack stack, FontRenderer fontRenderer, int x, int y, int page, int guiLeft, int guiTop, int guiHeight, int guiWidth)
	{
		if(page > 6 && GUIUtils.intersects(70, 10, x, y, 90, 65, guiLeft, guiTop))
		{
			EnumMatterState state = EnumMatterState.convert(stack.getItem());
			EnumThreat[] threats = Elements.getElementThreats(stack.getItemDamage(), state);
			if(threats != null)
			{
				EnumThreat threat = ArrayUtils.findObjectSkipNull(page - 7, threats);
				if(threat != null)
				{
					ArrayList<String> tooltip = new ArrayList<String>();
					tooltip.add(StatCollector.translateToLocal("elements.threats." + threat.getName() + ".name"));
					GUIUtils.drawHoveringText(tooltip, x, y, fontRenderer, guiHeight, guiWidth);
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void addSpecialEffects(ItemStack stack, Minecraft mc, FontRenderer fontRenderer, RenderItem itemRenderer, int page, float zLevel, int guiLeft, int guiTop)
	{
		if(page > 6)
		{
			EnumMatterState state = EnumMatterState.convert(stack.getItem());
			EnumThreat[] threats = Elements.getElementThreats(stack.getItemDamage(), state);
			if(threats != null)
			{
				EnumThreat threat = ArrayUtils.findObjectSkipNull(page - 7, threats);
				if(threat != null)
				{
					int ordinal = threat.ordinal();
					mc.renderEngine.bindTexture(TextureMap.locationItemsTexture);
					if(ConfigSettings.FANCY_GRAPHICS)
					{
						GUIUtils.setZLevel(zLevel);
						GUIUtils.drawTexturedModelRectFromIcon(guiLeft + 84, guiTop + 10, EnumThreat.icons[ordinal], 64, 64);
					}
					else
					{
						GUIUtils.setZLevel(zLevel);
						GUIUtils.drawTexturedModelRectFromIcon(guiLeft + 101, guiTop + 28, EnumThreat.icons[ordinal], 32, 32);
					}
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getNumberPages(ItemStack stack)
	{
		if(ItemHelper.isTestTube(stack))
		{
			EnumThreat[] threats = Elements.getElementThreats(stack.getItemDamage(), EnumMatterState.convert(stack.getItem()));
			if(threats != null)
			{
				return 6 + ArrayUtils.lengthExcludingNull(threats);
			}
		}
		return 6;
	}

}
