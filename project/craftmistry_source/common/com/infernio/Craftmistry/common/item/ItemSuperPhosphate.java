package com.infernio.Craftmistry.common.item;

import net.minecraft.block.BlockDispenser;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.util.FakePlayerFactory;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.item.predefined.ItemCM;
import com.infernio.Craftmistry.core.lib.StringLib;

public class ItemSuperPhosphate extends ItemCM {

	public ItemSuperPhosphate()
	{
		this.setUnlocalizedName(StringLib.ITEM_SUPER_PHOSPHATE_NAME);
		this.setMaxStackSize(32);
		BlockDispenser.dispenseBehaviorRegistry.putObject(this, new BehaviorSuperphosphate());
	}
	
	public static boolean apply(World world, int x, int y, int z)
	{
		EntityPlayer player = FakePlayerFactory.getMinecraft((WorldServer)world);
    	for(int x1 = -1; x1 < 2; ++x1)
    	{
    		for(int y1 = -1; y1 < 2; ++y1)
    		{
	    		for(int z1 = -1; z1 < 2; ++z1)
	    		{
	    			applyPhosphate(world, x + x1, y + y1, z + z1, player);
	    			world.playAuxSFX(2005, x + x1, y + y1, z + z1, 0);
	    		}
    		}
    	}
    	return true;
	}
	
    @Override
	public boolean onItemUse(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float par8, float par9, float par10)
    {
        if(!player.canPlayerEdit(x, y, z, side, stack))
        {
            return false;
        }
        else
        {
        	if(!player.capabilities.isCreativeMode)
        	{
        		player.inventory.decrStackSize(player.inventory.currentItem, 1);
        	}
        	for(int x1 = -1; x1 < 2; ++x1)
        	{
        		for(int y1 = -1; y1 < 2; ++y1)
        		{
        			for(int z1 = -1; z1 < 2; ++z1)
        			{
	        			applyPhosphate(world, x + x1, y + y1, z + z1, player);
	        			world.playAuxSFX(2005, x + x1, y + y1, z + z1, 0);
	        		}
        		}
        	}
        	return true;
        }
    }

	public static void applyPhosphate(World world, int x, int y, int z, EntityPlayer player)
	{
		ItemStack stack = new ItemStack(Blocks.stone, 1, 64);
		ItemDye.applyBonemeal(stack, world, x, y, z, player);
		Craftmistry.proxy.spawnSuperPhosphateParticles(world, x, y, z, 0);
		ItemDye.applyBonemeal(stack, world, x, y, z, player);
		Craftmistry.proxy.spawnSuperPhosphateParticles(world, x, y, z, 0);
	}

}
