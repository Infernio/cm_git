package com.infernio.Craftmistry.common.achievement;

import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraft.stats.AchievementList;
import net.minecraftforge.common.AchievementPage;

import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.common.FMLCommonHandler;

public class AchievementHandler {

	public static AchievementPage cmPage;

	public static Achievement firstSteps;
	public static Achievement testTubeStorage;
	public static Achievement theProperTool;
	public static Achievement basicMicrochips;
	public static Achievement theMoreYouKnow;

	public static void init()
	{
		FMLCommonHandler.instance().bus().register(new AchievementCraftingHandler());

		firstSteps = new Achievement(StringLib.ACHIEVEMENT_TEST_TUBES_NAME, StringLib.ACHIEVEMENT_TEST_TUBES_NAME, 0, 0, new ItemStack(ModItems.testTubeSolid, 1, 0), AchievementList.buildFurnace).setSpecial().registerStat();
		testTubeStorage = new Achievement(StringLib.ACHIEVEMENT_TEST_TUBE_RACK_NAME, StringLib.ACHIEVEMENT_TEST_TUBE_RACK_NAME, -3, 1, new ItemStack(ModBlocks.testTubeRack, 1, 0), firstSteps).registerStat();
		theProperTool = new Achievement(StringLib.ACHIEVEMENT_SCOOPULA_NAME, StringLib.ACHIEVEMENT_SCOOPULA_NAME, 1, 3, new ItemStack(ModItems.scoopula, 1, 0), firstSteps).registerStat();
		basicMicrochips = new Achievement(StringLib.ACHIEVEMENT_BASIC_MICROCHIPS_NAME, StringLib.ACHIEVEMENT_BASIC_MICROCHIPS_NAME, -2, 4, new ItemStack(ModItems.multiItem, 1, 8), theProperTool).registerStat();
		theMoreYouKnow = new Achievement(StringLib.ACHIEVEMENT_ANALYZER_NAME, StringLib.ACHIEVEMENT_ANALYZER_NAME, 2, 5, new ItemStack(ModBlocks.analyzer, 1, 0), basicMicrochips).registerStat();

		cmPage = new AchievementPage(StringLib.MODNAME, firstSteps, testTubeStorage, theProperTool, basicMicrochips, theMoreYouKnow);
		AchievementPage.registerAchievementPage(cmPage);
	}

}
