package com.infernio.Craftmistry.common.achievement;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.item.ModItems;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;

public class AchievementCraftingHandler {

	@SubscribeEvent
	public void handleCrafting(PlayerEvent.ItemCraftedEvent event)
	{
		ItemStack stack = event.crafting;
		EntityPlayer player = event.player;
		if(stack.getItem() == ModItems.testTubeSolid && stack.getItemDamage() == 0)
		{
			player.addStat(AchievementHandler.firstSteps, 1);
		}
		else if(stack.getItem() == Item.getItemFromBlock(ModBlocks.testTubeRack))
		{
			player.addStat(AchievementHandler.testTubeStorage, 1);
		}
		else if(stack.getItem() == Item.getItemFromBlock(ModBlocks.analyzer))
		{
			player.addStat(AchievementHandler.theMoreYouKnow, 1);
		}
	}

	//@SubscribeEvent
	public void handleSmelting(PlayerEvent.ItemSmeltedEvent event)
	{
		//TODO Maybe some smelting achievements
	}
}