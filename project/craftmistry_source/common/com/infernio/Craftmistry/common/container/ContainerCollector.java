package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotUpgrade;
import com.infernio.Craftmistry.common.item.IMachineUpgrade;
import com.infernio.Craftmistry.common.tile.TileCollector;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerCollector extends Container
{
    private TileCollector collector;
    private int lastEnergy = 0;

    public ContainerCollector(IInventory playerInv, IInventory collectorInv)
    {
        this.collector = (TileCollector)collectorInv;
        for(int j = 0; j < 3; ++j)
        {
            for(int k = 0; k < 9; ++k)
            {
                this.addSlotToContainer(new SlotOutput(collectorInv, k + j * 9, 8 + k * 18, 28 + j * 18));
            }
        }
        this.addSlotToContainer(new SlotUpgrade(collectorInv, TileCollector.MAXIMUM_UPGRADES, 27, 134, 7));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 98 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 155));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.collector.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastEnergy != this.collector.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 0, this.collector.energy);
            }
        }
        this.lastEnergy = this.collector.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.collector.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.collector.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot >= 0 && slot <= 27)
            {
            	if(!this.mergeItemStack(itemstack1, 28, 64, true))
            	{
            		return null;
            	}
            }
            if(itemstack1.getItem() != null && itemstack1.getItem() instanceof IMachineUpgrade && itemstack1.stackSize >= 1)
            {
            	Slot slot2 = ((Slot)this.inventorySlots.get(27));
            	ItemStack stack = slot2.getStack();
            	if(stack == null)
            	{
            		--itemstack1.stackSize;
            		slot2.putStack(new ItemStack(itemstack1.getItem(), 1, itemstack1.getItemDamage()));
            	}
            	stack = slot2.getStack();
            	while(itemstack1.stackSize > 0 && stack.stackSize < TileCollector.MAXIMUM_UPGRADES)
            	{
            		--itemstack1.stackSize;
            		++stack.stackSize;
            	}
            	if(itemstack1.stackSize <= 0)
                {
                    slotStack.putStack((ItemStack)null);
                }
            	return null;
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(player, itemstack1);
        }
        return itemstack;
    }
}
