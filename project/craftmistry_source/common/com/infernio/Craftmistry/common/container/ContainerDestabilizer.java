package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotTestTube;
import com.infernio.Craftmistry.common.tile.TileDestabilizer;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerDestabilizer extends Container
{
    private TileDestabilizer destabilizer;
    private int lastAmplifier = 0;
    private int lastEnergy = 0;

    public ContainerDestabilizer(IInventory playerInv, IInventory destabilizerInv)
    {
        this.destabilizer = (TileDestabilizer)destabilizerInv;
        this.addSlotToContainer(new SlotTestTube(destabilizerInv, 1, 0, 80, 20));
        int i;

        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 142));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.destabilizer.amplifier);
        crafting.sendProgressBarUpdate(this, 1, this.destabilizer.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastAmplifier != this.destabilizer.amplifier)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.destabilizer.amplifier);
            }
            if(this.lastEnergy != this.destabilizer.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.destabilizer.energy);
            }
        }
        this.lastAmplifier = this.destabilizer.amplifier;
        this.lastEnergy = this.destabilizer.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.destabilizer.amplifier = value; break;
        case 1: this.destabilizer.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.destabilizer.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot == 0)
            {
            	if(!this.mergeItemStack(itemstack1, 1, 37, false))
            	{
            		return null;
            	}
            }
            else if(slot >= 1 && slot <= 37)
            {
            	if(((Slot)this.inventorySlots.get(0)).getHasStack() || !TileDestabilizer.isValidTestTube(itemstack1))
                {
                    return null;
                }
                if(itemstack1.hasTagCompound() && itemstack1.stackSize == 1)
                {
                    ((Slot)this.inventorySlots.get(0)).putStack(itemstack1.copy());
                    itemstack1.stackSize = 0;
                }
                else if(itemstack1.stackSize >= 1)
                {
                    ((Slot)this.inventorySlots.get(0)).putStack(new ItemStack(itemstack1.getItem(), 1, itemstack1.getItemDamage()));
                    --itemstack1.stackSize;
                }
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(player, itemstack1);
        }
        return itemstack;
    }
}
