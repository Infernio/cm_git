package com.infernio.Craftmistry.common.container;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.item.ItemStack;

public class ContainerResearchPanel extends Container {

	private TileResearchPanel panel;
	private int lastEnergy = 0;

	public ContainerResearchPanel(TileResearchPanel panel)
	{
		this.panel = panel;
	}

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        ResearchNetwork network = this.panel.getNetwork();
        if(network != null && network.hasEnergyStorage())
        {
	        crafting.sendProgressBarUpdate(this, 0, network.getEnergyStorage().energy);
	    }
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        ResearchNetwork network = this.panel.getNetwork();
        boolean supplied = network.hasEnergyStorage();
        for (int i = 0; i < this.crafters.size(); ++i)
        {
        	ICrafting icrafting = (ICrafting)this.crafters.get(i);
        	if(network != null && supplied && this.lastEnergy != network.getEnergyStorage().energy)
        	{
        		icrafting.sendProgressBarUpdate(this, 0, network.getEnergyStorage().energy);
        	}
        }
        if(supplied)
        {
        	this.lastEnergy = network.getEnergyStorage().energy;
        }
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
    	switch(id)
    	{
    	case 0:
    		ResearchNetwork network = this.panel.getNetwork();
    		if(network != null && network.hasEnergyStorage())
    		{
	    		network.getEnergyStorage().energy = value;
	    	}
    		break;
    	}
    }

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer)
	{
		return true;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
	{
		return null;
	}

}
