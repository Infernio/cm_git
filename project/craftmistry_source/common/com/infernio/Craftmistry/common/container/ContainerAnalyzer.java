package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerAnalyzer extends Container {

private IInventory upperInv;
	
	public ContainerAnalyzer(IInventory playerInv, IInventory analyzerInv)
	{
		this.upperInv = analyzerInv;
		this.addSlots(analyzerInv);
		int l;
		int i1;
        for(l = 0; l < 3; ++l)
        {
            for(i1 = 0; i1 < 9; ++i1)
            {
                this.addSlotToContainer(new Slot(playerInv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
            }
        }
        for(l = 0; l < 9; ++l)
        {
            this.addSlotToContainer(new Slot(playerInv, l, 8 + l * 18, 141));
        }
	}
	
	private void addSlots(IInventory analyzerInv)
	{
		this.addSlotToContainer(new Slot(analyzerInv, 0, 26, 36));
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
	{
		ItemStack itemstack = null;
	    Slot slotStack = (Slot)this.inventorySlots.get(slot);
	    if(slotStack != null && slotStack.getHasStack())
	    {
	        ItemStack itemstack1 = slotStack.getStack();
	        itemstack = itemstack1.copy();
	        if(slot == 0)
	        {
	        	if(!this.mergeItemStack(itemstack1, 1, 37, true))
	        	{
	        		return null;
	        	}
	        }
	        else if(slot >= 1 && slot <= 36)
	        {
            	if(((Slot)this.inventorySlots.get(0)).getHasStack())
                {
                    return null;
                }
                if(itemstack1.hasTagCompound() && itemstack1.stackSize == 1)
                {
                    ((Slot)this.inventorySlots.get(0)).putStack(itemstack1.copy());
                    itemstack1.stackSize = 0;
                }
                else if(itemstack1.stackSize >= 1)
                {
                    ((Slot)this.inventorySlots.get(0)).putStack(new ItemStack(itemstack1.getItem(), 1, itemstack1.getItemDamage()));
                    --itemstack1.stackSize;
                }
	        }
	        if(itemstack1.stackSize <= 0)
	        {
	            slotStack.putStack((ItemStack)null);
	        }
	        else
	        {
	            slotStack.onSlotChanged();
	        }
	        if(itemstack1.stackSize == itemstack.stackSize)
	        {
	            return null;
	        }
	        slotStack.onPickupFromSlot(player, itemstack1);
	    }
	    return itemstack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return upperInv.isUseableByPlayer(player);
	}

}
