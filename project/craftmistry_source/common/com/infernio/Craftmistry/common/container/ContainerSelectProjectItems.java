package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;

public class ContainerSelectProjectItems extends Container {

	public ContainerSelectProjectItems(IInventory selectionInv, IInventory compartmentInv)
	{
		for(int chestRow = 0; chestRow < 6; chestRow++)
        {
            for(int chestCol = 0; chestCol < 9; chestCol++)
            {
                addSlotToContainer(new Slot(compartmentInv, chestCol + chestRow * 9, 8 + chestCol * 18, chestRow * 18 + 48));
            }
        }
        int leftCol = (176 - 162) / 2 + 1;
        int size = selectionInv.getSizeInventory();
        int rows = MathHelper.clamp_int(size / 9, 1, 3);
        int columns = MathHelper.clamp_int(size, 0, 9);
        int id = 0;
        for(int selectionRow = 0; selectionRow < rows; selectionRow++)
        {
            for(int selectionCol = 0; selectionCol < columns; selectionCol++)
            {
                addSlotToContainer(new Slot(selectionInv, id, leftCol + selectionCol * 18, 166 - (4 - selectionRow) * 18 + 75));
                ++id;
            }
        }
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer)
	{
		return true;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
	{
		return null;
	}

}
