package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotTestTube;

public class ContainerTestTubeRack extends Container {
	
	private IInventory upperInv;
	
	public ContainerTestTubeRack(IInventory playerInv, IInventory rackInv)
	{
		this.upperInv = rackInv;
		int l;
		int i1;
        for (l = 0; l < 3; ++l)
        {
            for (i1 = 0; i1 < 9; ++i1)
            {
                this.addSlotToContainer(new Slot(playerInv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
            }
        }
        for (l = 0; l < 9; ++l)
        {
            this.addSlotToContainer(new Slot(playerInv, l, 8 + l * 18, 142));
        }
		this.addSlots(rackInv);
	}
	
	private void addSlots(IInventory rackInv)
	{
		this.addSlotToContainer(new SlotTestTube(rackInv, 1, 0, 32, 34));
		this.addSlotToContainer(new SlotTestTube(rackInv, 1, 1, 50, 34));
		this.addSlotToContainer(new SlotTestTube(rackInv, 1, 2, 68, 34));
		this.addSlotToContainer(new SlotTestTube(rackInv, 1, 3, 86, 34));
		this.addSlotToContainer(new SlotTestTube(rackInv, 1, 4, 104, 34));
		this.addSlotToContainer(new SlotTestTube(rackInv, 1, 5, 122, 34));
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int i)
	{
		//TODO Add this.
		return null;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return upperInv.isUseableByPlayer(player);
	}

}
