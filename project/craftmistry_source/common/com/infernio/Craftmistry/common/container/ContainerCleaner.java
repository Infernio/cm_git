package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotTestTube;
import com.infernio.Craftmistry.common.tile.TileCleaner;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerCleaner extends Container
{
    private TileCleaner cleaner;
    private int lastProgress = 0;
    private int lastEnergy = 0;

    public ContainerCleaner(IInventory playerInv, IInventory cleanerInv)
    {
        this.cleaner = (TileCleaner)cleanerInv;
        this.addSlotToContainer(new SlotTestTube(cleanerInv, 64, 0, 56, 35));
        this.addSlotToContainer(new SlotOutput(cleanerInv, 1, 116, 35));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 142));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.cleaner.progress);
        crafting.sendProgressBarUpdate(this, 1, this.cleaner.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for(int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastProgress != this.cleaner.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.cleaner.progress);
            }
            if(this.lastEnergy != this.cleaner.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.cleaner.energy);
            }
        }
        this.lastProgress = this.cleaner.progress;
        this.lastEnergy = this.cleaner.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.cleaner.progress = value; break;
        case 1: this.cleaner.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.cleaner.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot == 1 || slot == 0)
            {
            	if(!this.mergeItemStack(itemstack1, 2, 38, false))
            	{
            		return null;
            	}
            }
            else if(slot >= 1 && slot <= 38 && TileCleaner.isValidTestTube(itemstack1))
            {
        		if(!this.mergeItemStack(itemstack1, 0, 1, true))
        		{
        			return null;
        		}
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(player, itemstack1);
        }
        return itemstack;
    }
}
