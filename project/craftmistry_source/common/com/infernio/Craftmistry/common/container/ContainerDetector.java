package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.tile.TileDetector;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerDetector extends Container
{
    private TileDetector detector;
    private int lastSetting = 0;
    private int lastEnergy = 0;
    private int lastConfig = 0;
    private int lastOutput = 0;

    public ContainerDetector(IInventory playerInv, IInventory detectorInv)
    {
        this.detector = (TileDetector)detectorInv;
        this.addSlotToContainer(new Slot(detectorInv, 0, 80, 20));
        int i;
        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 142));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.detector.energy);
        crafting.sendProgressBarUpdate(this, 1, this.detector.setting);
        crafting.sendProgressBarUpdate(this, 2, this.detector.redstoneConfig);
        crafting.sendProgressBarUpdate(this, 3, this.detector.redstoneOutput);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastEnergy != this.detector.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 0, this.detector.energy);
            }
            if(this.lastSetting != this.detector.setting)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.detector.setting);
            }
            if(this.lastConfig != this.detector.redstoneConfig)
            {
            	icrafting.sendProgressBarUpdate(this, 2, this.detector.redstoneConfig);
            }
            if(this.lastOutput != this.detector.redstoneOutput)
            {
            	icrafting.sendProgressBarUpdate(this, 3, this.detector.redstoneOutput);
            }
        }
        this.lastEnergy = this.detector.energy;
        this.lastSetting = this.detector.setting;
        this.lastConfig = this.detector.redstoneConfig;
        this.lastOutput = this.detector.redstoneOutput;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.detector.energy = value; break;
        case 1: this.detector.setting = value; break;
        case 2: this.detector.redstoneConfig = value; break;
        case 3: this.detector.redstoneOutput = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.detector.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot == 0)
            {
            	if(!this.mergeItemStack(itemstack1, 1, 37, true))
            	{
            		return null;
            	}
            }
            if(slot > 0)
            {
            	if(!this.mergeItemStack(itemstack1, 0, 1, true))
                {
                	return null;
                }
            }
	        if(itemstack1.stackSize <= 0)
	        {
	            slotStack.putStack((ItemStack)null);
	        }
	        else
	        {
	            slotStack.onSlotChanged();
	        }
	        if(itemstack1.stackSize == itemstack.stackSize)
	        {
	            return null;
	        }
	        slotStack.onPickupFromSlot(player, itemstack1);
	    }
	    return itemstack;
	}
}
