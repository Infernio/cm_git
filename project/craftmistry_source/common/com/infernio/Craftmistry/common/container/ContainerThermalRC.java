package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotTestTube;
import com.infernio.Craftmistry.common.tile.TileThermalRegulationChamber;
import com.infernio.Craftmistry.core.helper.ItemHelper;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerThermalRC extends Container {

	private TileThermalRegulationChamber thermalRC;
	private int lastTemperature = 0;
	private int lastTargetTemperature = 0;
	private int lastEnergy = 0;
	private boolean lastMaster = false;

	public ContainerThermalRC(IInventory playerInv, IInventory tile)
	{
		this.thermalRC = (TileThermalRegulationChamber)tile;
		this.addSlotToContainer(new SlotTestTube(tile, 1, 0, 80, 54));
        for(int row = 0; row < 3; ++row)
        {
            for(int column = 0; column < 9; ++column)
            {
                this.addSlotToContainer(new Slot(playerInv, column + row * 9 + 9, 8 + column * 18, 139 + row * 18));
            }
        }
        for(int column = 0; column < 9; ++column)
        {
            this.addSlotToContainer(new Slot(playerInv, column, 8 + column * 18, 197));
        }
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return this.thermalRC.isUseableByPlayer(player);
	}

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.thermalRC.temperature);
        crafting.sendProgressBarUpdate(this, 1, this.thermalRC.targetTemperature);
        crafting.sendProgressBarUpdate(this, 2, this.thermalRC.energy);
        crafting.sendProgressBarUpdate(this, 3, this.thermalRC.isMaster ? 1 : 0);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for(int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastTemperature != this.thermalRC.temperature)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.thermalRC.temperature);
            }
            if(this.lastTargetTemperature != this.thermalRC.targetTemperature)
            {
                icrafting.sendProgressBarUpdate(this, 1, this.thermalRC.targetTemperature);
            }
            if(this.lastEnergy != this.thermalRC.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 2, this.thermalRC.energy);
            }
            if(this.lastMaster != this.thermalRC.isMaster)
            {
            	icrafting.sendProgressBarUpdate(this, 3, this.thermalRC.isMaster ? 1 : 0);
            }
        }
        this.lastTemperature = this.thermalRC.temperature;
        this.lastTargetTemperature = this.thermalRC.targetTemperature;
        this.lastEnergy = this.thermalRC.energy;
        this.lastMaster = this.thermalRC.isMaster;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.thermalRC.temperature = value; break;
        case 1: this.thermalRC.targetTemperature = value; break;
        case 2: this.thermalRC.energy = value; break;
        case 3: this.thermalRC.clientMaster = value == 1; break;
        default: break;
        }
    }

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slot)
	{
		ItemStack itemstack = null;
	    Slot slotStack = (Slot)this.inventorySlots.get(slot);
	    if(slotStack != null && slotStack.getHasStack())
	    {
	        ItemStack itemstack1 = slotStack.getStack();
	        itemstack = itemstack1.copy();
	        if(slot == 0)
	        {
	        	if(!this.mergeItemStack(itemstack1, 1, 37, true))
	        	{
	        		return null;
	        	}
	        }
	        else if(slot >= 1 && slot <= 36)
	        {
            	if(((Slot)this.inventorySlots.get(0)).getHasStack())
                {
                    return null;
                }
            	if(!ItemHelper.isTestTubeAndFilled(itemstack1))
            	{
            		return null;
            	}
                if(itemstack1.hasTagCompound() && itemstack1.stackSize == 1)
                {
                    ((Slot)this.inventorySlots.get(0)).putStack(itemstack1.copy());
                    itemstack1.stackSize = 0;
                }
                else if(itemstack1.stackSize >= 1)
                {
                    ((Slot)this.inventorySlots.get(0)).putStack(new ItemStack(itemstack1.getItem(), 1, itemstack1.getItemDamage()));
                    --itemstack1.stackSize;
                }
	        }
	        if(itemstack1.stackSize <= 0)
	        {
	            slotStack.putStack((ItemStack)null);
	        }
	        else
	        {
	            slotStack.onSlotChanged();
	        }
	        if(itemstack1.stackSize == itemstack.stackSize)
	        {
	            return null;
	        }
	        slotStack.onPickupFromSlot(player, itemstack1);
	    }
	    return itemstack;
	}

}
