package com.infernio.Craftmistry.common.container.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.core.helper.ItemHelper;

public class SlotTestTube extends Slot {
	
	private int sizeLimit;
	
	public SlotTestTube(IInventory inv, int limit, int id, int x, int y)
	{
		super(inv, id, x, y);
		this.sizeLimit = limit;
	}
	
	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return stack != null && stack.getItem() != null && ItemHelper.isTestTube(stack);
	}
	
	@Override
	public int getSlotStackLimit()
	{
		return this.sizeLimit;
	}

}
