package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.tile.TileMineralExtractor;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerMineralExtractor extends Container {

	private TileMineralExtractor mineralExtractor;
    private int lastProgress = 0;
    private int lastAmount = 0;
    private int lastEnergy = 0;

    public ContainerMineralExtractor(IInventory playerInv, IInventory mineralExtractorInv)
    {
        this.mineralExtractor = (TileMineralExtractor)mineralExtractorInv;
        this.addSlotToContainer(new SlotOutput(mineralExtractorInv, 0, 116, 30));
        this.addSlotToContainer(new SlotOutput(mineralExtractorInv, 1, 134, 30));
        this.addSlotToContainer(new SlotOutput(mineralExtractorInv, 2, 152, 30));
        this.addSlotToContainer(new SlotOutput(mineralExtractorInv, 3, 116, 48));
        this.addSlotToContainer(new SlotOutput(mineralExtractorInv, 4, 134, 48));
        this.addSlotToContainer(new SlotOutput(mineralExtractorInv, 5, 152, 48));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 141));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.mineralExtractor.progress);
        crafting.sendProgressBarUpdate(this, 1, this.mineralExtractor.getFluidAmount());
        crafting.sendProgressBarUpdate(this, 2, this.mineralExtractor.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastProgress != this.mineralExtractor.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.mineralExtractor.progress);
            }
            if(this.lastAmount != this.mineralExtractor.getFluidAmount())
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.mineralExtractor.getFluidAmount());
            }
            if(this.lastEnergy != this.mineralExtractor.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 2, this.mineralExtractor.energy);
            }
        }
        this.lastProgress = this.mineralExtractor.progress;
        this.lastAmount = this.mineralExtractor.getFluidAmount();
        this.lastEnergy = this.mineralExtractor.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.mineralExtractor.progress = value; break;
        case 1: this.mineralExtractor.setFluidAmount(value); break;
        case 2: this.mineralExtractor.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.mineralExtractor.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
    	ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot >= 0 && slot <= 5)
            {
            	if(!this.mergeItemStack(itemstack1, 6, 41, false))
            	{
            		return null;
            	}
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(player, itemstack1);
        }
        return itemstack;
    }

}
