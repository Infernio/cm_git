package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotTestTube;
import com.infernio.Craftmistry.common.tile.TileAirFilter;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerAirFilter extends Container
{
    private TileAirFilter airFilter;
    private int lastProgress = 0;
    private int lastEnergy = 0;
    private int lastMode = 0;

    public ContainerAirFilter(IInventory playerInv, IInventory airFilterInv)
    {
        this.airFilter = (TileAirFilter)airFilterInv;
        this.addSlotToContainer(new SlotTestTube(airFilterInv, 64, 0, 56, 17));
        this.addSlotToContainer(new SlotOutput(airFilterInv, 1, 116, 35));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 141));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.airFilter.progress);
        crafting.sendProgressBarUpdate(this, 1, this.airFilter.energy);
        crafting.sendProgressBarUpdate(this, 2, this.airFilter.mode);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastProgress != this.airFilter.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.airFilter.progress);
            }
            if(this.lastEnergy != this.airFilter.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.airFilter.energy);
            }
            if(this.lastMode != this.airFilter.mode)
            {
            	icrafting.sendProgressBarUpdate(this, 2, this.airFilter.mode);
            }
        }
        this.lastProgress = this.airFilter.progress;
        this.lastEnergy = this.airFilter.energy;
        this.lastMode = this.airFilter.mode;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.airFilter.progress = value; break;
        case 1: this.airFilter.energy = value; break;
        case 2: this.airFilter.mode = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.airFilter.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot == 1)
            {
            	if(!this.mergeItemStack(itemstack1, 2, 38, true))
            	{
            		return null;
            	}
            	slotStack.onSlotChange(itemstack1, itemstack);
            }
            else if(slot >= 2 && slot <= 38)
            {
            	if(TileAirFilter.isValidTestTube(itemstack1))
            	{
            		if(!this.mergeItemStack(itemstack1, 0, 1, false))
            		{
            			return null;
            		}
            	}
            }
            else if(slot == 0)
            {
            	if(!this.mergeItemStack(itemstack1, 2, 38, false))
            	{
            		return null;
            	}
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(player, itemstack1);
        }
        return itemstack;
    }
}
