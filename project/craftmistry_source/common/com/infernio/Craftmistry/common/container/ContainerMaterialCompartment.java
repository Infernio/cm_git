package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;

public class ContainerMaterialCompartment extends Container {

	@SuppressWarnings("unused")
	private TileMaterialCompartment compartment;

	public ContainerMaterialCompartment(IInventory playerInv, IInventory compartmentInv)
	{
		this.compartment = (TileMaterialCompartment)compartmentInv;
        for(int chestRow = 0; chestRow < 6; chestRow++)
        {
            for(int chestCol = 0; chestCol < 9; chestCol++)
            {
                addSlotToContainer(new Slot(compartmentInv, chestCol + chestRow * 9, 8 + chestCol * 18, 48 + chestRow * 18));
            }
        }
        int leftCol = (176 - 162) / 2 + 1;
        for(int playerInvRow = 0; playerInvRow < 3; playerInvRow++)
        {
            for(int playerInvCol = 0; playerInvCol < 9; playerInvCol++)
            {
                addSlotToContainer(new Slot(playerInv, playerInvCol + playerInvRow * 9 + 9, leftCol + playerInvCol * 18, 166 - (4 - playerInvRow) * 18 + 75));
            }
        }
        for(int hotbarSlot = 0; hotbarSlot < 9; hotbarSlot++)
        {
            addSlotToContainer(new Slot(playerInv, hotbarSlot, leftCol + hotbarSlot * 18, 166 + 61));
        }
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return true;
	}

    @Override
    public ItemStack transferStackInSlot(EntityPlayer p, int i)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(i);
        if(slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            if(i < 54)
            {
                if(!this.mergeItemStack(itemstack1, 54, this.inventorySlots.size(), true))
                {
                    return null;
                }
            }
            else if(!this.mergeItemStack(itemstack1, 0, 54, false))
            {
                return null;
            }
            if(itemstack1.stackSize == 0)
            {
                slot.putStack(null);
            }
            else
            {
                slot.onSlotChanged();
            }
        }
        return itemstack;
    }

}
