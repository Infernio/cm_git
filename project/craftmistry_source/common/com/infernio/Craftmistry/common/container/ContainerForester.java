package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotUpgrade;
import com.infernio.Craftmistry.common.item.IMachineUpgrade;
import com.infernio.Craftmistry.common.tile.TileForester;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerForester extends Container {

	private TileForester forester;
	private int lastEnergy = 0;
	private int lastMaintained = 0;
	private int lastTask = 3;

	public ContainerForester(IInventory playerInv, IInventory foresterInv)
    {
		this.forester = (TileForester)foresterInv;
		int slot = 0;
		for(int row = 0; row < 4; ++row)
		{
			for(int column = 0; column < 4; ++column)
			{
		        this.addSlotToContainer(new Slot(foresterInv, slot, 90 + 18 * column, 56 + 18 * row));
		        ++slot;
			}
		}
		this.addSlotToContainer(new SlotUpgrade(foresterInv, TileForester.MAXIMUM_UPGRADES, slot, 6, 6));
		for(int column = 0; column < 3; ++column)
        {
            for(int row = 0; row < 9; ++row)
            {
                this.addSlotToContainer(new Slot(playerInv, row + column * 9 + 9, 8 + row * 18, 103 + column * 18 + 36));
            }
        }
        for(int column = 0; column < 9; ++column)
        {
            this.addSlotToContainer(new Slot(playerInv, column, 8 + column * 18, 161 + 36));
        }
    }

	@Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.forester.energy);
        crafting.sendProgressBarUpdate(this, 1, this.forester.getNumberNodes());
        crafting.sendProgressBarUpdate(this, 2, this.forester.getCurrentTask() != null ? this.forester.getCurrentTask().ordinal() : 3);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        int nodes = this.forester.getNumberNodes();
        int task = this.forester.getCurrentTask() != null ? this.forester.getCurrentTask().ordinal() : 3;
        for(int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastEnergy != this.forester.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 0, this.forester.energy);
            }
            if(this.lastMaintained != nodes)
            {
            	icrafting.sendProgressBarUpdate(this, 1, nodes);
            }
            if(this.lastTask != task)
            {
            	icrafting.sendProgressBarUpdate(this, 2, task);
            }
        }
        this.lastEnergy = this.forester.energy;
        this.lastMaintained = nodes;
        this.lastTask = task;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.forester.energy = value; break;
        case 1: this.forester.treesMaintained = value; break;
        case 2: this.forester.task = value; break;
        default: break;
        }
    }

	@Override
    public boolean canInteractWith(EntityPlayer player)
    {
        return this.forester.isUseableByPlayer(player);
    }

	@Override
    public ItemStack transferStackInSlot(EntityPlayer p, int i)
    {
		ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(i);
        if(slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            if(i < 16)
            {
                if(!this.mergeItemStack(itemstack1, 17, this.inventorySlots.size(), true))
                {
                    return null;
                }
            }
            else if(itemstack1.getItem() != null && itemstack1.getItem() instanceof IMachineUpgrade && itemstack1.stackSize >= 1)
            {
            	Slot slot2 = ((Slot)this.inventorySlots.get(27));
            	ItemStack stack = slot2.getStack();
            	if(stack == null)
            	{
            		--itemstack1.stackSize;
            		slot2.putStack(new ItemStack(itemstack1.getItem(), 1, itemstack1.getItemDamage()));
            	}
            	stack = slot2.getStack();
            	while(itemstack1.stackSize > 0 && stack.stackSize < TileForester.MAXIMUM_UPGRADES)
            	{
            		--itemstack1.stackSize;
            		++stack.stackSize;
            	}
            	if(itemstack1.stackSize <= 0)
                {
                    slot.putStack((ItemStack)null);
                }
            	return null;
            }
            else if(!this.mergeItemStack(itemstack1, 0, 16, false))
            {
                return null;
            }
            if(itemstack1.stackSize == 0)
            {
                slot.putStack(null);
            }
            else
            {
                slot.onSlotChanged();
            }
        }
        return itemstack;
    }
}
