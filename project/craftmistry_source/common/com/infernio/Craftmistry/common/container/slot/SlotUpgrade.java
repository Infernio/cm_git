package com.infernio.Craftmistry.common.container.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.item.IMachineUpgrade;

public class SlotUpgrade extends Slot {

	private int limit;

	public SlotUpgrade(IInventory inventory, int limit, int id, int x, int y)
	{
		super(inventory, id, x, y);
		this.limit = limit;
	}
	
	@Override
	public int getSlotStackLimit()
	{
		return this.limit;
	}

	@Override
	public boolean isItemValid(ItemStack stack)
	{
		if(stack != null && stack.getItem() instanceof IMachineUpgrade)
		{
			if(this.getHasStack())
			{
				return this.getStack().stackSize + stack.stackSize <= this.getSlotStackLimit();
			}
			return true;
		}
		return false;
	}

}
