package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotUpgrade;
import com.infernio.Craftmistry.common.tile.TileBreaker;
import com.infernio.InfernoCore.container.SlotTool;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerBreaker extends Container
{
    private TileBreaker breaker;
    private int lastEnergy = 0;

    public ContainerBreaker(IInventory playerInv, IInventory breakerInv)
    {
        this.breaker = (TileBreaker)breakerInv;
        this.addSlotToContainer(new SlotTool(breakerInv, 0, 60, 29));
        this.addSlotToContainer(new SlotTool(breakerInv, 1, 80, 29));
        this.addSlotToContainer(new SlotTool(breakerInv, 2, 100, 29));
        this.addSlotToContainer(new SlotUpgrade(breakerInv, 6, 3, 22, 50));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 141));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.breaker.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastEnergy != this.breaker.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 0, this.breaker.energy);
            }
        }
        this.lastEnergy = this.breaker.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.breaker.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer player)
    {
        return this.breaker.isUseableByPlayer(player);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
    	return null;
    }
}
