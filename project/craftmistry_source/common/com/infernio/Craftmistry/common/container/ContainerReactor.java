package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.tile.TileReactor;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerReactor extends Container
{
    private TileReactor reactor;
    private int lastProgress = 0;
    private int lastEnergy = 0;

    public ContainerReactor(IInventory playerInv, IInventory reactorInv)
    {
        this.reactor = (TileReactor)reactorInv;
        this.addSlotToContainer(new Slot(reactorInv, 0, 20, 17));
        this.addSlotToContainer(new Slot(reactorInv, 1, 38, 17));
        this.addSlotToContainer(new Slot(reactorInv, 2, 56, 17));
        this.addSlotToContainer(new Slot(reactorInv, 3, 20, 35));
        this.addSlotToContainer(new Slot(reactorInv, 4, 38, 35));
        this.addSlotToContainer(new Slot(reactorInv, 5, 56, 35));
        this.addSlotToContainer(new Slot(reactorInv, 6, 20, 53));
        this.addSlotToContainer(new Slot(reactorInv, 7, 38, 53));
        this.addSlotToContainer(new Slot(reactorInv, 8, 56, 53));
        this.addSlotToContainer(new SlotOutput(reactorInv, 9, 116, 35));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 141));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.reactor.progress);
        crafting.sendProgressBarUpdate(this, 1, this.reactor.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastProgress != this.reactor.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.reactor.progress);
            }
            if(this.lastEnergy != this.reactor.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.reactor.energy);
            }
        }
        this.lastProgress = this.reactor.progress;
        this.lastEnergy = this.reactor.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.reactor.progress = value; break;
        case 1: this.reactor.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.reactor.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();

            if(slot == 9)
            {
                if(!this.mergeItemStack(itemstack1, 10, 46, true))
                {
                    return null;
                }
                slotStack.onSlotChange(itemstack1, itemstack);
            }
            else if(slot >= 10 && slot < 37)
            {
                if(!this.mergeItemStack(itemstack1, 37, 46, false))
                {
                    return null;
                }
            }
            else if(slot >= 37 && slot < 46)
            {
                if (!this.mergeItemStack(itemstack1, 10, 37, false))
                {
                    return null;
                }
            }
            else if(!this.mergeItemStack(itemstack1, 10, 46, false))
            {
                return null;
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(player, itemstack1);
        }
        return itemstack;
    }
}
