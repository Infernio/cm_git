package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.tile.TileElectrolyser;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerElectrolyser extends Container
{
    private TileElectrolyser electrolyser;
    private int lastProgress = 0;
    private int lastEnergy = 0;
    private boolean lastDisabled = false;

    public ContainerElectrolyser(IInventory playerInv, IInventory infuserInv)
    {
        this.electrolyser = (TileElectrolyser)infuserInv;
        this.addSlotToContainer(new Slot(infuserInv, 0, 56, 17));
        this.addSlotToContainer(new Slot(infuserInv, 1, 56, 53));
        this.addSlotToContainer(new SlotOutput(infuserInv, 2, 116, 35));
        int i;
        for(i = 0; i < 3; ++i)
        {
            for(int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }
        for(i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 141));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.electrolyser.progress);
        crafting.sendProgressBarUpdate(this, 1, this.electrolyser.energy);
        crafting.sendProgressBarUpdate(this, 2, this.electrolyser.disabled ? 1 : 0);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastProgress != this.electrolyser.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.electrolyser.progress);
            }
            if(this.lastEnergy != this.electrolyser.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.electrolyser.energy);
            }
            if(this.lastDisabled != this.electrolyser.disabled)
            {
            	icrafting.sendProgressBarUpdate(this, 2, this.electrolyser.disabled ? 1 : 0);
            }
        }
        this.lastProgress = this.electrolyser.progress;
        this.lastEnergy = this.electrolyser.energy;
        this.lastDisabled = this.electrolyser.disabled;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.electrolyser.progress = value; break;
        case 1: this.electrolyser.energy = value; break;
        case 2: this.electrolyser.disabled = value == 1 ? true : false; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.electrolyser.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slot)
    {
    	return null;
    }
}
