package com.infernio.Craftmistry.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.common.container.slot.SlotTestTube;
import com.infernio.Craftmistry.common.recipe.InfuserRecipes;
import com.infernio.Craftmistry.common.tile.TileInfuser;
import com.infernio.InfernoCore.container.SlotOutput;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ContainerInfuser extends Container
{
    private TileInfuser infuser;
    private int lastProgress = 0;
    private int lastEnergy = 0;

    public ContainerInfuser(IInventory playerInv, IInventory infuserInv)
    {
        this.infuser = (TileInfuser)infuserInv;
        this.addSlotToContainer(new Slot(infuserInv, 0, 56, 17));
        this.addSlotToContainer(new SlotTestTube(infuserInv, 64, 1, 56, 53));
        this.addSlotToContainer(new SlotOutput(infuserInv, 2, 116, 35));
        int i;

        for (i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 141));
        }
    }

    @Override
    public void addCraftingToCrafters(ICrafting crafting)
    {
        super.addCraftingToCrafters(crafting);
        crafting.sendProgressBarUpdate(this, 0, this.infuser.progress);
        crafting.sendProgressBarUpdate(this, 1, this.infuser.energy);
    }

    @Override
    public void detectAndSendChanges()
    {
        super.detectAndSendChanges();
        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);
            if(this.lastProgress != this.infuser.progress)
            {
                icrafting.sendProgressBarUpdate(this, 0, this.infuser.progress);
            }
            if(this.lastEnergy != this.infuser.energy)
            {
            	icrafting.sendProgressBarUpdate(this, 1, this.infuser.energy);
            }
        }
        this.lastProgress = this.infuser.progress;
        this.lastEnergy = this.infuser.energy;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateProgressBar(int id, int value)
    {
        switch(id)
        {
        case 0: this.infuser.progress = value; break;
        case 1: this.infuser.energy = value; break;
        default: break;
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.infuser.isUseableByPlayer(par1EntityPlayer);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slot)
    {
        ItemStack itemstack = null;
        Slot slotStack = (Slot)this.inventorySlots.get(slot);
        if(slotStack != null && slotStack.getHasStack())
        {
            ItemStack itemstack1 = slotStack.getStack();
            itemstack = itemstack1.copy();
            if(slot == 2)
            {
                if(!this.mergeItemStack(itemstack1, 3, 39, true))
                {
                    return null;
                }
                slotStack.onSlotChange(itemstack1, itemstack);
            }
            else if(slot != 1 && slot != 0)
            {
                if(InfuserRecipes.infusing().getResult(itemstack1) != null)
                {
                    if(!this.mergeItemStack(itemstack1, 0, 1, false))
                    {
                        return null;
                    }
                }
                else if(TileInfuser.isValidTestTube(itemstack1))
                {
                    if(!this.mergeItemStack(itemstack1, 1, 2, false))
                    {
                        return null;
                    }
                }
                else if(slot >= 3 && slot < 30)
                {
                    if(!this.mergeItemStack(itemstack1, 30, 39, false))
                    {
                        return null;
                    }
                }
                else if(slot >= 30 && slot < 39 && !this.mergeItemStack(itemstack1, 3, 30, false))
                {
                    return null;
                }
            }
            else if(!this.mergeItemStack(itemstack1, 3, 39, false))
            {
                return null;
            }
            if(itemstack1.stackSize <= 0)
            {
                slotStack.putStack((ItemStack)null);
            }
            else
            {
                slotStack.onSlotChanged();
            }
            if(itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }
            slotStack.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }
        return itemstack;
    }
}
