package com.infernio.Craftmistry.common.chemistry;

import java.util.Random;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.api.chemistry.EnumThreat;
import com.infernio.Craftmistry.common.damage.CustomDS;
import com.infernio.Craftmistry.common.entity.ExplosionCustom;
import com.infernio.Craftmistry.common.item.ItemSpoiledFood;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.InfernoCore.util.ItemUtils;
import com.infernio.InfernoCore.util.PotionUtils;

public class ThreatHandler {

	private static Random rand = new Random();

	public static void updateElement(EnumMatterState state, int invPos, EntityPlayer player)
	{
		ItemStack stack = player.inventory.getStackInSlot(invPos);
		if(stack == null)
		{
			return;
		}
		int atomNumber = stack.getItemDamage();
		EnumThreat[] threats = Elements.getElementThreats(atomNumber, state);
		if(threats != null)
		{
			for(int i = 0; i < stack.stackSize; ++i)
			{
				for(EnumThreat threat : threats)
				{
					int chance = threat.getActivationChance();
					if(rand.nextInt(1000) <= chance)
					{
						World world = player.worldObj;
						if(threat.isServerOnly())
						{
							if(world.isRemote)
							{
								return;
							}
							process(threat, player, world, invPos, false);
						}
						else
						{
							process(threat, player, world, invPos, false);
						}
					}
				}
			}
		}
	}

	public static void updateCompound(int invPos, EntityPlayer player)
	{
		ItemStack stack = player.inventory.getStackInSlot(invPos);
		if(stack == null)
		{
			return;
		}
		int id = stack.getItemDamage();
		EnumThreat[] threats = Elements.getCompoundThreats(id);
		if(threats != null)
		{
			for(int i = 0; i < stack.stackSize; ++i)
			{
				for(EnumThreat threat : threats)
				{
					int chance = threat.getActivationChance();
					if(rand.nextInt(1000) <= chance)
					{
						World world = player.worldObj;
						if(threat.isServerOnly())
						{
							if(world.isRemote)
							{
								return;
							}
							process(threat, player, world, invPos, true);
						}
						else
						{
							process(threat, player, world, invPos, true);
						}
					}
				}
			}
		}
	}

	private static void process(EnumThreat threat, EntityPlayer player, World world, int invPos, boolean compound)
	{
		switch(threat)
		{
			case RADIOACTIVE:
			{
				PotionEffect effect = PotionUtils.getRandomNegativeEffect(rand, rand.nextInt(500));
				if(isValidRadioactivityEffect(effect))
				{
					PotionUtils.improveEffect(effect);
					player.addPotionEffect(effect);
				}
				break;
			}
			case EXPLOSIVE:
			{
				if(!player.onGround || player.isSprinting())
				{
					ExplosionCustom explosion = new ExplosionCustom(world, null, player.posX, player.posY, player.posZ, 1.0F, false);
					explosion.doExplosionA();
					explosion.doExplosionB();
					explosion.doExplosionC();
					player.inventory.decrStackSize(invPos, 1);
				}
				break;
			}
			case FLAMMABLE:
			{
				player.setFire(1);
				break;
			}
			case OXYDIZING:
			{
				if(player.isBurning())
				{
					player.setFire(5);
				}
				break;
			}
			case CORROSIVE:
			{
				if(compound)
				{
					player.attackEntityFrom(CustomDS.corrosionCompound, 2.0F);
					for(int i = 0; i < 4 + rand.nextInt(5); ++i)
					{
						player.inventory.damageArmor(1.0F);
					}
				}
				else
				{
					player.attackEntityFrom(CustomDS.corrosionElement, 2.0F);
					for(int i = 0; i < 4 + rand.nextInt(5); ++i)
					{
						player.inventory.damageArmor(1.0F);
					}
				}
				break;
			}
			case TOXIC:
			{
				InventoryPlayer inv = player.inventory;
				for(int slot = 0; slot < inv.mainInventory.length; ++slot)
				{
					ItemStack stack = inv.getStackInSlot(slot);
					if(stack != null && stack.getItem() instanceof ItemFood && stack.getItem() != ModItems.spoiledFood)
					{
						for(int slot1 = 0; slot1 < inv.mainInventory.length; ++slot1)
						{
							ItemStack stack1 = inv.getStackInSlot(slot1);
							if(stack1 != null && stack1.getItem() != null)
							{
								if(stack1.getItem() == ModItems.spoiledFood && stack1.stackSize < inv.getInventoryStackLimit())
								{
									NBTTagCompound tag = stack1.stackTagCompound;
									if(tag != null && tag.hasKey(ItemSpoiledFood.NBT_FOOD_ID) && tag.hasKey(ItemSpoiledFood.NBT_FOOD_META))
									{
										if(Item.itemRegistry.getNameForObject(stack.getItem()).equals(tag.getString(ItemSpoiledFood.NBT_FOOD_ID)) && stack.getItemDamage() == tag.getShort(ItemSpoiledFood.NBT_FOOD_META))
										{
											--stack.stackSize;
											if(stack.stackSize <= 0)
											{
												inv.setInventorySlotContents(slot, null);
											}
											++stack1.stackSize;
											return;
										}
									}
								}
							}
						}
						int emptySlot = ItemUtils.findEmptySlot(inv);
						if(emptySlot == -1)
						{
							--stack.stackSize;
							if(stack.stackSize <= 0)
							{
								inv.setInventorySlotContents(slot, null);
							}
							ItemStack newStack = new ItemStack(ModItems.spoiledFood, 1, 0);
							NBTTagCompound tag = new NBTTagCompound();
							tag.setString(ItemSpoiledFood.NBT_FOOD_ID, Item.itemRegistry.getNameForObject(stack.getItem()));
							tag.setShort(ItemSpoiledFood.NBT_FOOD_META, (short)stack.getItemDamage());
							newStack.setTagCompound(tag);
							ItemUtils.dropItem(world, player.posX, player.posY, player.posZ, newStack);
							return;
						}
						--stack.stackSize;
						if(stack.stackSize <= 0)
						{
							inv.setInventorySlotContents(slot, null);
						}
						ItemStack newStack = new ItemStack(ModItems.spoiledFood, 1, 0);
						NBTTagCompound tag = new NBTTagCompound();
						tag.setString(ItemSpoiledFood.NBT_FOOD_ID, Item.itemRegistry.getNameForObject(stack.getItem()));
						tag.setShort(ItemSpoiledFood.NBT_FOOD_META, (short)stack.getItemDamage());
						newStack.setTagCompound(tag);
						inv.setInventorySlotContents(emptySlot, newStack);
						return;
					}
				}
				break;
			}
			case HEALTH_HAZARD:
			{
				player.addPotionEffect(new PotionEffect(Potion.poison.id, rand.nextInt(250), 0));
				break;
			}
			default:
				break;
		}
		
	}

	private static boolean isValidRadioactivityEffect(PotionEffect effect)
	{
		if(effect == null)
		{
			return false;
		}
		int id = effect.getPotionID();
		if(id == Potion.blindness.id || id == Potion.confusion.id || id == Potion.harm.id || id == Potion.hunger.id || id == Potion.poison.id || id == Potion.moveSlowdown.id || id == Potion.weakness.id)
		{
			return true;
		}
		return false;
	}

}
