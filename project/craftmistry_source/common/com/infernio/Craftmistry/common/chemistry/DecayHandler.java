package com.infernio.Craftmistry.common.chemistry;

import java.util.Random;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.damage.CustomDS;
import com.infernio.InfernoCore.util.PlayerUtils;

public class DecayHandler {

	public static void decay(ItemStack stack, EntityPlayer player, World world, int atomNumber, ItemStack item, int slot, Random rand)
	{
		NBTTagCompound tag = stack.stackTagCompound;
		if(tag == null)
		{
			tag = new NBTTagCompound();
			stack.setTagCompound(tag);
		}
		if(!tag.hasKey("HalfLife"))
		{
			tag.setInteger("HalfLife", Elements.getHalfLife(atomNumber));
		}
		int halfLife = tag.getInteger("HalfLife");
		if(halfLife > 1)
		{
			if(rand.nextInt(20) == 5)
			{
				player.attackEntityFrom(CustomDS.radiation, 1);
			}
			tag.setInteger("HalfLife", halfLife - 1);
			return;
		}
		if(halfLife <= 1)
		{
			stack.setTagCompound(new NBTTagCompound());
			stack.stackTagCompound.setInteger("HalfLife", Elements.getHalfLife(atomNumber));
			int stackSize = stack.stackSize;
			player.inventory.setInventorySlotContents(slot, null);
			ItemStack decay = Elements.getDecayProduct(atomNumber, item);
			decay.stackSize = stackSize;
			PlayerUtils.giveItem(decay, world, player);
		}
	}

}
