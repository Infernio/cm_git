package com.infernio.Craftmistry.common.chemistry;

import static com.infernio.Craftmistry.api.chemistry.EnumElementType.*;
import static com.infernio.Craftmistry.api.chemistry.EnumMatterState.*;
import static com.infernio.Craftmistry.api.chemistry.EnumThreat.*;

import java.util.ArrayList;

import net.minecraft.item.ItemStack;

import com.infernio.Craftmistry.api.CraftmistryAPI;
import com.infernio.Craftmistry.api.chemistry.Compound;
import com.infernio.Craftmistry.api.chemistry.Element;
import com.infernio.Craftmistry.api.chemistry.EnumElementType;
import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.api.chemistry.EnumThreat;
import com.infernio.Craftmistry.core.logging.CMLogger;

public class Elements {

	private static final Element[] elements = new Element[256];
	private static final Compound[] compounds = new Compound[1024];

	public static void addElement(Element element)
	{
		int id = element.getAtomicNumber();
		if(id < 0 || id >= elements.length)
		{
			CMLogger.warning("Attemped to register an element with invalid Atomic Number! (" + id + ")");
			return;
		}
		if(elements[id] == null)
		{
			elements[id] = element;
			return;
		}
		throw new IllegalArgumentException("[Craftmistry] An element with Atomic Number " + id + " already exists! Check your configs!");
	}

	public static void addCompound(Compound compound)
	{
		int id = compound.getID();
		if(id < 0 || id >= compounds.length)
		{
			CMLogger.warning("Attemped to register an element with invalid Atomic Number! (" + id + ")");
			return;
		}
		if(compounds[id] == null)
		{
			compounds[id] = compound;
			return;
		}
		throw new IllegalArgumentException("[Craftmistry] A compound with ID " + id + " already exists! Check your configs!");
	}

	public static void initVanillaChemistry()
	{
		//Threats are from sigmaaldrich.com
		CMLogger.info("Testing Craftmistry Installation... Step 1 / 4 (Chemistry)");
		CraftmistryAPI.Chemistry.addElement(new Element(0, "Empty", UNKOWN_PROPERTIES, 0, 0, false, 0, 0));
		addElement(new Element(1, "Hydrogen", OTHER_NON_METAL, 14, 20, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, COMPRESSED_GAS}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(2, "Helium", NOBLE_GAS, 1, 4, false, 0, 0).setThreats(createArray(COMPRESSED_GAS), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(3, "Lithium", ALKALI_METAL, 454, 1615, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, CORROSIVE}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(4, "Beryllium", ALKALINE_EARTH_METAL, 1560, 2741, false, 0, 0).setThreats(new EnumThreat[]{TOXIC, HEALTH_HAZARD}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(5, "Boron", METALLOID, 2349, 4200, false, 0, 0).setThreats(createArray(IRRITANT), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(6, "Carbon", OTHER_NON_METAL, 3550, 3915, false, 0, 0));
		addElement(new Element(7, "Nitrogen", OTHER_NON_METAL, 63, 77, false, 0, 0).setThreats(createArray(COMPRESSED_GAS), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(8, "Oxygen", OTHER_NON_METAL, 54, 90, false, 0, 0).setThreats(new EnumThreat[]{OXYDIZING, COMPRESSED_GAS}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(9, "Fluorine", HALOGEN, 53, 85, false, 0, 0).setThreats(new EnumThreat[]{TOXIC, OXYDIZING, CORROSIVE, HEALTH_HAZARD}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(10, "Neon", NOBLE_GAS, 25, 27, false, 0, 0).setThreats(createArray(COMPRESSED_GAS), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(11, "Sodium", ALKALI_METAL, 370, 1156, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, CORROSIVE}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(12, "Magnesium", ALKALINE_EARTH_METAL, 923, 1363, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(13, "Aluminium", POST_TRANSITION_METAL, 933, 2792, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(14, "Silicon", POST_TRANSITION_METAL, 1687, 3538, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(15, "Phosphorus", OTHER_NON_METAL, 44, 281, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(16, "Sulfur", OTHER_NON_METAL, 388, 718, false, 0, 0).setThreats(createArray(IRRITANT), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(17, "Chlorine", HALOGEN, 172, 239, false, 0, 0).setThreats(new EnumThreat[]{OXYDIZING, COMPRESSED_GAS, TOXIC, ENVIRONMENTALLY_DAMAGING}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(18, "Argon", NOBLE_GAS, 83, 87, false, 0, 0));
		addElement(new Element(19, "Potassium", ALKALI_METAL, 336, 1032, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, CORROSIVE}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(20, "Calcium", ALKALINE_EARTH_METAL, 1115, 1757, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(21, "Scandium", TRANSITION_METAL, 1814, 3109, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(22, "Titanium", TRANSITION_METAL, 1941, 3560, false, 0, 0));
		addElement(new Element(23, "Vanadium", TRANSITION_METAL, 2183, 3680, false, 0, 0));
		addElement(new Element(24, "Chromium", TRANSITION_METAL, 2180, 2944, false, 0, 0).setThreats(createArray(ENVIRONMENTALLY_DAMAGING), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(25, "Manganese", TRANSITION_METAL, 1519, 2334, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(26, "Iron", TRANSITION_METAL, 1811, 3134, false, 0, 0));
		addElement(new Element(27, "Cobalt", TRANSITION_METAL, 1768, 3200, false, 0, 0).setThreats(createArray(HEALTH_HAZARD), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(28, "Nickel", TRANSITION_METAL, 1728, 3186, false, 0, 0).setThreats(new EnumThreat[]{IRRITANT, HEALTH_HAZARD}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(29, "Copper", TRANSITION_METAL, 1358, 2835, false, 0, 0));
		addElement(new Element(30, "Zinc", TRANSITION_METAL, 693, 1180, false, 0, 0));
		addElement(new Element(31, "Gallium", POST_TRANSITION_METAL, 303, 2477, false, 0, 0).setThreats(createArray(CORROSIVE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(32, "Germanium", METALLOID, 1211, 1721, false, 0, 0).setThreats(new EnumThreat[]{IRRITANT, FLAMMABLE}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(33, "Arsenic", METALLOID, 887, 1090, false, 0, 0).setThreats(new EnumThreat[]{TOXIC, ENVIRONMENTALLY_DAMAGING}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(34, "Selenium", OTHER_NON_METAL, 494, 958, false, 0, 0).setThreats(new EnumThreat[]{TOXIC, HEALTH_HAZARD}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(35, "Bromine", HALOGEN, 266, 332, false, 0, 0).setThreats(new EnumThreat[]{CORROSIVE, TOXIC, ENVIRONMENTALLY_DAMAGING}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(36, "Krypton", NOBLE_GAS, 116, 120, false, 0, 0).setThreats(createArray(COMPRESSED_GAS), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(37, "Rubidium", ALKALI_METAL, 312, 961, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, CORROSIVE}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(38, "Strontium", ALKALINE_EARTH_METAL, 1050, 1650, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, IRRITANT}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(39, "Yttrium", TRANSITION_METAL, 1799, 3609, false, 0, 0));
		addElement(new Element(40, "Zirconium", TRANSITION_METAL, 2128, 4682, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(41, "Niobium", TRANSITION_METAL, 2750, 5017, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(42, "Molybdenum", TRANSITION_METAL, 2896, 4912, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(43, "Technetium", TRANSITION_METAL, 2430, 4538, true, 191523, 44).setThreats(createArray(RADIOACTIVE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(44, "Ruthenium", TRANSITION_METAL, 2607, 4423, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(45, "Rhodium", TRANSITION_METAL, 2237, 3968, false, 0, 0).setThreats(createArray(FLAMMABLE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(46, "Palladium", TRANSITION_METAL,  1828, 3236, false, 0, 0));
		addElement(new Element(47, "Silver", TRANSITION_METAL, 1235, 2435, false, 0, 0).setThreats(createArray(IRRITANT), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(48, "Cadmium", TRANSITION_METAL, 594, 1040, false, 0, 0).setThreats(new EnumThreat[]{TOXIC, HEALTH_HAZARD, ENVIRONMENTALLY_DAMAGING}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(49, "Indium", POST_TRANSITION_METAL, 430, 2345, false, 0, 0).setThreats(createArray(IRRITANT), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(50, "Tin", POST_TRANSITION_METAL, 505, 2875, false, 0, 0).setThreats(createArray(IRRITANT), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(51, "Antimony", METALLOID, 904, 1860, false, 0, 0));
		addElement(new Element(52, "Tellurium", METALLOID, 723, 1261, false, 0, 0));
		addElement(new Element(53, "Iodine", HALOGEN, 387, 457, false, 0, 0));
		addElement(new Element(54, "Xenon", NOBLE_GAS, 161, 165, false, 0, 0));
		addElement(new Element(55, "Caesium", ALKALI_METAL, 302, 944, false, 0, 0));
		addElement(new Element(56, "Barium", ALKALINE_EARTH_METAL, 1000, 2118, false, 0, 0));
		addElement(new Element(57, "Lanthanum", RARE_EARTH_METAL, 1193, 3737, false, 0, 0));
		addElement(new Element(58, "Cerium", RARE_EARTH_METAL, 1068, 3716, false, 0, 0));
		addElement(new Element(59, "Praseodymium", RARE_EARTH_METAL, 1208, 3793, false, 0, 0));
		addElement(new Element(60, "Neodymium", RARE_EARTH_METAL, 1297, 3347, false, 0, 0));
		addElement(new Element(61, "Promethium", RARE_EARTH_METAL, 1315, 3273, true, 95817, 60).setThreats(createArray(RADIOACTIVE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(62, "Samarium", RARE_EARTH_METAL, 1345, 2067, false, 0, 0).setThreats(new EnumThreat[]{FLAMMABLE, HEALTH_HAZARD}, SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(63, "Europium", RARE_EARTH_METAL, 1099, 1802, false, 0, 0));
		addElement(new Element(64, "Gadolinium", RARE_EARTH_METAL, 1585, 3546, false, 0, 0));
		//TODO Texture (silvery white)
		addElement(new Element(65, "Terbium", RARE_EARTH_METAL, 1629, 3396, false, 0, 0));
		//TODO Texture (silvery white, grayish)
		addElement(new Element(66, "Dysprosium", RARE_EARTH_METAL, 1680, 2840, false, 0, 0));
		//TODO Texture (silvery white)
		addElement(new Element(67, "Holmium", RARE_EARTH_METAL, 1734, 2873, false, 0, 0));
		//TODO Texture (silvery white)
		addElement(new Element(68, "Erbium", RARE_EARTH_METAL, 1802, 3141, false, 0, 0));
		//TODO Texture (silvery gray)
		addElement(new Element(69, "Thulium", RARE_EARTH_METAL, 1818, 2003, false, 0, 0));
		//TODO Texture (silvery white)
		addElement(new Element(70, "Ytterbium", RARE_EARTH_METAL, 1097, 1469, false, 0, 0));
		//TODO Texture (silvery white)
		addElement(new Element(71, "Lutetium", RARE_EARTH_METAL, 1925, 3675, false, 0, 0));
		//TODO Texture (steel gray)
		addElement(new Element(72, "Hafnium", RARE_EARTH_METAL, 2506, 4876, false, 0, 0));
		//TODO Moar Elements
		addElement(new Element(79, "Gold", TRANSITION_METAL, 1337, 3129, false, 0, 0));
		addElement(new Element(82, "Lead", POST_TRANSITION_METAL, 601, 2022, false, 0, 0));
		addElement(new Element(84, "Polonium", POST_TRANSITION_METAL, 527, 1235, true, 651627, 82).setThreats(createArray(RADIOACTIVE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(86, "Radon", NOBLE_GAS, 202, 211, true, 305125, 84).setThreats(createArray(RADIOACTIVE), SOLID, LIQUID, GAS, PLASMA));
		addElement(new Element(255, "Debuggium", UNKOWN_PROPERTIES, 42, 1337, true, 1000, 79).setThreats(new EnumThreat[]{RADIOACTIVE, EXPLOSIVE, FLAMMABLE, TOXIC, IRRITANT, COMPRESSED_GAS, CORROSIVE, HEALTH_HAZARD, OXYDIZING, ENVIRONMENTALLY_DAMAGING}, SOLID, LIQUID, GAS, PLASMA));

		CraftmistryAPI.Chemistry.addCompound(new Compound(0, "sulfuricAcid", "H2SO4", "SulfuricAcid").setThreats(createArray(CORROSIVE)));
		addCompound(new Compound(1, "tricalciumPhosphate", "Ca3O8P2", "TricalciumPhosphate"));
		addCompound(new Compound(2, "water", "H2O", "Water"));
		addCompound(new Compound(3, "potassiumNitrate", "KNO3", "PotassiumNitrate").setThreats(createArray(OXYDIZING)));
		addCompound(new Compound(4, "sodiumChloride", "NaCl", "SodiumChloride"));
		addCompound(new Compound(5, "steel", "Fe3C", "Steel"));
		addCompound(new Compound(6, "potassiumChloride", "KCl", "PotassiumChloride"));
	}

	public static String getElementName(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : "" + elements[atomNumber].getName();
	}

	public static String getNormalName(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : "" + elements[atomNumber].getNormalName();
	}

	public static EnumElementType getType(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : elements[atomNumber].getType();
	}

	public static int getMeltingPoint(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? 0 : elements[atomNumber].getMeltingPoint();
	}

	public static int getBoilingPoint(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? 0 : elements[atomNumber].getBoilingPoint();
	}

	public static EnumMatterState[] getPossibleStates(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : elements[atomNumber].getPossibleStates();
	}

	public static boolean isElementRadioactive(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? false : elements[atomNumber].isRadioactive();
	}

	public static int getHalfLife(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? 0 : elements[atomNumber].getHalfLife();
	}

	public static int getDecayMeta(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? 0 : elements[atomNumber].getDecayMeta();
	}

	public static ItemStack getDecayProduct(int atomNumber, ItemStack stack)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : elements[atomNumber].getDecayProduct(stack);
	}

	public static EnumThreat[] getElementThreats(int atomNumber, EnumMatterState state)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : elements[atomNumber].getThreats(state);
	}

	public static String getElementModId(int atomNumber)
	{
		return atomNumber >= elements.length || atomNumber < 0 || elements[atomNumber] == null ? null : elements[atomNumber].getModId();
	}

	public static String getCompoundName(int id)
	{
		return id >= compounds.length || id < 0 || compounds[id] == null ? null : compounds[id].getName();
	}

	public static String getCompoundFormula(int id)
	{
		return id >= compounds.length || id < 0 || compounds[id] == null ? null : compounds[id].getFormula();
	}

	public static String getCompoundTextureName(int id)
	{
		return id >= compounds.length || id < 0 || compounds[id] == null ? null : compounds[id].getTextureName();
	}

	public static EnumThreat[] getCompoundThreats(int id)
	{
		return id >= compounds.length || id < 0 || compounds[id] == null ? null : compounds[id].getThreats();
	}

	public static String getCompoundModId(int id)
	{
		return id >= compounds.length || id < 0 || compounds[id] == null ? null : compounds[id].getModID();
	}

	public static ArrayList<Integer> getValidElements()
	{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for(int i = 0; i < elements.length; ++i)
		{
			if(elements[i] != null)
			{
				ret.add(i);
			}
		}
		return ret;
	}

	public static ArrayList<Integer> getValidCompounds()
	{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for(int i = 0; i < compounds.length; ++i)
		{
			if(compounds[i] != null)
			{
				ret.add(i);
			}
		}
		return ret;
	}

	private static EnumThreat[] createArray(EnumThreat threat)
	{
		return new EnumThreat[]{threat};
	}

}
