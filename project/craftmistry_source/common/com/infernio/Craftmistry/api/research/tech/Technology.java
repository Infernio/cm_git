package com.infernio.Craftmistry.api.research.tech;

import com.infernio.Craftmistry.api.research.PanelProperties;

import net.minecraft.world.World;

/**
 * This class describes a technology that can be researched.
 * @author Infernio
 */
public class Technology {

	public String name;
	public int level;
	public String classID;
	public Technology[] requirements;
	public Class<? extends Technology> techClass;

	public Technology(String name, int level, Technology[] requirements)
	{
		this(name, level, "default", Technology.class, requirements);
	}

	public Technology(String name, int level, String classID, Class<? extends Technology> clazz, Technology[] requirements)
	{
		this.name = name;
		this.level = level;
		this.requirements = requirements;
		this.classID = classID;
		this.techClass = clazz;
	}

	public void apply(World world, PanelProperties panel) {}

}
