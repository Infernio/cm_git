package com.infernio.Craftmistry.api.research;

import com.infernio.Craftmistry.api.research.tech.Technology;

import net.minecraft.item.ItemStack;

public interface IResearchRegistry {

	int calculatePrimary(ItemStack stack);

	int calculateSecondary(ItemStack stack);

	void registerWeights(ItemStack stack, int weight1, int weight2);

	void addProduct(ItemStack stack, EnumProjectSize minimum, int weight1, int weight2);

	void registerTechnology(Technology tech, EnumProjectSize minimum, int weight1, int weight2);

	ItemStack getProductFor(int x, int y, EnumProjectSize size);

	Technology getTechnologyFor(int x, int y, EnumProjectSize size);

	Class<? extends Technology> findClassFromString(String classID);

}
