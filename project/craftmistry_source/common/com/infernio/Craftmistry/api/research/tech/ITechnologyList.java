package com.infernio.Craftmistry.api.research.tech;

import java.util.List;

import com.infernio.Craftmistry.api.CraftmistryAPI.Research;

/**
 * Allows access to every Tech that has been added to this list.<br>
 * By default, every Tech from vanilla Craftmistry is registered here and you can register your own
 * using {@link #addTechnology(Technology)} (That's optional).
 * @author Infernio
 * @see {@link Research#techList}
 */
public interface ITechnologyList {

	/**
	 * Get every Tech that has been added to this list.
	 * @return Every Tech that has been added to this list.
	 */
	public List<Technology> getTechnologies();

	/**
	 * Get a Tech from it's name.
	 * @param tech The name of the Tech to find.
	 * @return The tech that has been added with that name, or null if no Tech was found.
	 */
	public Technology getTechnology(String tech);

	/**
	 * Add a Tech to this Tech list. It can afterwards be accessed using {@link #getTechnologies()} and {@link #getTechnology(String)}.
	 * @param tech The Tech to add to this list.
	 */
	public void addTechnology(Technology tech);

}
