package com.infernio.Craftmistry.api.research;

/**
 * This class describes the size of a project.<br>
 * Bigger projects require more time, power and resources but can unlock higher tiers of research.
 * @author Infernio
 */
public enum EnumProjectSize {

	/** Tiny project size. 10 minutes real-life time. */
	TINY(12000),
	/** Small project size. 15 minutes real-life time. */
	SMALL(18000),
	/** Normal project size. 20 minutes real-life time. */
	MEDIUM(24000),
	/** Big project size. Half an hour real-life time. */
	BIG(36000),
	/** Huge Project size. One hour real-life time. */
	HUGE(72000),
	/** This is only used when reading projects from NBT. */
	UNKNOWN(0);

	/** The number of ticks that have to pass until the project is finished. */
	public final long progressFinished;

	private EnumProjectSize(long progress)
	{
		progressFinished = progress;
	}

}
