package com.infernio.Craftmistry.api.research;

import net.minecraft.nbt.NBTTagCompound;

/**
 * This class describes a Research Panel's properties.<br>
 * You can modify a panel's properties by overriding {@linkplain com.infernio.Craftmistry.api.research.tech.Technology#apply(net.minecraft.world.World, PanelProperties) apply()} in your Tech.
 * @author Infernio
 */
public class PanelProperties {

	/** The amount of EU / MJ required to run a single tick. This number is multiplied by the number of projects currently running. */
	public int powerConsumption;
	/** The number of slots that you can use to select items in the Item Selection Screen. */
	public int selectionSize;

	public PanelProperties() {}

	/**
	 * Read these properties from NBT.
	 * @param tag An NBT Tag.
	 */
	public void readFromNBT(NBTTagCompound tag)
	{
		this.powerConsumption = tag.getShort("PowerConsumption");
		this.selectionSize = tag.getShort("SelectionSize");
	}

	/**
	 * Write these properties to NBT.
	 * @param tag An NBT Tag.
	 */
	public void writeToNBT(NBTTagCompound tag)
	{
		tag.setShort("PowerConsumption", (short)this.powerConsumption);
		tag.setShort("SelectionSize", (short)this.selectionSize);
	}

}
