package com.infernio.Craftmistry.api.item;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Allows you to add custom lines of information to items in the Analyzer.
 * <br>This class exists on both server and client, <b>however, the methods in here are all client-only.</b>
 * @author Infernio
 * @see NormalInformation
 */
public interface ISpecialInfo {

	/**
	 * Return the special information on the specified page.
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @param page The page the user has currently open.
	 * @return The special information the Analyzer GUI should display on that page.
	 */
	@SideOnly(Side.CLIENT)
	String[] getSpecialInfo(ItemStack stack, int page);

	/**
	 * Whether or not page 0 will contain the default info (smelting, durability etc.)
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @return True if the Analyzer should show the default information.
	 */
	@SideOnly(Side.CLIENT)
	boolean printDefaultInfo(ItemStack stack);

	
	/**
	 * Add special effects to the page containing the information.<br>
	 * Called by drawScreen().
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @param fontRenderer A FontRenderer instance.
	 * @param x The current x position that the mouse it at.
	 * @param y The current y position that the mouse it at.
	 * @param page The page the user has currently open.
	 * @param guiLeft (this.width - this.xSize) / 2
	 * @param guiTop (this.height - this.ySize) / 2
	 * @param guiHeight Height of the Analyzer GUI.
	 * @param guiWidth Width of the Analyzer GUI.
	 */
	@SideOnly(Side.CLIENT)
	void addToScreen(ItemStack stack, FontRenderer fontRenderer, int x, int y, int page, int guiLeft, int guiTop, int guiHeight, int guiWidth);

	/**
	 * Add special effects to the page containing the information.<br>
	 * Called by drawGuiContainerBackgroundLayer.
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @param mc The current Minecraft instance.
	 * @param fontRenderer A FontRenderer instance.
	 * @param itemRenderer A RenderItem instance.
	 * @param page The page the user has currently open.
	 * @param zLevel The current GUI zLevel.
	 * @param guiLeft (this.width - this.xSize) / 2
	 * @param guiTop (this.height - this.ySize) / 2
	 */
	@SideOnly(Side.CLIENT)
	void addSpecialEffects(ItemStack stack, Minecraft mc, FontRenderer fontRenderer, RenderItem itemRenderer, int page, float zLevel, int guiLeft, int guiTop);

	/**
	 * How many pages the user can open.
	 * @param An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @return The number of pages that the ISpecialInfo will be displayed on.
	 */
	@SideOnly(Side.CLIENT)
	int getNumberPages(ItemStack stack);

}
