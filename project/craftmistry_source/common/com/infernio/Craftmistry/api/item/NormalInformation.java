package com.infernio.Craftmistry.api.item;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;

import com.infernio.Craftmistry.api.CraftmistryAPI;

/**
 * An easy way of adding information to items in the analyzer. This is meant as a convenience class for those who don't want
 * any special effects or NBT-related information, so that they don't have to write a custom implementation.
 * Don't forget to register the information using {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI.Items#addItemInformation(int, int, ISpecialInfo) CraftmistryAPI.addItemInformation()} though.
 * @author Infernio
 */
public class NormalInformation implements ISpecialInfo {

	private ArrayList<List<String>> information;
	private boolean printDefault;
	private int pages;
	
	/**
	 * An easy way of adding information to items in the analyzer.<br>
	 * Some useful parameters: (For formatting help see the
	 * {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI.Items#addItemInformation(int, int, ISpecialInfo) Registration Method}).
	 * <br>
	 * <b>%s</b> - Split the information into two lines at the point where this is added.
	 * <br>Example: "This is a%stest" will result in:
	 * <br>This is a
	 * <br>test
	 * <br><b>%a</b> - Make a paragraph at that line. Place wood in the analyzer to see them (Look between the burn time and 'Smelting product:').
	 * <br><b>%p</b> - The following lines will be displayed on the corresponding page. Must have a number behind it:
	 * <br>NOT %pThis is a test, BUT %p2This is a test
	 * @param info The information to add.
	 */
	public NormalInformation(String info)
	{
		this(info, false, 0);
	}
	
	/**
	 * An easy way of adding information to items in the analyzer.<br>
	 * Some useful parameters: (For formatting help see the
	 * {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI.Items#addItemInformation(int, int, ISpecialInfo) Registration Method}).
	 * <br>
	 * <b>%s</b> - Split the information into two lines at the point where this is added.
	 * <br>Example: "This is a%stest" will result in:
	 * <br>This is a
	 * <br>test
	 * <br><b>%a</b> - Make a paragraph at that line. Place wood in the analyzer to see them (Look between the burn time and 'Smelting product:').
	 * <br><b>%p</b> - The following lines will be displayed on the corresponding page. Must have a number behind it:
	 * <br>NOT %pThis is a test, BUT %p2This is a test
	 * @param info The information to add.
	 * @param printDefaultInfo printDefaultInfo Whether or not the analyzer is supposed to show 'default' information (Burn time, etc.).
	 */
	public NormalInformation(String info, boolean printDefaultInfo)
	{
		this(info, printDefaultInfo, 0);
	}
	
	/**
	 * An easy way of adding information to items in the analyzer.<br>
	 * Some useful parameters: (For formatting help see the
	 * {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI.Items#addItemInformation(int, int, ISpecialInfo) Registration Method}).
	 * <br>
	 * <b>%s</b> - Split the information into two lines at the point where this is added.
	 * <br>Example: "This is a%stest" will result in:
	 * <br>This is a
	 * <br>test
	 * <br><b>%a</b> - Make a paragraph at that line. Place wood in the analyzer to see them (Look between the burn time and 'Smelting product:').
	 * <br><b>%p</b> - The following lines will be displayed on the corresponding page. Must have a number behind it:
	 * <br>NOT %pThis is a test, BUT %p2This is a test
	 * @param info The information to add.
	 * @param printDefaultInfo Whether or not the analyzer is supposed to show 'default' information (Burn time, etc.).
	 * @param pages How many pages we should use
	 */
	public NormalInformation(String info, boolean printDefaultInfo, int pages)
	{
		String[] information = info.split("%s");
		this.information = new ArrayList<List<String>>();
		int num = 0;
		for(String s : information)
		{
			if(s.startsWith("%p"))
			{
				s = s.substring(2);
				try
				{
					String s2 = String.valueOf(s.charAt(0));
					num = Integer.valueOf(s2).intValue();
				}
				catch(Throwable e)
				{
					CraftmistryAPI.getLogger().warn("Attempted to register invalid item information!");
					e.printStackTrace();
				}
				s = s.substring(1);
			}
			for(int i = 0; i < num; i++)
			{
				if(this.information.size() < num)
				{
					this.information.add(new ArrayList<String>());
				}
			}
			List<String> list = this.information.get(num - 1);
			list.add(s);
		}
		this.printDefault = printDefaultInfo;
		this.pages = pages;
	}
	
	/**
	 * Return the special information on the specified page.
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @param page The page the user has currently open.
	 * @return The special information the Analyzer GUI should display on that page.
	 */
	@Override
	public String[] getSpecialInfo(ItemStack stack, int page)
	{
		List<String> info = this.information.get(MathHelper.clamp_int(page, 0, this.information.size() - 1));
		return info.toArray(new String[info.size()]);
	}

	/**
	 * Whether or not page 0 will contain the default info (smelting, durability etc.)
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @return True if the Analyzer should show the default information.
	 */
	@Override
	public boolean printDefaultInfo(ItemStack stack)
	{
		return this.printDefault;
	}

	/**
	 * Add special effects to the page containing the information.<br>
	 * Called by drawScreen().
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @param fontRenderer A FontRenderer instance.
	 * @param x The current x position that the mouse it at.
	 * @param y The current y position that the mouse it at.
	 * @param page The page the user has currently open.
	 * @param guiLeft (this.width - this.xSize) / 2
	 * @param guiTop (this.height - this.ySize) / 2
	 * @param guiHeight Height of the Analyzer GUI.
	 * @param guiWidth Width of the Analyzer GUI.
	 */
	@Override
	public void addToScreen(ItemStack stack, FontRenderer fontRenderer, int x, int y, int page, int guiLeft, int guiTop, int guiHeight, int guiWidth) {}

	/**
	 * Add special effects to the page containing the information.
	 * @param stack An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @param mc The current Minecraft instance.
	 * @param fontRenderer A FontRenderer instance.
	 * @param itemRenderer A RenderItem instance.
	 * @param page The page the user has currently open.
	 * @param zLevel The current GUI zLevel.
	 * @param guiLeft (this.width - this.xSize) / 2
	 * @param guiTop (this.height - this.ySize) / 2
	 */
	@Override
	public void addSpecialEffects(ItemStack stack, Minecraft mc, FontRenderer fontRenderer, RenderItem itemRenderer, int page, float zLevel, int guiLeft, int guiTop) {}

	/**
	 * How many pages the user can open.
	 * @param An ItemStack representation of the item, so you can have information changed on NBT etc.
	 * @return The number of pages that the ISpecialInfo will be displayed on.
	 */
	@Override
	public int getNumberPages(ItemStack stack)
	{
		return this.pages;
	}

}
