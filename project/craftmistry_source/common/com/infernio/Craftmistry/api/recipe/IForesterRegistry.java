package com.infernio.Craftmistry.api.recipe;

import java.util.List;

import com.infernio.Craftmistry.api.tile.ITreeHandler;

import net.minecraft.item.ItemStack;

/**
 * A registry interface for the forester.
 * <br>For every tree registered, a new node that stores name, saplings, leaves and wood is created.
 * Note that multiple saplings, leaves and wood can be registered per node by simply supplying the
 * methods below with already registered names.
 * <br><br>You can use {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI.Recipes#foresterRegistry this API object} to
 * register custom trees.
 * @author Infernio
 */
public interface IForesterRegistry
{
	/**
	 * Register a full tree with a name, sapling, a leaf block and a wood block.
	 * <br>Note that if the name is already registered, the registry will instead
	 * register sapling, leaves and wood on the already existing node if they have
	 * not yet been registered.
	 * @param name The name for this tree. Does not have to be user-friendly.
	 * Note that you do need to have a localization for this name, formatted like this:
	 * <code>forester.tree.TREE_NAME.name=USER_FRIENDLY_NAME</code>
	 * @param sapling The sapling to register for this tree. Trees can have multiple saplings.
	 * @param leaves The leaves to register for this tree. Trees can have multiple leaves.
	 * @param wood The wood to register for this tree. Trees can have multiple wood blocks.
	 */
	void registerTree(String name, ItemStack sapling, ItemStack leaves, ItemStack wood);

	/**
	 * Register a full tree with a name, sapling, a leaf block and a wood block.
	 * <br>Note that if the name is already registered, the registry will instead
	 * register sapling, leaves and wood on the already existing node if they have
	 * not yet been registered.
	 * <br><br>This method allows you to register multiple saplings / leaves / wood at a time.
	 * @param name The name for this tree. Does not have to be user-friendly.
	 * Note that you do need to have a localization for this name, formatted like this:
	 * <code>forester.tree.TREE_NAME.name=USER_FRIENDLY_NAME</code>
	 * @param saplings The saplings to register for this tree.
	 * @param leaves The leaves to register for this tree.
	 * @param wood The wood to register for this tree.
	 */
	void registerTree(String name, ItemStack[] saplings, ItemStack[] leaves, ItemStack... wood);

	/**
	 * Register these saplings for the specified treeName. If a node with this name
	 * does not exist, it will be created.
	 * <br>Check out {@link #registerTree(String, ItemStack, ItemStack, ItemStack)}
	 * for an in-depth explanation of how the registry works.
	 * @param name The name for this tree.
	 * @param saplings The saplings to register for this tree.
	 */
	void registerSaplings(String name, ItemStack... saplings);

	/**
	 * Register these leaves for the specified treeName. If a node with this name
	 * does not exist, it will be created.
	 * <br>Check out {@link #registerTree(String, ItemStack, ItemStack, ItemStack)}
	 * for an in-depth explanation of how the registry works.
	 * @param name The name for this tree.
	 * @param leaves The leaves to register for this tree.
	 */
	void registerLeaves(String name, ItemStack... leaves);

	/**
	 * Register these wood blocks for the specified treeName. If a node with this name
	 * does not exist, it will be created.
	 * <br>Check out {@link #registerTree(String, ItemStack, ItemStack, ItemStack)}
	 * for an in-depth explanation of how the registry works.
	 * @param name The name for this tree.
	 * @param wood The wood blocks to register for this tree.
	 */
	void registerWood(String name, ItemStack... wood);

	/**
	 * Register a special tree handler for the specfied tree.
	 * <br>See {@link ITreeHandler} for more information.
	 * @param tree The tree to register this handler for.
	 * @param handler The handler to register.
	 */
	void registerTreeHandler(String tree, ITreeHandler handler);

	List<ItemStack> getSaplings(String name);

	List<ItemStack> getLeaves(String name);

	List<ItemStack> getWood(String name);
}
