package com.infernio.Craftmistry.api.recipe;

import java.util.Arrays;
import java.util.Comparator;

import com.infernio.Craftmistry.api.HashUtils;
import com.infernio.InfernoCore.util.ItemUtils;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * This class is a hash-conform collection of ItemStack objects.
 * @author Infernio
 */
public class ItemCollection {

	/** An ItemStack comparator for sorting the input. */
	private static final ItemStackComparator stackComparator = new ItemStackComparator();

	/** The input ItemStacks. */
	private final ItemStack[] input;
	/** The sorted input ItemStacks. The input array gets sorted when creating, since this class is immutable. This is used for equals(). */
	private final ItemStack[] sortedInput;
	/** The hash code of this class. Since this class is immutable, calculating the hash immediately and then just returning it gives way better performance. */
	private final int hash;

	/**
	 * Construct a new ReactorInput object that stores the given ItemStacks.
	 * @param input The ItemStacks to store.
	 */
	public ItemCollection(ItemStack... input)
	{
		if(input == null)
		{
			input = new ItemStack[0];
		}
		this.input = input;
		this.sortedInput = new ItemStack[input.length];
		System.arraycopy(this.input, 0, this.sortedInput, 0, this.input.length);
		Arrays.sort(this.sortedInput, stackComparator);
		this.hash = this.computeHash();
	}

	/**
	 * Computes the hash code for this object. A lot of XOR is done here,
	 * and since this class is immutable anyways, it's better
	 * to just compute the hash once when the object is created
	 * rather than calculating it over and over again.
	 * @return The hash code for this class.
	 */
	private int computeHash()
	{
		int[] hashes = new int[this.input.length];
		for(int i = 0; i < this.input.length; ++i)
		{
			hashes[i] = HashUtils.hashItemStack(this.input[i]);
		}
		return HashUtils.combineHashes(hashes);
	}

	/**
	 * Return all the ItemStacks stored in this collection.
	 * @return All the ItemStacks stored in this collection.
	 */
	public ItemStack[] getItems()
	{
		return this.input;
	}

	/**
	 * This implementation relies on this class being immutable.
	 * <br>Since the fields within this class cannot be modifed once constructed,
	 * the hash is computed once and then simply returned here.
	 * <br>See {@linkplain Object#hashCode() hashCode from Object} for information on hashCode().
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return this.hash;
	}

	/**
	 * This implementation relies on a sorted array that contains
	 * the input ItemStacks.
	 * <br>See {@linkplain Object#equals(Object) equals from Object} for information on equals().
	 */
	@Override
	public boolean equals(Object other)
	{
		if(other instanceof ItemCollection)
		{
			ItemCollection input1 = (ItemCollection)other;
			for(int i = 0; i < this.sortedInput.length; ++i)
			{
				if(!ItemUtils.areItemStacksEqual(this.sortedInput[i], input1.sortedInput[i], true))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * A simple ItemStack comparator.
	 * @author Infernio
	 */
	private static final class ItemStackComparator implements Comparator<ItemStack> {

		//See Comparator.compare()
		@Override
		public int compare(ItemStack stack1, ItemStack stack2)
		{
			int ids = Item.itemRegistry.getNameForObject(stack1.getItem()).compareTo(Item.itemRegistry.getNameForObject(stack2.getItem()));
			if(ids == 0)
			{
				int meta1 = stack1.getItemDamage();
				int meta2 = stack2.getItemDamage();
				if(meta1 == meta2)
				{
					int size1 = stack1.stackSize;
					int size2 = stack2.stackSize;
					return size1 == size2 ? 0 : (size1 < size2 ? -1 : 1);
				}
				return meta1 < meta2 ? -1 : 1;
			}
			return ids;
		}

	}

}
