package com.infernio.Craftmistry.api.plasmatic;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

/**
 * This class marks a node point for the builder.
 * @author Infernio
 */
public abstract class BuildNode {

	/**
	 * Apply this node point at the specified coordinates.
	 * @param world Current world instance.
	 * @param x X coordinate.
	 * @param y Y coordinate.
	 * @param z Z coordinate.
	 */
	public abstract void apply(World world, int x, int y, int z);

	/**
	 * This method should return a unique identifier for this BuildNode class.<br>
	 * Only one identifier must be returned per class and this identifier must <strong>not</strong> change <strong>even after a restart of Minecraft</strong>,
	 * as it is actually used to recreate a BuildNode from NBT (see INodeBuilder and BuilderRegistry).<br>
	 * For an overview of the Plasmatic API and the Builder API, see plasmaticAPI.txt.
	 * @see INodeBuilder
	 * @see BuilderRegistry
	 * @return A unique identifier for this BuildNode class.
	 */
	public abstract String getIdentifier();

	/**
	 * Write this node to NBT.<br>
	 * Must be overwritten if this node has special information on it (a block ID, for example),
	 * that should persist over the restart of Minecraft.
	 * @param tag An NBT Tag Compound.
	 */
	public void writeToNBT(NBTTagCompound tag) {}

	/**
	 * Read this node from NBT.<br>
	 * Must be overwritten if this node has special information on it (a block ID, for example),
	 * that should persist over the restart of Minecraft.
	 * @param tag An NBT Tag Compound.
	 */
	public void readFromNBT(NBTTagCompound tag) {}

}
