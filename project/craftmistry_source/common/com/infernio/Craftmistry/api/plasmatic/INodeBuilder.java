package com.infernio.Craftmistry.api.plasmatic;

public interface INodeBuilder {

	public BuildNode buildNode(String identifier);

}
