package com.infernio.Craftmistry.api.plasmatic;

import java.util.ArrayList;
import java.util.HashMap;

public class BuilderRegistry {

	private static HashMap<String, INodeBuilder> nodeRegistry = new HashMap<String, INodeBuilder>();
	private static HashMap<String, INodeCreator> creatorRegistry = new HashMap<String, INodeCreator>();
	private static ArrayList<BuildNode> nodes = new ArrayList<BuildNode>();

	public static void registerNodeBuilder(String identifier, INodeBuilder builder)
	{
		if(!nodeRegistry.containsKey(identifier))
		{
			nodeRegistry.put(identifier, builder);
		}
	}

	public static void registerNodeCreator(String identifier, INodeCreator creator)
	{
		if(!creatorRegistry.containsKey(identifier))
		{
			creatorRegistry.put(identifier, creator);
		}
	}

	public static INodeBuilder getBuilder(String identifier)
	{
		return nodeRegistry.get(identifier);
	}

	public static INodeCreator getCreator(String identifier)
	{
		return creatorRegistry.get(identifier);
	}

	public static void registerNode(BuildNode node)
	{
		if(!nodes.contains(node))
		{
			nodes.add(node);
		}
	}

	public static ArrayList<BuildNode> getNodes()
	{
		return new ArrayList<BuildNode>(nodes);
	}

}
