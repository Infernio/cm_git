package com.infernio.Craftmistry.api.plasmatic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * An ArrayList to which elements can only be added.<br>
 * Iterations are <strong>not</strong> possible either.<br>
 * Retrieving elements is only possible through {@link #get(int)}.
 * @author Infernio
 */
public final class NodeList extends ArrayList<BuildNode> {

	/** Serial version UID, only here to prevent warnings. */
	private static final long serialVersionUID = 1L;

	@Override
	public BuildNode set(int index, BuildNode element)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public BuildNode remove(int index)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object element)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> collection)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> collection)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void removeRange(int from, int to)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Iterator<BuildNode> iterator()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<BuildNode> listIterator()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public ListIterator<BuildNode> listIterator(int index)
	{
		throw new UnsupportedOperationException();
	}

}
