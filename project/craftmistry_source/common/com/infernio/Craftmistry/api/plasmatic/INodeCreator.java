package com.infernio.Craftmistry.api.plasmatic;

import net.minecraft.world.World;

public interface INodeCreator {

	void createNode(String identifier, NodeList list, World world, int x, int y, int z);

}
