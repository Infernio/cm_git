package com.infernio.Craftmistry.api.chemistry;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import net.minecraft.item.ItemStack;

/**
 * This class describes the properties and the behavior of an element.
 * It stores data like states of matter, Half-Life, Element type, Symbol, Name etc.
 * This also includes the threats that this element poses
 * Using the {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI.Chemistry#addElement(Element) Craftmistry API},
 * you can make custom Elements.
 * @author Infernio
 */
public final class Element {

	/** 
	 * The atomic number of this Element (meta value). You can register a total of 256 elements.
	 * IDs in use:
	 * 0 - 118 and 255.
	 */
	private int id;
	/** The name of this Element */
	private String name;
	/** The name without lower casing - used when registering icons. */
	private String normalName;
	/** The Element type */
	private EnumElementType type;
	/** The possible states of matter */
	private EnumMatterState[] states;
	/** The Half-Life of this Element */
	private int halfLife;
	/** Whether or not this Element is radioactive */
	private boolean radioactive;
	/** The Meta into which this element decays */
	private int decayMeta;
	/** The temperature (in kelvin) at which this element melts */
	private int meltingPoint;
	/** The temperature (in kelvin) at which this element boils */
	private int boilingPoint;
	/** The Threats that this element poses while in solid state of matter */
	private EnumThreat[] solidThreats;
	/** The Threats that this element poses while in liquid state of matter */
	private EnumThreat[] liquidThreats;
	/** The Threats that this element poses while in gas state of matter */
	private EnumThreat[] gasThreats;
	/** The Threats that this element poses while in plasma state of matter */
	private EnumThreat[] plasmaThreats;
	/** (optional) A mod id to use when registering the element icon. The registered icon will be: <code>assets\MODID\textures\items\itemTestTubeELEMENTNAME.png</code> */
	private String modid;

	/**
	 * Create a new Element.
	 * <br>Make certain that you have a key called 'element.ELEMENT_NAME.symbol' in a language file, as the element symbol will otherwise not show up.
	 * @param atomicNumber The atomic number of this Element (meta value). You can register a total of 256 elements.
	 * IDs in use:
	 * 0 - 118 and 255.
	 * @param name The name of this Element
	 * @param type The Element type
	 * @param meltingPoint The temperature (in kelvin) at which this element melts
	 * @param boilingPoint The temperature (in kelvin) at which this element boils
	 * @param radioactive Whether or not this Element is radioactive
	 * @param halfLife The Half-Life of this Element
	 * @param decayMeta The Meta Value / Atomic Number of the Element into which this element decays
	 * @param possibleStates The possible states of matter (optional). Leaving this blank will register all 4 states of matter.
	 */
	public Element(int atomicNumber, String name, EnumElementType type, int meltingPoint, int boilingPoint, boolean radioactive, int halfLife, int decayMeta, EnumMatterState... possibleStates)
	{
		this(atomicNumber, name, type, meltingPoint, boilingPoint, radioactive, halfLife, decayMeta, "craftmistry", possibleStates);
	}

	/**
	 * Create a new Element.
	 * <br>Make certain that you have a key called 'element.ELEMENT_NAME.symbol' in a language file, as the element symbol will otherwise not show up.
	 * @param atomicNumber The atomic number of this Element (meta value). You can register a total of 256 elements.
	 * IDs in use:
	 * 0 - 118 and 255.
	 * @param name The name of this Element
	 * @param type The Element type
	 * @param meltingPoint The temperature (in kelvin) at which this element melts
	 * @param boilingPoint The temperature (in kelvin) at which this element boils
	 * @param radioactive Whether or not this Element is radioactive
	 * @param halfLife The Half-Life of this Element
	 * @param decayMeta The Meta Value / Atomic Number of the Element into which this element decays
	 * @param modid (optional) A mod id to use when registering the element icon. The registered icon will be: <code>assets\MODID\textures\items\itemTestTubeELEMENTNAME.png</code>
	 * @param possibleStates The possible states of matter (optional). Leaving this blank will register all 4 states of matter.
	 */
	public Element(int atomicNumber, String name, EnumElementType type, int meltingPoint, int boilingPoint, boolean radioactive, int halfLife, int decayMeta, String modid, EnumMatterState... possibleStates)
	{
		this.id = atomicNumber;
		this.name = name.toLowerCase(Locale.ENGLISH);
		this.normalName = name;
		this.type = type;
		this.meltingPoint = meltingPoint;
		this.boilingPoint = boilingPoint;
		this.radioactive = radioactive;
		this.halfLife = halfLife;
		this.decayMeta = decayMeta;
		this.modid = modid;
		if(possibleStates == null || possibleStates.length <= 0)
		{
			possibleStates = new EnumMatterState[]{EnumMatterState.SOLID, EnumMatterState.LIQUID, EnumMatterState.GAS, EnumMatterState.PLASMA};
		}
		this.states = possibleStates;
	}

	/**
	 * A version of setThreats that accepts more than one EnumMatterState. This will just perform setThreats on all given EnumMatterStates.
	 * @param threats Threats that this element should pose. If any of these threats have already been registered, they won't be added again.
	 * @param states The states in which these threats should be active.
	 * @return This Element instance for ease in constructing.
	 */
	public Element setThreats(EnumThreat[] threats, EnumMatterState... states)
	{
		for(EnumMatterState state : states)
		{
			this.setThreats(threats, state);
		}
		return this;
	}

	/**
	 * Change the threats that this element poses in the specified state of matter.
	 * @param threats Threats that this element should pose. If any of these threats have already been registered, they won't be added again.
	 * @param state The state in which these threats should be active.
	 * @return This Element instance for ease in constructing.
	 */
	public Element setThreats(EnumThreat[] threats, EnumMatterState state)
	{
		if(threats == null || state == null)
		{
			return this;
		}
		switch(state)
		{
		case SOLID:
			if(this.solidThreats == null)
			{
				this.solidThreats = threats;
				return this;
			}
			List<EnumThreat> solidThreats = Arrays.asList(this.solidThreats);
			for(EnumThreat threat : threats)
			{
				if(!solidThreats.contains(threat))
				{
					solidThreats.add(threat);
				}
			}
			this.solidThreats = solidThreats.toArray(new EnumThreat[solidThreats.size()]);
			break;
		case LIQUID:
			if(this.liquidThreats == null)
			{
				this.liquidThreats = threats;
				return this;
			}
			List<EnumThreat> liquidThreats = Arrays.asList(this.liquidThreats);
			for(EnumThreat threat : threats)
			{
				if(!liquidThreats.contains(threat))
				{
					liquidThreats.add(threat);
				}
			}
			this.liquidThreats = liquidThreats.toArray(new EnumThreat[liquidThreats.size()]);
			break;
		case GAS:
			if(this.gasThreats == null)
			{
				this.gasThreats = threats;
				return this;
			}
			List<EnumThreat> gasThreats = Arrays.asList(this.gasThreats);
			for(EnumThreat threat : threats)
			{
				if(!gasThreats.contains(threat))
				{
					gasThreats.add(threat);
				}
			}
			this.gasThreats = gasThreats.toArray(new EnumThreat[gasThreats.size()]);
			break;
		case PLASMA:
			if(this.plasmaThreats == null)
			{
				this.plasmaThreats = threats;
				return this;
			}
			List<EnumThreat> plasmaThreats = Arrays.asList(this.plasmaThreats);
			for(EnumThreat threat : threats)
			{
				if(!plasmaThreats.contains(threat))
				{
					plasmaThreats.add(threat);
				}
			}
			this.plasmaThreats = plasmaThreats.toArray(new EnumThreat[plasmaThreats.size()]);
			break;
		default:
			break;
		}
		return this;
	}

	/**
	 * Remove this EnumThreat from the list of threats for the specified state of matter.
	 * <br>Mostly used internally.
	 * @param threat The Threat to remove.
	 * @param state The state of matter that this Element is currently in.
	 * @return This Element instance for ease in constructing.
	 */
	public Element removeThreat(EnumThreat threat, EnumMatterState state)
	{
		if(threat == null || state == null)
		{
			return this;
		}
		switch(state)
		{
		case SOLID:
			if(this.solidThreats != null)
			{
				for(int i = 0; i < this.solidThreats.length; i++)
				{
					EnumThreat threat2 = this.solidThreats[i];
					if(threat2 == threat)
					{
						this.solidThreats[i] = null;
					}
				}
			}
			break;
		case LIQUID:
			if(this.liquidThreats != null)
			{
				for(int i = 0; i < this.liquidThreats.length; i++)
				{
					EnumThreat threat2 = this.liquidThreats[i];
					if(threat2 == threat)
					{
						this.liquidThreats[i] = null;
					}
				}
			}
			break;
		case GAS:
			if(this.gasThreats != null)
			{
				for(int i = 0; i < this.gasThreats.length; i++)
				{
					EnumThreat threat2 = this.gasThreats[i];
					if(threat2 == threat)
					{
						this.gasThreats[i] = null;
					}
				}
			}
			break;
		case PLASMA:
			for(int i = 0; i < this.plasmaThreats.length; i++)
			{
				EnumThreat threat2 = this.plasmaThreats[i];
				if(threat2 == threat)
				{
					this.plasmaThreats[i] = null;
				}
			}
			break;
		default:
			break;
		}
		return this;
	}

	/**
	 * @return The atomic number.
	 */
	public int getAtomicNumber()
	{
		return this.id;
	}

	/**
	 * @return The name.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @return The name without lower casing - used when registering icons.
	 */
	public String getNormalName()
	{
		return this.normalName;
	}

	/**
	 * @return The element type.
	 */
	public EnumElementType getType()
	{
		return this.type;
	}

	/**
	 * @return The melting point.
	 */
	public int getMeltingPoint()
	{
		return this.meltingPoint;
	}

	/**
	 * @return The boiling point.
	 */
	public int getBoilingPoint()
	{
		return this.boilingPoint;
	}

	/**
	 * @return The possible states of matter.
	 */
	public EnumMatterState[] getPossibleStates()
	{
		return this.states;
	}

	/**
	 * @return The Half-Life.
	 */
	public int getHalfLife()
	{
		return this.halfLife;
	}

	/**
	 * @return Whether or not this Element is radioactive.
	 */
	public boolean isRadioactive()
	{
		return this.radioactive;
	}

	/**
	 * @param stack The current state ID of the test tube (so that radioactive liquids decay into liquids etc.)
	 * @return The ItemStack into which this element decays into. Never called if this Element is not radioactive.
	 */
	public ItemStack getDecayProduct(ItemStack stack)
	{
		return new ItemStack(stack.getItem(), 1, this.decayMeta);
	}

	/**
	 * @return The Meta into which this element decays
	 */
	public int getDecayMeta()
	{
		return this.decayMeta;
	}

	/**
	 * @return The mod id.
	 */
	public String getModId()
	{
		return this.modid;
	}

	/**
	 * @param state The state of matter this Element is currently in.
	 * @return The threats that this Element poses in this state of matter.
	 */
	public EnumThreat[] getThreats(EnumMatterState state)
	{
		switch(state)
		{
		case SOLID:
			return this.solidThreats;
		case LIQUID:
			return this.liquidThreats;
		case GAS:
			return this.gasThreats;
		case PLASMA:
			return this.plasmaThreats;
		default:
			return null;
		}
	}

}
