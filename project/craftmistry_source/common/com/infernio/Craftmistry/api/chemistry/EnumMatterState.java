package com.infernio.Craftmistry.api.chemistry;

import net.minecraft.item.Item;

import com.infernio.Craftmistry.api.CraftmistryAPI;

/**
 * The states of matter that an element can have.
 * @author Infernio
 */
public enum EnumMatterState {

	/** Solid state of matter*/
	SOLID("solid"),
	/** Liquid state of matter*/
	LIQUID("liquid"),
	/** Gas state of matter*/
	GAS("gas"),
	/** Plasma state of matter, not 100% implemented yet */
	PLASMA("plasma");

	/** The unlocalized name this state of matter has ingame. */
	public final String unlocalizedName;

	private EnumMatterState(String name)
	{
		this.unlocalizedName = name;
	}

	/**
	 * Convert this state of matter item to the corresponding EnumMatterState.
	 * @param item The state of matter item. See {@linkplain CraftmistryAPI.Chemistry here}.
	 * @return The corresponding state of matter.
	 * @see {@linkplain CraftmistryAPI.Chemistry#STATE_SOLID_ITEM Solid Item}
	 * @see {@linkplain CraftmistryAPI.Chemistry#STATE_LIQUID_ITEM Liquid Item}
	 * @see {@linkplain CraftmistryAPI.Chemistry#STATE_GAS_ITEM Gas Item}
	 * @see {@linkplain CraftmistryAPI.Chemistry#STATE_PLASMA_ITEM Plasma Item}
	 */
	public static EnumMatterState convert(Item item)
	{
		if(item == CraftmistryAPI.Chemistry.STATE_SOLID_ITEM)
		{
			return SOLID;
		}
		if(item == CraftmistryAPI.Chemistry.STATE_LIQUID_ITEM)
		{
			return LIQUID;
		}
		if(item == CraftmistryAPI.Chemistry.STATE_GAS_ITEM)
		{
			return GAS;
		}
		if(item == CraftmistryAPI.Chemistry.STATE_PLASMA_ITEM)
		{
			return PLASMA;
		}
		return null;
	}

}
