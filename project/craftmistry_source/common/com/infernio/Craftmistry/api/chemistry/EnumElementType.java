package com.infernio.Craftmistry.api.chemistry;

/**
 * The different types of elements. Examples: Lithium (Alkali Metal), Oxygen (Other Non-Metal).
 * @author Infernio
 */
public enum EnumElementType {

	/** Alakali metal type */
	ALKALI_METAL("Alkali Metal", "alkaliMetal"),
	/** Alkaline earth metal type */
	ALKALINE_EARTH_METAL("Alkaline Earth Metal", "alkalineEarthMetal1%salkalineEarthMetal2"),
	/** Rare earth metal type */
	RARE_EARTH_METAL("Rare Earth Metal", "rareEarthMetal1%srareEarthMetal2"),
	/** Actinide type */
	ACTINIDE("Actinide", "actinide"),
	/** Transition metal type */
	TRANSITION_METAL("Transition Metal", "transitionMetal1%stransitionMetal2"),
	/** Post-Transition metal type */
	POST_TRANSITION_METAL("Post-Transition Metal", "postTransitionMetal1%spostTransitionMetal2"),
	/** Other non-metal type */
	OTHER_NON_METAL("Other Non-Metal", "otherNonMetal1%sotherNonMetal2"),
	/** Metalloid type */
	METALLOID("Metalloid", "metalloid"),
	/** Halogen type */
	HALOGEN("Halogen", "halogen"),
	/** Noble Gas type */
	NOBLE_GAS("Noble Gas", "nobleGas"),
	/** Unknown properties type */
	UNKOWN_PROPERTIES("Unknown Properties", "unknownProperties1%sunknownProperties2");

	private EnumElementType(String ingameName, String formatName)
	{
		this.name = ingameName;
		this.fName = formatName;
	}

	/** The formatted name */
	private String name;
	/** The unformatted name */
	private String fName;

	/**
	 * Get the formatted ingame name of this element type.
	 * Unused so far.
	 * @return The formatted name of this element type.
	 */
	public String getIngameName()
	{
		return this.name;
	}

	/**
	 * Get the unformatted and unlocalized name of this element type.
	 * Used by the Analyzer GUI.
	 * @return The unformatted and unlocalized name of this element type.
	 */
	public String getUnformattedName()
	{
		return this.fName;
	}

}
