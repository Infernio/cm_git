package com.infernio.Craftmistry.api.chemistry;

import java.util.Arrays;
import java.util.List;

/**
 * This class describes the properties and the behavior of a Compound.
 * Just like the Element class, it stores compound data like id, name or formula.
 * Using the {@linkplain com.infernio.Craftmistry.api.CraftmistryAPI#addCompound(Compound) Craftmistry API},
 * you can make custom compounds.
 * @author Infernio
 */
public final class Compound {

	/** 
	 * The unique id (meta value) that this compound has. <b>Every id can only be assigned once!</b>
	 * You can register a total of 1024 compounds.
	 * IDs reserved for Craftmistry (do not use these if you want your mod to be upwards / backwards compatible!):
	 * 0 - 500
	 */
	private int id;
	/** The name of this compound */
	private String name;
	/** The formula of this compound */
	private String formula;
	/** The name that will be used to register the compound texture */
	private String textureName;
	/** The threats that this compound poses */
	private EnumThreat[] threats;
	/** (optional) A mod id to use when registering the compound icon. The registered icon will be: <code>assets\MODID\textures\items\itemTestTubeTEXTURENAME.png</code> */
	private String modid;

	/**
	 * Create a new Compound.
	 * @param id The unique id (meta value) that this compound has. <b>Every id can only be assigned once!</b>
	 * You can register a total of 1024 compounds.
	 * IDs reserved for Craftmistry (do not use these if you want your mod to be upwards / backwards compatible!):
	 * 0 - 500
	 * @param name The unlocalized name of this compound
	 * @param formula The formula of this compound
	 * @param textureName The name that will be used to register the compound texture
	 */
	public Compound(int id, String name, String formula, String textureName)
	{
		this(id, name, formula, textureName ,"craftmistry");
	}

	/**
	 * Create a new Compound.
	 * @param id The unique id (meta value) that this compound has. <b>Every id can only be assigned once!</b>
	 * You can register a total of 1024 compounds.
	 * IDs reserved for Craftmistry (do not use these if you want your mod to be upwards / backwards compatible!):
	 * 0 - 500
	 * @param name The unlocalized name of this compound
	 * @param formula The formula of this compound
	 * @param textureName The name that will be used to register the compound texture
	 * @param modid (optional) A mod id to use when registering the compound icon.
	 * The registered icon will be: <code>assets\MODID\textures\items\itemTestTubeTEXTURENAME.png</code>
	 */
	public Compound(int id, String name, String formula, String textureName, String modid)
	{
		this.id = id;
		this.name = name;
		this.formula = formula;
		this.textureName = textureName;
		this.modid = modid;
	}

	/**
	 * Change the threats that this compound poses.
	 * @param threats Threats that this compound should pose. If any of these threats have already been registered, they won't be added again.
	 * @return This Compound instance for ease in constructing.
	 */
	public Compound setThreats(EnumThreat[] threats)
	{
		if(threats != null)
		{
			if(this.threats == null)
			{
				this.threats = threats;
				return this;
			}
			List<EnumThreat> oldThreats = Arrays.asList(this.threats);
			for(EnumThreat threat : threats)
			{
				if(!oldThreats.contains(threat))
				{
					oldThreats.add(threat);
				}
			}
			this.threats = oldThreats.toArray(new EnumThreat[oldThreats.size()]);
		}
		return this;
	}

	/**
	 * Remove this EnumThreat from the list of threats.
	 * <br>Mostly used internally.
	 * @param threat The Threat to remove.
	 * @return This Compound instance for ease in constructing.
	 */
	public Compound removeThreat(EnumThreat threat)
	{
		if(this.threats != null)
		{
			for(int i = 0; i < this.threats.length; i++)
			{
				EnumThreat threat2 = this.threats[i];
				if(threat2 == threat)
				{
					this.threats[i] = null;
				}
			}
		}
		return this;
	}

	/**
	 * @return The unique id.
	 */
	public int getID()
	{
		return this.id;
	}

	/**
	 * @return The name.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @return The formula.
	 */
	public String getFormula()
	{
		return this.formula;
	}

	/**
	 * @return The texture name.
	 */
	public String getTextureName()
	{
		return this.textureName;
	}

	/**
	 * @return The threats that this compound poses.
	 */
	public EnumThreat[] getThreats()
	{
		return this.threats;
	}

	/**
	 * @return The mod id.
	 */
	public String getModID()
	{
		return this.modid;
	}

}
