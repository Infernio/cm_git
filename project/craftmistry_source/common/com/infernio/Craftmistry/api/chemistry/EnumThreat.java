package com.infernio.Craftmistry.api.chemistry;

import net.minecraft.util.IIcon;

/**
 * This class describes how dangerous an element is.
 * @author Infernio
 */
//TODO Somewhat unfinished
public enum EnumThreat {

	/** This element is radioactive, meaning that it'll give blindness, poison, hunger etc. */
	RADIOACTIVE(30, true, "radioactive", "radioactive"),
	/** This element is explosive, meaning that it has a chance to explode. */
	EXPLOSIVE(2, true, "explosive", "explosive"),
	/** This element is flammable, meaning that it has a chance to set the owner of the test tube on fire. */
	FLAMMABLE(3, true, "flammable", "flammable"),
	/** This element is oxydizing, meaning that it has a chance to increase the duration of existing fire. */
	OXYDIZING(5, true, "oxydizing", "oxydizing"),
	/** This doesn't do too much, it is just here for the sake of having all threats in game. */
	COMPRESSED_GAS(0, true, "compressed_gas", "compressedGas"),
	/** This element is corrosive, meaning that it has a chance to inflict damage to the owner of the test tube and also damage his armor. */
	CORROSIVE(5, true, "corrosive", "corrosive"),
	/** This element is toxic, meaning that it will slowly poison your food. */
	TOXIC(3, true, "toxic", "toxic"),
	/** TODO Make something out of this */
	IRRITANT(5, true, "irritant", "irritant"),
	/** This element is a health hazard, meaning that it has a chance to poison the owner of the test tube and inflict damage to them. */
	HEALTH_HAZARD(5, true, "health_hazard", "healthHazard"),
	/** TODO Make something out of this */
	ENVIRONMENTALLY_DAMAGING(5, true, "environmentally_damaging", "environmentallyDamaging");

	/** Chance of this threat activating (in per mille) */
	private int chance;
	/** If true, this EnumThreat's effect will only be applied on the server */
	private boolean serverOnly;
	/** The name of this threat's pictogram */
	private String textureName;
	/** This name will be localized and shown when you hover over this threat's pictogram in the analyzer GUI */
	private String name;

	/** Array of threat icons */
	public static IIcon[] icons;

	private EnumThreat(int chance, boolean serverOnly, String textureName, String name)
	{
		this.chance = chance;
		this.serverOnly = serverOnly;
		this.textureName = textureName;
		this.name = name;
	}

	/**
	 * @return The activation chance.
	 */
	public int getActivationChance()
	{
		return this.chance;
	}

	/**
	 * @return True if this EnumThreat's effect is server-only.
	 */
	public boolean isServerOnly()
	{
		return this.serverOnly;
	}

	/**
	 * @return The name of this threat's pictogram. 
	 */
	public String getTextureName()
	{
		return this.textureName;
	}

	/**
	 * @return The unlocalized ingame name.
	 */
	public String getName()
	{
		return this.name;
	}

}
