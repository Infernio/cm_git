package com.infernio.Craftmistry.api;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Utility class for dealing with hash codes.
 * @author Infernio
 */
public class HashUtils {

	/**
	 * Hashes the given ItemStack by its Item String ID, stack size and meta value.
	 * <br><br><b>Warning:</b>
	 * <br>This method is can sometimes take a little while to fully compute the
	 * hash code, therefore it is <b>not</b> recommended that you call this often.
	 * @param stack The ItemStack to hash.
	 * @return A hash code for this ItemStack.
	 */
	public static int hashItemStack(ItemStack stack)
	{
		return ((Item.itemRegistry.getNameForObject(stack.getItem()).hashCode() + stack.stackSize) * 11) ^ ((stack.getItemDamage() + stack.stackSize) * 11);
	}

	/**
	 * Combines multiple hash codes into one.
	 * <br>Uses XOR, and is therefore commutative.
	 * @param hashes The hash codes to combine.
	 * @return A hash code that has been combined from the given ones.
	 */
	public static int combineHashes(int... hashes)
	{
		int combined = 0;
		for(int hash : hashes)
		{
			if(hash == 0)
			{
				combined = hash;
			}
			else
			{
				combined = combined ^ hash;
			}
		}
		return combined;
	}

}
