package com.infernio.Craftmistry.api;

/**
 * Thrown if the Craftmistry installation is not intact.
 * @author Infernio
 */
public class CorruptedInstallationException extends RuntimeException {

	/**
	 * The serial version UID. (Only here to prevent warnings)
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new CorruptedInstallationException.
	 * @see RuntimeException
	 */
	public CorruptedInstallationException()
	{
		super();
	}

	/**
	 * Constructs a new CorruptedInstallationException with the specified message.
	 * @param message The specified message.
	 * @see RuntimeException
	 */
	public CorruptedInstallationException(String message)
	{
		super(message);
	}

	/**
	 * Constructs a new CorruptedInstallationException with the specified message and the specified cause.
	 * @param message The specified message.
	 * @param cause The {@link Throwable} that caused this exception to occur.
	 * @see RuntimeException
	 */
	public CorruptedInstallationException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
