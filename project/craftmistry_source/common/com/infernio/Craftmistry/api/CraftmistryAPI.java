package com.infernio.Craftmistry.api;

import java.lang.reflect.Method;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.StatCollector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.infernio.Craftmistry.api.chemistry.Compound;
import com.infernio.Craftmistry.api.chemistry.Element;
import com.infernio.Craftmistry.api.chemistry.EnumElementType;
import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.api.chemistry.EnumThreat;
import com.infernio.Craftmistry.api.item.ISpecialInfo;
import com.infernio.Craftmistry.api.recipe.IForesterRegistry;
import com.infernio.Craftmistry.api.research.IResearchRegistry;
import com.infernio.Craftmistry.api.research.tech.ITechnologyList;
import com.infernio.Craftmistry.api.tile.IDetectorMode;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;

/**
 * The main class of the Craftmistry API.
 * <br>If documentation is missing or if you have a question / problem, please PM me on the Minecraft forums.
 * @author Infernio
 */
public class CraftmistryAPI {

	/** The Craftmistry API logger */
	private static Logger log;
	/** The class that stores elements and compounds */
	private static Class<?> elements;
	/** The class that stores scoopula drops and item information */
	private static Class<?> itemHelper;
	/** The class that allows you to register Craftmistry recipes */
	private static Class<?> recipes;
	/** The class that allows you to use tile entity related methods */
	private static Class<?> tileHandler;

	/**
	 * Log a warning to the user.
	 * @param method The method that failed to be reflected.
	 * @param exception The exception that occurred while attemping to reflect.
	 * @param params The parameters that were used for the call.
	 */
	static void printWarning(String method, Throwable exception, Object... params)
	{
		log.warn("Failed to reflect a method: " + method + " [" + exception + "]");
		if(params != null && params.length > 0)
		{
			log.warn("Parameters used for call:");
			for(Object obj : params)
			{
				if(obj != null)
				{
					log.warn(obj.toString());
				}
				else
				{
					log.warn("null");
				}
			}
		}
		log.warn("Exception Type: " + exception);
		if(exception != null)
		{
			log.warn("Exception Message: " + exception.getMessage());
			log.warn("Localized Exception Message: " + exception.getLocalizedMessage());
		}
		if(FMLCommonHandler.instance().getSide().isClient())
		{
			log.warn("Is Client: true");
			log.warn("Current Client Instance: " + Minecraft.getMinecraft());
		}
		else
		{
			log.warn("Is Client: false");
		}
		log.warn("Current Server Instance: " + MinecraftServer.getServer());
		if(exception == null || exception.getStackTrace() == null)
		{
			log.warn("Stack Trace Status: unavailable");
		}
		else
		{
			log.warn("Stack Trace Status: available");
			StackTraceElement[] elements = exception.getStackTrace();
			log.warn("Stack Trace Length: " + elements.length);
			exception.printStackTrace();
		}
	}


	/**
	 * Load the helper classes, load the logger and throw an exception if the installation is corrupt.
	 * Called every time an API function is used.
	 */
	static void initAPI()
	{
		if(getLogger() == null)
		{
			log = LogManager.getLogger("Craftmistry API");
		}
		if(elements == null || itemHelper == null || recipes == null || tileHandler == null)
		{
			try
			{
				elements = Class.forName("com.infernio.Craftmistry.common.chemistry.Elements");
				itemHelper = Class.forName("com.infernio.Craftmistry.core.helper.ItemHelper");
				recipes = Class.forName("com.infernio.Craftmistry.common.recipe.RecipeCreator");
				tileHandler = Class.forName("com.infernio.Craftmistry.core.handler.TileHandler");
			}
			catch(Throwable e)
			{
				throw new CorruptedInstallationException("The Craftmistry installation is corrupted!", e);
			}
		}
	}

	/**
	 * Get the API specific logger instance.
	 * <br>Used by {@linkplain com.infernio.Craftmistry.api.item.NormalInformation NormalInformation}.
	 * @return The API specific logger instance.
	 */
	public static Logger getLogger()
	{
		return log;
	}

	/**
	 * The main Chemistry API sub-class. Use this to register Elements or Compounds, use it to find Element
	 * names, symbols, decay time etc.
	 * @author Infernio
	 */
	public static class Chemistry {

		/** 
		 * Set during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * this is the test tube item storing elements in {@linkplain EnumMatterState#SOLID solid} state.
		 * Make sure to use {@linkplain cpw.mods.fml.common.event.FMLInitializationEvent Init} 
		 * or {@linkplain cpw.mods.fml.common.event.FMLPostInitializationEvent Post-Init}, as things
		 * may otherwise not go so well. Setting this to a different value will not have an impact
		 * on Craftmistry's test tubes themselves but will probably break other mods / addons that rely on
		 * this value.
		 */
		public static Item STATE_SOLID_ITEM;

		/** 
		 * Set during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * this is the test tube item storing elements in {@linkplain EnumMatterState#LIQUID liquid} state.
		 * Make sure to use {@linkplain cpw.mods.fml.common.event.FMLInitializationEvent Init}
		 * or {@linkplain cpw.mods.fml.common.event.FMLPostInitializationEvent Post-Init}, as things
		 * may otherwise not go so well. Setting this to a different value will not have an impact
		 * on Craftmistry's test tubes themselves but will probably break other mods / addons that rely on
		 * this value.
		 */
		public static Item STATE_LIQUID_ITEM;

		/** 
		 * Set during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * this is the test tube item storing elements in {@linkplain EnumMatterState#GAS gas} state.
		 * Make sure to use {@linkplain cpw.mods.fml.common.event.FMLInitializationEvent Init} 
		 * or {@linkplain cpw.mods.fml.common.event.FMLPostInitializationEvent Post-Init}, as things
		 * may otherwise not go so well. Setting this to a different value will not have an impact
		 * on Craftmistry's test tubes themselves but will probably break other mods / addons that rely on
		 * this value.
		 */
		public static Item STATE_GAS_ITEM;

		/** 
		 * Set during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * this is the test tube item storing elements in {@linkplain EnumMatterState#PLASMA plasma} state.
		 * Make sure to use {@linkplain cpw.mods.fml.common.event.FMLInitializationEvent Init} 
		 * or {@linkplain cpw.mods.fml.common.event.FMLPostInitializationEvent Post-Init}, as things
		 * may otherwise not go so well. Setting this to a different value will not have an impact
		 * on Craftmistry's test tubes themselves but will probably break other mods / addons that rely on
		 * this value.
		 */
		public static Item STATE_PLASMA_ITEM;

		/**
		 * Set during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * this is the test tube item storing compounds (e.g. Sulfuric Acid).
		 * Make sure to use {@linkplain cpw.mods.fml.common.event.FMLInitializationEvent Init} 
		 * or {@linkplain cpw.mods.fml.common.event.FMLPostInitializationEvent Post-Init}, as things
		 * may otherwise not go so well. Setting this to a different value will not have an impact
		 * on Craftmistry's test tubes themselves but will probably break other mods / addons that rely on
		 * this value.
		 */
		public static Item COMPOUND_ITEM;

		/**
		 * Add a new Element to the list of registered elements.
		 * Make sure to call this during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * as your icon will otherwise not be registered and your element won't exist as a fluid. You can register a total of 256 elements.
		 * IDs in use by Craftmistry:
		 * 0 - 118 and 255.
		 * @param element The element to add.
		 * @see Element
		 */
		public static void addElement(Element element)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("addElement", Element.class);
				method.invoke(null, element);
			}
			catch(Throwable e)
			{
				printWarning("Elements.addElement()", e, element);
			}
		}

		/**
		 * Add a new Compound to the list of registerd compounds.
		 * Make sure to call this during {@linkplain cpw.mods.fml.common.event.FMLPreInitializationEvent Pre-Init},
		 * as your icon will otherwise not be registered. You can register a total of 1024 compounds.
		 * IDs reserved for Craftmistry (do not use these if you want your mod to be upwards / backwards compatible!):
		 * 0 - 500
		 * @param compound The compound to add.
		 * @see Compound
		 */
		public static void addCompound(Compound compound)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("addCompound", Compound.class);
				method.invoke(null, compound);
			}
			catch(Throwable e)
			{
				printWarning("Elements.addCompound()", e, compound);
			}
		}
	
		/**
		 * Get the unlocalized name of an element by its atomic number.
		 * <br>This name can be localized using something similiar to this:
		 * <br><code>String localized = StatCollector.translateToLocal("element." + CraftmistryAPI.Chemistry.getElementName(ATOMIC_NUMBER) + ".name");</code>
		 * @param atomicNumber The atomic number of that element (meta value).
		 * @return The unlocalized name of the Element, or null if that Element does not exist.
		 */
		public static String getElementName(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getElementName", int.class);
				return (String)method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getElementName()", e, Integer.valueOf(atomicNumber));
				return null;
			}
		}
	
		/**
		 * Get the symbol of the element with the specified atomic number.
		 * @param atomicNumber The atomic number of the element (meta value).
		 * @return The symbol of that element, localized to the current language (if that language for some reason overrides symbols).
		 */
		public static String getElementSymbol(int atomicNumber)
		{
			initAPI();
			return StatCollector.translateToLocal("element." + getElementName(atomicNumber) + ".symbol");
		}
	
		/**
		 * Get the element type of the element with the specified atomic number.
		 * @param atomicNumber The atomic number of the element (meta value).
		 * @return The Element type of that element, or null if the element does not exist or has null as its element type.
		 */
		public static EnumElementType getElementType(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getType", int.class);
				return (EnumElementType)method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getType()", e, Integer.valueOf(atomicNumber));
				return null;
			}
		}
	
		/**
		 * Get the possible element states for an element.
		 * @param atomicNumber The atomic number of the element (meta value).
		 * @return The possible states for that atomic number.
		 */
		public static EnumMatterState[] getElementStates(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getPossibleStates", int.class);
				return (EnumMatterState[])method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getPossibleStates()", e, Integer.valueOf(atomicNumber));
				return null;
			}
		}
	
		/**
		 * Find out whether or not the element with the specified atomic number is radioactive.
		 * @param atomicNumber The atomic number of the element (meta value).
		 * @return True if the element is radioactive, false if it isn't or the element does not exist.
		 */
		public static boolean isElementRadioactive(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("isElementRadioactive", int.class);
				return (Boolean)method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.isElementRadioactive()", e, Integer.valueOf(atomicNumber));
				return false;
			}
		}
	
		/**
		 * Get the Half-Life of the element with the specified atomic number.
		 * @param atomicNumber The atomic number of the element (meta value).
		 * @return The Half-Life of that element, or -1 if the element does not exist.
		 */
		public static int getHalfLife(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getHalfLife", int.class);
				return (Integer)method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getHalfLife()", e, Integer.valueOf(atomicNumber));
				return -1;
			}
		}
	
		/**
		 * Get the Meta into which this element decays.
		 * @param atomicNumber The atomic number of the element (meta value)
		 * @return The meta into which the element decays, or -1 if that element does not exist.
		 */
		public static int getDecayMeta(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getDecayMeta", int.class);
				return (Integer)method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getDecayMeta()", e, Integer.valueOf(atomicNumber));
				return -1;
			}
		}
	
		/**
		 * Get the ItemStack into which this element decays.
		 * @param atomicNumber The atomic number of the element (meta value)
		 * @param stateID The ID of the test tube that element is in
		 * @return The ItemStack into which the element decays, or null if that element does not exist.
		 */
		public static ItemStack getDecayProduct(int atomicNumber, int stateID)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getDecayProduct", int.class, int.class);
				return (ItemStack)method.invoke(null, atomicNumber, stateID);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getDecayProduct()", e, Integer.valueOf(atomicNumber), Integer.valueOf(stateID));
				return null;
			}
		}

		/**
		 * Get the threats that an element poses while it's in the specified state of matter.
		 * @param atomicNumber The atomic number of that element.
		 * @param state The state of matter that the element is in.
		 * @return The threats that this element poses in the specified state of matter.
		 */
		public static EnumThreat[] getElementThreats(int atomicNumber, EnumMatterState state)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getElementThreats", int.class, EnumMatterState.class);
				return (EnumThreat[])method.invoke(null, Integer.valueOf(atomicNumber), state);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getElementThreats()", e, Integer.valueOf(atomicNumber), state);
				return null;
			}
		}

		/**
		 * Get the Mod ID of the element with the specified atomic number.
		 * @param atomicNumber The atomic number of the element (meta value).
		 * @return The Mod ID of that element, or null if the element does not exist.
		 */
		public static String getElementModId(int atomicNumber)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getElementModId", int.class);
				return (String)method.invoke(null, atomicNumber);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getElementModId()", e, Integer.valueOf(atomicNumber));
				return null;
			}
		}
	
		/**
		 * Get the name of the compound with the specified id.
		 * @param id The id of the compound (meta value)
		 * @return The name of the compound, or null if the compound does not exist.
		 */
		public static String getCompoundName(int id)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getCompoundName", int.class);
				return (String)method.invoke(null, id);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getCompoundName()", e, Integer.valueOf(id));
				return null;
			}
		}
	
		/**
		 * Get the formula of the compound with the specified id.
		 * @param id The id of the compound (meta value)
		 * @return The formula of the compound, or null if the compound does not exist.
		 */
		public static String getCompoundFormula(int id)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getCompoundFormula", int.class);
				return (String)method.invoke(null, id);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getCompoundFormula()", e, Integer.valueOf(id));
				return null;
			}
		}
	
		/**
		 * Get the texture name of the compound with the specified id.
		 * @param id The id of the compound (meta value)
		 * @return The texture name of the compound, or null if the compound does not exist.
		 */
		public static String getCompoundTextureName(int id)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getCompoundTextureName", int.class);
				return (String)method.invoke(null, id);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getCompoundTextureName()", e, Integer.valueOf(id));
				return null;
			}
		}

		/**
		 * Get the threats that a compound poses.
		 * @param id The ID of the Compound.
		 * @return The threats that this compound poses.
		 */
		public static EnumThreat[] getCompoundThreats(int id)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getCompoundThreats", int.class);
				return (EnumThreat[])method.invoke(null, Integer.valueOf(id));
			}
			catch(Throwable e)
			{
				printWarning("Elements.getCompoundThreats()", e, Integer.valueOf(id));
				return null;
			}
		}

		/**
		 * Get the Mod ID of the compound with the specified id.
		 * @param id The id of the compound (meta value)
		 * @return The Mod ID of the compound, or null if the compound does not exist.
		 */
		public static  String getCompoundModId(int id)
		{
			initAPI();
			try
			{
				Method method = elements.getMethod("getCompoundModId", int.class);
				return (String)method.invoke(null, id);
			}
			catch(Throwable e)
			{
				printWarning("Elements.getCompoundModId()", e, Integer.valueOf(id));
				return null;
			}
		}
	
		/**
		 * Get whether or not this ItemStack is a Test Tube.
		 * @param stack The ItemStack to check.
		 * @return True if the ItemStack is a test tube (solid, liquid, gas or plasma), false if it isn't, the ItemStack is null or the element doesn't exist.
		 */
		public static boolean isItemTestTube(ItemStack stack)
		{
			initAPI();
			try
			{
				Method method = itemHelper.getMethod("isTestTube", ItemStack.class);
				return (Boolean)method.invoke(null, stack);
			}
			catch(Throwable e)
			{
				printWarning("ItemHelper.isTestTube()", e, stack);
				return false;
			}
		}
	}

	/**
	 * The main Items API sub-class. Use this to register scoopula drops, add
	 * item information etc.
	 * @author Infernio
	 */
	public static class Items {

		/**
		 * Add a custom drop for the scoopula.
		 * @param block The block that will trigger the drop.
		 * @param blockMeta The block meta that will trigger the drop.
		 * @param replacement The block to replace the original with.
		 * @param metaToReplace The meta to replace the block with.
		 * @param drops A collection of ItemStacks to drop when the block was right clicked.
		 * @param chances The chances for each drop to occur.
		 */
		public static void addScoopulaDrop(Block block, int blockMeta, Block replacement, int metaToReplace, ItemStack[] drops, int[] chances)
		{
			initAPI();
			try
			{
				Method method = itemHelper.getMethod("addBlockBreak", Block.class, int.class, Block.class, int.class, ItemStack[].class, int[].class);
				method.invoke(null, block, blockMeta, replacement, metaToReplace, drops, chances);
			}
			catch(Throwable e)
			{
				printWarning("ItemHelper.addBlockBreak()", e, block, Integer.valueOf(blockMeta), replacement, Integer.valueOf(metaToReplace), drops, chances);
				return;
			}
		}

		/**
		 * Add custom lines of information to items. These lines will then be displayed in the analyzer.
		 * If you just want to easily add in custom lines of information, use {@linkplain com.infernio.Craftmistry.api.item.NormalInformation NormalInformation}.
		 * <br><b>You should only call this on the client, if possible.</b> It is possible to localize the different lines by typing something like '%p1tile.myTile.information.page1.line1%stile.myTile.information.page1.line2'
		 * and then adding localizations for the two non-localized strings into a .lang file or by localizing the entire string.
		 * <br><br><b>How to use</b>
		 * <br>Here's an example. This is how diamonds could receive custom information:<br>
		 * <br><i>Java code</i>
		 * <br><code>CraftmistryAPI.Items.addItemInfo(Items.diamond, 0, new NormalInformation(StatCollector.translateToLocal("analyzer.diamond.info"), false, 1));</code>
		 * <br><br><i>Language file (en_US.lang)</i>
		 * <br><code>analyzer.diamond.info=%p1A very%sprecious%smineral.%sIt also%sseems to be%svery durable.%s%p2Surprisingly,%sit consists%sof carbon,%sjust like coal.</code>
		 * @param item The item ID.
		 * @param meta The item meta.
		 * @param information The information to register.
		 * @see {@linkplain com.infernio.Craftmistry.api.item.NormalInformation NormalInformation}
		 */
		public static void addItemInformation(Item item, int meta, ISpecialInfo information)
		{
			initAPI();
			try
			{
				Method method = itemHelper.getMethod("addItemInfo", Item.class, int.class, ISpecialInfo.class);
				method.invoke(null, item, meta, information);
			}
			catch(Throwable e)
			{
				printWarning("ItemHelper.addItemInfo()", e, item, Integer.valueOf(meta), information);
				return;
			}
		}
	}

	/**
	 * The main Recipe API sub-class. Use this to register recipes for nearly all Craftmistry machines.
	 * @author Infernio
	 */
	public static class Recipes {

		/**
		 * The Forester Registry object. Call methods from this to register custom trees so that the forester
		 * can maintain them. This value gets set during {@link FMLInitializationEvent}, therefore using the
		 * forester registry is only advisable during <b>post-init</b>.
		 * <br><br>Under <b>NO</b> circumstances should this object be set to a different value, since
		 * that will break all other mods using it.
		 */
		public static IForesterRegistry foresterRegistry;

		/**
		 * Add a new recipe to the list of compressor recipes.
		 * <br><br><b>Note:</b> The required stack size for this recipe will be automatically set to 1.
		 * Also, the energy required for this recipe will be set to a total of 7500 EU
		 * (The Compressor needs 1250 ticks to compress one item -> 6 EU per tick).
		 * (Every tick, the Compressor takes the total amount ofenergy required for its current recipe and divides it by 1250 to get the current EU per tick)
		 * See the other versions of this method if you want to assign these manually.
		 * @param item The item to register.
		 * @param meta The item meta value to register.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @see Recipes#addCompressorRecipe(int, int, int, ItemStack)
		 * @see Recipes#addCompressorRecipe(int, int, int, ItemStack, int)
		 */
		public static void addCompressorRecipe(Item item, int meta, ItemStack result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addCompressorRecipe", Item.class, int.class, ItemStack.class);
				method.invoke(null, item, meta, result);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addCompressorRecipe()", e, item, Integer.valueOf(meta), result);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of compressor recipes.
		 * <br><br><b>Note:</b> The energy required for this recipe will be set to a total of 7500 EU
		 * (The Compressor needs 1250 ticks to compress one item -> 6 EU per tick). 
		 * (Every tick, the Compressor takes the total amount of energy required for its current recipe and divides it by 1250 to get the current EU per tick)
		 * See the other version of this method if you want to assign these manually.
		 * @param item The item to register.
		 * @param amount The amount of items required in order to get the result.
		 * @param meta The item meta value to register.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @see Recipes#addCompressorRecipe(int, int, ItemStack)
		 * @see Recipes#addCompressorRecipe(int, int, int, ItemStack, int)
		 */
		public static void addCompressorRecipe(Item item, int amount, int meta, ItemStack result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addCompressorRecipe", Item.class, int.class, int.class, ItemStack.class);
				method.invoke(null, item, amount, meta, result);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addCompressorRecipe()", e, item, Integer.valueOf(amount), Integer.valueOf(meta), result);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of compressor recipes.
		 * <br><br><b>Note:</b> The Compressor needs 1250 ticks to process one compression. Therefore, you should
		 * think about how much EU you want the compressor to consume PER TICK. Then, multiply that number
		 * by 1250 to get the total energy required. (Every tick, the Compressor takes the total amount of
		 * energy required for its current recipe and divides it by 1250 to get the current EU per tick)
		 * @param item The item to register.
		 * @param amount The amount of items required in order to get the result.
		 * @param meta The item meta value to register.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @param energyRequired The energy required to use this recipe.
		 * @see Recipes#addCompressorRecipe(int, int, ItemStack)
		 * @see Recipes#addCompressorRecipe(int, int, int, ItemStack)
		 */
		public static void addCompressorRecipe(Item item, int amount, int meta, ItemStack result, int energyRequired)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addCompressorRecipe", Item.class, int.class, int.class, ItemStack.class, int.class);
				method.invoke(null, item, amount, meta, result, energyRequired);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addCompressorRecipe()", e, item, Integer.valueOf(amount), Integer.valueOf(meta), result, Integer.valueOf(energyRequired));
				return;
			}
		}

		/**
		 * Add a new recipe to the list of infuser recipes.
		 * <br><br><b>Note:</b> The amount of energy will atomatically be set to 600 EU (600 EU / 100 ticks -> 6 EU per tick).
		 * Take a look at the other version of this method if you want to set the required energy for yourself.
		 * The Infuser needs 100 ticks to process one infusion. Therefore, you should
		 * think about how much EU you want the infuser to consume PER TICK. Then, multiply that number
		 * by 100 to get the total energy required. (Every tick, the Infuser takes the total amount of
		 * energy required for its current recipe and divides it by 100 to get the current EU per tick)
		 * @param item The item to register.
		 * @param meta The item meta value to register.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @see Recipes#addInfuserRecipe(int, int, ItemStack, int)
		 */
		public static void addInfuserRecipe(Item item, int meta, ItemStack result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addInfuserRecipe", Item.class, int.class, ItemStack.class);
				method.invoke(null, item, meta, result);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addInfuserRecipe()", e, item, Integer.valueOf(meta), result);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of infuser recipes.
		 * <br><br><b>Note:</b> The Infuser needs 100 ticks to process one infusion. Therefore, you should
		 * think about how much EU you want the infuser to consume PER TICK. Then, multiply that number
		 * by 100 to get the total energy required. (Every tick, the Infuser takes the total amount of
		 * energy required for its current recipe and divides it by 100 to get the current EU per tick)
		 * @param item The item id to register.
		 * @param meta The item meta value to register.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @param energyRequired The energy required for this recipe.
		 * @see Recipes#addInfuserRecipe(int, int, ItemStack)
		 */
		public static void addInfuserRecipe(Item item, int meta, ItemStack result, int energyRequired)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addInfuserRecipe", Item.class, int.class, ItemStack.class, int.class);
				method.invoke(null, item, meta, result, energyRequired);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addInfuserRecipe()", e, item, Integer.valueOf(meta), result, Integer.valueOf(energyRequired));
				return;
			}
		}

		/**
		 * Add a new recipe to the list of reactor recipes.
		 * <br>This will, by default, also register a deconstructor recipe. See {@link #addOneWayReactorRecipe(ItemStack, ItemStack...)}.
		 * <br><br><b>Here's an example</b>
		 * <br><code>CraftmistryAPI.Recipes.addReactorRecipe(new ItemStack(CraftmistryAPI.Chemistry.COMPOUND_ITEM, 1, 2), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 2, 1), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 1, 8));</code>
		 * <br>This would add the recipe for Water (Two Hydrogen (H), One Oxygen (O) -> H2O).
		 * <br>You should of course import COMPOUND_ITEM etc. statically.
		 * <br><br><b>Note:</b> The Reactor needs 100 ticks to process one reaction. Therefore, you should
		 * think about how much EU you want the reactor to consume PER TICK. Then, multiply that number
		 * by 100 to get the total energy required. (Every tick, the Reactor takes the total amount of
		 * energy required for its current recipe and divides it by 200 to get the current EU per tick)
		 * The energy required for this recipe will automatically be set to 20000 EU (20000 EU / 200 -> 100 EU per tick).
		 * Take a look at the other version of this method if you want to assign the energy required manually.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @param input The items that need to be put into the reaction 3x3 grid.
		 * @see Recipes#addReactorRecipe(ItemStack, ItemStack...)
		 */
		public static void addReactorRecipe(ItemStack result, ItemStack... input)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addReactorRecipe", ItemStack.class, ItemStack[].class);
				method.invoke(null, result, input);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addReactorRecipe()", e, result, input);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of reactor recipes.
		 * <br>This will, by default, also register a deconstructor recipe. See {@link #addOneWayReactorRecipe(int, ItemStack, ItemStack...)}.
		 * <br><br><b>Here's an example</b>
		 * <br><code>CraftmistryAPI.Recipes.addReactorRecipe(new ItemStack(CraftmistryAPI.Chemistry.COMPOUND_ITEM, 1, 2), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 2, 1), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 1, 8));</code>
		 * <br>This would add the recipe for Water (Two Hydrogen (H), One Oxygen (O) -> H2O).
		 * <br>You should of course import COMPOUND_ITEM etc. statically.
		 * <br><br><b>Note:</b> The Reactor needs 100 ticks to process one reaction. Therefore, you should
		 * think about how much EU you want the reactor to consume PER TICK. Then, multiply that number
		 * by 100 to get the total energy required. (Every tick, the Reactor takes the total amount of
		 * energy required for its current recipe and divides it by 200 to get the current EU per tick)
		 * @param result The ItemStack that will be produced by this recipe.
		 * @param input The items that need to be put into the reaction 3x3 grid.
		 * @param energyRequired The energy that is required in order to finish the reaction.
		 * @see Recipes#addReactorRecipe(ItemStack, ItemStack...)
		 */
		public static void addReactorRecipe(int energyRequired, ItemStack result, ItemStack... input)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addReactorRecipe", int.class, ItemStack.class, ItemStack[].class);
				method.invoke(null, energyRequired, result, input);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addReactorRecipe()", e, Integer.valueOf(energyRequired), result, input);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of reactor recipes.
		 * <br>This method will <b>not</b> register a deconstructor recipe. See {@link #addReactorRecipe(ItemStack, ItemStack...)}.
		 * <br><br><b>Here's an example</b>
		 * <br><code>CraftmistryAPI.Recipes.addReactorRecipe(new ItemStack(CraftmistryAPI.Chemistry.COMPOUND_ITEM, 1, 2), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 2, 1), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 1, 8));</code>
		 * <br>This would add the recipe for Water (Two Hydrogen (H), One Oxygen (O) -> H2O).
		 * <br>You should of course import COMPOUND_ITEM etc. statically.
		 * <br><br><b>Note:</b> The Reactor needs 100 ticks to process one reaction. Therefore, you should
		 * think about how much EU you want the reactor to consume PER TICK. Then, multiply that number
		 * by 100 to get the total energy required. (Every tick, the Reactor takes the total amount of
		 * energy required for its current recipe and divides it by 200 to get the current EU per tick)
		 * The energy required for this recipe will automatically be set to 20000 EU (20000 EU / 200 -> 100 EU per tick).
		 * Take a look at the other version of this method if you want to assign the energy required manually.
		 * @param result The ItemStack that will be produced by this recipe.
		 * @param input The items that need to be put into the reaction 3x3 grid.
		 * @see Recipes#addReactorRecipe(ItemStack, ItemStack...)
		 */
		public static void addOneWayReactorRecipe(ItemStack result, ItemStack... input)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addOneWayReactorRecipe", ItemStack.class, ItemStack[].class);
				method.invoke(null, result, input);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addOneWayReactorRecipe()", e, result, input);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of reactor recipes.
		 * <br>This method will <b>not</b> register a deconstructor recipe. See {@link #addReactorRecipe(int, ItemStack, ItemStack...)}.
		 * <br><br><b>Here's an example</b>
		 * <br><code>CraftmistryAPI.Recipes.addReactorRecipe(new ItemStack(CraftmistryAPI.Chemistry.COMPOUND_ITEM, 1, 2), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 2, 1), new ItemStack(CraftmistryAPI.Chemistry.STATE_GAS_ITEM, 1, 8));</code>
		 * <br>This would add the recipe for Water (Two Hydrogen (H), One Oxygen (O) -> H2O).
		 * <br>You should of course import COMPOUND_ITEM etc. statically.
		 * <br><br><b>Note:</b> The Reactor needs 100 ticks to process one reaction. Therefore, you should
		 * think about how much EU you want the reactor to consume PER TICK. Then, multiply that number
		 * by 100 to get the total energy required. (Every tick, the Reactor takes the total amount of
		 * energy required for its current recipe and divides it by 200 to get the current EU per tick)
		 * @param result The ItemStack that will be produced by this recipe.
		 * @param input The items that need to be put into the reaction 3x3 grid.
		 * @param energyRequired The energy that is required in order to finish the reaction.
		 * @see Recipes#addReactorRecipe(ItemStack, ItemStack...)
		 */
		public static void addOneWayReactorRecipe(int energyRequired, ItemStack result, ItemStack... input)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addOneWayReactorRecipe", int.class, ItemStack.class, ItemStack[].class);
				method.invoke(null, energyRequired, result, input);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addOneWayReactorRecipe()", e, Integer.valueOf(energyRequired), result, input);
				return;
			}
		}

		/**
		 * TODO Documentation
		 * @param item
		 * @param meta
		 * @param result
		 */
		public static void addDeconstructorRecipe(Item item, int meta, ItemStack... result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addDeconstructorRecipe", Item.class, int.class, ItemStack[].class);
				method.invoke(null, item, meta, result);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addDeconstructorRecipe()", e, item, Integer.valueOf(meta), result);
				return;
			}
		}

		/**
		 * TODO Documentation
		 * @param item
		 * @param meta
		 * @param energyRequired
		 * @param result
		 */
		public static void addDeconstructorRecipe(Item item, int meta, int energyRequired, ItemStack... result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addDeconstructorRecipe", Item.class, int.class, ItemStack[].class, int.class);
				method.invoke(null, item, meta, result, energyRequired);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addDeconstructorRecipe()", e, Integer.valueOf(energyRequired), item, Integer.valueOf(meta), result);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of Air Filter recipes.
		 * <br><br><b>Note:</b> Every tick, the Air Filter takes the energy required and divides it by the duration
		 * to get the EU per tick required.
		 * @param element The name of the element this recipe will produce.
		 * @param duration The time (in ticks) that is nedded to create the result.
		 * @param energyRequired The energy required to create the result.
		 * @param result The result of this recipe.
		 */
		public static void addAirFilterRecipe(String element, int duration, int energyRequired, ItemStack result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addAirFilterRecipe", String.class, int.class, int.class, ItemStack.class);
				method.invoke(null, element, energyRequired, duration, result);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addAirFilterRecipe()", e, element, Integer.valueOf(energyRequired), Integer.valueOf(duration), result);
				return;
			}
		}

		/**
		 * Add a new recipe to the list of mineral extractor recipes.
		 * <br>Every time the mineral extractor extracts stuff from mineral water,
		 * there will be a chance that it will pick the ItemStack from this method.
		 * @param result The result of this recipe.
		 * @param chance The chance (in percent) of this recipe occurring.
		 */
		public static void addMineralExtractorRecipe(ItemStack result, int chance)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addMineralExtractorRecipe", ItemStack.class, int.class);
				method.invoke(null, result, chance);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addMineralExtractorRecipe()", e, result, Integer.valueOf(chance));
			}
		}

		/**
		 * Add a new recipe to the list of electrolyser recipes.
		 * @param item1 The first item.
		 * @param meta1 Metadata of the first item.
		 * @param item2 The second item.
		 * @param meta2 Metadata of the second item.
		 * @param result Result of this recipe.
		 */
		public static void addElectrolyserRecipe(Item item1, int meta1, Item item2, int meta2, ItemStack result)
		{
			initAPI();
			try
			{
				Method method = recipes.getMethod("addElectrolyserRecipe", Item.class, int.class, Item.class, int.class, ItemStack.class);
				method.invoke(null, item1, meta1, item2, meta2, result);
			}
			catch(Throwable e)
			{
				printWarning("RecipeCreator.addElectrolyserRecipe()", e, item1, Integer.valueOf(meta1), item2, Integer.valueOf(meta2), result);
			}
		}

	}

	/**
	 * The main Tile Entity API sub-class. Use this to black-list blocks for the Breaker etc.
	 * @author Infernio
	 */
	public static class Tiles {

		/**
		 * Add an exclusion to the Breaker black list.
		 * @param blockID The block ID to exclude.
		 */
		public static void addBreakerBlacklist(String blockID)
		{
			initAPI();
			try
			{
				Method method = tileHandler.getMethod("registerBreakerBlacklist", String.class);
				method.invoke(null, blockID);
			}
			catch(Throwable e)
			{
				printWarning("TileHandler.registerBreakerBlacklist()", e, blockID);
				return;
			}
		}

		/**
		 * Add a new Detector Mode to the mode list.
		 * @param mode The mode to add.
		 */
		public static void addDetectorMode(IDetectorMode mode)
		{
			initAPI();
			try
			{
				Method method = tileHandler.getMethod("addDetectorMode", IDetectorMode.class);
				method.invoke(null, mode);
			}
			catch(Throwable e)
			{
				printWarning("TileHandler.addDetectorMode()", e, mode);
				return;
			}
		}
	}

	/**
	 * The main Research API sub-class. Use this to register custom research.
	 * @author Infernio
	 */
	public static class Research {

		/** The current Research Registry. <strong>Do NOT change the value of this field</strong>, as that will pretty much disable addons that use this to register Research. */
		public static IResearchRegistry registry;
		/** The current Technology List. <strong>Do NOT change the value of this field</strong>, as that will pretty much disable addons that use this to register Technologies. */
		public static ITechnologyList techList;
	}

}
