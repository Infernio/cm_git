package com.infernio.Craftmistry.api.tile;

import net.minecraft.world.World;

/**
 * This class allows you to check saplings / wood / leaves yourself,
 * so you can use NBT data, tile entites etc. for your trees.
 * @author Infernio
 */
public interface ITreeHandler
{
	/**
	 * Check if the sapling at the given location is a valid sapling for this tree.
	 * @param tree The type of tree maintained at the given location.
	 * @param world The current world instance.
	 * @param x The x coordinate of the sapling.
	 * @param y The y coordinate of the sapling.
	 * @param z The z coordinate of the sapling.
	 * @return True if the sapling planted at the given location is valid for this tree.
	 */
	boolean isSaplingValid(String tree, World world, int x, int y, int z);

	/**
	 * Check if the wood at the given location is a valid wood for this tree.
	 * @param tree The type of tree maintained at the given location.
	 * @param world The current world instance.
	 * @param x The x coordinate of the sapling.
	 * @param y The y coordinate of the sapling.
	 * @param z The z coordinate of the sapling.
	 * @return True if the wood at the given location is valid for this tree.
	 */
	boolean isWoodValid(String tree, World world, int x, int y, int z);

	/**
	 * Check if the leaf at the given location is a valid leaf for this tree.
	 * @param tree The type of tree maintained at the given location.
	 * @param world The current world instance.
	 * @param x The x coordinate of the sapling.
	 * @param y The y coordinate of the sapling.
	 * @param z The z coordinate of the sapling.
	 * @return True if the leaf at the given location is valid for this tree.
	 */
	boolean isLeafValid(String tree, World worldObj, int x, int y, int z);
}
