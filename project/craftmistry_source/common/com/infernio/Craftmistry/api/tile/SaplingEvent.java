package com.infernio.Craftmistry.api.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import cpw.mods.fml.common.eventhandler.Cancelable;

public class SaplingEvent extends WorldEvent
{
	public SaplingEvent(World world)
	{
		super(world);
	}

	@Cancelable
	public static class CanPlantSaplingEvent extends SaplingEvent
	{
		public final ItemStack sapling;
		public final int x;
		public final int y;
		public final int z;

		public CanPlantSaplingEvent(ItemStack sapling, World world, int x, int y, int z)
		{
			super(world);
			this.sapling = sapling;
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	@Cancelable
	public static class PlantSaplingEvent extends SaplingEvent
	{
		public final ItemStack sapling;
		public final int x;
		public final int y;
		public final int z;

		public boolean planted;
	
		public PlantSaplingEvent(ItemStack sapling, World world, int x, int y, int z)
		{
			super(world);
			this.sapling = sapling;
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
