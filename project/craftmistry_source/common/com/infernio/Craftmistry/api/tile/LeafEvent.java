package com.infernio.Craftmistry.api.tile;

import net.minecraft.inventory.IInventory;
import net.minecraft.world.World;
import net.minecraftforge.event.world.WorldEvent;
import cpw.mods.fml.common.eventhandler.Cancelable;

public class LeafEvent extends WorldEvent
{
	public LeafEvent(World world)
	{
		super(world);
	}

	@Cancelable
	public static class CanHarvestLeafEvent extends LeafEvent
	{
		public final String tree;
		public final int x;
		public final int y;
		public final int z;

		public CanHarvestLeafEvent(String tree, World world, int x, int y, int z)
		{
			super(world);
			this.tree = tree;
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	@Cancelable
	public static class HarvestLeafEvent extends WoodEvent
	{
		public final String tree;
		public final IInventory forester;
		public final int x;
		public final int y;
		public final int z;

		public boolean harvested;

		public HarvestLeafEvent(String tree, World world, IInventory forester, int x, int y, int z)
		{
			super(world);
			this.tree = tree;
			this.forester = forester;
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}
}
