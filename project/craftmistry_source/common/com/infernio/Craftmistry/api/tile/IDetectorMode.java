package com.infernio.Craftmistry.api.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

/**
 * This interface can be used to create custom detector modes.
 * @author Infernio
 */
public interface IDetectorMode {

	/**
	 * Determins whether power should be drained and {@link #detect(ItemStack, World)} should be called even if no item is inside the detector.
	 * @return True if an item is required in order for power to be drained.
	 */
	boolean requiresItem();

	/**
	 * This should return an unlocalized name for this detector mode.
	 * @return An unlocalized name for this detector mode.
	 */
	String getUnlocalizedName();

	/**
	 * This should return an ItemStack that will be displayed in the GUI.
	 * @return
	 */
	ItemStack getDisplayStack();


	/**
	 * This method is called every tick if an item is in the detector or {@link #requiresItem()} returns false.
	 * @param stack The ItemStack currently in the detector. Can be null if {@link #requiresItem()} returns false.
	 * @param world The current World instance.
	 * @return True to output the current redstone configuration as a redstone signal. If it returns false, redstone output is set to 0.
	 */
	boolean detect(ItemStack stack, World world);

}
