package com.infernio.Craftmistry.client.gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.common.container.ContainerTestTubeRack;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUITestTubeRack extends GuiContainer {

	private IInventory upperInv;
	private IInventory lowerInv;
	
	public GUITestTubeRack(IInventory playerInv,  IInventory rackInv)
	{
		super(new ContainerTestTubeRack(playerInv, rackInv));
		this.upperInv = rackInv;
		this.lowerInv = playerInv;
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j) 
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.upperInv.getInventoryName()), 8, 6,4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_TEST_TUBE_RACK_RESOURCE);
		int var5 = (this.width - this.xSize) / 2;
		int var6 = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(var5, var6, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(var5, var6 + 6 * 18 + 17, 0, 126, this.xSize, 96);
	}

}
