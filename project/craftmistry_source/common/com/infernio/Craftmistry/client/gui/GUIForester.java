package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.Craftmistry.common.container.ContainerForester;
import com.infernio.Craftmistry.common.tile.TileForester;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIForester extends GuiContainer {

	private IInventory player;
	private TileForester forester;
    private int inventoryRows;
	private Tooltip upgradeInfo;

	public GUIForester(IInventory playerInv, IInventory foresterInv)
	{
		super(new ContainerForester(playerInv, foresterInv));
		this.player = playerInv;
		this.forester = (TileForester)foresterInv;
		short short1 = 222;
        int i = short1 - 108;
        this.inventoryRows = 6;
        this.ySize = i + this.inventoryRows * 18;
		this.upgradeInfo = Tooltip.loadLocalizedTooltip("tile.upgradeSlot.tooltip");
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_LIGHTING);
		itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, this.mc.renderEngine, new ItemStack(Blocks.sapling, 1, 0), this.guiLeft + 12, this.guiTop + 61);
		itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, this.mc.renderEngine, new ItemStack(Blocks.crafting_table, 1, 0), this.guiLeft + 12, this.guiTop + 79);
		GL11.glPopMatrix();
		GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
        this.fontRendererObj.drawString("" + this.forester.treesMaintained, this.guiLeft + 32, this.guiTop + 66, -1);
        this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMForester.task" + this.forester.task), this.guiLeft + 32, this.guiTop + 83, -1);
        if(this.func_146978_c(12, 61, 16, 16, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMForester.treesMaintained"));
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        }
        if(this.func_146978_c(12, 77, 16, 16, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMForester.currentTask"));
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        }
        if(this.forester.getStackInSlot(16) == null && this.func_146978_c(6, 6, 16, 16, x, y))
        {
        	this.drawSlotInfo(x, y);
        }
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
        this.fontRendererObj.drawString(this.forester.hasCustomInventoryName() ? this.forester.getInventoryName() : I18n.format(this.forester.getInventoryName(), new Object[0]), 28, 6, 4210752);
        this.fontRendererObj.drawString(this.player.hasCustomInventoryName() ? this.player.getInventoryName() : I18n.format(this.player.getInventoryName(), new Object[0]), 8, this.ySize - 96 + 2, 4210752);
	}

	private void drawSlotInfo(int x, int y)
    {
        ArrayList<String> list = new ArrayList<String>();
        list.add(Tooltip.resolveColors(StatCollector.translateToLocal("tile.upgradeSlot.name")));
        if(GuiScreen.isShiftKeyDown())
        {
        	list.addAll(this.upgradeInfo.tooltip);
        }
        else
        {
        	list.add(Tooltip.resolveColors(StatCollector.translateToLocal("tile.upgradeSlot.holdShift")));
        }
        list.add(Tooltip.resolveColors(StatCollector.translateToLocal("tile.upgradeSlot.noneInstalled")));
        this.drawHoveringText(list, x, y, this.fontRendererObj);
    }

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(ResourceLib.GUI_FORESTER_RESOURCE);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.inventoryRows * 18 + 17);
        this.drawTexturedModalRect(k, l + this.inventoryRows * 18 + 17, 0, 126, this.xSize, 96);
	}
}
