package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.container.ContainerResearchPanel;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageOpenResearchGui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIResearchPanel extends GuiContainer {

	static final ItemStack materialCompartment = new ItemStack(ModBlocks.research, 1, 2);
	static final ItemStack researchPanel = new ItemStack(ModBlocks.research);
	static final ItemStack selectItems = new ItemStack(Items.writable_book);
	private TileResearchPanel panel;
	private boolean changing;

	public GUIResearchPanel(TileResearchPanel tile)
	{
		super(new ContainerResearchPanel(tile));
		this.panel = tile;
		this.changing = false;
		this.ySize = 136;
        this.xSize = 195;
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_LIGHTING);
		itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, materialCompartment, this.guiLeft + 34, this.guiTop - 9);
		itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, researchPanel, this.guiLeft + 6, this.guiTop - 9);
		itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, selectItems, this.guiLeft + 62, this.guiTop - 9);
		GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
        if(this.func_146978_c(0, -14, 28, 30, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + "Project Management");
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        }
        if(this.func_146978_c(30, -14, 28, 30, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + "Material Compartment");
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        	if(Mouse.isButtonDown(0) && !this.changing)
			{
        		this.changing = true;
				PacketHandler.sendToServer(new MessageOpenResearchGui(this.panel.xCoord, this.panel.yCoord, this.panel.zCoord, (short)IntLib.GUI_MATERIAL_COMPARTMENT_ID, Craftmistry.instance.modGUIID, "MaterialCompartment"));
				return;
			}
        }
		if(this.func_146978_c(60, -14, 28, 30, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + "Item Selection");
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        	if(Mouse.isButtonDown(0) && !this.changing)
			{
        		this.changing = true;
				PacketHandler.sendToServer(new MessageOpenResearchGui(this.panel.xCoord, this.panel.yCoord, this.panel.zCoord, (short)IntLib.GUI_SELECT_PROJECT_ITEMS_ID, Craftmistry.instance.modGUIID, "Panel"));
				return;
			}
        }
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_PANEL_RESOURCE);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l - 17, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(k, l + 6 * 18, 0, 126, this.xSize, 96);
	}

}
