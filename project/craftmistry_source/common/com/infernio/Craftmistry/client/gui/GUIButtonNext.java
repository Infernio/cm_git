package com.infernio.Craftmistry.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIButtonNext extends GuiButton
{
    private final boolean mirrored;

    public GUIButtonNext(int par1, int par2, int par3, boolean par4)
    {
        super(par1, par2, par3, 12, 19, "");
        this.mirrored = par4;
    }

    @Override
    public void drawButton(Minecraft par1Minecraft, int par2, int par3)
    {
        if(this.visible)
        {
        	par1Minecraft.renderEngine.bindTexture(ResourceLib.GUI_ANALYZER_RESOURCE);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            boolean flag = par2 >= this.xPosition && par3 >= this.yPosition && par2 < this.xPosition + this.width && par3 < this.yPosition + this.height;
            int k = 0;
            int l = 176;
            if (!this.enabled)
            {
                l += this.width * 2;
            }
            else if (flag)
            {
                l += this.width;
            }
            if (!this.mirrored)
            {
                k += this.height;
            }
            this.drawTexturedModalRect(this.xPosition, this.yPosition, l, k, this.width, this.height);
        }
    }
}
