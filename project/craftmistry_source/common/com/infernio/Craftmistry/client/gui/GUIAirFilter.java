package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import codechicken.nei.recipe.GuiCraftingRecipe;

import com.infernio.Craftmistry.common.container.ContainerAirFilter;
import com.infernio.Craftmistry.common.recipe.AirFilterRecipes;
import com.infernio.Craftmistry.common.tile.TileAirFilter;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageAirFilterMode;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIAirFilter extends GuiContainer {

	private final int maxTargets = AirFilterRecipes.recipes().getRegistrationSize() - 1;
	private IInventory lowerInv;
	private TileAirFilter airFilter;
	private GUIButtonNext nextModeButton;
	private GUIButtonNext prevModeButton;
	private Tooltip help;

	public GUIAirFilter(IInventory playerInv, IInventory airFilterInv)
	{
		super(new ContainerAirFilter(playerInv, airFilterInv));
		this.lowerInv = playerInv;
		this.airFilter = (TileAirFilter)airFilterInv;
		this.help = Tooltip.loadLocalizedTooltip("tile.blockCMAirFilter.tooltip");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initGui()
	{
        super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.buttonList.add(this.nextModeButton = new GUIButtonNext(1, i + 67, j + 45, true));
        this.buttonList.add(this.prevModeButton = new GUIButtonNext(2, i + 5, j + 45, false));
        int mode = this.airFilter.mode;
		nextModeButton.enabled = mode < maxTargets;
		prevModeButton.enabled = mode > 0;
	}

	@Override
    public void updateScreen()
    {
		super.updateScreen();
		int mode = this.airFilter.mode;
		nextModeButton.enabled = mode < maxTargets;
		prevModeButton.enabled = mode > 0;
    }

	@Override
	protected void mouseClicked(int x, int y, int eventButton)
	{
		if(CompatHandler.neiPresent && this.func_146978_c(79, 34, 24, 17, x, y) && eventButton == 0)
		{
			GuiCraftingRecipe.openRecipeGui("craftmistry.airfilter");
			return;
		}
		super.mouseClicked(x, y, eventButton);
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        if(this.func_146978_c(5, 45, 12, 19, x, y) && this.prevModeButton.enabled)
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMAirFilter.changeFilter"));
        	this.drawHoveringText(list, x, y, fontRendererObj);
        }
        if(this.func_146978_c(67, 45, 12, 19, x, y) && this.nextModeButton.enabled)
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMAirFilter.changeFilter"));
        	this.drawHoveringText(list, x, y, fontRendererObj);
        }
        if(CompatHandler.neiPresent && this.func_146978_c(79, 34, 24, 17, x, y))
		{
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(StatCollector.translateToLocal("cm_neiplugin.general.recipes"));
        	this.drawHoveringText(list, x, y, fontRendererObj);
		}
        if(this.func_146978_c(157, 4, 15, 15, x, y))
        {
        	this.drawHoveringText(this.help.tooltip, x, y, fontRendererObj);
        }
        GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
	}

	@Override
	protected void actionPerformed(GuiButton button)
    {
		boolean flag = false;
		if(button == this.nextModeButton)
		{
			++this.airFilter.mode;
			flag = true;
		}
		if(button == this.prevModeButton)
		{
			--this.airFilter.mode;
			flag = true;
		}
		if(flag)
		{
			this.updateScreen();
			PacketHandler.sendToServer(new MessageAirFilterMode(this.airFilter.xCoord, this.airFilter.yCoord, this.airFilter.zCoord, this.airFilter.mode));
		}
    }

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.airFilter.getInventoryName()), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
		this.fontRendererObj.drawString(AirFilterRecipes.recipes().convert(this.airFilter.mode), 23, 51, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_AIR_FILTER_RESOURCE);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(k, l + 6 * 18 + 17, 0, 126, this.xSize, 96);
		int progress = this.airFilter.getProgressScaled(24);
        this.drawTexturedModalRect(k + 79, l + 34, 176, 38, progress + 1, 16);
	}

}
