package com.infernio.Craftmistry.client.gui;

import java.util.List;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.container.ContainerStabilizer;
import com.infernio.Craftmistry.common.tile.TileStabilizer;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageStabilizerAmplifier;
import com.infernio.InfernoCore.client.gui.GUIUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIStabilizer extends GuiContainer {

	private IInventory lowerInv;
	private TileStabilizer stabilizer;
	private GUIButtonNext incrAmplButton;
	private GUIButtonNext decrAmplButton;
	
	public GUIStabilizer(IInventory playerInv, IInventory airFilterInv)
	{
		super(new ContainerStabilizer(playerInv, airFilterInv));
		this.lowerInv = playerInv;
		this.stabilizer = (TileStabilizer)airFilterInv;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void initGui()
	{
        super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.buttonList.add(this.incrAmplButton = new GUIButtonNext(1, i + 110, j + 45, true));
        this.buttonList.add(this.decrAmplButton = new GUIButtonNext(2, i + 55, j + 45, false));
		int amplifier = this.stabilizer.amplifier;
		incrAmplButton.enabled = amplifier < 10;
		decrAmplButton.enabled = amplifier > 1;
	}
	
	@Override
    public void updateScreen()
    {
		super.updateScreen();
		int amplifier = this.stabilizer.amplifier;
		incrAmplButton.enabled = amplifier < 10;
		decrAmplButton.enabled = amplifier > 1;
    }

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		Slot slot = GUIUtils.getSlotAtPosition(this.inventorySlots, x, y, this.guiLeft, this.guiTop);
		if(slot != null && slot.getHasStack())
		{
			this.renderToolTip(slot.getStack(), x, y);
		}
	}

	@Override
	protected void actionPerformed(GuiButton button)
    {
		boolean flag = false;
		if(button == this.incrAmplButton)
		{
			++stabilizer.amplifier;
			flag = true;
		}
		if(button == this.decrAmplButton)
		{
			--stabilizer.amplifier;
			flag = true;
		}
		if(flag)
		{
			this.updateScreen();
			PacketHandler.sendToServer(new MessageStabilizerAmplifier(this.stabilizer.xCoord, this.stabilizer.yCoord, this.stabilizer.zCoord, this.stabilizer.amplifier));
		}
    }
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void renderToolTip(ItemStack stack, int x, int y)
	{
        List list = stack.getTooltip(this.mc.thePlayer, this.mc.gameSettings.advancedItemTooltips);
        if(Elements.isElementRadioactive(stack.getItemDamage()))
        {
        	if(stack.stackTagCompound != null)
        	{
		        list.add(EnumChatFormatting.GOLD + "Half-Life: " + stack.stackTagCompound.getInteger("HalfLife") + "t");
	        }
        }
        for(int k = 0; k < list.size(); ++k)
        {
            if(k == 0)
            {
                list.set(k, "\u00a7" + Integer.toHexString(stack.getRarity().rarityColor.getFormattingCode()) + (String)list.get(k));
            }
            else
            {
                list.set(k, EnumChatFormatting.GRAY + (String)list.get(k));
            }
        }
        FontRenderer font = stack.getItem().getFontRenderer(stack);
        drawHoveringText(list, x, y, (font == null ? fontRendererObj : font));
	}
	
	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.stabilizer.getInventoryName()), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
		if(this.stabilizer.amplifier == 10)
		{
			this.fontRendererObj.drawString("10.0x", 75, 51, 4210752);
		}
		else
		{
			this.fontRendererObj.drawString(this.stabilizer.amplifier + ".0x", 80, 51, 4210752);
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_STABILIZER_RESOURCE);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(k, l + 6 * 18 + 17, 0, 126, this.xSize, 96);
	}
	
}
