package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.Craftmistry.Craftmistry;
import com.infernio.Craftmistry.common.container.ContainerMaterialCompartment;
import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageOpenResearchGui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIMaterialCompartment extends GuiContainer {

	private int inventoryRows;
	private IInventory player;
	private TileMaterialCompartment compartment;
	private boolean changing;

	public GUIMaterialCompartment(IInventory playerInv, IInventory compartmentInv)
	{
		super(new ContainerMaterialCompartment(playerInv, compartmentInv));
		this.player = playerInv;
		this.compartment = (TileMaterialCompartment)compartmentInv;
        this.inventoryRows = compartmentInv.getSizeInventory() / 9;
        this.ySize = 144 + this.inventoryRows * 18;
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_LIGHTING);
		itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, GUIResearchPanel.materialCompartment, this.guiLeft + 36, this.guiTop + 9);
		itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, GUIResearchPanel.researchPanel, this.guiLeft + 8, this.guiTop + 9);
		itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, GUIResearchPanel.selectItems, this.guiLeft + 63, this.guiTop + 9);
		GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
		if(this.func_146978_c(0, 0, 28, 30, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + "Project Management");
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
			if(Mouse.isButtonDown(0) && !this.changing)
			{
				this.changing = true;
				PacketHandler.sendToServer(new MessageOpenResearchGui(this.compartment.xCoord, this.compartment.yCoord, this.compartment.zCoord, (short)IntLib.GUI_PANEL_ID, Craftmistry.instance.modGUIID, "Panel"));
				return;
			}
        }
        if(this.func_146978_c(30, 0, 28, 30, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + "Material Compartment");
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        }
		if(this.func_146978_c(60, 0, 28, 30, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + "Item Selection");
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        	if(Mouse.isButtonDown(0) && !this.changing)
			{
        		this.changing = true;
				PacketHandler.sendToServer(new MessageOpenResearchGui(this.compartment.xCoord, this.compartment.yCoord, this.compartment.zCoord, (short)IntLib.GUI_SELECT_PROJECT_ITEMS_ID, Craftmistry.instance.modGUIID, "Panel"));
				return;
			}
        }
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
		this.fontRendererObj.drawString(this.compartment.hasCustomInventoryName() ? this.compartment.getInventoryName() : StatCollector.translateToLocal(this.compartment.getInventoryName()), 8, 36, 4210752);
        this.fontRendererObj.drawString(this.player.hasCustomInventoryName() ? this.player.getInventoryName() : StatCollector.translateToLocal(this.player.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(ResourceLib.GUI_MATERIAL_COMAPRTMENT_RESOURCE);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 108 + 57);
        this.drawTexturedModalRect(k, l + 108 + 17, 0, 126, this.xSize, 125);
	}

}
