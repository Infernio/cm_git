package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.container.ContainerDetector;
import com.infernio.Craftmistry.common.detector.DetectorModes;
import com.infernio.Craftmistry.common.tile.TileDetector;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageDetectorConfig;
import com.infernio.Craftmistry.core.network.message.MessageDetectorSettings;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIDetector extends GuiContainer {

	private IInventory lowerInv;
	private TileDetector detector;
	private GUIButtonNext incrSettingButton;
	private GUIButtonNext decrSettingButton;
	private GuiTextField outputPowerField;
	private Tooltip help;

	public GUIDetector(IInventory playerInv, IInventory detectorInv)
	{
		super(new ContainerDetector(playerInv, detectorInv));
		this.lowerInv = playerInv;
		this.detector = (TileDetector)detectorInv;
		this.help = Tooltip.loadLocalizedTooltip("tile.blockCMDetector.tooltip");
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initGui()
	{
        super.initGui();
        Keyboard.enableRepeatEvents(true);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.buttonList.add(this.incrSettingButton = new GUIButtonNext(1, i + 110, j + 45, true));
        this.buttonList.add(this.decrSettingButton = new GUIButtonNext(2, i + 55, j + 45, false));
        this.incrSettingButton.enabled = true;
        this.decrSettingButton.enabled = true;
        this.outputPowerField = new GuiTextField(this.fontRendererObj, i + 50, j + 24, 25, 12);
        this.outputPowerField.setTextColor(-1);
        this.outputPowerField.setDisabledTextColour(-1);
        this.outputPowerField.setEnableBackgroundDrawing(false);
        this.outputPowerField.setMaxStringLength(2);
        this.outputPowerField.setText("" + this.detector.redstoneConfig);
	}

    @Override
	public void onGuiClosed()
    {
        super.onGuiClosed();
        Keyboard.enableRepeatEvents(false);
    }

    @Override
	protected void keyTyped(char key, int i)
    {
        if(this.outputPowerField.textboxKeyTyped(key, i))
        {
        	try
        	{
	            this.detector.redstoneConfig = MathHelper.clamp_int(Integer.parseInt(this.outputPowerField.getText()), 0, 15);
	            PacketHandler.sendToServer(new MessageDetectorConfig(this.detector.xCoord, this.detector.yCoord, this.detector.zCoord, this.detector.redstoneConfig));
        	}
        	catch(NumberFormatException e)
        	{
        		this.detector.redstoneConfig = 0;
	            PacketHandler.sendToServer(new MessageDetectorConfig(this.detector.xCoord, this.detector.yCoord, this.detector.zCoord, 0));
        	}
        }
        else
        {
            super.keyTyped(key, i);
        }
    }

    @Override
	protected void mouseClicked(int x, int y, int button)
    {
        super.mouseClicked(x, y, button);
        this.outputPowerField.mouseClicked(x, y, button);
    }

	@Override
	protected void actionPerformed(GuiButton button)
    {
		boolean flag = false;
		if(button == this.incrSettingButton)
		{
			int setting = this.detector.setting + 1;
			if(setting < TileDetector.SETTINGS)
			{
				this.detector.setting = setting;
				flag = true;
			}
			else
			{
				setting = 0;
				this.detector.setting = setting;
				flag = true;
			}
		}
		else if(button == this.decrSettingButton)
		{
			int setting = this.detector.setting - 1;
			if(setting >= 0)
			{
				this.detector.setting = setting;
				flag = true;
			}
			else
			{
				setting = TileDetector.SETTINGS - 1;
				this.detector.setting = setting;
				flag = true;
			}
		}
		if(flag)
		{
            PacketHandler.sendToServer(new MessageDetectorSettings(this.detector.xCoord, this.detector.yCoord, this.detector.zCoord, this.detector.setting));
		}
    }
	
	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glDisable(GL11.GL_LIGHTING);
        this.outputPowerField.drawTextBox();
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_LIGHTING);
        if(!this.outputPowerField.isFocused())
        {
        	this.outputPowerField.setText("" + this.detector.redstoneConfig);
        }
        if(this.func_146978_c(157, 4, 15, 15, x, y))
        {
        	this.drawHoveringText(this.help.tooltip, x, y, this.fontRendererObj);
        }
        if(this.func_146978_c(110, 45, 12, 19, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMDetector.nextCond"));
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        }
        if(this.func_146978_c(55, 45, 12, 19, x, y))
        {
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMDetector.prevCond"));
        	this.drawHoveringText(list, x, y, this.fontRendererObj);
        }
        IDetectorMode mode = DetectorModes.detections().getMode(this.detector.setting);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        if(mode != null)
        {
	        itemRender.renderItemAndEffectIntoGUI(this.fontRendererObj, this.mc.renderEngine, mode.getDisplayStack(), k + 80, l + 46);
	        itemRender.renderItemOverlayIntoGUI(this.fontRendererObj, this.mc.renderEngine, mode.getDisplayStack(), k + 80, l + 46);
	        if(this.func_146978_c(80, 46, 16, 16, x, y))
	        {
	        	this.drawMode(mode, x, y);
	        }
        }
        GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
	}

    private void drawMode(IDetectorMode mode, int x, int y)
    {
    	ArrayList<String> list = new ArrayList<String>();
        list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal(mode.getUnlocalizedName()));
        this.drawHoveringText(list, x, y, this.fontRendererObj);
    }

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.detector.getInventoryName()), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_DETECTOR_RESOURCE);
		int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
        this.drawTexturedModalRect(k + 45, l + 20, 0, this.ySize, 25, 16);
	}

}
