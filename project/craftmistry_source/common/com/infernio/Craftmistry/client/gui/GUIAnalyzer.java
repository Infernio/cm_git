package com.infernio.Craftmistry.client.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.api.item.ISpecialInfo;
import com.infernio.Craftmistry.common.container.ContainerAnalyzer;
import com.infernio.Craftmistry.common.recipe.InfuserRecipes;
import com.infernio.Craftmistry.common.tile.TileAnalyzer;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.nei.NEIHandler;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIAnalyzer extends GuiContainer {

	private IInventory upperInv;
    private GUIButtonNext nextPageButton;
    private GUIButtonNext previousPageButton;
    private Tooltip help;
	
	public GUIAnalyzer(IInventory playerInv, IInventory analyzerInv)
	{
		super(new ContainerAnalyzer(playerInv, analyzerInv));
		this.upperInv = analyzerInv;
		this.help = Tooltip.loadLocalizedTooltip("tile.blockCMAnalyzer.tooltip");
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		TileAnalyzer tile = (TileAnalyzer)upperInv;
		ItemStack analyzedItem = tile.getStackInSlot(0);
		if(analyzedItem != null && ItemHelper.hasItemSpecialInfo(analyzedItem.getItem(), analyzedItem.getItemDamage()))
		{
			ISpecialInfo info = ItemHelper.getInformation(analyzedItem.getItem(), analyzedItem.getItemDamage());
			if(info != null)
			{
				GUIUtils.setZLevel(zLevel);
				info.addToScreen(analyzedItem, this.fontRendererObj, x, y, tile.currentPage, guiLeft, guiTop, height, width);
				GUIUtils.resetColorSelection();
			}
		}
		if(this.func_146978_c(8, 4, 15, 15, x, y))
        {
        	this.drawHoveringText(this.help.tooltip, x, y, this.fontRendererObj);
        }
	}

    @SuppressWarnings("unchecked")
    @Override
	public void initGui()
    {
        super.initGui();
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.buttonList.add(this.nextPageButton = new GUIButtonNext(1, i + 161, j + 57, true));
        this.buttonList.add(this.previousPageButton = new GUIButtonNext(2, i + 59, j + 57, false));
        TileAnalyzer tile = (TileAnalyzer)this.upperInv;
        ItemStack stack = tile.getStackInSlot(0);
        if(stack != null && ItemHelper.hasItemSpecialInfo(stack.getItem(), stack.getItemDamage()))
        {
            this.nextPageButton.enabled = tile.currentPage < ItemHelper.getInformation(stack.getItem(), stack.getItemDamage()).getNumberPages(stack);
            this.previousPageButton.enabled = tile.currentPage > 0;
        }
        else
        {
        	if(InfuserRecipes.infusing().getResult(stack) != null)
        	{
        		this.nextPageButton.enabled = tile.currentPage < 1;
        		this.previousPageButton.enabled = tile.currentPage > 0;
        	}
        	else
        	{
        		tile.currentPage = 0;
        		this.nextPageButton.enabled = false;
        		this.previousPageButton.enabled = false;
        	}
        }
    }
	
	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j) 
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.upperInv.getInventoryName()), 24, 6, 4210752);
		TileAnalyzer tile = (TileAnalyzer)upperInv;
		ItemStack analyzedItem = tile.getStackInSlot(0);
		if(analyzedItem != null)
		{
			if(ItemHelper.hasItemSpecialInfo(analyzedItem.getItem(), analyzedItem.getItemDamage()))
			{
				ISpecialInfo info = ItemHelper.getInformation(analyzedItem.getItem(), analyzedItem.getItemDamage());
				if(info != null)
				{
					if(info.printDefaultInfo(analyzedItem) && tile.currentPage == 0)
					{
						if(analyzedItem.isItemStackDamageable())
						{
							this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.durability"), 74, 15, -1);
							this.fontRendererObj.drawString("" + (analyzedItem.getMaxDamage() - analyzedItem.getItemDamage()) + "/" + analyzedItem.getMaxDamage(), 74, 25, -1);
						}
						if(TileEntityFurnace.isItemFuel(analyzedItem))
						{
							if(analyzedItem.isItemStackDamageable())
							{
								this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.burnTime"), 74, 40, -1);
								this.fontRendererObj.drawString("" + TileEntityFurnace.getItemBurnTime(analyzedItem), 74, 50, -1);
							}
							else
							{
								this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.burnTime"), 74, 15, -1);
								this.fontRendererObj.drawString("" + TileEntityFurnace.getItemBurnTime(analyzedItem), 74, 25, -1);
							}
						}
						ItemStack result = FurnaceRecipes.smelting().getSmeltingResult(analyzedItem);
						if(result != null)
						{
							if(analyzedItem.isItemStackDamageable())
							{
								this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.smelting"), 74, 40, -1);
								itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, result, 74, 50);
								this.fontRendererObj.drawString("(" + result.stackSize + ")" , 90, 40, -1);
							}
							else
							{
								if(TileEntityFurnace.isItemFuel(analyzedItem))
								{
									this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.smelting"), 74, 40, -1);
									itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, result, 74, 50);
									this.fontRendererObj.drawString("(" + result.stackSize + ")" , 95, 56, -1);
								}
								else
								{
									this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.smelting"), 74, 15, -1);
									itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, result, 74, 25);
									this.fontRendererObj.drawString("(" + result.stackSize + ")" , 95, 29, -1);
								}
							}
						}
						ItemStack infusionResult = InfuserRecipes.infusing().getResult(analyzedItem);
						if(infusionResult != null && tile.currentPage == 1)
						{
							this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.infusion"), 74, 15, -1);
							itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, NEIHandler.EMPTY_TEST_TUBE, 74, 25);
							this.fontRendererObj.drawString("+" , 93, 29, -1);
							itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, analyzedItem, 102, 25);
							this.fontRendererObj.drawString("->" , 123, 29, -1);
							itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, infusionResult, 135, 25);
						}
					}
					String[] information = info.getSpecialInfo(analyzedItem, tile.currentPage);
					int currentLine = 5;
					if(information != null)
					{
						for(String s : information)
						{
							if(s.startsWith("%a") && currentLine != 5)
							{
								currentLine += 15;
								s = s.substring(2);
							}
							else
							{
								currentLine += 10;
							}
							if(StatCollector.canTranslate(s))
							{
								s = StatCollector.translateToLocal(s);
							}
							this.fontRendererObj.drawString(s, 74, currentLine, -1);
						}
					}
				}
			}
			else
			{
				if(analyzedItem.isItemStackDamageable())
				{
					this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.durability"), 74, 15, -1);
					this.fontRendererObj.drawString("" + (analyzedItem.getMaxDamage() - analyzedItem.getItemDamage()) + "/" + analyzedItem.getMaxDamage(), 74, 25, -1);
				}
				if(TileEntityFurnace.isItemFuel(analyzedItem))
				{
					if(analyzedItem.isItemStackDamageable())
					{
						this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.burnTime"), 74, 40, -1);
						this.fontRendererObj.drawString("" + TileEntityFurnace.getItemBurnTime(analyzedItem), 74, 50, -1);
					}
					else
					{
						this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.burnTime"), 74, 15, -1);
						this.fontRendererObj.drawString("" + TileEntityFurnace.getItemBurnTime(analyzedItem), 74, 25, -1);
					}
				}
				ItemStack result = FurnaceRecipes.smelting().getSmeltingResult(analyzedItem);
				if(result != null)
				{
					if(analyzedItem.isItemStackDamageable())
					{
						this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.smelting"), 74, 40, -1);
						itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, result, 74, 50);
						this.fontRendererObj.drawString("(" + result.stackSize + ")" , 90, 40, -1);
					}
					else
					{
						if(TileEntityFurnace.isItemFuel(analyzedItem))
						{
							this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.smelting"), 74, 40, -1);
							itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, result, 74, 50);
							this.fontRendererObj.drawString("(" + result.stackSize + ")" , 95, 56, -1);
						}
						else
						{
							this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.smelting"), 74, 15, -1);
							itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, result, 74, 25);
							this.fontRendererObj.drawString("(" + result.stackSize + ")" , 95, 29, -1);
						}
					}
				}
				ItemStack infusionResult = InfuserRecipes.infusing().getResult(analyzedItem);
				if(infusionResult != null && tile.currentPage == 1)
				{
					this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.infusion"), 74, 15, -1);
					itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, NEIHandler.EMPTY_TEST_TUBE, 74, 25);
					this.fontRendererObj.drawString("+" , 93, 29, -1);
					itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, analyzedItem, 102, 25);
					this.fontRendererObj.drawString("->" , 123, 29, -1);
					itemRender.renderItemAndEffectIntoGUI(fontRendererObj, this.mc.renderEngine, infusionResult, 135, 25);
				}
			}
		}
		else
		{
			this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.helpLine1"), 74, 15, -1);
			this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.helpLine2"), 74, 25, -1);
			this.fontRendererObj.drawString(StatCollector.translateToLocal("tile.blockCMAnalyzer.helpLine3"), 74, 35, -1);
		}
	}
	
	@Override
    public void updateScreen()
    {
        super.updateScreen();
        TileAnalyzer tile = (TileAnalyzer)this.upperInv;
        ItemStack stack = tile.getStackInSlot(0);
        if(stack != null && ItemHelper.hasItemSpecialInfo(stack.getItem(), stack.getItemDamage()))
        {
            this.nextPageButton.enabled = tile.currentPage < ItemHelper.getInformation(stack.getItem(), stack.getItemDamage()).getNumberPages(stack);
            this.previousPageButton.enabled = tile.currentPage > 0;
        }
        else
        {
        	if(InfuserRecipes.infusing().getResult(stack) != null)
        	{
        		this.nextPageButton.enabled = tile.currentPage < 1;
        		this.previousPageButton.enabled = tile.currentPage > 0;
        	}
        	else
        	{
        		tile.currentPage = 0;
        		this.nextPageButton.enabled = false;
        		this.previousPageButton.enabled = false;
        	}
        }
    }
	
	@Override
	protected void actionPerformed(GuiButton button)
    {
        boolean flag = false;
        TileAnalyzer tile = (TileAnalyzer)this.upperInv;

        if(button == this.nextPageButton)
        {
        	++tile.currentPage;
            flag = true;
        }
        else if(button == this.previousPageButton)
        {
            --tile.currentPage;
            flag = true;
        }

        if(flag)
        {
        	this.updateScreen();
        }
    }

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_ANALYZER_RESOURCE);
		int var5 = (this.width - this.xSize) / 2;
		int var6 = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(var5, var6, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(var5, var6 + 6 * 18 + 17, 0, 126, this.xSize, 96);
		TileAnalyzer tile = (TileAnalyzer)upperInv;
		ItemStack analyzedItem = tile.getStackInSlot(0);
		if(analyzedItem != null && ItemHelper.hasItemSpecialInfo(analyzedItem.getItem(), analyzedItem.getItemDamage()))
		{
			ISpecialInfo info = ItemHelper.getInformation(analyzedItem.getItem(), analyzedItem.getItemDamage());
			if(info != null)
			{
				info.addSpecialEffects(analyzedItem, mc, fontRendererObj, itemRender, tile.currentPage, zLevel, guiLeft, guiTop);
			}
		}
	}

}
