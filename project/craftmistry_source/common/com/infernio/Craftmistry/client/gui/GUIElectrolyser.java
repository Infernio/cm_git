package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import codechicken.nei.recipe.GuiCraftingRecipe;

import com.infernio.Craftmistry.common.container.ContainerElectrolyser;
import com.infernio.Craftmistry.common.tile.TileElectrolyser;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIElectrolyser extends GuiContainer {

	private IInventory lowerInv;
	private TileElectrolyser electrolyser;
	private Tooltip help;

	public GUIElectrolyser(IInventory playerInv, IInventory electrolyserInv)
	{
		super(new ContainerElectrolyser(playerInv, electrolyserInv));
		this.lowerInv = playerInv;
		this.electrolyser = (TileElectrolyser)electrolyserInv;
		this.help = Tooltip.loadLocalizedTooltip("tile.blockCMElectrolyser.tooltip");
	}

	@Override
	protected void mouseClicked(int x, int y, int eventButton)
	{
		if(CompatHandler.neiPresent && this.func_146978_c(79, 34, 24, 17, x, y) && eventButton == 0)
		{
			GuiCraftingRecipe.openRecipeGui("craftmistry.electrolyser");
			return;
		}
		super.mouseClicked(x, y, eventButton);
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        if(this.func_146978_c(157, 4, 15, 15, x, y))
        {
        	this.drawHoveringText(this.help.tooltip, x, y, fontRendererObj);
        }
        if(CompatHandler.neiPresent && this.func_146978_c(79, 34, 24, 17, x, y))
		{
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(StatCollector.translateToLocal("cm_neiplugin.general.recipes"));
        	this.drawHoveringText(list, x, y, fontRendererObj);
		}
        GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.electrolyser.getInventoryName()), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_ELECTROLYSER_RESOURCE);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(k, l + 6 * 18 + 17, 0, 126, this.xSize, 96);
		int progressScaled = this.electrolyser.getProgressScaled(24);
        this.drawTexturedModalRect(k + 79, l + 34, 176, 14, progressScaled + 1, 16);
        if(this.electrolyser.progress > 0 && !this.electrolyser.disabled)
        {
	        int progress = this.electrolyser.bubbleProgress;
	        if(progress > 0)
	        {
	            int height = this.scaleBubbleHeight(progress);
	            if(height > 0)
	            {
	                this.drawTexturedModalRect(k + 41, l + 56 - height, 176, 60 - height, 12, height);
	            }
	        }
	        if(this.electrolyser.bubbleCooldown < 10)
	        {
	        	++this.electrolyser.bubbleCooldown;
	        }
	        else
	        {
	        	this.electrolyser.bubbleCooldown = 0;
	        	if(this.electrolyser.bubbleProgress > 0)
	        	{
	        		--this.electrolyser.bubbleProgress;
	        	}
	        	else
	        	{
	        		this.electrolyser.bubbleProgress = 300;
	        	}
	        }
        }
        else
        {
        	this.electrolyser.bubbleProgress = 300;
        }
	}

	private int scaleBubbleHeight(int progress)
	{
		int ret = 0;
		int scale = progress / 2 % 7;
        switch(scale)
        {
            case 0:
                ret = 29;
                break;
            case 1:
                ret = 24;
                break;
            case 2:
                ret = 20;
                break;
            case 3:
                ret = 16;
                break;
            case 4:
                ret = 11;
                break;
            case 5:
                ret = 6;
                break;
            case 6:
                ret = 0;
                break;
        }
        return ret;
	}
}
