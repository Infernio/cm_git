package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import codechicken.nei.recipe.GuiCraftingRecipe;

import com.infernio.Craftmistry.common.container.ContainerCompressor;
import com.infernio.Craftmistry.common.tile.TileCompressor;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUICompressor extends GuiContainer {

	private IInventory lowerInv;
	private TileCompressor compressor;
	private Tooltip help;

	public GUICompressor(IInventory playerInv, IInventory compressorInv)
	{
		super(new ContainerCompressor(playerInv, compressorInv));
		this.lowerInv = playerInv;
		this.compressor = (TileCompressor)compressorInv;
		this.help = Tooltip.loadLocalizedTooltip("tile.blockCMCompressor.tooltip");
	}

	@Override
	protected void mouseClicked(int x, int y, int eventButton)
	{
		if(CompatHandler.neiPresent && this.func_146978_c(79, 34, 24, 17, x, y) && eventButton == 0)
		{
			GuiCraftingRecipe.openRecipeGui("craftmistry.compressing");
			return;
		}
		super.mouseClicked(x, y, eventButton);
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();;
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        if(this.func_146978_c(157, 4, 15, 15, x, y))
        {
        	this.drawHoveringText(this.help.tooltip, x, y, fontRendererObj);
        }
        if(CompatHandler.neiPresent && this.func_146978_c(79, 34, 24, 17, x, y))
		{
        	ArrayList<String> list = new ArrayList<String>();
        	list.add(StatCollector.translateToLocal("cm_neiplugin.general.recipes"));
        	this.drawHoveringText(list, x, y, fontRendererObj);
		}
        GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.compressor.getInventoryName()), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_COMPRESSOR_RESOURCE);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(k, l + 6 * 18 + 17, 0, 126, this.xSize, 96);
		int progress = this.compressor.getProgressScaled(24);
        this.drawTexturedModalRect(k + 79, l + 34, 176, 14, progress + 1, 16);
	}

}
