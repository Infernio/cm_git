package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.Craftmistry.common.container.ContainerCollector;
import com.infernio.Craftmistry.common.tile.TileCollector;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.InfernoCore.gui.Tooltip;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUICollector extends GuiContainer {

	private IInventory lowerInv;
	private TileCollector collector;
	private Tooltip help;
	private Tooltip upgradeInfo;

	public GUICollector(IInventory playerInv, IInventory collectorInv)
	{
		super(new ContainerCollector(playerInv, collectorInv));
		this.lowerInv = playerInv;
		this.collector = (TileCollector)collectorInv;
		this.ySize += 10;
		this.help = Tooltip.loadLocalizedTooltip("tile.blockCMCollector.tooltip");
		this.upgradeInfo = Tooltip.loadLocalizedTooltip("tile.upgradeSlot.tooltip");
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int i, int j)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.collector.getInventoryName()), 8, 6, 4210752);
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.lowerInv.getInventoryName()), 8, this.ySize - 93 + 2, 4210752);
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
        GL11.glPushMatrix();
        RenderHelper.enableGUIStandardItemLighting();
        GL11.glDisable(GL11.GL_LIGHTING);
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_LIGHTING);
        if(this.func_146978_c(152, 7, 15, 15, x, y))
        {
        	this.drawHoveringText(this.help.tooltip, x, y, fontRendererObj);
        }
        if(this.collector.getStackInSlot(27) == null && this.func_146978_c(134, 7, 16, 16, x, y))
        {
        	this.drawSlotInfo(x, y);
        }
        GL11.glPopMatrix();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glEnable(GL11.GL_LIGHTING);
        RenderHelper.enableStandardItemLighting();
	}

    private void drawSlotInfo(int x, int y)
    {
        ArrayList<String> list = new ArrayList<String>();
        list.add(Tooltip.resolveColors(StatCollector.translateToLocal("tile.upgradeSlot.name")));
        if(GuiScreen.isShiftKeyDown())
        {
        	list.addAll(this.upgradeInfo.tooltip);
        }
        else
        {
        	list.add(Tooltip.resolveColors(StatCollector.translateToLocal("tile.upgradeSlot.holdShift")));
        }
        list.add(Tooltip.resolveColors(StatCollector.translateToLocal("tile.upgradeSlot.noneInstalled")));
        this.drawHoveringText(list, x, y, this.fontRendererObj);
    }

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(ResourceLib.GUI_COLLECTOR_RESOURCE);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 6 * 18 + 17);
		this.drawTexturedModalRect(k, l + 6 * 18 + 17, 0, 126, this.xSize, 96);
	}

}
