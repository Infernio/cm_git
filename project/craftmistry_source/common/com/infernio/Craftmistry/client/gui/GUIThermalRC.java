package com.infernio.Craftmistry.client.gui;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.common.container.ContainerThermalRC;
import com.infernio.Craftmistry.common.tile.TileThermalRegulationChamber;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.network.message.MessageThermalRCSlider;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.math.MathUtils;
import com.infernio.InfernoCore.math.Measurements;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIThermalRC extends GuiContainer {

	private TileThermalRegulationChamber thermalRC;
	private IInventory player;
	private int sliderY;
	private GuiTextField temperatureInput;
	private GuiTextField currentTemperature;

	public GUIThermalRC(IInventory playerInv, IInventory tile)
	{
		super(new ContainerThermalRC(playerInv, tile));
		this.thermalRC = (TileThermalRegulationChamber)tile;
        this.player = playerInv;
		short short1 = 222;
        int i = short1 - 108;
        this.ySize = i + 6 * 18;
	}

	@Override
	public void initGui()
	{
		super.initGui();
        double temperature = this.thermalRC.targetTemperature;
        temperature -= 4500D;
        temperature /= 35D;
        this.sliderY = (int)-temperature;
        Keyboard.enableRepeatEvents(true);
        int x = (this.width - this.xSize) / 2;
        int y = (this.height - this.ySize) / 2;
        this.currentTemperature = new GuiTextField(this.fontRendererObj, x + 76, y + 89, 50, 12);
        this.currentTemperature.setTextColor(-1);
        this.currentTemperature.setFocused(false);
        this.currentTemperature.setDisabledTextColour(-1);
        this.currentTemperature.setEnableBackgroundDrawing(false);
        this.currentTemperature.setMaxStringLength(4);
        this.currentTemperature.setText("" + this.thermalRC.temperature);
        this.temperatureInput = new GuiTextField(this.fontRendererObj, x + 75, y + 28, 50, 12);
        this.temperatureInput.setTextColor(-1);
        this.currentTemperature.setFocused(false);
        this.temperatureInput.setDisabledTextColour(-1);
        this.temperatureInput.setEnableBackgroundDrawing(false);
        this.temperatureInput.setMaxStringLength(4);
        this.temperatureInput.setText("" + this.thermalRC.targetTemperature);
	}

    @Override
	public void onGuiClosed()
    {
        super.onGuiClosed();
        Keyboard.enableRepeatEvents(false);
    }

	@Override
	public void drawScreen(int x, int y, float f)
	{
		super.drawScreen(x, y, f);
		GL11.glDisable(GL11.GL_LIGHTING);
		this.temperatureInput.drawTextBox();
		this.currentTemperature.drawTextBox();
		if(this.func_146978_c(30, 9, 8, 124, x, y))
		{
			ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMMultiBlock.currentTemperature"));
        	list.add(EnumChatFormatting.GRAY + "" + this.thermalRC.temperature + "°K");
        	list.add(EnumChatFormatting.GRAY + "" + Measurements.kelvinToCelsius(this.thermalRC.temperature) + "°C");
        	list.add(EnumChatFormatting.GRAY + "" + Measurements.celsiusToFahrenheit(Measurements.kelvinToCelsius(this.thermalRC.temperature)) + "°F");
        	this.drawHoveringText(list, x, y, fontRendererObj);
		}
        if(!this.temperatureInput.isFocused())
        {
        	this.temperatureInput.setText("" + this.thermalRC.targetTemperature);
        }
        this.currentTemperature.setText("" + this.thermalRC.temperature);
		boolean temperatureChanged = false;
		if(this.func_146978_c(130, 9, 20, 150, x, y))
		{
			EntityPlayer player = ((InventoryPlayer)this.player).player;
			if(player.inventory.getItemStack() == null && Mouse.isButtonDown(0))
			{
				Slot slot = GUIUtils.getSlotAtPosition(this.inventorySlots, x, y, this.guiLeft, this.guiTop);
				if(slot == null || !slot.getHasStack())
				{
					double d = MathUtils.percentValue(124, y - this.guiTop);
					this.sliderY = (int)d;
					double temperature = -d;
					temperature *= 35D;
					temperature += 4500D;
					PacketHandler.sendToServer(new MessageThermalRCSlider(this.thermalRC.xCoord, this.thermalRC.yCoord, this.thermalRC.zCoord, MathHelper.clamp_int((int)temperature, 0, 5000)));
					temperatureChanged = true;
				}
			}
			GL11.glEnable(GL11.GL_LIGHTING);
		}
		if(this.func_146978_c(136, 9, 8, 124, x, y))
		{
			ArrayList<String> list = new ArrayList<String>();
        	list.add(EnumChatFormatting.WHITE + StatCollector.translateToLocal("tile.blockCMMultiBlock.targetTemperature"));
        	list.add(EnumChatFormatting.GRAY + "" + this.thermalRC.targetTemperature + "°K");
        	list.add(EnumChatFormatting.GRAY + "" + Measurements.kelvinToCelsius(this.thermalRC.targetTemperature) + "°C");
        	list.add(EnumChatFormatting.GRAY + "" + Measurements.celsiusToFahrenheit(Measurements.kelvinToCelsius(this.thermalRC.targetTemperature)) + "°F");
        	this.drawHoveringText(list, x, y, fontRendererObj);
		}
		if(!temperatureChanged)
		{
	        double temperature = this.thermalRC.targetTemperature;
	        temperature -= 4500D;
	        temperature /= 35D;
	        this.sliderY = (int)-temperature;
		}
	}

    @Override
	protected void keyTyped(char key, int i)
    {
    	if(this.temperatureInput.textboxKeyTyped(key, i))
    	{
    		try
    		{
    			PacketHandler.sendToServer(new MessageThermalRCSlider(this.thermalRC.xCoord, this.thermalRC.yCoord, this.thermalRC.zCoord, MathHelper.clamp_int(Integer.parseInt(this.temperatureInput.getText()), 0, 4300)));
    		}
    		catch(NumberFormatException e) {}
    	}
    	else
    	{
    		super.keyTyped(key, i);
    	}
    }

    @Override
	protected void mouseClicked(int x, int y, int button)
    {
        super.mouseClicked(x, y, button);
        this.temperatureInput.mouseClicked(x, y, button);
    }

	@Override
    protected void drawGuiContainerForegroundLayer(int x, int y)
	{
		this.fontRendererObj.drawString(StatCollector.translateToLocal(this.thermalRC.getInventoryName()), 20, 2, 4210752);
        this.fontRendererObj.drawString(this.player.hasCustomInventoryName() ? this.player.getInventoryName() : StatCollector.translateToLocal(this.player.getInventoryName()), 64, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(ResourceLib.GUI_THERMAL_REGULATION_CHAMBER_RESOURCE);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, 6 * 18 + 17);
        this.drawTexturedModalRect(k, l + 6 * 18 + 17, 0, 126, this.xSize, 96);
        this.drawTexturedModalRect(k + 135, l + this.sliderY, 176, 0, 14, 5);
        double temperature = this.thermalRC.temperature;
        temperature -= 4500D;
        temperature /= 35D;
        this.drawTexturedModalRect(k + 27, (int)(l + -temperature), 176, 0, 14, 5);
	}

}
