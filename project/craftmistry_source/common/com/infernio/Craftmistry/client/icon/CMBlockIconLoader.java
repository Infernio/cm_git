package com.infernio.Craftmistry.client.icon;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraftforge.fluids.Fluid;

import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.fluid.Fluids;
import com.infernio.InfernoCore.client.icon.IIconSubscriber;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class CMBlockIconLoader implements IIconSubscriber {

	@Override
	public void loadIcons(IIconRegister iconRegister)
	{
		Fluid fluid;
		for(int i = 0; i < 256; ++i)
		{
			String name = Elements.getElementName(i);
			String modid = Elements.getElementModId(i);
			if(Fluids.elementsLiquid[i] != null)
			{
				fluid = Fluids.elementsLiquid[i];
				fluid.setIcons(iconRegister.registerIcon(modid + ":fluids/liquid/" + name + "_still"), iconRegister.registerIcon(modid + ":fluids/" + "liquid_" + name + "_flow"));
			}
			if(Fluids.elementsGaseous[i] != null)
			{
				fluid = Fluids.elementsGaseous[i];
				fluid.setIcons(iconRegister.registerIcon(modid + ":fluids/gas/" + name + "_still"), iconRegister.registerIcon(modid + ":fluids/" + "gas_" + name + "_flow"));
			}
		}
	}

	@Override
	public void postLoadIcons(IIconRegister iconRegister)
	{
		Fluids.sulfuricAcid.setIcons(Fluids.sulfuricAcidBlock.getBlockTextureFromSide(1), Fluids.sulfuricAcidBlock.getBlockTextureFromSide(2));
		Fluids.mineralWater.setIcons(Fluids.mineralWaterBlock.getBlockTextureFromSide(1), Fluids.mineralWaterBlock.getBlockTextureFromSide(2));
	}

	@Override
	public boolean shouldPostLoad()
	{
		return true;
	}

	@Override
	public int getTextureType()
	{
		return 0;
	}

}
