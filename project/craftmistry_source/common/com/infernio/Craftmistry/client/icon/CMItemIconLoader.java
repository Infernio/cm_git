package com.infernio.Craftmistry.client.icon;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

import com.infernio.Craftmistry.api.chemistry.EnumThreat;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.icon.IIconSubscriber;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class CMItemIconLoader implements IIconSubscriber {

	@Override
	public void loadIcons(IIconRegister iconRegister)
	{
		EnumThreat[] threats = EnumThreat.values();
		EnumThreat.icons = new IIcon[threats.length];
		for(int i = 0; i < threats.length; ++i)
		{
			EnumThreat threat = threats[i];
			if(ConfigSettings.FANCY_GRAPHICS)
			{
				EnumThreat.icons[i] = iconRegister.registerIcon(StringLib.MODID + ":pictograms/" + threat.getTextureName() + "_high");
			}
			else
			{
				EnumThreat.icons[i] = iconRegister.registerIcon(StringLib.MODID + ":pictograms/" + threat.getTextureName());
			}
		}
	}

	@Override
	public void postLoadIcons(IIconRegister iconRegister) {}

	@Override
	public boolean shouldPostLoad()
	{
		return true;
	}

	@Override
	public int getTextureType()
	{
		return 1;
	}
}
