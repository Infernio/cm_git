// Date: 21.10.2013 14:50:48
// Template version 1.1
// Java generated by Techne
// Keep in mind that you still need to fill in some blanks
// - ZeuX

package com.infernio.Craftmistry.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelMachineFrame extends ModelBase
{
    private ModelRenderer BottomFrame1;
    private ModelRenderer BottomFrame2;
    private ModelRenderer BottomFrame3;
    private ModelRenderer BottomFrame4;
    private ModelRenderer SideFrame1;
    private ModelRenderer SideFrame2;
    private ModelRenderer SideFrame3;
    private ModelRenderer SideFrame4;
    private ModelRenderer TopFrame1;
    private ModelRenderer TopFrame2;
    private ModelRenderer TopFrame3;
    private ModelRenderer TopFrame4;
    private ModelRenderer BottomBeam1;
    private ModelRenderer BottomBeam2;
    private ModelRenderer BottomBeam3;
    private ModelRenderer TopBeam1;
    private ModelRenderer TopBeam2;
    private ModelRenderer TopBeam3;

    public ModelMachineFrame()
    {
    	textureWidth = 128;
    	textureHeight = 128;

    	BottomFrame1 = new ModelRenderer(this, 0, 0);
    	BottomFrame1.addBox(0F, 0F, 0F, 1, 1, 12);
    	BottomFrame1.setRotationPoint(-7F, 23F, -6F);
    	BottomFrame1.setTextureSize(128, 128);
    	BottomFrame1.mirror = true;
    	this.setRotation(BottomFrame1, 0F, 0F, 0F);
    	BottomFrame2 = new ModelRenderer(this, 0, 0);
    	BottomFrame2.addBox(0F, 0F, 0F, 1, 1, 12);
    	BottomFrame2.setRotationPoint(6F, 23F, -6F);
    	BottomFrame2.setTextureSize(128, 128);
    	BottomFrame2.mirror = true;
    	this.setRotation(BottomFrame2, 0F, 0F, 0F);
    	BottomFrame3 = new ModelRenderer(this, 0, 0);
    	BottomFrame3.addBox(0F, 0F, 0F, 12, 1, 1);
    	BottomFrame3.setRotationPoint(-6F, 23F, -7F);
    	BottomFrame3.setTextureSize(128, 128);
    	BottomFrame3.mirror = true;
    	this.setRotation(BottomFrame3, 0F, 0F, 0F);
    	BottomFrame4 = new ModelRenderer(this, 0, 0);
    	BottomFrame4.addBox(0F, 0F, 0F, 12, 1, 1);
    	BottomFrame4.setRotationPoint(-6F, 23F, 6F);
    	BottomFrame4.setTextureSize(128, 128);
    	BottomFrame4.mirror = true;
    	this.setRotation(BottomFrame4, 0F, 0F, 0F);
    	SideFrame1 = new ModelRenderer(this, 0, 14);
    	SideFrame1.addBox(0F, 0F, 0F, 1, 10, 1);
    	SideFrame1.setRotationPoint(-7F, 14F, -7F);
    	SideFrame1.setTextureSize(128, 128);
    	SideFrame1.mirror = true;
    	this.setRotation(SideFrame1, 0F, 0F, 0F);
    	SideFrame2 = new ModelRenderer(this, 0, 14);
    	SideFrame2.addBox(0F, 0F, 0F, 1, 10, 1);
    	SideFrame2.setRotationPoint(6F, 14F, -7F);
    	SideFrame2.setTextureSize(128, 128);
    	SideFrame2.mirror = true;
    	this.setRotation(SideFrame2, 0F, 0F, 0F);
    	SideFrame3 = new ModelRenderer(this, 0, 14);
    	SideFrame3.addBox(0F, 0F, 0F, 1, 10, 1);
    	SideFrame3.setRotationPoint(6F, 14F, 6F);
    	SideFrame3.setTextureSize(128, 128);
    	SideFrame3.mirror = true;
    	this.setRotation(SideFrame3, 0F, 0F, 0F);
    	SideFrame4 = new ModelRenderer(this, 0, 14);
    	SideFrame4.addBox(0F, 0F, 0F, 1, 10, 1);
    	SideFrame4.setRotationPoint(-7F, 14F, 6F);
    	SideFrame4.setTextureSize(128, 128);
    	SideFrame4.mirror = true;
    	this.setRotation(SideFrame4, 0F, 0F, 0F);
    	TopFrame1 = new ModelRenderer(this, 0, 0);
    	TopFrame1.addBox(0F, 0F, 0F, 1, 1, 12);
    	TopFrame1.setRotationPoint(-7F, 14F, -6F);
    	TopFrame1.setTextureSize(128, 128);
    	TopFrame1.mirror = true;
    	this.setRotation(TopFrame1, 0F, 0F, 0F);
    	TopFrame2 = new ModelRenderer(this, 0, 0);
    	TopFrame2.addBox(0F, 0F, 0F, 1, 1, 12);
     	TopFrame2.setRotationPoint(6F, 14F, -6F);
     	TopFrame2.setTextureSize(128, 128);
     	TopFrame2.mirror = true;
     	this.setRotation(TopFrame2, 0F, 0F, 0F);
     	TopFrame3 = new ModelRenderer(this, 0, 0);
     	TopFrame3.addBox(0F, 0F, 0F, 12, 1, 1);
     	TopFrame3.setRotationPoint(-6F, 14F, -7F);
     	TopFrame3.setTextureSize(128, 128);
     	TopFrame3.mirror = true;
     	this.setRotation(TopFrame3, 0F, 0F, 0F);
     	TopFrame4 = new ModelRenderer(this, 0, 0);
     	TopFrame4.addBox(0F, 0F, 0F, 12, 1, 1);
     	TopFrame4.setRotationPoint(-6F, 14F, 6F);
     	TopFrame4.setTextureSize(128, 128);
     	TopFrame4.mirror = true;
     	this.setRotation(TopFrame4, 0F, 0F, 0F);
     	BottomBeam1 = new ModelRenderer(this, 0, 0);
     	BottomBeam1.addBox(0F, 0F, 0F, 2, 1, 12);
     	BottomBeam1.setRotationPoint(-1F, 23F, -6F);
     	BottomBeam1.setTextureSize(128, 128);
     	BottomBeam1.mirror = true;
     	this.setRotation(BottomBeam1, 0F, 0F, 0F);
     	BottomBeam2 = new ModelRenderer(this, 0, 0);
     	BottomBeam2.addBox(0F, 0F, 0F, 5, 1, 2);
     	BottomBeam2.setRotationPoint(-6F, 23F, -1F);
     	BottomBeam2.setTextureSize(128, 128);
     	BottomBeam2.mirror = true;
     	this.setRotation(BottomBeam2, 0F, 0F, 0F);
     	BottomBeam3 = new ModelRenderer(this, 0, 0);
     	BottomBeam3.addBox(0F, 0F, 0F, 5, 1, 2);
     	BottomBeam3.setRotationPoint(1F, 23F, -1F);
     	BottomBeam3.setTextureSize(128, 128);
     	BottomBeam3.mirror = true;
     	this.setRotation(BottomBeam3, 0F, 0F, 0F);
     	TopBeam1 = new ModelRenderer(this, 0, 0);
      	TopBeam1.addBox(0F, 0F, 0F, 2, 1, 12);
      	TopBeam1.setRotationPoint(-1F, 14F, -6F);
      	TopBeam1.setTextureSize(128, 128);
      	TopBeam1.mirror = true;
      	this.setRotation(TopBeam1, 0F, 0F, 0F);
      	TopBeam2 = new ModelRenderer(this, 0, 0);
      	TopBeam2.addBox(0F, 0F, 0F, 5, 1, 2);
      	TopBeam2.setRotationPoint(-6F, 14F, -1F);
      	TopBeam2.setTextureSize(128, 128);
      	TopBeam2.mirror = true;
      	this.setRotation(TopBeam2, 0F, 0F, 0F);
      	TopBeam3 = new ModelRenderer(this, 0, 0);
      	TopBeam3.addBox(0F, 0F, 0F, 5, 1, 2);
     	TopBeam3.setRotationPoint(1F, 14F, -1F);
     	TopBeam3.setTextureSize(128, 128);
     	TopBeam3.mirror = true;
     	this.setRotation(TopBeam3, 0F, 0F, 0F);
    }
  
    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
    	super.render(entity, f, f1, f2, f3, f4, f5);
    	this.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    	BottomFrame1.render(f5);
    	BottomFrame2.render(f5);
    	BottomFrame3.render(f5);
    	BottomFrame4.render(f5);
    	SideFrame1.render(f5);
    	SideFrame2.render(f5);
    	SideFrame3.render(f5);
    	SideFrame4.render(f5);
    	TopFrame1.render(f5);
    	TopFrame2.render(f5);
    	TopFrame3.render(f5);
    	TopFrame4.render(f5);
    	BottomBeam1.render(f5);
    	BottomBeam2.render(f5);
    	BottomBeam3.render(f5);
    	TopBeam1.render(f5);
    	TopBeam2.render(f5);
    	TopBeam3.render(f5);
  	}

    private void setRotation(ModelRenderer model, float x, float y, float z)
  	{
    	model.rotateAngleX = x;
    	model.rotateAngleY = y;
    	model.rotateAngleZ = z;
  	}
  
    public void renderAll(float scale)
    {
    	BottomFrame1.render(scale);
    	BottomFrame2.render(scale);
    	BottomFrame3.render(scale);
    	BottomFrame4.render(scale);
    	SideFrame1.render(scale);
    	SideFrame2.render(scale);
    	SideFrame3.render(scale);
    	SideFrame4.render(scale);
    	TopFrame1.render(scale);
    	TopFrame2.render(scale);
    	TopFrame3.render(scale);
    	TopFrame4.render(scale);
    	BottomBeam1.render(scale);
    	BottomBeam2.render(scale);
    	BottomBeam3.render(scale);
    	TopBeam1.render(scale);
    	TopBeam2.render(scale);
    	TopBeam3.render(scale);
    }

    public void renderAll()
    {
    	this.renderAll(0.0625F);
    }

}
