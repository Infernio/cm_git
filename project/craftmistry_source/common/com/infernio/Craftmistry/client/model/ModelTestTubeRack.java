package com.infernio.Craftmistry.client.model;

//Date: 12.04.2013 21:54:04
//Template version 1.1
//Java generated by Techne
//Keep in mind that you still need to fill in some blanks
//- ZeuX

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import com.infernio.Craftmistry.common.tile.TileTestTubeRack;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelTestTubeRack extends ModelBase
{
    private ModelRenderer MainRack;
    private ModelRenderer SupportBeam;
    private ModelRenderer TestTube1;
    private ModelRenderer TestTube2;
    private ModelRenderer TestTube3;
    private ModelRenderer TestTube4;
    private ModelRenderer TestTube5;
    private ModelRenderer TestTube6;
    private ModelRenderer RackPart1;
    private ModelRenderer RackPart2;
    private ModelRenderer MainHolder;
    private ModelRenderer SecondaryHolder;
    private ModelRenderer HolderPart1;
    private ModelRenderer HolderPart2;
    private ModelRenderer HolderPart3;
    private ModelRenderer HolderPart4;
    private ModelRenderer HolderPart5;
    private ModelRenderer HolderPart6;
    private ModelRenderer HolderPart7;
  
    public ModelTestTubeRack()
    {
    	textureWidth = 64;
    	textureHeight = 64;
    
        MainRack = new ModelRenderer(this, 0, 0);
        MainRack.addBox(0F, 0F, 0F, 13, 8, 1);
        MainRack.setRotationPoint(6F, 0F, 0F);
        MainRack.setTextureSize(64, 64);
        MainRack.mirror = true;
        this.setRotation(MainRack, 0F, 0F, 0F);
        SupportBeam = new ModelRenderer(this, 0, 18);
        SupportBeam.addBox(0F, 0F, 0F, 13, 1, 7);
        SupportBeam.setRotationPoint(6F, 0F, 1F);
        SupportBeam.setTextureSize(64, 64);
        SupportBeam.mirror = true;
        this.setRotation(SupportBeam, 0F, 0F, 0F);
        TestTube1 = new ModelRenderer(this, 29, 0);
        TestTube1.addBox(0F, 0F, 0F, 1, 6, 1);
        TestTube1.setRotationPoint(7F, 2F, 3F);
        TestTube1.setTextureSize(64, 64);
        TestTube1.mirror = true;
        this.setRotation(TestTube1, 0F, 0F, 0F);
        TestTube2 = new ModelRenderer(this, 33, 0);
        TestTube2.addBox(0F, 0F, 0F, 1, 6, 1);
        TestTube2.setRotationPoint(9F, 2F, 3F);
        TestTube2.setTextureSize(64, 64);
        TestTube2.mirror = true;
        this.setRotation(TestTube2, 0F, 0F, 0F);
        TestTube3 = new ModelRenderer(this, 37, 0);
        TestTube3.addBox(0F, 0F, 0F, 1, 6, 1);
        TestTube3.setRotationPoint(11F, 2F, 3F);
        TestTube3.setTextureSize(64, 64);
        TestTube3.mirror = true;
        this.setRotation(TestTube3, 0F, 0F, 0F);
        TestTube4 = new ModelRenderer(this, 29, 7);
        TestTube4.addBox(0F, 0F, 0F, 1, 6, 1);
        TestTube4.setRotationPoint(13F, 2F, 3F);
        TestTube4.setTextureSize(64, 64);
        TestTube4.mirror = true;
        this.setRotation(TestTube4, 0F, 0F, 0F);
        TestTube5 = new ModelRenderer(this, 33, 7);
        TestTube5.addBox(0F, 0F, 0F, 1, 6, 1);
        TestTube5.setRotationPoint(15F, 2F, 3F);
        TestTube5.setTextureSize(64, 64);
        TestTube5.mirror = true;
        this.setRotation(TestTube5, 0F, 0F, 0F);
        TestTube6 = new ModelRenderer(this, 37, 7);
        TestTube6.addBox(0F, 0F, 0F, 1, 6, 1);
        TestTube6.setRotationPoint(17F, 2F, 3F);
        TestTube6.setTextureSize(64, 64);
        TestTube6.mirror = true;
        this.setRotation(TestTube6, 0F, 0F, 0F);
        RackPart1 = new ModelRenderer(this, 0, 11);
        RackPart1.addBox(0F, 0F, 0F, 13, 2, 1);
        RackPart1.setRotationPoint(6F, 4F, 1F);
        RackPart1.setTextureSize(64, 64);
        RackPart1.mirror = true;
        this.setRotation(RackPart1, 0F, 0F, 0F);
        RackPart2 = new ModelRenderer(this, 0, 15);
        RackPart2.addBox(0F, 0F, 0F, 13, 1, 1);
        RackPart2.setRotationPoint(6F, 4F, 2F);
        RackPart2.setTextureSize(64, 64);
        RackPart2.mirror = true;
        this.setRotation(RackPart2, 0F, 0F, 0F);
        MainHolder = new ModelRenderer(this, 0, 0);
        MainHolder.addBox(0F, 0F, 0F, 13, 1, 1);
        MainHolder.setRotationPoint(6F, 4F, 4F);
        MainHolder.setTextureSize(64, 64);
        MainHolder.mirror = true;
        this.setRotation(MainHolder, 0F, 0F, 0F);
        SecondaryHolder = new ModelRenderer(this, 0, 27);
        SecondaryHolder.addBox(0F, 0F, 0F, 11, 1, 1);
        SecondaryHolder.setRotationPoint(7F, 1F, 3F);
        SecondaryHolder.setTextureSize(64, 64);
        SecondaryHolder.mirror = true;
        this.setRotation(SecondaryHolder, 0F, 0F, 0F);
        HolderPart1 = new ModelRenderer(this, 0, 0);
        HolderPart1.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart1.setRotationPoint(18F, 4F, 3F);
        HolderPart1.setTextureSize(64, 64);
        HolderPart1.mirror = true;
        this.setRotation(HolderPart1, 0F, 0F, 0F);
        HolderPart2 = new ModelRenderer(this, 0, 0);
        HolderPart2.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart2.setRotationPoint(6F, 4F, 3F);
        HolderPart2.setTextureSize(64, 64);
        HolderPart2.mirror = true;
        this.setRotation(HolderPart2, 0F, 0F, 0F);
        HolderPart3 = new ModelRenderer(this, 0, 0);
        HolderPart3.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart3.setRotationPoint(16F, 4F, 3F);
        HolderPart3.setTextureSize(64, 64);
        HolderPart3.mirror = true;
        this.setRotation(HolderPart3, 0F, 0F, 0F);
        HolderPart4 = new ModelRenderer(this, 0, 0);
        HolderPart4.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart4.setRotationPoint(14F, 4F, 3F);
        HolderPart4.setTextureSize(64, 64);
        HolderPart4.mirror = true;
        this.setRotation(HolderPart4, 0F, 0F, 0F);
        HolderPart5 = new ModelRenderer(this, 0, 0);
        HolderPart5.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart5.setRotationPoint(12F, 4F, 3F);
        HolderPart5.setTextureSize(64, 64);
        HolderPart5.mirror = true;
        this.setRotation(HolderPart5, 0F, 0F, 0F);
        HolderPart6 = new ModelRenderer(this, 0, 0);
        HolderPart6.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart6.setRotationPoint(10F, 4F, 3F);
        HolderPart6.setTextureSize(64, 64);
        HolderPart6.mirror = true;
        this.setRotation(HolderPart6, 0F, 0F, 0F);
        HolderPart7 = new ModelRenderer(this, 0, 0);
        HolderPart7.addBox(0F, 0F, 0F, 1, 1, 1);
        HolderPart7.setRotationPoint(8F, 4F, 3F);
        HolderPart7.setTextureSize(64, 64);
        HolderPart7.mirror = true;
        this.setRotation(HolderPart7, 0F, 0F, 0F);
    }
  
    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
    {
    	super.render(entity, f, f1, f2, f3, f4, f5);
    	setRotationAngles(f5, f, f1, f2, f3, f4, entity);
    	MainRack.render(f5);
        SupportBeam.render(f5);
        TestTube1.render(f5);
        TestTube2.render(f5);
        TestTube3.render(f5);
        TestTube4.render(f5);
        TestTube5.render(f5);
        TestTube6.render(f5);
        RackPart1.render(f5);
        RackPart2.render(f5);
        MainHolder.render(f5);
        SecondaryHolder.render(f5);
        HolderPart1.render(f5);
        HolderPart2.render(f5);
        HolderPart3.render(f5);
        HolderPart4.render(f5);
        HolderPart5.render(f5);
        HolderPart6.render(f5);
        HolderPart7.render(f5);
    }

    public void render(ItemStack stack, float f)
    {
        MainRack.render(f);
        SupportBeam.render(f);
        NBTTagCompound tag = stack.getTagCompound();
        if(tag != null)
        {
	        if(tag.getInteger("testTube_0") > 0)
	        {
	        	TestTube1.render(f);
	        }
	        if(tag.getInteger("testTube_1") > 0)
	        {
	        	TestTube2.render(f);
	        }
	        if(tag.getInteger("testTube_2") > 0)
	        {
	        	TestTube3.render(f);
	        }
	        if(tag.getInteger("testTube_3") > 0)
	        {
	        	TestTube4.render(f);
	        }
	        if(tag.getInteger("testTube_4") > 0)
	        {
	        	TestTube5.render(f);
	        }
	        if(tag.getInteger("testTube_5") > 0)
	        {
	        	TestTube6.render(f);
	        }
        }
        RackPart1.render(f);
        RackPart2.render(f);
        MainHolder.render(f);
        SecondaryHolder.render(f);
        HolderPart1.render(f);
        HolderPart2.render(f);
        HolderPart3.render(f);
        HolderPart4.render(f);
        HolderPart5.render(f);
        HolderPart6.render(f);
        HolderPart7.render(f);;
    }
    
    public void renderAll(TileTestTubeRack tile)
    {
        MainRack.render(0.0625F);
        SupportBeam.render(0.0625F);
        if(tile.hasSlotTestTube(0))
        {
        	TestTube1.render(0.0625F);
        }
        if(tile.hasSlotTestTube(1))
        {
        	TestTube2.render(0.0625F);
        }
        if(tile.hasSlotTestTube(2))
        {
        	TestTube3.render(0.0625F);
        }
        if(tile.hasSlotTestTube(3))
        {
        	TestTube4.render(0.0625F);
        }
        if(tile.hasSlotTestTube(4))
        {
        	TestTube5.render(0.0625F);
        }
        if(tile.hasSlotTestTube(5))
        {
        	TestTube6.render(0.0625F);
        }
        RackPart1.render(0.0625F);
        RackPart2.render(0.0625F);
        MainHolder.render(0.0625F);
        SecondaryHolder.render(0.0625F);
        HolderPart1.render(0.0625F);
        HolderPart2.render(0.0625F);
        HolderPart3.render(0.0625F);
        HolderPart4.render(0.0625F);
        HolderPart5.render(0.0625F);
        HolderPart6.render(0.0625F);
        HolderPart7.render(0.0625F);
    }

    private void setRotation(ModelRenderer model, float x, float y, float z)
    {
    	model.rotateAngleX = x;
    	model.rotateAngleY = y;
    	model.rotateAngleZ = z;
    }

}
