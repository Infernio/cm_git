package com.infernio.Craftmistry.client.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.client.model.ModelMaterialCompartment;
import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.client.FMLClientHandler;

public class RendererMaterialCompartment extends TileEntitySpecialRenderer {

	private ModelMaterialCompartment model = new ModelMaterialCompartment();

	private void renderMaterialCompartment(TileMaterialCompartment tile, double x, double y, double z, float f)
	{
		GL11.glPushMatrix();

		FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_MATERIAL_COMPARTMENT_RESOURCE);

		GL11.glTranslated(x + 0.5D, y + 1.5D, z + 0.5D);
		GL11.glRotatef(-180.0F, 1.0F, 0.0F, 0.0F);

        model.renderAll();

        GL11.glPopMatrix();
	}

	@Override
	public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) 
	{
		this.renderMaterialCompartment((TileMaterialCompartment)var1, var2, var4, var6, var8);
	}

}
