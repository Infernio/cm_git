package com.infernio.Craftmistry.client.renderer;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.client.model.ModelTestTubeRack;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ItemTestTubeRackRenderer implements IItemRenderer {

	private ModelTestTubeRack model = new ModelTestTubeRack();
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) 
	{
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) 
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) 
	{
		switch(type)
		{
		case ENTITY: 
		{
			this.renderTestTubeRack(item, -0.5F, 0F, -0.5F);
			break;
		}
		case EQUIPPED:
		{
			this.renderTestTubeRack(item, 0F, 0.4F, 0F);
			break;
		}
		case INVENTORY:
		{
			this.renderTestTubeRack(item, 1F, 0.65F, 1F);
			break;
		}
		case EQUIPPED_FIRST_PERSON:
		{
			this.renderTestTubeRack(item, 0F, 0.4F, 0F);
			break;
		}
		default: break;
		}
	}
	
	private void renderTestTubeRack(ItemStack stack, float x, float y, float z)
	{
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_TEST_TUBE_RACK_RESOURCE);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)x - 0.28F, (float)y, (float)z);
        if(ConfigSettings.FANCY_GRAPHICS)
        {
	        GL11.glEnable(GL11.GL_BLEND);
	        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        }
	    model.render(stack, 0.0625F);
        if(ConfigSettings.FANCY_GRAPHICS)
        {
        	GL11.glDisable(GL11.GL_BLEND);
        }
        GL11.glPopMatrix();
	}

}
