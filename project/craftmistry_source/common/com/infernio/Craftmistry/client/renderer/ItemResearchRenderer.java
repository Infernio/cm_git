package com.infernio.Craftmistry.client.renderer;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.client.model.ModelMaterialCompartment;
import com.infernio.Craftmistry.client.model.ModelResearchPanel;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.client.FMLClientHandler;

public class ItemResearchRenderer implements IItemRenderer {

	private ModelResearchPanel researchPanel = new ModelResearchPanel();
	private ModelMaterialCompartment materialCompartment = new ModelMaterialCompartment();

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) 
	{
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) 
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) 
	{
		this.renderResearchTile(item, 1F, 0.65F, 1F);
	}

	private void renderResearchTile(ItemStack stack, float x, float y, float z)
	{
		switch(stack.getItemDamage())
		{
		case 0:
	        GL11.glPushMatrix();
			FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_RESEARCH_PANEL_RESOURCE);
	        GL11.glScalef(1.6F, 1.6F, 1.6F);
			GL11.glTranslated(x - 0.5D, y + 0.7D, z - 0.5D);
			GL11.glRotatef(-180.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
	        researchPanel.renderAll(0.0625F);
	        GL11.glPopMatrix();
	        break;
		case 1:
			break;
		case 2:
			GL11.glPushMatrix();
			FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_MATERIAL_COMPARTMENT_RESOURCE);
	        GL11.glScalef(1.23F, 1.23F, 1.23F);
			GL11.glTranslated(x - 0.5D, y + 0.8D, z - 0.5D);
			GL11.glRotatef(-180.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
	        materialCompartment.renderAll(0.0625F);
	        GL11.glPopMatrix();
			break;
		case 3:
			break;
		case 4:
			break;
		default: break;
		}
	}

}
