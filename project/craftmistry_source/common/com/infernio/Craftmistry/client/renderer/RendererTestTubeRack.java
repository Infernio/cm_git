package com.infernio.Craftmistry.client.renderer;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.client.model.ModelTestTubeRack;
import com.infernio.Craftmistry.common.tile.TileTestTubeRack;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RendererTestTubeRack extends TileEntitySpecialRenderer {

	private ModelTestTubeRack model = new ModelTestTubeRack();
	
	private void renderTestTubeRack(TileTestTubeRack tile, double x, double y, double z, float f)
	{
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_TEST_TUBE_RACK_RESOURCE);
		int facing = tile.getWorldObj().getBlockMetadata(tile.xCoord, tile.yCoord, tile.zCoord);
		GL11.glPushMatrix();
		switch(facing)
		{
		case 0:
			GL11.glTranslated(x - 0.27D, y, z);
			break;
		case 1:
			GL11.glTranslated(x, y, z + 1.27D);
			break;
		case 2:
			GL11.glTranslated(x + 1.27D, y, z + 1.0D);
			break;
		case 3:
			GL11.glTranslated(x + 1.0D, y, z - 0.27D);
			break;
		default:
			GL11.glTranslated(x, y, z);
			break;
		}
		GL11.glRotatef((facing * 90.0F), 0.0F, 1.0F, 0.0F);
	    GL11.glDisable(GL11.GL_LIGHTING);
        if(ConfigSettings.FANCY_GRAPHICS)
        {
        	GL11.glEnable(GL11.GL_BLEND);
        	GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        }
        model.renderAll(tile);
		GL11.glEnable(GL11.GL_LIGHTING);
		if(ConfigSettings.FANCY_GRAPHICS)
		{
			GL11.glDisable(GL11.GL_BLEND);
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) 
	{
		this.renderTestTubeRack((TileTestTubeRack)var1, var2, var4, var6, var8);
	}

}
