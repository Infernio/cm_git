package com.infernio.Craftmistry.client.renderer;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.client.model.ModelResearchPanel;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.client.FMLClientHandler;

public class RendererResearchPanel extends TileEntitySpecialRenderer {

	private ModelResearchPanel model = new ModelResearchPanel();

	private void renderResearchPanel(TileResearchPanel tile, double x, double y, double z, float f)
	{
        int facing = tile.getFacing();

        GL11.glPushMatrix();

        switch(facing)
        {
        case 0:
        case 2:
        	GL11.glTranslated(x + 0.5F, y + 1.53D, z + 0.5D);
        	break;
        default:
        	GL11.glTranslated(x + 0.6F, y + 1.53D, z + 0.5D);	
        	break;
        }

        GL11.glRotatef((facing * 90.0F), 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-180.0F, 1.0F, 0.0F, 0.0F);

        GL11.glDisable(GL11.GL_LIGHTING);

		FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_RESEARCH_PANEL_RESOURCE);
		model.renderAll();

        GL11.glEnable(GL11.GL_LIGHTING);

        GL11.glPopMatrix();
	}

	@Override
	public void renderTileEntityAt(TileEntity var1, double var2, double var4, double var6, float var8) 
	{
		this.renderResearchPanel((TileResearchPanel)var1, var2, var4, var6, var8);
	}

}
