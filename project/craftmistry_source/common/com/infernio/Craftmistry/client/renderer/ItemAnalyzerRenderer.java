package com.infernio.Craftmistry.client.renderer;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.client.model.ModelAnalyzer;
import com.infernio.Craftmistry.core.lib.ResourceLib;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ItemAnalyzerRenderer implements IItemRenderer {

	private ModelAnalyzer model = new ModelAnalyzer();
	
	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type) 
	{
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) 
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) 
	{
		switch(type)
		{
		case ENTITY: 
		{
			this.renderAnalyzer(item, -0.5F, 0F, -0.5F);
			break;
		}
		case EQUIPPED:
		{
			this.renderAnalyzer(item, 0F, 0.4F, 0F);
			break;
		}
		case INVENTORY:
		{
			this.renderAnalyzer(item, 1F, 0.65F, 1F);
			break;
		}
		case EQUIPPED_FIRST_PERSON:
		{
			this.renderAnalyzer(item, 0F, 0.4F, 0F);
			break;
		}
		default: break;
		}
	}
	
	private void renderAnalyzer(ItemStack stack, float x, float y, float z)
	{
		FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_ANALYZER_RESOURCE);
        GL11.glPushMatrix();
        GL11.glTranslatef((float)x - 0.28F, (float)y, (float)z);
        model.renderAll(0.0625F);
        GL11.glPopMatrix();
	}

}
