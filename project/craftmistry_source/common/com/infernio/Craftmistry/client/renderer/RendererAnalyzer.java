package com.infernio.Craftmistry.client.renderer;

import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;

import org.lwjgl.opengl.GL11;

import com.infernio.Craftmistry.client.model.ModelAnalyzer;
import com.infernio.Craftmistry.common.tile.TileAnalyzer;
import com.infernio.Craftmistry.core.lib.ResourceLib;
import com.infernio.InfernoCore.client.render.RenderUtils;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RendererAnalyzer extends TileEntitySpecialRenderer {
	
	private final ModelAnalyzer model = new ModelAnalyzer();
	private RenderItem itemRenderer;

	public RendererAnalyzer()
	{
		this.itemRenderer = new RenderItem()
		{
			@Override
			public boolean shouldBob()
			{
				return true;
			}
		};
		this.itemRenderer.setRenderManager(RenderManager.instance);
	}

	private void renderAnalyzer(TileAnalyzer tile, double x, double y, double z, float f)
	{
        GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_CULL_FACE);

        GL11.glPushMatrix();

        GL11.glTranslatef((float)x - 0.28F, (float)y, (float)z);

		FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLib.MODEL_ANALYZER_RESOURCE);
		model.renderAll();

		GL11.glPopMatrix();

		GL11.glPushMatrix();

		RenderUtils.renderItem(tile.getStackInSlot(0), tile.getWorldObj(), x, y, z, true, 2);

        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_CULL_FACE);

        GL11.glPopMatrix();
	}

	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float f)
	{
		this.renderAnalyzer((TileAnalyzer)tile, x, y, z, f);
	}

}
