package com.infernio.Craftmistry;

import net.minecraftforge.common.MinecraftForge;

import com.infernio.Craftmistry.common.achievement.AchievementHandler;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.command.CommandHandler;
import com.infernio.Craftmistry.common.creativetab.CMTab;
import com.infernio.Craftmistry.common.damage.CustomDS;
import com.infernio.Craftmistry.common.entity.EntityHandler;
import com.infernio.Craftmistry.common.fluid.Fluids;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.plasmatic.PlasmaticLoader;
import com.infernio.Craftmistry.common.recipe.RecipeCreator;
import com.infernio.Craftmistry.common.research.ResearchHandler;
import com.infernio.Craftmistry.common.worldgen.WorldGenerator;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.config.ConfigLoader;
import com.infernio.Craftmistry.core.handler.CMEventHandler;
import com.infernio.Craftmistry.core.handler.GUIHandler;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.Craftmistry.core.logging.CMLogger;
import com.infernio.Craftmistry.core.network.PacketHandler;
import com.infernio.Craftmistry.core.proxy.CommonProxy;
import com.infernio.InfernoCore.gui.GUIRegistry;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;

@Mod(modid = StringLib.MODID, name = StringLib.MODNAME, version = StringLib.VERSION, dependencies = StringLib.DEPENDENCIES, useMetadata = true)
public class Craftmistry {

	public static final CMTab cmTab = new CMTab();

	public static boolean energyModsDetected;

	public GUIHandler guiHandler = new GUIHandler();
	public short modGUIID;

	@SidedProxy(clientSide = "com.infernio.Craftmistry.core.proxy.ClientProxy", serverSide = "com.infernio.Craftmistry.core.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Instance(StringLib.MODID)
	public static Craftmistry instance;

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) 
    {
    	//Initialize the custom commands
    	CommandHandler.initCommands(event);
    }

	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
		if(!Loader.isModLoaded("InfernoCore"))
		{
			proxy.throwInfernoCoreException();
		}

		CMLogger.info("Beginning Craftmistry Pre-Initialization");

		//Load the config
		ConfigLoader.init(event);

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Config");
		}

		//Add custom entities
		EntityHandler.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded entities");
		}

		//Register tick handlers for client and server
		proxy.registerTickHandlers();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Tick Handlers");
		}

		//Add default elements
		Elements.initVanillaChemistry();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Chemistry");
		}

		//Register Icon subscribers
		proxy.registerIconHandlers();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Icon Subscribers");
		}

		//Add custom items
		ModItems.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Items");
		}

		//Add custom blocks
		ModBlocks.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Blocks");
		}

		//Add custom fluids
		Fluids.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Fluids");
		}

		//Add recipes for items
		ModItems.addRecipes();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Item Recipes");
		}

		//Load research
		ResearchHandler.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Research");
		}

		//Load plasmatic stuff
		PlasmaticLoader.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Plasmatic Loader");
		}

		//Initialize the Craftmistry achievements
		AchievementHandler.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Achievements");
		}

		//Load custom model IDs
		proxy.loadModels();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Models");
		}

		CMLogger.info("Finished Craftmistry Pre-Initialization");
	}

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		CMLogger.info("Beginning Craftmistry Initialization");

		//Register block / item renderers
		proxy.registerRenderers();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Renderers");
		}

		//Load default scoopula drops
		ItemHelper.initVanillaDrops();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Scoopula Drops");
		}

		//Register Craftmistry's event handler
		MinecraftForge.EVENT_BUS.register(new CMEventHandler());

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Event Handler");
		}

		//Load custom Damage Sources
		CustomDS.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Damage Sources");
		}

		//Generate the ores
		WorldGenerator.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded World Generation");
		}

		//Add infuser recipes etc.
		RecipeCreator.createRecipes();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Recipes");
		}

		//Add recipes for increasing compatibility
		CompatHandler.addRecipes();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded compatibility recipes");
		}

		//Register the GUI Handler
		NetworkRegistry.INSTANCE.registerGuiHandler(this, this.guiHandler);

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded GUI Handler");
		}

		//Register out packet handler
		PacketHandler.init();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Packet Handler");
		}

		//Register this mod in the GUI Registry
		this.modGUIID = GUIRegistry.getFreeID();
		GUIRegistry.registerMod(this.modGUIID, this);

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded GUI ID");
		}

		//Send recipe information to EE3
		if(CompatHandler.ee3Present)
		{
			CompatHandler.sendRecipeInformation();
	
			if(CompatHandler.debug)
			{
				CMLogger.debug("Successfully loaded recipe information.");
			}
		}

		if(CompatHandler.neiPresent)
		{
			//Load NEI plugin
			proxy.initNEIPlugin();

			if(CompatHandler.debug)
			{
				CMLogger.debug("Successfully loaded NEI Plugin");
			}
		}
		else
		{
			CMLogger.debug("NEI is not loaded, skipping Plugin loading.");
		}

		//Load tile entity textures
		proxy.initTiles();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Tile Entity Renderers");
		}

		//Load key bindings
		proxy.loadKeyBindings();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Key Bindings");
		}

		CMLogger.info("Finished Craftmistry Initialization");
	}

	@EventHandler
	public void postLoad(FMLPostInitializationEvent event)
	{
		CMLogger.info("Beginning Craftmistry Post-Initialization");

		//Register black-listed blocks for the breaker
		CompatHandler.initBreakerBlacklist();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Breaker Blacklist");
		}

		//Because GregTech (for some reason) adds all dusts to a dust -> ingot smelting - making the compressor useless.
		CompatHandler.removeDustSmelting();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Dust Smelting Recipe Removal");
		}

		//Initialze scoopula ore drop compatibility
		CompatHandler.initOreDrops();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Scoopula Ore Drops");
		}

		//Load default information
		proxy.initVanillaInfo();

		if(CompatHandler.debug)
		{
			CMLogger.debug("Successfully loaded Item Information");
		}

		CMLogger.info("Finished Craftmistry Post-Initialization");
	}

}
