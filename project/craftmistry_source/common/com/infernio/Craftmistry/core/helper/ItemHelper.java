package com.infernio.Craftmistry.core.helper;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import com.infernio.Craftmistry.api.CraftmistryAPI;
import com.infernio.Craftmistry.api.item.ISpecialInfo;
import com.infernio.Craftmistry.api.item.NormalInformation;
import com.infernio.Craftmistry.common.block.BlockDrop;
import com.infernio.Craftmistry.common.block.BlockTrigger;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.item.CompoundInformation;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.item.TestTubeInformation;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;

public class ItemHelper {

	private static HashMap<BlockTrigger, BlockDrop> entries = new HashMap<BlockTrigger, BlockDrop>();
	private static HashMap<Item, HashMap<Integer, ISpecialInfo>> information = new HashMap<Item, HashMap<Integer, ISpecialInfo>>();

	public static void addBlockBreak(Block block, int blockMeta, Block replacement, int replacementMeta, ItemStack[] stacks, int[] chances)
	{
		if(!entries.containsKey(new BlockTrigger(block, blockMeta)))
		{
			entries.put(new BlockTrigger(block, blockMeta), new BlockDrop(replacement, replacementMeta, stacks, chances));
		}
	}

	public static void addItemInfo(Item item, int meta, ISpecialInfo info)
	{
		if(!information.containsKey(item))
		{
			HashMap<Integer, ISpecialInfo> map = new HashMap<Integer, ISpecialInfo>();
			map.put(meta, info);
			information.put(item, map);
		}
		else
		{
			if(!information.get(item).containsKey(meta))
			{
				HashMap<Integer, ISpecialInfo> map = information.get(item);
				map.put(meta, info);
				information.put(item, map);
			}
		}
	}
	
	public static boolean specialDropOn(Block block, int blockMeta)
	{
		return entries.containsKey(new BlockTrigger(block, blockMeta));
	}
	
	public static boolean hasItemSpecialInfo(Item item, int meta)
	{
		return information.containsKey(item) ? information.get(item).containsKey(meta) : false;
	}
	
	public static ISpecialInfo getInformation(Item item, int meta)
	{
		return information.containsKey(item) ? information.get(item).get(meta) : null;
	}
	
	public static void doSpecialDrop(World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
		int meta = world.getBlockMetadata(x, y, z);
		BlockTrigger trigger = new BlockTrigger(block, meta);
		BlockDrop drop = entries.get(trigger);
		if(drop != null)
		{
			drop.drop(world, x, y, z);
		}
	}

	public static void initVanillaInfo()
	{
		CMLogger.info("Testing Craftmistry Installation... Step 4 / 4 (Item Information)");
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			CraftmistryAPI.Items.addItemInformation(Item.getItemFromBlock(ModBlocks.analyzer), 0, new NormalInformation(StatCollector.translateToLocal("analyzer.blockCMAnalyzer.info")));
			ArrayList<Integer> elements = Elements.getValidElements();
			TestTubeInformation tinfo = new TestTubeInformation();
			for(int i = 0; i < elements.size(); ++i)
			{
				if(i > 0)
				{
					int meta = elements.get(i);
					addItemInfo(ModItems.testTubeSolid, meta, tinfo);
					addItemInfo(ModItems.testTubeLiquid, meta, tinfo);
					addItemInfo(ModItems.testTubeGas, meta, tinfo);
					addItemInfo(ModItems.testTubePlasma, meta, tinfo);
				}
			}
			ArrayList<Integer> compounds = Elements.getValidCompounds();
			CompoundInformation cinfo = new CompoundInformation();
			for(int i = 0; i < compounds.size(); ++i)
			{
				addItemInfo(ModItems.testTubeCompound, compounds.get(i), cinfo);
			}
			NormalInformation scoopula = new NormalInformation(StatCollector.translateToLocal("analyzer.itemCMScoopula.info"), true, 1);
			for(int meta = 0; meta <= 9; ++meta)
			{
				addItemInfo(ModItems.scoopula, meta, scoopula);
			}
			addItemInfo(ModItems.testTubeSolid, 0, new NormalInformation(StatCollector.translateToLocal("analyzer.testTubeEmpty.info")));
			addItemInfo(Item.getItemFromBlock(ModBlocks.airFilter), 0, new NormalInformation(StatCollector.translateToLocal("analyzer.blockCMAirFilter.info")));
			addItemInfo(Item.getItemFromBlock(ModBlocks.cleaner), 0, new NormalInformation(StatCollector.translateToLocal("analyzer.blockCMCleaner.info")));
			addItemInfo(Item.getItemFromBlock(ModBlocks.compressor), 0, new NormalInformation(StatCollector.translateToLocal("analyzer.blockCMCompressor.info")));
			addItemInfo(Item.getItemFromBlock(ModBlocks.destabilizer), 0, new NormalInformation(StatCollector.translateToLocal("analyzer.blockCMDestabilizer.info")));
			addItemInfo(Item.getItemFromBlock(ModBlocks.testTubeRack), 0, new NormalInformation(StatCollector.translateToLocal("analyzer.blockCMTestTubeRack.info")));
		}
		CMLogger.info("The Craftmistry Installation seems to be intact");
	}

	public static void initVanillaDrops()
	{
		if(!ConfigSettings.AUTO_ASSIGN_SCOOPULA_DROPS)
		{
			CMLogger.info("Testing Craftmistry Installation... Step 3 / 4 (Scoopula drops)");
			CraftmistryAPI.Items.addScoopulaDrop(ModBlocks.craftmistryOre, 0, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 0), new ItemStack(ModItems.rawMineral, 1, 0), new ItemStack(ModItems.rawMineral, 1, 0)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(ModBlocks.craftmistryOre, 1, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 1), new ItemStack(ModItems.rawMineral, 1, 1), new ItemStack(ModItems.rawMineral, 1, 1)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(ModBlocks.craftmistryOre, 2, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 2), new ItemStack(ModItems.rawMineral, 1, 2), new ItemStack(ModItems.rawMineral, 1, 2)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(ModBlocks.craftmistryOre, 3, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 3), new ItemStack(ModItems.rawMineral, 1, 3), new ItemStack(ModItems.rawMineral, 1, 3)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(ModBlocks.craftmistryOre, 4, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 4), new ItemStack(ModItems.rawMineral, 1, 4), new ItemStack(ModItems.rawMineral, 1, 4)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(ModBlocks.craftmistryOre, 5, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 5), new ItemStack(ModItems.rawMineral, 1, 5), new ItemStack(ModItems.rawMineral, 1, 5)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(ModBlocks.craftmistryOre, 6, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.multiItem, 1, 5)
			}, new int[]{
					100, 80, 20, 80
			});
			ItemHelper.addBlockBreak(ModBlocks.craftmistryOre, 7, Blocks.stone, 0, new ItemStack[]{
				new ItemStack(ModItems.rawMineral, 1, 11), new ItemStack(ModItems.rawMineral, 1, 11), new ItemStack(ModItems.rawMineral, 1, 11)
			}, new int[]{
				100, 80, 20
			});
			ItemHelper.addBlockBreak(ModBlocks.craftmistryOre, 8, Blocks.stone, 0, new ItemStack[]{
				new ItemStack(ModItems.rawMineral, 1, 13), new ItemStack(ModItems.rawMineral, 1, 13), new ItemStack(ModItems.rawMineral, 1, 13)
			}, new int[]{
				100, 80, 20
			});
			addBlockBreak(Blocks.gold_ore, 0, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 8), new ItemStack(ModItems.rawMineral, 1, 8), new ItemStack(ModItems.rawMineral, 1, 8)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(Blocks.coal_ore, 0, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 7), new ItemStack(ModItems.rawMineral, 1, 7), new ItemStack(ModItems.rawMineral, 1, 7)
			}, new int[]{
					100, 80, 20
			});
			addBlockBreak(Blocks.iron_ore, 0, Blocks.stone, 0, new ItemStack[]{
					new ItemStack(ModItems.rawMineral, 1, 6), new ItemStack(ModItems.rawMineral, 1, 6), new ItemStack(ModItems.rawMineral, 1, 6)
			}, new int[]{
					100, 80, 20
			});
		}
	}

	public static boolean isTestTube(ItemStack stack)
	{
		return stack != null ? stack.getItem() == ModItems.testTubeSolid || stack.getItem() == ModItems.testTubeLiquid || stack.getItem() == ModItems.testTubeGas || stack.getItem() == ModItems.testTubePlasma || stack.getItem() == ModItems.testTubeCompound : false;
	}
	
	public static boolean isTestTubeAndFilled(ItemStack stack)
	{
		return isTestTube(stack) && stack.getItemDamage() > 0;
	}
}
