package com.infernio.Craftmistry.core.config;

public class ConfigSettings {

	public static boolean DEBUG_MODE;
	public static boolean FANCY_GRAPHICS;
	public static boolean AUTO_ASSIGN_SCOOPULA_DROPS;
	public static boolean DISABLE_DOUBLE_WORLD_GEN;
	public static boolean ADJUST_CREEPER_DROPS;
	public static String[] BREAKER_BLACKLIST;
	public static boolean GENERATE_MANA_SPRINGS;
	public static boolean GENERATE_OBSIDIAN_CIRCLES;
	public static boolean GENERATE_ROOTS;
	public static boolean GENERATE_MINERAL_WATER_LAKES;
	public static boolean COMPAT_RECIPES;
	public static boolean RESPECT_NO_TILE_DROPS;
	public static int KEY_POCKET_DIMENSION_GIVE_STACK;
	public static int KEY_POCKET_DIMENSION_CLEAR_STACK;
}
