package com.infernio.Craftmistry.core.config;

import java.io.File;
import java.util.Locale;

import net.minecraftforge.common.config.Configuration;

import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;

public class ConfigLoader {

	public static final String id = StringLib.MODID.toLowerCase(Locale.ENGLISH);

	private static String idForKeyBinding(String key)
	{
		return id + ".key." + key + ".id";
	}

	public static void init(FMLPreInitializationEvent event)
	{
		File dir = event.getModConfigurationDirectory();
		Configuration config = new Configuration(new File(dir.getName() + "/Craftmistry.cfg"));

		//Load the config
		config.load();

		//General
		config.addCustomCategoryComment(Configuration.CATEGORY_GENERAL, "General Mod Settings");
		ConfigSettings.DEBUG_MODE = config.get(Configuration.CATEGORY_GENERAL, id + ".general.debug", false, "Set to true to enable detailed logs with stack traces.").getBoolean(false);
		ConfigSettings.COMPAT_RECIPES = config.get(Configuration.CATEGORY_GENERAL, id + ".general.compatRecipes", true, "Set to true to enable compatibility recipes e.g. sulfur dust from IC2 can be used in the infuser.").getBoolean(true);
		ConfigSettings.RESPECT_NO_TILE_DROPS = config.get(Configuration.CATEGORY_GENERAL, id + ".general.noTileDrops", false, "Set to true to make the Breaker respect the noTileDrops game rule. If that one is true, the Breaker won't drop items or add them to adjacent inventories.").getBoolean(false);

		//Items
		config.addCustomCategoryComment(StringLib.CATEGORY_ITEM, "Adjust item settings here");
		ConfigSettings.AUTO_ASSIGN_SCOOPULA_DROPS = config.get(StringLib.CATEGORY_ITEM, id + ".item.scoopula.autoAssign", true, "Set this to true to make Craftmistry automatically assign scoopula drops to compatible ores from other mods for better compatibility.").getBoolean(false);

		//Blocks
		config.addCustomCategoryComment(StringLib.CATEGORY_BLOCK, "Adjust block settings here");
		ConfigSettings.BREAKER_BLACKLIST = config.get(StringLib.CATEGORY_BLOCK, id + ".block.breaker.blacklist", new String[]{}, "Put the string ids of blocks (not the numbers) you don't want to have broken by the Breaker here.").getStringList();

		//World
		config.addCustomCategoryComment(StringLib.CATEGORY_WORLD, "Adjust world settings here");
		ConfigSettings.DISABLE_DOUBLE_WORLD_GEN = config.get(StringLib.CATEGORY_WORLD, id + ".world.disableDoubleGen", true, "Set to true to make Craftmistry only generate ores when no other mod adds comparable ones. If you set this to true, you should also set 'craftmistry.item.scoopula.autoAssignDrops' to true, as you will otherwise not be able to use any of Craftmistry's features.").getBoolean(true);
		ConfigSettings.GENERATE_MANA_SPRINGS = config.get(StringLib.CATEGORY_WORLD, id + ".world.enableManaSprings", true, "Set to true to enable generation of rare obsidian pillars containing a mana spring and add a chance for obsidian circles to contain a mana spring. Mana Springs will slowly spawn small amounts of xp.").getBoolean(true);
		ConfigSettings.GENERATE_OBSIDIAN_CIRCLES = config.get(StringLib.CATEGORY_WORLD, id + ".world.enableObsidianCircles", true, "Set to true to enable generation of rare obsidian circles containing a piece of glowstone. If 'enableManaSprings' is set to true as well, there is a 1% that an obsidian circle will contain a mana spring.").getBoolean(true);
		ConfigSettings.GENERATE_ROOTS = config.get(StringLib.CATEGORY_WORLD, id + ".world.enableRoots", true, "Set to true to enable generation of big wooden roots that stick out of the ground.").getBoolean(true);
		ConfigSettings.GENERATE_MINERAL_WATER_LAKES = config.get(StringLib.CATEGORY_WORLD, id + ".world.enableMineralWaterLakes", true, "Set to true to enable generation of mineral water lakes. Disabling this option will prevent certain Craftmistry elements from being obtainable.").getBoolean(true);

		//Entities
		config.addCustomCategoryComment(StringLib.CATEGORY_ENTITY, "Adjust entity settings here");
		ConfigSettings.ADJUST_CREEPER_DROPS = config.get(StringLib.CATEGORY_ENTITY, id + ".entites.creeper.dropSulfur", true, "Set to true to make creepers drop 0-2 raw sulfur.").getBoolean(true);

		//Key Bindings
		config.addCustomCategoryComment(StringLib.CATEGORY_KEY_BINDINGS, "Adjust key binding settings here. See http://www.minecraftwiki.net/wiki/Key_codes for key codes.");
		ConfigSettings.KEY_POCKET_DIMENSION_GIVE_STACK = config.get(StringLib.CATEGORY_KEY_BINDINGS, idForKeyBinding("pdGiveStack"), 34).getInt(34); //34 == Keyboard.KEY_G
		ConfigSettings.KEY_POCKET_DIMENSION_CLEAR_STACK = config.get(StringLib.CATEGORY_KEY_BINDINGS, idForKeyBinding("pdClearStack"), 33).getInt(33); //33 == Keyboard.KEY_F

		//Graphics
		if(FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT)
		{
			config.addCustomCategoryComment(StringLib.CATEGORY_GRAPHICS, "Adjust graphic settings here");
			boolean mcFancy = FMLClientHandler.instance().getClient().gameSettings.fancyGraphics;
			ConfigSettings.FANCY_GRAPHICS = config.get(StringLib.CATEGORY_GRAPHICS, id + ".graphics.high", mcFancy, "Use fancy graphics?").getBoolean(mcFancy);
		}
		else
		{
			//Server doesn't have rendering capabilities
			ConfigSettings.FANCY_GRAPHICS = false;
		}

		//Save the changes made
		if(config.hasChanged())
		{
			config.save();
		}
	}

}
