package com.infernio.Craftmistry.core.proxy;

import net.minecraft.world.World;

import com.infernio.Craftmistry.core.lib.StringLib;

public class CommonProxy {
	
	public void initTiles() {}

	public void loadModels() {}

	public void registerTickHandlers() {}
	
	public void spawnSuperPhosphateParticles(World world, int x, int y, int z, int i) {}
	
	public void registerRenderers() {}

	public void initVanillaInfo() {}

	public void registerIconHandlers() {}

	public void throwInfernoCoreException()
	{
		throw new RuntimeException(StringLib.INFERNO_CORE_NOT_LOADED_MESSAGE_1 + " " + StringLib.INFERNO_CORE_NOT_LOADED_MESSAGE_2);
	}

	public void initNEIPlugin() {}

	public void loadKeyBindings() {}

}
