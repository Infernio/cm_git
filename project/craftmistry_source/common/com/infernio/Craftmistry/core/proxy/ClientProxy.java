package com.infernio.Craftmistry.core.proxy;

import net.minecraft.client.renderer.entity.RenderSnowball;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;

import com.infernio.Craftmistry.InfernoCoreNotPresentException;
import com.infernio.Craftmistry.client.icon.CMBlockIconLoader;
import com.infernio.Craftmistry.client.icon.CMItemIconLoader;
import com.infernio.Craftmistry.client.renderer.ItemAnalyzerRenderer;
import com.infernio.Craftmistry.client.renderer.ItemResearchRenderer;
import com.infernio.Craftmistry.client.renderer.ItemTestTubeRackRenderer;
import com.infernio.Craftmistry.client.renderer.RendererAnalyzer;
import com.infernio.Craftmistry.client.renderer.RendererMaterialCompartment;
import com.infernio.Craftmistry.client.renderer.RendererResearchPanel;
import com.infernio.Craftmistry.client.renderer.RendererTestTubeRack;
import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.entity.EntityGrenade;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.tick.ClientTickHandler;
import com.infernio.Craftmistry.common.tile.TileAnalyzer;
import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;
import com.infernio.Craftmistry.common.tile.TileTestTubeRack;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.KeyLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.Craftmistry.core.nei.NEIHandler;
import com.infernio.InfernoCore.client.icon.IconRegistry;
import com.infernio.InfernoCore.key.KeyRegistry;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;

public class ClientProxy extends CommonProxy {

	@Override
	public void loadModels()
	{
		IntLib.TEST_TUBE_RACK_MODEL_ID = RenderingRegistry.getNextAvailableRenderId();
		IntLib.ANALYZER_MODEL_ID = RenderingRegistry.getNextAvailableRenderId();
		IntLib.RESEARCH_MODEL_ID = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void initTiles()
	{
		ClientRegistry.bindTileEntitySpecialRenderer(TileTestTubeRack.class, new RendererTestTubeRack());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.testTubeRack), new ItemTestTubeRackRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TileAnalyzer.class, new RendererAnalyzer());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.analyzer), new ItemAnalyzerRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(TileResearchPanel.class, new RendererResearchPanel());
		ClientRegistry.bindTileEntitySpecialRenderer(TileMaterialCompartment.class, new RendererMaterialCompartment());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModBlocks.research), new ItemResearchRenderer());
	}

	@Override
	public void registerTickHandlers()
	{
		FMLCommonHandler.instance().bus().register(new ClientTickHandler());
	}

	@Override
	public void spawnSuperPhosphateParticles(World world, int x, int y, int z, int i)
	{
		ItemDye.func_150918_a(world, x, y, z, i);
	}

	@Override
	public void registerRenderers()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityGrenade.class, new RenderSnowball(ModItems.grenade, 0));
	}

	@Override
	public void initVanillaInfo()
	{
		ItemHelper.initVanillaInfo();
	}

	@Override
	public void registerIconHandlers()
	{
		IconRegistry.getInstance().registerSubscriber(new CMItemIconLoader());
		IconRegistry.getInstance().registerSubscriber(new CMBlockIconLoader());
	}

	@Override
	public void throwInfernoCoreException()
	{
		throw new InfernoCoreNotPresentException();
	}

	@Override
	public void initNEIPlugin()
	{
		NEIHandler.init();
	}

	@Override
	public void loadKeyBindings()
	{
		KeyLib.PD_GIVE_STACK = new KeyBinding(StringLib.KEY_POCKET_DIMENSION_GIVE_STACK_NAME, ConfigSettings.KEY_POCKET_DIMENSION_GIVE_STACK, StringLib.KEY_CATEGORY_CRAFTMISTRY);
		ClientRegistry.registerKeyBinding(KeyLib.PD_GIVE_STACK);
		KeyRegistry.registerKeyBinding(KeyLib.PD_GIVE_STACK);
		KeyLib.PD_CLEAR_STACK = new KeyBinding(StringLib.KEY_POCKET_DIMENSION_CLEAR_STACK_NAME, ConfigSettings.KEY_POCKET_DIMENSION_CLEAR_STACK, StringLib.KEY_CATEGORY_CRAFTMISTRY);
		KeyRegistry.registerKeyBinding(KeyLib.PD_CLEAR_STACK);
		ClientRegistry.registerKeyBinding(KeyLib.PD_CLEAR_STACK);
	}

}
