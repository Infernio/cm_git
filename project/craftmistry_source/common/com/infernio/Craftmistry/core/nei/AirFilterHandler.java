package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.common.recipe.AirFilterRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.util.ItemUtils;

public class AirFilterHandler extends TemplateRecipeHandler {

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.airFilterRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "craftmistry:" + StringLib.AIR_FILTER_GUI_TEXTURE.replaceFirst("\\.png", "_noqm.png");
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.airfilter";
	}

	@Override
	public void drawExtras(int recipe)
	{
	    CachedAirFilterRecipe crecipe = (CachedAirFilterRecipe)this.arecipes.get(recipe);
	    GUIUtils.fontRenderer.drawString(crecipe.energy + " EU", 71, 9, -8355712, false);
	    GUIUtils.fontRenderer.drawString(crecipe.element, 15, 40, 4210752, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(84, 23, 24, 18), "craftmistry.airfilter", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.airfilter")) && (this.getClass() == AirFilterHandler.class))
		{
			Map<String, ItemStack> recipes = AirFilterRecipes.recipes().getRecipeList();
			for(Entry<String, ItemStack> entry : recipes.entrySet())
			{
				ItemStack stack = entry.getValue();
				String element = entry.getKey();
				this.arecipes.add(new CachedAirFilterRecipe(stack, element, AirFilterRecipes.recipes().getEnergy(element)));
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		Map<String, ItemStack> recipes = AirFilterRecipes.recipes().getRecipeList();
		for(Entry<String, ItemStack> entry : recipes.entrySet())
		{
			ItemStack stack = entry.getValue();
			if(NEIClientUtils.areStacksSameTypeCrafting(result, stack))
			{
				String element = entry.getKey();
				this.arecipes.add(new CachedAirFilterRecipe(stack, element, AirFilterRecipes.recipes().getEnergy(element)));
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		if(ItemUtils.areItemStacksEqual(ingredient, NEIHandler.EMPTY_TEST_TUBE, false))
		{
			Map<String, ItemStack> recipes = AirFilterRecipes.recipes().getRecipeList();
			for(Entry<String, ItemStack> entry : recipes.entrySet())
			{
				String element = entry.getKey();
				ItemStack output = entry.getValue();
				CachedAirFilterRecipe recipe = new CachedAirFilterRecipe(output, element, AirFilterRecipes.recipes().getEnergy(element));
				recipe.setIngredientPermutation(recipe.ingredients, ingredient);
				this.arecipes.add(recipe);
		    }
		}
	}

	public class CachedAirFilterRecipe extends CachedRecipe
	{
		public List<PositionedStack> ingredients;
		public PositionedStack result;
		public String element;
		public int energy;

		public CachedAirFilterRecipe(ItemStack output, String element, int energy)
		{
			this.result = new PositionedStack(output, 111, 24);
			this.element = element;
			this.ingredients = Arrays.asList(new PositionedStack(NEIHandler.EMPTY_TEST_TUBE, 51, 6));
			this.energy = energy;
		}

		@Override
		public List<PositionedStack> getIngredients()
		{
            return getCycledIngredients(AirFilterHandler.this.cycleticks / 48, this.ingredients);
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}

}
