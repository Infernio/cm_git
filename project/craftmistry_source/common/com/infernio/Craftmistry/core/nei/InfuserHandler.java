package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.common.recipe.InfuserRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.util.ItemUtils;

public class InfuserHandler extends TemplateRecipeHandler {

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.infuserRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "craftmistry:" + StringLib.INFUSER_GUI_TEXTURE.replaceFirst(File.separator + ".png", "_noqm.png");
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.infusing";
	}

	@Override
	public void drawExtras(int recipe)
	{
	    int energy = ((CachedInfusionRecipe)this.arecipes.get(recipe)).energy;
	    GUIUtils.fontRenderer.drawString(energy + " EU", 71, 9, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(84, 23, 24, 18), "craftmistry.infusing", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.infusing")) && (this.getClass() == InfuserHandler.class))
		{
			Map<Item, HashMap<Integer, ItemStack>> recipes = InfuserRecipes.infusing().getRecipeList();
			for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
			{
				for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
				{
					ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
					this.arecipes.add(new CachedInfusionRecipe(input, entry2.getValue(), InfuserRecipes.infusing().getEnergyRequired(input)));
				}
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		Map<Item, HashMap<Integer, ItemStack>> recipes = InfuserRecipes.infusing().getRecipeList();
		for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
		{
			for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
			{
				if(NEIClientUtils.areStacksSameTypeCrafting(result, entry2.getValue()))
				{
					ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
					this.arecipes.add(new CachedInfusionRecipe(input, entry2.getValue(), InfuserRecipes.infusing().getEnergyRequired(input)));
				}
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		Map<Item, HashMap<Integer, ItemStack>> recipes = InfuserRecipes.infusing().getRecipeList();
		if(ItemUtils.areItemStacksEqual(ingredient, NEIHandler.EMPTY_TEST_TUBE, false))
		{
			for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
			{
				for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
				{
					ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
					CachedInfusionRecipe recipe = new CachedInfusionRecipe(input, entry2.getValue(), InfuserRecipes.infusing().getEnergyRequired(input));
					recipe.setIngredientPermutation(recipe.ingredients, ingredient);
					if(!this.arecipes.contains(recipe))
					{
						this.arecipes.add(recipe);
					}
				}
			}
		}
		else
		{
			for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
			{
				for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
				{
					ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
					if(NEIHandler.doesRecipeRequire(Arrays.asList(input), ingredient))
					{
						CachedInfusionRecipe recipe = new CachedInfusionRecipe(input, entry2.getValue(), InfuserRecipes.infusing().getEnergyRequired(input));
						recipe.setIngredientPermutation(recipe.ingredients, ingredient);
						this.arecipes.add(recipe);
					}
				}
		    }
		}
	}

	public class CachedInfusionRecipe extends CachedRecipe
	{
		public ArrayList<PositionedStack> ingredients;
		public PositionedStack result;
		public int energy;

		public CachedInfusionRecipe(ItemStack input, ItemStack output, int energy)
		{
			this.result = new PositionedStack(output, 111, 24);
			this.ingredients = new ArrayList<PositionedStack>();
			PositionedStack stack = new PositionedStack(input, 51, 6);
			this.ingredients.add(stack);
			this.ingredients.add(new PositionedStack(NEIHandler.EMPTY_TEST_TUBE, 51, 42));
			this.energy = energy;
		}

		@Override
		public List<PositionedStack> getIngredients()
		{
            return getCycledIngredients(InfuserHandler.this.cycleticks / 48, this.ingredients);
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}

}
