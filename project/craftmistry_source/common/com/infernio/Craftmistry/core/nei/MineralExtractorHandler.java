package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.common.fluid.Fluids;
import com.infernio.Craftmistry.common.recipe.MineralExtractorRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.gui.GUIUtils;

public class MineralExtractorHandler extends TemplateRecipeHandler {

	private int ticksSinceChange = 0;
	private int currentMultiplier = 1;

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.mineralExtractorRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "craftmistry:" + StringLib.MINERAL_EXTRACTOR_GUI_TEXTURE.replaceFirst(File.separator + ".png", "_noqm.png");
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.mineralExtractor";
	}

	@Override
	public void onUpdate()
	{
		if(!NEIClientUtils.shiftKey())
		{
			if(this.ticksSinceChange >= 20)
			{
				this.ticksSinceChange = 0;
				++this.currentMultiplier;
				if(this.currentMultiplier > 10)
				{
					this.currentMultiplier = 1;
				}
			}
			else
			{
				++this.ticksSinceChange;
			}
		}
	}

	@Override
	public void drawBackground(int recipe)
	{
	    GUIUtils.renderTank(-16, -9, 8, 18, 5 * this.currentMultiplier, Fluids.mineralWater);
	    super.drawBackground(recipe);
	}

	@Override
	public void drawExtras(int recipe)
	{
	    int energy = ((CachedMineralExtractorRecipe)this.arecipes.get(recipe)).chance;
	    GUIUtils.fontRenderer.drawString(energy + "%", 64, 16, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(58, 23, 24, 18), "craftmistry.mineralExtractor", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.mineralExtractor")) && (this.getClass() == MineralExtractorHandler.class))
		{
			ArrayList<ItemStack> recipes = MineralExtractorRecipes.extractions().getRecipes();
			for(ItemStack stack : recipes)
			{
				this.arecipes.add(new CachedMineralExtractorRecipe(stack, MineralExtractorRecipes.extractions().getChance(stack)));
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		ArrayList<ItemStack> recipes = MineralExtractorRecipes.extractions().getRecipes();
		for(ItemStack stack : recipes)
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(result, stack))
			{
				this.arecipes.add(new CachedMineralExtractorRecipe(stack, MineralExtractorRecipes.extractions().getChance(stack)));
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		FluidStack fluid = FluidContainerRegistry.getFluidForFilledItem(ingredient);
		if(fluid != null && fluid.amount > 0 && fluid.getFluid() == Fluids.mineralWater)
		{
			ArrayList<ItemStack> recipes = MineralExtractorRecipes.extractions().getRecipes();
			for(ItemStack stack : recipes)
			{
				this.arecipes.add(new CachedMineralExtractorRecipe(stack, MineralExtractorRecipes.extractions().getChance(stack)));
			}
		}
	}

	public class CachedMineralExtractorRecipe extends CachedRecipe
	{
		public PositionedStack result;
		public int chance;

		public CachedMineralExtractorRecipe(ItemStack output, int chance)
		{
			this.result = new PositionedStack(output, 111, 19);
			this.chance = chance;
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}

}
