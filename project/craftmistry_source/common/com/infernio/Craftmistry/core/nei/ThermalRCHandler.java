package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.api.chemistry.EnumMatterState;
import com.infernio.Craftmistry.common.chemistry.Elements;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.tile.TileThermalRegulationChamber;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.gui.GUIUtils;

public class ThermalRCHandler extends TemplateRecipeHandler {

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.thermalRCRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "craftmistry:" + StringLib.COMPRESSOR_GUI_TEXTURE.replaceFirst(File.separator + ".png", "_noqm.png");
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.thermalrc";
	}

	@Override
	public void drawExtras(int recipe)
	{
		CachedThermalRCRecipe crecipe = (CachedThermalRCRecipe)this.arecipes.get(recipe);
	    GUIUtils.fontRenderer.drawString(crecipe.energy + " EU", 71, 9, -8355712, false);
	    ItemStack output = crecipe.result.item;
	    ItemStack input = crecipe.ingredients.get(0).item;
	    if(output != null && output.getItem() != null && input != null && input.getItem() != null)
	    {
	    	Item item = output.getItem();
	    	Item item2 = input.getItem();
	    	if(item == ModItems.testTubeSolid && item2 == ModItems.testTubeLiquid)
	    	{
			    GUIUtils.fontRenderer.drawString("< " + crecipe.meltingPoint + "°K", 64, 46, -8355712, false);
			}
	    	if(item == ModItems.testTubeLiquid && item2 == ModItems.testTubeSolid)
	    	{
	    		GUIUtils.fontRenderer.drawString("> " + crecipe.meltingPoint + "°K", 64, 46, -8355712, false);
	    	}
	    	if(item == ModItems.testTubeLiquid && item2 == ModItems.testTubeGas)
	    	{
	    		GUIUtils.fontRenderer.drawString("< " + crecipe.boilingPoint + "°K", 64, 46, -8355712, false);
	    	}
	    	if(item == ModItems.testTubeGas && item2 == ModItems.testTubeLiquid)
	    	{
	    		GUIUtils.fontRenderer.drawString("> " + crecipe.boilingPoint + "°K", 64, 46, -8355712, false);
	    	}
	    }
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(84, 23, 24, 18), "craftmistry.thermalrc", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.thermalrc")) && (this.getClass() == ThermalRCHandler.class))
		{
			ArrayList<Integer> elements = Elements.getValidElements();
			for(Integer i : elements)
			{
				int atomNumber = i.intValue();
				if(atomNumber == 0)
				{
					continue;
				}
				List<EnumMatterState> states = Arrays.asList(Elements.getPossibleStates(atomNumber));
				int meltingPoint = Elements.getMeltingPoint(atomNumber);
				int boilingPoint = Elements.getBoilingPoint(atomNumber);
				if(states.contains(EnumMatterState.SOLID))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeSolid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
				if(states.contains(EnumMatterState.LIQUID))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeSolid, 1, atomNumber), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeGas, 1, atomNumber), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
				if(states.contains(EnumMatterState.GAS))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeGas, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		if(!ItemHelper.isTestTubeAndFilled(result))
		{
			return;
		}
		ArrayList<Integer> elements = Elements.getValidElements();
		for(Integer i : elements)
		{
			int atomNumber = i.intValue();
			if(atomNumber == 0)
			{
				continue;
			}
			if(result.getItemDamage() == atomNumber)
			{
				List<EnumMatterState> states = Arrays.asList(Elements.getPossibleStates(atomNumber));
				int meltingPoint = Elements.getMeltingPoint(atomNumber);
				int boilingPoint = Elements.getBoilingPoint(atomNumber);
				Item item = result.getItem();
				if(item == ModItems.testTubeSolid && states.contains(EnumMatterState.SOLID))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeSolid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
				if(item == ModItems.testTubeLiquid && states.contains(EnumMatterState.LIQUID))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeSolid, 1, atomNumber), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeGas, 1, atomNumber), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
				if(item == ModItems.testTubeGas && states.contains(EnumMatterState.GAS))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeGas, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		if(!ItemHelper.isTestTubeAndFilled(ingredient))
		{
			return;
		}
		ArrayList<Integer> elements = Elements.getValidElements();
		for(Integer i : elements)
		{
			int atomNumber = i.intValue();
			if(atomNumber == 0)
			{
				continue;
			}
			if(ingredient.getItemDamage() == atomNumber)
			{
				List<EnumMatterState> states = Arrays.asList(Elements.getPossibleStates(atomNumber));
				int meltingPoint = Elements.getMeltingPoint(atomNumber);
				int boilingPoint = Elements.getBoilingPoint(atomNumber);
				Item item = ingredient.getItem();
				if(item == ModItems.testTubeLiquid && states.contains(EnumMatterState.SOLID))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeSolid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
				if((item == ModItems.testTubeSolid || item == ModItems.testTubeGas) && states.contains(EnumMatterState.LIQUID))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeSolid, 1, atomNumber), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeGas, 1, atomNumber), new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
				if(item == ModItems.testTubeLiquid && states.contains(EnumMatterState.GAS))
				{
					this.arecipes.add(new CachedThermalRCRecipe(new ItemStack(ModItems.testTubeLiquid, 1, atomNumber), new ItemStack(ModItems.testTubeGas, 1, atomNumber), meltingPoint, boilingPoint, TileThermalRegulationChamber.ENERGY_CHANGE_STATE));
				}
			}
		}
	}

	public class CachedThermalRCRecipe extends CachedRecipe
	{
		public List<PositionedStack> ingredients;
		public PositionedStack result;
		public int boilingPoint;
		public int meltingPoint;
		public int energy;

		public CachedThermalRCRecipe(ItemStack input, ItemStack output, int meltingPoint, int boilingPoint, int energy)
		{
			this.result = new PositionedStack(output, 111, 24);
			this.ingredients = new ArrayList<PositionedStack>();
			this.meltingPoint = meltingPoint;
			this.boilingPoint = boilingPoint;
			this.ingredients = Arrays.asList(new PositionedStack(input, 51, 24));
			this.energy = energy;
		}

		@Override
		public List<PositionedStack> getIngredients()
		{
            return getCycledIngredients(ThermalRCHandler.this.cycleticks / 48, this.ingredients);
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}

}
