package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.api.recipe.ItemCollection;
import com.infernio.Craftmistry.common.recipe.ReactorRecipes;
import com.infernio.InfernoCore.client.gui.GUIUtils;

public class ReactorHandler extends TemplateRecipeHandler {

	public static final int[][] gridPositions = {{ 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 }, { 0, 2 }, { 1, 2 }, { 2, 0 }, { 2, 1 }, { 2, 2 }};

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.reactorRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "minecraft:textures/gui/container/crafting_table.png";
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.reactions";
	}

	@Override
	public void drawExtras(int recipe)
	{
	    int energy = ((CachedReactionRecipe)this.arecipes.get(recipe)).energy;
	    GUIUtils.fontRenderer.drawString(energy + " EU", 84, 9, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(84, 23, 24, 18), "craftmistry.reactions", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.reactions")) && (this.getClass() == ReactorHandler.class))
		{
			Map<ItemCollection, ItemStack> recipes = ReactorRecipes.reactions().getRecipeList();
			for(Entry<ItemCollection, ItemStack> entry : recipes.entrySet())
			{
				ItemStack[] items = entry.getKey().getItems();
				if(items != null)
				{
					this.arecipes.add(new CachedReactionRecipe(items, entry.getValue(), ReactorRecipes.reactions().getEnergyRequired(items)));
				}
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		Map<ItemCollection, ItemStack> recipes = ReactorRecipes.reactions().getRecipeList();
		for(Entry<ItemCollection, ItemStack> entry : recipes.entrySet())
		{
			ItemStack output = entry.getValue();
			if(NEIClientUtils.areStacksSameTypeCrafting(result, output))
			{
				ItemStack[] items = entry.getKey().getItems();
				this.arecipes.add(new CachedReactionRecipe(items, entry.getValue(), ReactorRecipes.reactions().getEnergyRequired(items)));
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		Map<ItemCollection, ItemStack> recipes = ReactorRecipes.reactions().getRecipeList();
		for(Entry<ItemCollection, ItemStack> entry : recipes.entrySet())
		{
			ItemStack[] items = entry.getKey().getItems();
			if(NEIHandler.doesRecipeRequire(items, ingredient))
			{
				CachedReactionRecipe recipe = new CachedReactionRecipe(items, entry.getValue(), ReactorRecipes.reactions().getEnergyRequired(items));
				recipe.setIngredientPermutation(recipe.ingredients, ingredient);
				this.arecipes.add(recipe);
			}
	    }
	}

	public class CachedReactionRecipe extends CachedRecipe
	{
		public ArrayList<PositionedStack> ingredients;
		public PositionedStack result;
		public int energy;

		public CachedReactionRecipe(ItemStack[] items, ItemStack output, int energy)
		{
			this.result = new PositionedStack(output, 119, 24);
			this.ingredients = new ArrayList<PositionedStack>();
			for(int i = 0; i < items.length; ++i)
			{
				if(items[i] != null)
				{
					PositionedStack stack = new PositionedStack(items[i], 25 + ReactorHandler.gridPositions[i][0] * 18, 6 + ReactorHandler.gridPositions[i][1] * 18);
			        this.ingredients.add(stack);
				}
			}
			this.energy = energy;
		}

		@Override
		public List<PositionedStack> getIngredients()
		{
            return getCycledIngredients(ReactorHandler.this.cycleticks / 48, this.ingredients);
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}

}
