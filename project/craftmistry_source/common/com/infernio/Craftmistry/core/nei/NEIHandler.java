package com.infernio.Craftmistry.core.nei;

import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.api.API;
import codechicken.nei.recipe.DefaultOverlayHandler;

import com.infernio.Craftmistry.client.gui.GUIAirFilter;
import com.infernio.Craftmistry.client.gui.GUICompressor;
import com.infernio.Craftmistry.client.gui.GUIElectrolyser;
import com.infernio.Craftmistry.client.gui.GUIInfuser;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.compat.CompatHandler;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.gui.Tooltip;
import com.infernio.InfernoCore.io.IOUtils;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class NEIHandler {

	public static final ItemStack EMPTY_TEST_TUBE = new ItemStack(ModItems.testTubeSolid, 1, 0);

	public static void init()
	{
		API.registerRecipeHandler(new InfuserHandler());
		API.registerUsageHandler(new InfuserHandler());
		API.registerRecipeHandler(new CompressorHandler());
		API.registerUsageHandler(new CompressorHandler());
		API.registerRecipeHandler(new ReactorHandler());
		API.registerUsageHandler(new ReactorHandler());
		API.registerRecipeHandler(new AirFilterHandler());
		API.registerUsageHandler(new AirFilterHandler());
		API.registerRecipeHandler(new MineralExtractorHandler());
		API.registerUsageHandler(new MineralExtractorHandler());
		API.registerRecipeHandler(new ElectrolyserHandler());
		API.registerUsageHandler(new ElectrolyserHandler());
		//Register Thermal RC last
		API.registerRecipeHandler(new ThermalRCHandler());
		API.registerUsageHandler(new ThermalRCHandler());

		API.registerGuiOverlay(GUIInfuser.class, "craftmistry.infusing");
		API.registerGuiOverlayHandler(GUIInfuser.class, new DefaultOverlayHandler(), "craftmistry.infusing");
		API.registerGuiOverlay(GUICompressor.class, "craftmistry.compressing");
		API.registerGuiOverlayHandler(GUICompressor.class, new DefaultOverlayHandler(), "craftmistry.compressing"); 
		//TODO Bugged ATM
		//API.registerGuiOverlay(GUIReactor.class, "craftmistry.reactions");
		//API.registerGuiOverlayHandler(GUIReactor.class, new DefaultOverlayHandler(), "craftmistry.reactions"); 
		API.registerGuiOverlay(GUIAirFilter.class, "craftmistry.airfilter");
		API.registerGuiOverlayHandler(GUIAirFilter.class, new DefaultOverlayHandler(), "craftmistry.airfilter");
		API.registerGuiOverlay(GUIElectrolyser.class, "craftmistry.electrolyser");
		API.registerGuiOverlayHandler(GUIElectrolyser.class, new DefaultOverlayHandler(), "craftmistry.electrolyser");
	}

	public static void printNEIMessage()
	{
		if(!CompatHandler.neiPresent && !hasSeenMessage())
		{
			Tooltip message = Tooltip.loadLocalizedTooltip("cm_neiplugin.general.missingNei");
			GuiNewChat chat = FMLClientHandler.instance().getClient().ingameGUI.getChatGUI();
			for(String msg : message.tooltip)
			{
				chat.printChatMessage(new ChatComponentText(msg));
			}
		}
	}

	private static boolean hasSeenMessage()
	{
		if(IOUtils.getBoolean(StringLib.CRAFTMISTRY_DATA_STORAGE_LOCATION, StringLib.NEI_MSG_FILE, "hasSeen"))
		{
			return true;
		}
		IOUtils.setBoolean(StringLib.CRAFTMISTRY_DATA_STORAGE_LOCATION, StringLib.NEI_MSG_FILE, "hasSeen", true);
		return false;
	}

	public static boolean doesRecipeRequire(Iterable<ItemStack> recipe, ItemStack ingredient)
	{
		for(ItemStack item : recipe)
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(item, ingredient))
			{
				return true;
			}
		}
		return false;
	}

	public static boolean doesRecipeRequire(ItemStack[] recipe, ItemStack ingredient)
	{
		for(ItemStack item : recipe)
		{
			if(NEIClientUtils.areStacksSameTypeCrafting(item, ingredient))
			{
				return true;
			}
		}
		return false;
	}

}
