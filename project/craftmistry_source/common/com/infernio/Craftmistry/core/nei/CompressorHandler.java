package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.common.recipe.CompressorRecipes;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.gui.GUIUtils;

public class CompressorHandler extends TemplateRecipeHandler {

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.compressorRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "craftmistry:" + StringLib.COMPRESSOR_GUI_TEXTURE.replaceFirst(File.separator + ".png", "_noqm.png");
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.compressing";
	}

	@Override
	public void drawExtras(int recipe)
	{
	    int energy = ((CachedCompressionRecipe)this.arecipes.get(recipe)).energy;
	    GUIUtils.fontRenderer.drawString(energy + " EU", 71, 9, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(84, 23, 24, 18), "craftmistry.compressing", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.compressing")) && (this.getClass() == CompressorHandler.class))
		{
			Map<Item, HashMap<Integer, ItemStack>> recipes = CompressorRecipes.compressing().getRecipeList();
			for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
			{
				for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
				{
					ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
					this.arecipes.add(new CachedCompressionRecipe(input, entry2.getValue(), CompressorRecipes.compressing().getEnergyRequired(input)));
				}
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		Map<Item, HashMap<Integer, ItemStack>> recipes = CompressorRecipes.compressing().getRecipeList();
		for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
		{
			for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
			{
				if(NEIClientUtils.areStacksSameTypeCrafting(result, entry2.getValue()))
				{
					ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
					this.arecipes.add(new CachedCompressionRecipe(input, entry2.getValue(), CompressorRecipes.compressing().getEnergyRequired(input)));
				}
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		Map<Item, HashMap<Integer, ItemStack>> recipes = CompressorRecipes.compressing().getRecipeList();
		for(Entry<Item, HashMap<Integer, ItemStack>> entry : recipes.entrySet())
		{
			for(Entry<Integer, ItemStack> entry2 : entry.getValue().entrySet())
			{
				ItemStack input = new ItemStack(entry.getKey(), 1, entry2.getKey());
				if(NEIHandler.doesRecipeRequire(Arrays.asList(input), ingredient))
				{
					CachedCompressionRecipe recipe = new CachedCompressionRecipe(input, entry2.getValue(), CompressorRecipes.compressing().getEnergyRequired(input));
					recipe.setIngredientPermutation(recipe.ingredients, ingredient);
					this.arecipes.add(recipe);
				}
			}
	    }
	}

	public class CachedCompressionRecipe extends CachedRecipe
	{
		public ArrayList<PositionedStack> ingredients;
		public PositionedStack result;
		public int energy;

		public CachedCompressionRecipe(ItemStack input, ItemStack output, int energy)
		{
			this.result = new PositionedStack(output, 111, 24);
			this.ingredients = new ArrayList<PositionedStack>();
			input.stackSize = CompressorRecipes.compressing().getStackSizeToRemove(input);
			PositionedStack stack = new PositionedStack(input, 51, 24);
			this.ingredients.add(stack);
			this.energy = energy;
		}

		@Override
		public List<PositionedStack> getIngredients()
		{
            return getCycledIngredients(CompressorHandler.this.cycleticks / 48, this.ingredients);
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}

}
