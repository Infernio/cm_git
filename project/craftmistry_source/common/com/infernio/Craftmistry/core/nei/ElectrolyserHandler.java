package com.infernio.Craftmistry.core.nei;

import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import codechicken.nei.NEIClientUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.TemplateRecipeHandler;

import com.infernio.Craftmistry.common.recipe.ElectrolyserRecipes;
import com.infernio.Craftmistry.common.recipe.ElectrolyserRecipes.ItemPair;
import com.infernio.Craftmistry.common.tile.TileElectrolyser;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.InfernoCore.client.gui.GUIUtils;
import com.infernio.InfernoCore.util.ItemUtils;

public class ElectrolyserHandler extends TemplateRecipeHandler {

	@Override
	public String getRecipeName()
	{
		return StatCollector.translateToLocal("cm_neiplugin.electrolyserRecipesName");
	}

	@Override
	public String getGuiTexture()
	{
		return "craftmistry:" + StringLib.ELECTROLYSER_GUI_TEXTURE.replaceFirst(File.separator + ".png", "_noqm.png");
	}

	@Override
	public String getOverlayIdentifier()
	{
		return "craftmistry.electrolyser";
	}

	@Override
	public void drawExtras(int recipe)
	{
	    GUIUtils.fontRenderer.drawString((TileElectrolyser.ENERGY_REQUIRED * TileElectrolyser.PROGRESS_FINISHED) + " EU", 71, 9, -8355712, false);
	}

	@Override
	public void loadTransferRects()
	{
		this.transferRects.add(new TemplateRecipeHandler.RecipeTransferRect(new Rectangle(84, 23, 24, 18), "craftmistry.electrolyser", new Object[0]));
	}

	@Override
	public void loadCraftingRecipes(String outputId, Object... results)
	{
		if((outputId.equals("craftmistry.electrolyser")) && (this.getClass() == ElectrolyserHandler.class))
		{
			Map<ItemPair, ItemStack> recipes = ElectrolyserRecipes.electrolysing().getRecipeList();
			for(Entry<ItemPair, ItemStack> entry : recipes.entrySet())
			{
				ItemPair item = entry.getKey();
				ItemStack input1 = new ItemStack(item.item1, 1, item.meta1);
				ItemStack input2 = new ItemStack(item.item2, 1, item.meta2);
				List<ItemStack> input = Arrays.asList(input1, input2);
				this.arecipes.add(new CachedElectrolyserRecipe(input, entry.getValue()));
			}
	    }
		else
	    {
			super.loadCraftingRecipes(outputId, results);
	    }
	}

	@Override
	public void loadCraftingRecipes(ItemStack result)
	{
		Map<ItemPair, ItemStack> recipes = ElectrolyserRecipes.electrolysing().getRecipeList();
		for(Entry<ItemPair, ItemStack> entry : recipes.entrySet())
		{
			ItemStack result1 = entry.getValue();
			if(NEIClientUtils.areStacksSameTypeCrafting(result, result1))
			{
				ItemPair item = entry.getKey();
				ItemStack input1 = new ItemStack(item.item1, 1, item.meta1);
				ItemStack input2 = new ItemStack(item.item2, 1, item.meta2);
				List<ItemStack> input = Arrays.asList(input1, input2);
				this.arecipes.add(new CachedElectrolyserRecipe(input, result1));
			}
		}
	}

	@Override
	public void loadUsageRecipes(ItemStack ingredient)
	{
		Map<ItemPair, ItemStack> recipes = ElectrolyserRecipes.electrolysing().getRecipeList();
		if(ItemUtils.areItemStacksEqual(ingredient, NEIHandler.EMPTY_TEST_TUBE, false))
		{
			for(Entry<ItemPair, ItemStack> entry : recipes.entrySet())
			{
				ItemPair item = entry.getKey();
				ItemStack input1 = new ItemStack(item.item1, 1, item.meta1);
				ItemStack input2 = new ItemStack(item.item2, 1, item.meta2);
				List<ItemStack> input = Arrays.asList(input1, input2);
				CachedElectrolyserRecipe recipe = new CachedElectrolyserRecipe(input, entry.getValue());
				recipe.setIngredientPermutation(recipe.ingredients, ingredient);
				if(!this.arecipes.contains(recipe))
				{
					this.arecipes.add(recipe);
				}
			}
		}
		else
		{
			recipes = ElectrolyserRecipes.electrolysing().getRecipeList();
			for(Entry<ItemPair, ItemStack> entry : recipes.entrySet())
			{
				ItemPair item = entry.getKey();
				ItemStack input1 = new ItemStack(item.item1, 1, item.meta1);
				ItemStack input2 = new ItemStack(item.item2, 1, item.meta2);
				List<ItemStack> input = Arrays.asList(input1, input2);
				if(NEIHandler.doesRecipeRequire(input, ingredient))
				{
					CachedElectrolyserRecipe recipe = new CachedElectrolyserRecipe(input, entry.getValue());
					recipe.setIngredientPermutation(recipe.ingredients, ingredient);
					this.arecipes.add(recipe);
				}
		    }
		}
	}

	public class CachedElectrolyserRecipe extends CachedRecipe
	{
		public ArrayList<PositionedStack> ingredients;
		public PositionedStack result;

		public CachedElectrolyserRecipe(List<ItemStack> input, ItemStack output)
		{
			this.result = new PositionedStack(output, 111, 24);
			this.ingredients = new ArrayList<PositionedStack>();
			this.ingredients.add(new PositionedStack(input.get(0), 51, 6));
			this.ingredients.add(new PositionedStack(input.get(1), 51, 42));
		}

		@Override
		public List<PositionedStack> getIngredients()
		{
            return getCycledIngredients(ElectrolyserHandler.this.cycleticks / 48, this.ingredients);
		}

		@Override
		public PositionedStack getResult()
		{
			return this.result;
		}
	}
}
