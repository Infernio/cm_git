package com.infernio.Craftmistry.core.lib;

import java.util.Locale;

public class StringLib {

	//General Mod Strings
	public static final String MODID = "Craftmistry";
	public static final String MODNAME = "Craftmistry";
	public static final String VERSION_PREFIX = "A"; //Changes from A to B to R when state of development changes (Alpha, Beta, Release)
	public static final String VESION_RAW = "0"; //Incremented in case of VERY big changes
	public static final String VERSION_MAJOR = "9"; //Incremented in case of big changes (API rewrite, Minecraft version update. etc.)
	public static final String VERSION_MINOR = "4"; //Incremented in case of medium changes (e.g. Function addition)
	public static final String VERSION_BUILD = "111"; //Incremented every time a new commit is pushed
	public static final String VERSION = VERSION_PREFIX + VESION_RAW + "." + VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_BUILD;
	public static final String CHANNEL = "Craftmistry";
	public static final String DEPENDENCIES = "required-after:InfernoCore@[A0.1.4,)";
	public static final String INFERNO_CORE_NOT_LOADED_MESSAGE_1 = "Inferno Core is not installed!";
	public static final String INFERNO_CORE_NOT_LOADED_MESSAGE_2 = "Craftmistry relies on this libary and will not load without it.";

	//Item Strings
	public static final String ITEM_TEST_TUBE_NAME = "itemCMTestTube";
	public static final String ITEM_TEST_TUBE_SANE_NAME = "test_tube";
	public static final String ITEM_RAW_MINERAL_NAME = "itemCMRawMineral";
	public static final String ITEM_RAW_MINERAL_SANE_NAME = "raw_mineral_dust";
	public static final String ITEM_CRAFTMISTRY_INGOT_NAME = "itemCMIngot";
	public static final String ITEM_CRAFTMISTRY_INGOT_SANE_NAME = "ingot";
	public static final String ITEM_SCOOPULA_NAME = "itemCMScoopula";
	public static final String ITEM_SCOOPULA_SANE_NAME = "scoopula";
	public static final String ITEM_OXYGEN_MASK_NAME = "itemCMOxygenMask";
	public static final String ITEM_OXYGEN_MASK_SANE_NAME = "oxygen_mask";
	public static final String ITEM_BUCKET_NAME = "itemCMBucket";
	public static final String ITEM_BUCKET_SANE_NAME = "bucket";
	public static final String ITEM_SUPER_PHOSPHATE_NAME = "itemCMSuperPhosphate";
	public static final String ITEM_SUPER_PHOSPHATE_SANE_NAME = "super_phosphate";
	public static final String ITEM_GRENADE_NAME = "itemCMGrenade";
	public static final String ITEM_GRENADE_SANE_NAME = "grenade";
	public static final String ITEM_HIGH_ENERGY_CREATOR_NAME = "itemCMHighEnergyCreator";
	public static final String ITEM_HIGH_ENERGY_CREATOR_SANE_NAME = "high_energy_creator";
	public static final String ITEM_UPGRADE_NAME = "itemCMUpgrade";
	public static final String ITEM_UPGRADE_SANE_NAME = "upgrade";
	public static final String ITEM_MULTI_NAME = "itemCMMulti";
	public static final String ITEM_MULTI_SANE_NAME = "multi_item";
	public static final String ITEM_SPOILED_FOOD_NAME = "itemCMSpoiledFood";
	public static final String ITEM_SPOILED_FOOD_SANE_NAME = "spoiled_food";
	public static final String ITEM_NOVA_SHOVEL_NAME = "itemCMNovaShovel";
	public static final String ITEM_NOVA_SHOVEL_SANE_NAME = "nova_shovel";
	public static final String ITEM_NOVA_PICKAXE_NAME = "itemCMNovaPickaxe";
	public static final String ITEM_NOVA_PICKAXE_SANE_NAME = "nova_pickaxe";
	public static final String ITEM_NOVA_AXE_NAME = "itemCMNovaAxe";
	public static final String ITEM_NOVA_AXE_SANE_NAME = "nova_axe";
	public static final String ITEM_NOVA_SHEARS_NAME = "itemCMNovaShears";
	public static final String ITEM_NOVA_SHEARS_SANE_NAME = "nova_shears";
	public static final String ITEM_POCKET_DIMENSION_NAME = "itemCMPocketDimension";
	public static final String ITEM_POCKET_DIMENSION_SANE_NAME = "pocket_dimension";
	public static final String ITEM_TREE_CONTROLLER_NAME = "itemCMTreeController";
	public static final String ITEM_TREE_CONTROLLER_SANE_NAME = "tree_controller";

	//Armor Strings
	public static final String ARMOR_FOLDER = MODID.toLowerCase(Locale.ENGLISH) + ":" + "textures/armor/";
	public static final String OXYGEN_MASK_TEXTURE = ARMOR_FOLDER + "oxygenMask.png";

	//Block Strings
	public static final String BLOCK_TEST_TUBE_RACK_NAME = "blockCMTestTubeRack";
	public static final String BLOCK_TEST_TUBE_RACK_SANE_NAME = "test_tube_rack";
	public static final String BLOCK_CRAFTMISTRY_ORE_NAME = "blockCraftmistryOre";
	public static final String BLOCK_CRAFTMISTRY_ORE_SANE_NAME = "ore";
	public static final String BLOCK_ANALYZER_NAME = "blockCMAnalyzer";
	public static final String BLOCK_ANALYZER_SANE_NAME = "analyzer";
	public static final String BLOCK_INFUSER_NAME = "blockCMInfuser";
	public static final String BLOCK_INFUSER_SANE_NAME = "infuser";
	public static final String BLOCK_AIR_FILTER_NAME = "blockCMAirFilter";
	public static final String BLOCK_AIR_FILTER_SANE_NAME = "air_filter";
	public static final String BLOCK_DESTABILIZER_NAME = "blockCMDestabilizer";
	public static final String BLOCK_DESTABILIZER_SANE_NAME = "destabilizer";
	public static final String BLOCK_COMPRESSOR_NAME = "blockCMCompressor";
	public static final String BLOCK_COMPRESSOR_SANE_NAME = "compressor";
	public static final String BLOCK_CLEANER_NAME = "blockCMCleaner";
	public static final String BLOCK_CLEANER_SANE_NAME = "cleaner";
	public static final String BLOCK_STABILIZER_NAME = "blockCMStabilizer";
	public static final String BLOCK_STABILIZER_SANE_NAME = "stabilizer";
	public static final String BLOCK_REACTOR_NAME = "blockCMReactor";
	public static final String BLOCK_REACTOR_SANE_NAME = "reactor";
	public static final String BLOCK_BREAKER_NAME = "blockCMBreaker";
	public static final String BLOCK_BREAKER_SANE_NAME = "breaker";
	public static final String BLOCK_COLLECTOR_NAME = "blockCMCollector";
	public static final String BLOCK_COLLECTOR_SANE_NAME = "collector";
	public static final String BLOCK_DETECTOR_NAME = "blockCMDetector";
	public static final String BLOCK_DETECTOR_SANE_NAME = "detector";
	public static final String BLOCK_SULFURIC_ACID_NAME = "blockCMSulfuricAcid";
	public static final String BLOCK_SULFURIC_ACID_SANE_NAME = "sulfuric_acid";
	public static final String BLOCK_RESEARCH_MULTI_NAME = "blockCMResearchMulti";
	public static final String BLOCK_RESEARCH_MULTI_SANE_NAME = "research";
	public static final String BLOCK_RESEARCH_PANEL_NAME = "blockCMResearchPanel";
	public static final String BLOCK_RESEARCH_PANEL_SANE_NAME = "research_panel";
	public static final String BLOCK_RESEARCH_COMPUTER_NAME = "blockCMResearchComputer";
	public static final String BLOCK_RESEARCH_COMPUTER_SANE_NAME = "research_computer";
	public static final String BLOCK_RESEARCH_MATERIAL_COMPARTMENT_NAME = "blockCMResearchMaterialCompartment";
	public static final String BLOCK_RESEARCH_MATERIAL_COMPARTMENT_SANE_NAME = "material_compartment";
	public static final String BLOCK_RESEARCH_ENERGY_STORAGE_NAME = "blockCMResearchEnergyStorage";
	public static final String BLOCK_RESEARCH_ENERGY_STORAGE_SANE_NAME = "energy_storage";
	public static final String BLOCK_RESEARCH_PRODUCTION_FACILITY_NAME = "blockCMResearchProductionFacility";
	public static final String BLOCK_RESEARCH_PRODUCTION_FACILITY_SANE_NAME = "production_facility";
	public static final String BLOCK_MANA_SPRING_NAME = "blockCMManaSpring";
	public static final String BLOCK_MANA_SPRING_SANE_NAME = "mana_spring";
	public static final String BLOCK_MULTI_BLOCK_NAME = "blockCMMultiBlock";
	public static final String BLOCK_MULTI_BLOCK_SANE_NAME = "thermal_regulation_chamber";
	public static final String BLOCK_CRAFTING_COMPONENT_NAME = "blockCMCraftingComponent";
	public static final String BLOCK_CRAFTING_COMPONENT_SANE_NAME = "crafting_component";
	public static final String BLOCK_MINERAL_WATER_NAME = "blockCMMineralWater";
	public static final String BLOCK_MINERAL_WATER_SANE_NAME = "mineral_water";
	public static final String BLOCK_MINERAL_EXTRACTOR_NAME = "blockCMMineralExtractor";
	public static final String BLOCK_MINERAL_EXTRACTOR_SANE_NAME = "mineral_extractor";
	public static final String BLOCK_ELECTROLYSER_NAME = "blockCMElectrolyser";
	public static final String BLOCK_ELECTROLYSER_SANE_NAME = "electrolyser";
	public static final String BLOCK_FORESTER_NAME = "blockCMForester";
	public static final String BLOCK_FORESTER_SANE_NAME = "forester";

	//Model Strings
	public static final String MODEL_FOLDER = "textures/models/";
	public static final String TEST_TUBE_RACK_TEXTURE = MODEL_FOLDER + "modelTestTubeRack.png";
	public static final String RESEARCH_PANEL_TEXTURE = MODEL_FOLDER + "modelResearchPanel.png";
	public static final String ANALYZER_TEXTURE = MODEL_FOLDER + "modelAnalyzer.png";
	public static final String MATERIAL_COMPARTMENT_TEXTURE = MODEL_FOLDER + "modelMaterialCompartment.png";
	public static final String AIR_FILTER_TEXTURE = MODEL_FOLDER + "modelAirFilter.png";
	public static final String MACHINE_FRAME_BASIC_TEXTURE = MODEL_FOLDER + "modelMachineFrame_basic.png";
	public static final String MACHINE_FRAME_CHEMICAL_TEXTURE = MODEL_FOLDER + "modelMachineFrame_chemical.png";
	public static final String MACHINE_FRAME_HARDENED_TEXTURE = MODEL_FOLDER + "modelMachineFrame_hardened.png";
	public static final String MACHINE_FRAME_ADVANCED_TEXTURE = MODEL_FOLDER + "modelMachineFrame_advanced.png";

	//Tile Entity Strings
	public static final String TEST_TUBE_RACK_ID = "tileCMTestTubeRack";
	public static final String ANALYZER_ID = "tileCMAnalyzer";
	public static final String INFUSER_ID = "tileCMInfuser";
	public static final String AIR_FILTER_ID = "tileCMAirFilter";
	public static final String DESTABILIZER_ID = "tileCMDestabilizer";
	public static final String CLEANER_ID = "tileCMCleaner";
	public static final String COMPRESSOR_ID = "tileCMCompressor";
	public static final String STABILIZER_ID = "tileCMStabilizer";
	public static final String REACTOR_ID = "tileCMReactor";
	public static final String BREAKER_ID = "tileCMBreaker";
	public static final String COLLECTOR_ID = "tileCMCollector";
	public static final String DETECTOR_ID = "tileCMDetector";
	public static final String RESEARCH_PANEL_ID = "tileCMResearchPanel";
	public static final String COMPUTER_ID = "tileCMComputer";
	public static final String MATERIAL_COMPARTMENT_ID = "tileCMMaterialCompartment";
	public static final String ENERGY_STORAGE_ID = "tileCMEnergyStorage";
	public static final String PRODUCTION_FACILITY_ID = "tileCMProductionFacility";
	public static final String MANA_SPRING_ID = "tileCMManaSpring";
	public static final String THERMAL_REGULATION_CHAMBER_ID = "tileCMThermalRC";
	public static final String PLASMATIC_BUILDER_ID = "tileCMPlasmaticBuilder";
	public static final String PLASMATIC_LASER_ID = "tileCMPlasmaticLaser";
	public static final String MINERAL_EXTRACTOR_ID = "tileCMMineralExtractor";
	public static final String ELECTROLYSER_ID = "tileCMElectrolyser";
	public static final String FORESTER_ID = "tileCMForester";

	//GUI Strings
	public static final String GUI_FOLDER = "textures/gui/";
	public static final String TEST_TUBE_RACK_GUI_TEXTURE = GUI_FOLDER + "GUITestTubeRack.png";
	public static final String ANALYZER_GUI_TEXTURE = GUI_FOLDER + "GUIAnalyzer.png";
	public static final String INFUSER_GUI_TEXTURE = GUI_FOLDER + "GUIInfuser.png";
	public static final String AIR_FILTER_GUI_TEXTURE = GUI_FOLDER + "GUIAirFilter.png";
	public static final String DESTABILIZER_GUI_TEXTURE = GUI_FOLDER + "GUIDestabilizer.png";
	public static final String COMPRESSOR_GUI_TEXTURE = GUI_FOLDER + "GUICompressor.png";
	public static final String CLEANER_GUI_TEXTURE = GUI_FOLDER + "GUICleaner.png";
	public static final String STABILIZER_GUI_TEXTURE = GUI_FOLDER + "GUIStabilizer.png";
	public static final String REACTOR_GUI_TEXTURE = GUI_FOLDER + "GUIReactor.png";
	public static final String BREAKER_GUI_TEXTURE = GUI_FOLDER + "GUIBreaker.png";
	public static final String COLLECTOR_GUI_TEXTURE = GUI_FOLDER + "GUICollector.png";
	public static final String DETECTOR_GUI_TEXTURE = GUI_FOLDER + "GUIDetector.png";
	public static final String MATERIAL_COMPARTMENT_GUI_TEXTURE = GUI_FOLDER + "GUIMaterialCompartment.png";
	public static final String ENERGY_STORAGE_GUI_TEXTURE = GUI_FOLDER + "GUIEnergyStorage.png";
	public static final String RESEARCH_PANEL_GUI_TEXTURE = GUI_FOLDER + "GUIResearchPanel.png";
	public static final String THERMAL_REGULATION_CHAMBER_GUI_TEXTURE = GUI_FOLDER + "GUIThermalRegulationChamber.png";
	public static final String SELECT_PROJECT_ITEMS_GUI_TEXTURE = GUI_FOLDER + "GUISelectProjectItems.png";
	public static final String MINERAL_EXTRACTOR_GUI_TEXTURE = GUI_FOLDER + "GUIMineralExtractor.png";
	public static final String ELECTROLYSER_GUI_TEXTURE = GUI_FOLDER + "GUIElectrolyser.png";
	public static final String FORESTER_GUI_TEXTURE = GUI_FOLDER + "GUIForester.png";

	//Container Strings
	public static final String CONTAINER_TEST_TUBE_RACK_NAME = "container.CMTestTubeRack";
	public static final String CONTAINER_ANALYZER_NAME = "container.CMAnalyzer";
	public static final String CONTAINER_INFUSER_NAME = "container.CMInfuser";
	public static final String CONTAINER_AIR_FILTER_NAME = "container.CMAirFilter";
	public static final String CONTAINER_DESTABILIZER_NAME = "container.CMDestabilizer";
	public static final String CONTAINER_COMPRESSOR_NAME = "container.CMCompressor";
	public static final String CONTAINER_CLEANER_NAME = "container.CMCleaner";
	public static final String CONTAINER_STABILIZER_NAME = "container.CMStabilizer";
	public static final String CONTAINER_REACTOR_NAME = "container.CMReactor";
	public static final String CONTAINER_BREAKER_NAME = "container.CMBreaker";
	public static final String CONTAINER_COLLECTOR_NAME = "container.CMCollector";
	public static final String CONTAINER_DETECTOR_NAME = "container.CMDetector";
	public static final String CONTAINER_MATERIAL_COMPARTMENT_NAME = "container.CMMaterialCompartment";
	public static final String CONTAINER_ENERGY_STORAGE_NAME = "container.CMEnergyStorage";
	public static final String CONTAINER_SELECTION_INVENTORY_NAME = "container.researchSelectionInv";
	public static final String CONTAINER_MINERAL_EXTRACTOR_NAME = "container.CMMineralExtractor";
	public static final String CONTAINER_ELECTROLYSER_NAME = "container.CMElectrolyser";
	public static final String CONTAINER_FORESTER_NAME = "container.CMForester";

	//Entity Strings
	public static final String ENTITY_GRENADE_NAME = "EntityCMGrenade";
	public static final String ENTITY_ALKALI_ITEM_NAME = "EntityCMAlkaliItem";

	//Command Strings
	public static final String COMMAND_POCKET_DIMENSION_USAGE = "/pocketdimension <ID> <Meta>";
	public static final String COMMAND_PD_USAGE = "/pd <ID> <Meta>";

	//Fluid Strings
	public static final String FLUID_SULFURIC_ACID_NAME = "SulfuricAcid";
	public static final String FLUID_MINERAL_WATER_NAME = "MineralWater";

	//Config Strings
	public static final String CATEGORY_BLOCK = "Blocks";
	public static final String CATEGORY_ITEM = "Items";
	public static final String CATEGORY_GRAPHICS = "Graphics";
	public static final String CATEGORY_ENTITY = "Entity";
	public static final String CATEGORY_WORLD = "World";
	public static final String CATEGORY_KEY_BINDINGS = "Key Bindings";

	//Achievement Strings
	public static final String ACHIEVEMENT_TEST_TUBES_NAME = "craftTestTubes";
	public static final String ACHIEVEMENT_TEST_TUBE_RACK_NAME = "craftTestTubeRack";
	public static final String ACHIEVEMENT_SCOOPULA_NAME = "useScoopula";
	public static final String ACHIEVEMENT_BASIC_MICROCHIPS_NAME = "craftBasicMicrochips";
	public static final String ACHIEVEMENT_BASIC_MACHINE_CONTROLLER_NAME = "craftBasicMachineController";
	public static final String ACHIEVEMENT_ANALYZER_NAME = "craftAnalyzer";

	//Compatibility Strings
	public static final String CRAFTMISTRY_DATA_STORAGE_LOCATION = "craftmistry";
	//TODO Fix this once Pahi is back on track
	//public static final String EE3_IMC_ADD_RECIPE = "recipe-add";
	public static final String NEI_MSG_FILE = "nei_msg";

	//Key Binding Strings
	public static final String KEY_CATEGORY_CRAFTMISTRY = "key.category.craftmistry";
	public static final String KEY_POCKET_DIMENSION_GIVE_STACK_NAME = "key.pdGiveStack";
	public static final String KEY_POCKET_DIMENSION_CLEAR_STACK_NAME = "key.pdClearStack";
}
