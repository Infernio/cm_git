package com.infernio.Craftmistry.core.lib;

import net.minecraft.util.ResourceLocation;

import com.infernio.Craftmistry.core.config.ConfigLoader;

public class ResourceLib {

	public static final ResourceLocation GUI_AIR_FILTER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.AIR_FILTER_GUI_TEXTURE);
	public static final ResourceLocation GUI_ANALYZER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.ANALYZER_GUI_TEXTURE);
	public static final ResourceLocation GUI_BREAKER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.BREAKER_GUI_TEXTURE);
	public static final ResourceLocation GUI_CLEANER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.CLEANER_GUI_TEXTURE);
	public static final ResourceLocation GUI_COLLECTOR_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.COLLECTOR_GUI_TEXTURE);
	public static final ResourceLocation GUI_COMPRESSOR_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.COMPRESSOR_GUI_TEXTURE);
	public static final ResourceLocation GUI_DESTABILIZER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.DESTABILIZER_GUI_TEXTURE);
	public static final ResourceLocation GUI_DETECTOR_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.DETECTOR_GUI_TEXTURE);
	public static final ResourceLocation GUI_INFUSER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.INFUSER_GUI_TEXTURE);
	public static final ResourceLocation GUI_REACTOR_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.REACTOR_GUI_TEXTURE);
	public static final ResourceLocation GUI_STABILIZER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.STABILIZER_GUI_TEXTURE);
	public static final ResourceLocation GUI_MINERAL_EXTRACTOR = new ResourceLocation(ConfigLoader.id, StringLib.MINERAL_EXTRACTOR_GUI_TEXTURE);
	public static final ResourceLocation GUI_TEST_TUBE_RACK_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.TEST_TUBE_RACK_GUI_TEXTURE);
	public static final ResourceLocation GUI_PANEL_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.RESEARCH_PANEL_GUI_TEXTURE);
	public static final ResourceLocation GUI_MATERIAL_COMAPRTMENT_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.MATERIAL_COMPARTMENT_GUI_TEXTURE);
	public static final ResourceLocation GUI_THERMAL_REGULATION_CHAMBER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.THERMAL_REGULATION_CHAMBER_GUI_TEXTURE);
	public static final ResourceLocation GUI_SELECT_PROJECT_ITEMS_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.SELECT_PROJECT_ITEMS_GUI_TEXTURE);
	public static final ResourceLocation GUI_ELECTROLYSER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.ELECTROLYSER_GUI_TEXTURE);
	public static final ResourceLocation GUI_FORESTER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.FORESTER_GUI_TEXTURE);
	public static final ResourceLocation MODEL_ANALYZER_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.ANALYZER_TEXTURE);
	public static final ResourceLocation MODEL_TEST_TUBE_RACK_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.TEST_TUBE_RACK_TEXTURE);
	public static final ResourceLocation MODEL_RESEARCH_PANEL_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.RESEARCH_PANEL_TEXTURE);
	public static final ResourceLocation MODEL_MATERIAL_COMPARTMENT_RESOURCE = new ResourceLocation(ConfigLoader.id, StringLib.MATERIAL_COMPARTMENT_TEXTURE);
}
