package com.infernio.Craftmistry.core.lib;

import net.minecraftforge.fluids.FluidContainerRegistry;

public class IntLib {

	public static int TEST_TUBE_RACK_MODEL_ID;
	public static int ANALYZER_MODEL_ID;
	public static int RESEARCH_MODEL_ID;
	public static final int TEST_TUBE_VOLUME = FluidContainerRegistry.BUCKET_VOLUME / 10;
	public static final int GUI_TEST_TUBE_RACK_ID = 0;
	public static final int GUI_ANALYZER_ID = 1;
	public static final int GUI_INFUSER_ID = 2;
	public static final int GUI_AIR_FILTER_ID = 3;
	public static final int GUI_DESTABILIZER_ID = 4;
	public static final int GUI_COMPRESSOR_ID = 5;
	public static final int GUI_CLEANER_ID = 6;
	public static final int GUI_STABILIZER_ID = 7;
	public static final int GUI_MINERAL_EXTRACTOR_ID = 8;
	public static final int GUI_REACTOR_ID = 9;
	public static final int GUI_BREAKER_ID = 10;
	public static final int GUI_COLLECTOR_ID = 11;
	public static final int GUI_DETECTOR_ID = 12;
	public static final int GUI_PANEL_ID = 13;
	public static final int GUI_MATERIAL_COMPARTMENT_ID = 14;
	public static final int GUI_ENERGY_STORAGE_ID = 15;
	public static final int GUI_THERMAL_REGULATION_CHAMBER_ID = 16;
	public static final int GUI_PROJECT_CUSTOMIZATION_SCREEN_ID = 17;
	public static final int GUI_SELECT_PROJECT_ITEMS_ID = 18;
	public static final int GUI_ELECTROLYSER_ID = 19;
	public static final int GUI_FORESTER_ID = 20;
	public static final byte PACKET_AIR_FILTER_MODE_SELECTION = 0;
	public static final byte PACKET_DESTABILIZER_AMPLIFIER = 1;
	public static final byte PACKET_STABILIZER_AMPLFIER = 2;
	public static final byte PACKET_DETECTOR_CHANGE_SETTINGS = 3;
	public static final byte PACKET_DETECTOR_CHANGE_CONFIG = 4;
	public static final byte PACKET_THERMAL_RC_CHANGE_SLIDER = 5;
	public static final byte PACKET_UPDATE_CLIENT_RESEARCH_PANEL = 6;
	public static final byte PACKET_OPEN_RESEARCH_MB_GUI = 7;
	public static final byte PACKET_FORESTER_BONE_MEAL = 8;
}
