package com.infernio.Craftmistry.core.handler;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;

import com.infernio.Craftmistry.common.fluid.BlockMineralWater;
import com.infernio.Craftmistry.common.fluid.BlockSulfuricAcid;
import com.infernio.Craftmistry.common.fluid.Fluids;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.core.config.ConfigSettings;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class CMEventHandler {

	@SubscribeEvent
	public void handleEntityDrop(LivingDropsEvent event)
	{
		EntityLivingBase entity = event.entityLiving;
		if(ConfigSettings.ADJUST_CREEPER_DROPS && entity instanceof EntityCreeper)
		{
			ArrayList<EntityItem> drops = event.drops;
			int amount = entity.worldObj.rand.nextInt(3 + event.lootingLevel);
			if(amount == 0)
			{
				return;
			}
			drops.add(new EntityItem(entity.worldObj, entity.posX, entity.posY, entity.posZ, new ItemStack(ModItems.multiItem, amount, 7)));
			event.setResult(Result.ALLOW);
		}
	}

	@SubscribeEvent
	public void onBucketFill(FillBucketEvent event)
	{
		MovingObjectPosition pos = event.target;
		World world = event.world;
		Block block = world.getBlock(pos.blockX, pos.blockY, pos.blockZ);
		if(block == Fluids.sulfuricAcidBlock)
		{
			BlockSulfuricAcid acid = (BlockSulfuricAcid)block;
			if(!acid.isSourceBlock(world, pos.blockX, pos.blockY, pos.blockZ))
			{
				event.setCanceled(true);
				return;
			}
			world.setBlockToAir(pos.blockX, pos.blockY, pos.blockZ);
			event.result = new ItemStack(ModItems.buckets, 1, 0);
			event.setResult(Result.ALLOW);
		}
		else if(block == Fluids.mineralWaterBlock)
		{
			BlockMineralWater water = (BlockMineralWater)block;
			if(!water.isSourceBlock(world, pos.blockX, pos.blockY, pos.blockZ))
			{
				event.setCanceled(true);
				return;
			}
			world.setBlockToAir(pos.blockX, pos.blockY, pos.blockZ);
			event.result = new ItemStack(ModItems.buckets, 1, 1);
			event.setResult(Result.ALLOW);
		}
	}

}
