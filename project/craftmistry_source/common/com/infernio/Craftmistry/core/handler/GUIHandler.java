package com.infernio.Craftmistry.core.handler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.Craftmistry.client.gui.*;
import com.infernio.Craftmistry.common.container.*;
import com.infernio.Craftmistry.common.tile.TileMaterialCompartment;
import com.infernio.Craftmistry.common.tile.TileResearchPanel;
import com.infernio.Craftmistry.core.lib.IntLib;

import cpw.mods.fml.common.network.IGuiHandler;

public class GUIHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile == null)
		{
			return null;
		}
		switch(id)
		{
		case IntLib.GUI_TEST_TUBE_RACK_ID: return new ContainerTestTubeRack(player.inventory, (IInventory)tile);
		case IntLib.GUI_ANALYZER_ID: return new ContainerAnalyzer(player.inventory, (IInventory)tile);
		case IntLib.GUI_INFUSER_ID: return new ContainerInfuser(player.inventory, (IInventory)tile);
		case IntLib.GUI_AIR_FILTER_ID: return new ContainerAirFilter(player.inventory, (IInventory)tile);
		case IntLib.GUI_DESTABILIZER_ID: return new ContainerDestabilizer(player.inventory, (IInventory)tile);
		case IntLib.GUI_COMPRESSOR_ID: return new ContainerCompressor(player.inventory, (IInventory)tile);
		case IntLib.GUI_CLEANER_ID: return new ContainerCleaner(player.inventory, (IInventory)tile);
		case IntLib.GUI_STABILIZER_ID: return new ContainerStabilizer(player.inventory, (IInventory)tile);
		case IntLib.GUI_MINERAL_EXTRACTOR_ID: return new ContainerMineralExtractor(player.inventory, (IInventory)tile);
		case IntLib.GUI_REACTOR_ID: return new ContainerReactor(player.inventory, (IInventory)tile);
		case IntLib.GUI_BREAKER_ID: return new ContainerBreaker(player.inventory, (IInventory)tile);
		case IntLib.GUI_COLLECTOR_ID: return new ContainerCollector(player.inventory, (IInventory)tile);
		case IntLib.GUI_DETECTOR_ID: return new ContainerDetector(player.inventory, (IInventory)tile);
		case IntLib.GUI_PANEL_ID: return new ContainerResearchPanel((TileResearchPanel)tile);
		case IntLib.GUI_SELECT_PROJECT_ITEMS_ID:
			TileResearchPanel panel = (TileResearchPanel)tile;
			if(panel.getNetwork() != null && panel.getNetwork().hasMaterialCompartment())
			{
				return new ContainerSelectProjectItems(panel.selectedItems, panel.getNetwork().getMaterialCompartment());
			}
			break;
		case IntLib.GUI_MATERIAL_COMPARTMENT_ID: return new ContainerMaterialCompartment(player.inventory, (IInventory)tile);
		case IntLib.GUI_THERMAL_REGULATION_CHAMBER_ID: return new ContainerThermalRC(player.inventory, (IInventory)tile);
		case IntLib.GUI_ELECTROLYSER_ID: return new ContainerElectrolyser(player.inventory, (IInventory)tile);
		case IntLib.GUI_FORESTER_ID: return new ContainerForester(player.inventory, (IInventory)tile);
		case IntLib.GUI_PROJECT_CUSTOMIZATION_SCREEN_ID: /* TODO Create Container */ break;
		default: break;
		}
		return null;
	}

	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile == null)
		{
			return null;
		}
		switch(id)
		{
		case IntLib.GUI_TEST_TUBE_RACK_ID: return new GUITestTubeRack(player.inventory, (IInventory)tile);
		case IntLib.GUI_ANALYZER_ID: return new GUIAnalyzer(player.inventory, (IInventory)tile);
		case IntLib.GUI_INFUSER_ID: return new GUIInfuser(player.inventory, (IInventory)tile);
		case IntLib.GUI_AIR_FILTER_ID: return new GUIAirFilter(player.inventory, (IInventory)tile);
		case IntLib.GUI_DESTABILIZER_ID: return new GUIDestabilizer(player.inventory, (IInventory)tile);
		case IntLib.GUI_COMPRESSOR_ID: return new GUICompressor(player.inventory, (IInventory)tile);
		case IntLib.GUI_CLEANER_ID: return new GUICleaner(player.inventory, (IInventory)tile);
		case IntLib.GUI_STABILIZER_ID: return new GUIStabilizer(player.inventory, (IInventory)tile);
		case IntLib.GUI_MINERAL_EXTRACTOR_ID: return new GUIMineralExtractor(player.inventory, (IInventory)tile);
		case IntLib.GUI_REACTOR_ID: return new GUIReactor(player.inventory, (IInventory)tile);
		case IntLib.GUI_BREAKER_ID: return new GUIBreaker(player.inventory, (IInventory)tile);
		case IntLib.GUI_COLLECTOR_ID: return new GUICollector(player.inventory, (IInventory)tile);
		case IntLib.GUI_DETECTOR_ID: return new GUIDetector(player.inventory, (IInventory)tile);
		case IntLib.GUI_PANEL_ID: return new GUIResearchPanel((TileResearchPanel)tile);
		case IntLib.GUI_SELECT_PROJECT_ITEMS_ID:
			TileResearchPanel panel = (TileResearchPanel)tile;
			TileEntity materialCompartment = world.getTileEntity(panel.materialCompartmentX, panel.materialCompartmentY, panel.materialCompartmentZ);
			if(materialCompartment instanceof TileMaterialCompartment)
			{
				return new GUISelectProjectItems(panel.selectedItems, (TileMaterialCompartment)materialCompartment);
			}
			break;
		case IntLib.GUI_MATERIAL_COMPARTMENT_ID: return new GUIMaterialCompartment(player.inventory, (IInventory)tile);
		case IntLib.GUI_THERMAL_REGULATION_CHAMBER_ID: return new GUIThermalRC(player.inventory, (IInventory)tile);
		case IntLib.GUI_ELECTROLYSER_ID: return new GUIElectrolyser(player.inventory, (IInventory)tile);
		case IntLib.GUI_FORESTER_ID: return new GUIForester(player.inventory, (IInventory)tile);
		case IntLib.GUI_PROJECT_CUSTOMIZATION_SCREEN_ID: /* TODO Create GUI */ break;
		default: break;
		}
		return null;
	}

}
