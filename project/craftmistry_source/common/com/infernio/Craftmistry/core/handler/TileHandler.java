package com.infernio.Craftmistry.core.handler;

import com.infernio.Craftmistry.api.tile.IDetectorMode;
import com.infernio.Craftmistry.common.detector.DetectorModes;
import com.infernio.Craftmistry.common.tile.TileBreaker;

public class TileHandler {

	public static void registerBreakerBlacklist(String blockID)
	{
		TileBreaker.blacklist.add(blockID);
	}

	public static void addDetectorMode(IDetectorMode mode)
	{
		DetectorModes.detections().registerMode(mode);
	}

}
