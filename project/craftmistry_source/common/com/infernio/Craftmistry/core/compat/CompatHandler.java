package com.infernio.Craftmistry.core.compat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;

import com.infernio.Craftmistry.common.block.ModBlocks;
import com.infernio.Craftmistry.common.item.ItemHighEnergyCreator;
import com.infernio.Craftmistry.common.item.ModItems;
import com.infernio.Craftmistry.common.recipe.InfuserRecipes;
import com.infernio.Craftmistry.common.tile.TileBreaker;
import com.infernio.Craftmistry.core.config.ConfigSettings;
import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.registry.GameRegistry;

public class CompatHandler {

	public static final boolean debug = ConfigSettings.DEBUG_MODE;
	public static final boolean ic2Present = Loader.isModLoaded("IC2");
	public static final boolean buildcraftPresent = Loader.isModLoaded("BuildCraft|Core");
	public static final boolean ironChestPresent = Loader.isModLoaded("IronChest");
	public static final boolean thaumCraftPresent = Loader.isModLoaded("Thaumcraft");
	public static final boolean neiPresent = Loader.isModLoaded("NotEnoughItems");
	public static final boolean ee3Present = Loader.isModLoaded("EE3");
	public static boolean sulfurAvailable = false;
	public static boolean copperAvailable = false;
	public static boolean tinAvailable = false;
	public static boolean silverAvailable = false;
	public static boolean aluminiumAvailable = false;
	public static boolean cobaltAvailable = false;
	public static boolean haliteAvailable = false;
	public static boolean berylAvailable = false;
	public static boolean fluoriteAvailable = false;
	private static Class<?> bcTransport;

	public static void init()
	{
		if(ConfigSettings.DISABLE_DOUBLE_WORLD_GEN)
		{
			ArrayList<ItemStack> ores = OreDictionary.getOres("oreSulfur");
			if(!ores.isEmpty())
			{
				sulfurAvailable = true;
			}
			ores = OreDictionary.getOres("oreCopper");
			if(!ores.isEmpty())
			{
				copperAvailable = true;
			}
			ores = OreDictionary.getOres("oreTin");
			if(!ores.isEmpty())
			{
				tinAvailable = true;
			}
			ores = OreDictionary.getOres("oreSilver");
			if(!ores.isEmpty())
			{
				silverAvailable = true;
			}
			ores = OreDictionary.getOres("oreAluminium");
			if(!ores.isEmpty())
			{
				aluminiumAvailable = true;
			}
			ores = OreDictionary.getOres("oreAluminum");
			if(!ores.isEmpty())
			{
				aluminiumAvailable = true;
			}
			ores = OreDictionary.getOres("oreCobalt");
			if(!ores.isEmpty())
			{
				cobaltAvailable = true;
			}
			ores = OreDictionary.getOres("oreHalite");
			if(!ores.isEmpty())
			{
				haliteAvailable = true;
			}
			ores = OreDictionary.getOres("oreSalt");
			if(!ores.isEmpty())
			{
				haliteAvailable = true;
			}
			ores = OreDictionary.getOres("oreBeryl");
			if(!ores.isEmpty())
			{
				berylAvailable = true;
			}
			ores = OreDictionary.getOres("oreFluorite");
			if(!ores.isEmpty())
			{
				fluoriteAvailable = true;
			}
		}
		if(buildcraftPresent)
		{
			try
			{
				bcTransport = Class.forName("buildcraft.BuildCraftTransport");
			}
			catch(ClassNotFoundException e)
			{
				printClassLoadingWarning(e, "BuildCraft");
			}
			catch(SecurityException e)
			{
				printSecurityWarning(e, "BuildCraft");
			}
		}
		OreDictionary.registerOre("ingotCopper", new ItemStack(ModItems.ingot, 1, 0));
		OreDictionary.registerOre("ingotTin", new ItemStack(ModItems.ingot, 1, 1));
		OreDictionary.registerOre("ingotSilver", new ItemStack(ModItems.ingot, 1, 2));
		OreDictionary.registerOre("ingotAluminium", new ItemStack(ModItems.ingot, 1, 3));
		OreDictionary.registerOre("ingotSteel", new ItemStack(ModItems.ingot, 1, 4));
		OreDictionary.registerOre("dustSulfur", new ItemStack(ModItems.rawMineral, 1, 0));
		OreDictionary.registerOre("naturalSulfur", new ItemStack(ModItems.multiItem, 1, 7));
		OreDictionary.registerOre("dustCoppper", new ItemStack(ModItems.rawMineral, 1, 1));
		OreDictionary.registerOre("dustTin", new ItemStack(ModItems.rawMineral, 1, 2));
		OreDictionary.registerOre("dustSilver", new ItemStack(ModItems.rawMineral, 1, 3));
		OreDictionary.registerOre("dustAluminium", new ItemStack(ModItems.rawMineral, 1, 4));
		OreDictionary.registerOre("naturalAluminum", new ItemStack(ModItems.rawMineral, 1, 4));
		OreDictionary.registerOre("dustCobalt", new ItemStack(ModItems.rawMineral, 1, 5));
		OreDictionary.registerOre("dustIron", new ItemStack(ModItems.rawMineral, 1, 6));
		OreDictionary.registerOre("dustCoal", new ItemStack(ModItems.rawMineral, 1, 7));
		OreDictionary.registerOre("dustGold", new ItemStack(ModItems.rawMineral, 1, 8));
		OreDictionary.registerOre("dustHalite", new ItemStack(ModItems.rawMineral, 1, 9));
		OreDictionary.registerOre("dustCompressedHalite", new ItemStack(ModItems.rawMineral, 1, 10));
		OreDictionary.registerOre("rawSalt", new ItemStack(ModItems.multiItem, 1, 5));
		OreDictionary.registerOre("refinedSalt", new ItemStack(ModItems.multiItem, 1, 6));
		OreDictionary.registerOre("oreSulfur", new ItemStack(ModBlocks.craftmistryOre, 1, 0));
		OreDictionary.registerOre("oreCopper", new ItemStack(ModBlocks.craftmistryOre, 1, 1));
		OreDictionary.registerOre("oreTin", new ItemStack(ModBlocks.craftmistryOre, 1, 2));
		OreDictionary.registerOre("oreSilver", new ItemStack(ModBlocks.craftmistryOre, 1, 3));
		OreDictionary.registerOre("oreAluminium", new ItemStack(ModBlocks.craftmistryOre, 1, 4));
		OreDictionary.registerOre("oreAluminum", new ItemStack(ModBlocks.craftmistryOre, 1, 4));
		OreDictionary.registerOre("oreCobalt", new ItemStack(ModBlocks.craftmistryOre, 1, 5));
		OreDictionary.registerOre("oreHalite", new ItemStack(ModBlocks.craftmistryOre, 1, 6));
		OreDictionary.registerOre("oreBeryl", new ItemStack(ModBlocks.craftmistryOre, 1, 7));
		OreDictionary.registerOre("oreFluorite", new ItemStack(ModBlocks.craftmistryOre, 1, 8));
	}

	public static void initBreakerBlacklist()
	{
		TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(ModBlocks.breaker));
		TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(ModBlocks.collector));
		TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(Blocks.bedrock));
		TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(Blocks.lever));
		TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(Blocks.chest));
		for(String id : ConfigSettings.BREAKER_BLACKLIST)
		{
			TileBreaker.blacklist.add(id);
		}
		if(ic2Present)
    	{
    		TileBreaker.blacklist.add(Item.itemRegistry.getNameForObject(ic2.api.item.Items.getItem("copperCableBlock").getItem()));
    	}
		if(buildcraftPresent)
		{
			try
			{
				Block pipe = (Block)bcTransport.getDeclaredField("genericPipeBlock").get(null);
				if(pipe != null)
				{
					TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(pipe));
				}
			}
			catch(NoSuchFieldException e)
			{
				printFieldLoadingWarning(e, "BuildCraft");
			}
			catch(IllegalArgumentException e)
			{
				printFieldLoadingWarning(e, "BuildCraft");
			}
			catch(IllegalAccessException e)
			{
				printSecurityWarning(e, "BuildCraft");
			}
			catch(SecurityException e)
			{
				printSecurityWarning(e, "BuildCraft");
			}
		}
		if(ironChestPresent)
		{
			try
			{
				Class<?> ironChest = Class.forName("cpw.mods.ironchest.IronChest");
				Block chest = (Block)ironChest.getDeclaredField("ironChestBlock").get(null);
				if(chest != null)
				{
					TileBreaker.blacklist.add(Block.blockRegistry.getNameForObject(chest));
				}
			}
			catch(ClassNotFoundException e)
			{
				printClassLoadingWarning(e, "IronChest");
			}
			catch(NoSuchFieldException e)
			{
				printFieldLoadingWarning(e, "IronChest");
			}
			catch(IllegalArgumentException e)
			{
				printFieldLoadingWarning(e, "IronChest");
			}
			catch(IllegalAccessException e)
			{
				printSecurityWarning(e, "IronChest");
			}
			catch(SecurityException  e)
			{
				printSecurityWarning(e, "IronChest");
			}
		}
	}

	public static void initOreDrops()
	{
		if(ConfigSettings.AUTO_ASSIGN_SCOOPULA_DROPS)
		{
			CMLogger.info("Testing Craftmistry Installation... Step 3 / 4 (Scoopula drops)");
			ArrayList<ItemStack> ores = OreDictionary.getOres("oreSulfur");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 0), new ItemStack(ModItems.rawMineral, 1, 0), new ItemStack(ModItems.rawMineral, 1, 0)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreCopper");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 1), new ItemStack(ModItems.rawMineral, 1, 1), new ItemStack(ModItems.rawMineral, 1, 1)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreTin");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 2), new ItemStack(ModItems.rawMineral, 1, 2), new ItemStack(ModItems.rawMineral, 1, 2)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreSilver");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 3), new ItemStack(ModItems.rawMineral, 1, 3), new ItemStack(ModItems.rawMineral, 1, 3)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreAluminium");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 4), new ItemStack(ModItems.rawMineral, 1, 4), new ItemStack(ModItems.rawMineral, 1, 4)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreAluminum");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 4), new ItemStack(ModItems.rawMineral, 1, 4), new ItemStack(ModItems.rawMineral, 1, 4)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreCobalt");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 5), new ItemStack(ModItems.rawMineral, 1, 5), new ItemStack(ModItems.rawMineral, 1, 5)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreHalite");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.multiItem, 1, 5)
					}, new int[]{
							100, 80, 20, 80
					});
				}
			}
			ores = OreDictionary.getOres("oreSalt");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.rawMineral, 1, 9), new ItemStack(ModItems.multiItem, 1, 5)
					}, new int[]{
							100, 80, 20, 80
					});
				}
			}
			ores = OreDictionary.getOres("oreBeryl");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 11), new ItemStack(ModItems.rawMineral, 1, 11), new ItemStack(ModItems.rawMineral, 1, 11)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreFluorite");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 13), new ItemStack(ModItems.rawMineral, 1, 13), new ItemStack(ModItems.rawMineral, 1, 13)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreIron");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 6), new ItemStack(ModItems.rawMineral, 1, 6), new ItemStack(ModItems.rawMineral, 1, 6)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreCoal");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 7), new ItemStack(ModItems.rawMineral, 1, 7), new ItemStack(ModItems.rawMineral, 1, 7)
					}, new int[]{
						100, 80, 20
					});
				}
			}
			ores = OreDictionary.getOres("oreGold");
			for(ItemStack ore : ores)
			{
				if(ore != null && ore.getItem() != null)
				{
					ItemHelper.addBlockBreak(Block.getBlockFromItem(ore.getItem()), ore.getItemDamage(), Blocks.stone, 0, new ItemStack[]{
						new ItemStack(ModItems.rawMineral, 1, 8), new ItemStack(ModItems.rawMineral, 1, 8), new ItemStack(ModItems.rawMineral, 1, 8)
					}, new int[]{
						100, 80, 20
					});
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public static void removeDustSmelting()
	{
		Map<ItemStack, ItemStack> recipes = FurnaceRecipes.smelting().getSmeltingList();
		Iterator<ItemStack> ite2 = recipes.keySet().iterator();
		ItemStack input;
		while(ite2.hasNext())
		{
			input = ite2.next();
			if(input != null && input.getItem() != null && input.getItem() == ModItems.rawMineral)
			{
				ite2.remove();
			}
		}
	}

	public static void addRecipes()
	{
		if(CompatHandler.ic2Present)
		{
			ItemStack hecFull = new ItemStack(ModItems.highEnergyCreator, 1, 1);
			hecFull.setTagCompound(new NBTTagCompound());
			hecFull.stackTagCompound.setInteger("charge", ItemHighEnergyCreator.MAX_CHARGE);
			ItemStack fullCrystal = ic2.api.item.Items.getItem("lapotronCrystal").copy();
			fullCrystal.setItemDamage(1);
			fullCrystal.setTagCompound(new NBTTagCompound());
			fullCrystal.stackTagCompound.setInteger("charge", 1000000);
			GameRegistry.addRecipe(new ShapedOreRecipe(hecFull, new Object[]{
				"TAT","ACA","ILI",'L',fullCrystal,'C',Blocks.crafting_table,'I',Items.iron_ingot,'T',"ingotTin",'A',"ingotAluminium"
			}));
			ItemStack hecEmpty = new ItemStack(ModItems.highEnergyCreator, 1, 26);
			hecEmpty.setTagCompound(new NBTTagCompound());
			hecEmpty.stackTagCompound.setInteger("charge", 0);
			ItemStack emptyCrystal = ic2.api.item.Items.getItem("lapotronCrystal").copy();
			emptyCrystal.setItemDamage(26);
			emptyCrystal.setTagCompound(new NBTTagCompound());
			emptyCrystal.stackTagCompound.setInteger("charge", 0);
			GameRegistry.addRecipe(new ShapedOreRecipe(hecEmpty, new Object[]{
				"TAT","ACA","ILI",'L',emptyCrystal,'C',Blocks.crafting_table,'I',Items.iron_ingot,'T',"ingotTin",'A',"ingotAluminium"
			}));
		}
		if(ConfigSettings.COMPAT_RECIPES)
		{
			if(ic2Present)
			{
				ItemStack dust = ic2.api.item.Items.getItem("sulfurDust");
				if(dust != null)
				{
					InfuserRecipes.infusing().addRecipe(dust.getItem(), dust.getItemDamage(), new ItemStack(ModItems.testTubeSolid, 1, 16));
				}
			}
		}
	}

	public static void sendRecipeInformation()
	{
		//FIXME Pahi's code is not actually responding to IMC messages. Wait for him to re-implement this feature.
//		for(Entry<Item, HashMap<Integer, ItemStack>> entry : InfuserRecipes.infusing().getRecipeList().entrySet())
//		{
//			
//			input.add(new ItemStack(ModItems.testTubeSolid, 1, 0));
//			RecipeMapping recipe = new RecipeMapping(entry.getValue(), input);
//			FMLInterModComms.sendMessage("EE3", StringLib.EE3_IMC_ADD_RECIPE, recipe.toJson());
//		}
//		for(Entry<List<Integer>, ItemStack> entry : CompressorRecipes.compressing().getRecipeList().entrySet())
//		{
//			ArrayList<ItemStack> input = ItemUtils.constructNoStackSize(entry.getKey());
//			for(ItemStack stack : input)
//			{
//				stack.stackSize = CompressorRecipes.compressing().getStackSizeToRemove(stack);
//			}
//			RecipeMapping recipe = new RecipeMapping(entry.getValue(), input);
//			FMLInterModComms.sendMessage("EE3", StringLib.EE3_IMC_ADD_RECIPE, recipe.toJson());
//		}
//		List<ItemStack> emptyTestTube = Arrays.asList(new ItemStack(ModItems.testTubeSolid, 1, 0));
//		for(Entry<String, ItemStack> entry : AirFilterRecipes.recipes().getRecipeList().entrySet())
//		{
//			RecipeMapping recipe = new RecipeMapping(entry.getValue(), emptyTestTube);
//			FMLInterModComms.sendMessage("EE3", StringLib.EE3_IMC_ADD_RECIPE, recipe.toJson());
//		}
//		for(Entry<List<Integer>, ItemStack> entry : ReactorRecipes.reactions().getRecipeList().entrySet())
//		{
//			RecipeMapping recipe = new RecipeMapping(entry.getValue(), ItemUtils.construct(entry.getKey()));
//			FMLInterModComms.sendMessage("EE3", StringLib.EE3_IMC_ADD_RECIPE, recipe.toJson());
//		}
	}

	private static void printSecurityWarning(Throwable e, String modid)
	{
		CMLogger.warning("Craftmistry found a " + modid + " installation but was denied access to " + modid + " classes / fields / methods!");
		CMLogger.warning("This Craftmistry version (" + StringLib.VERSION + ") is probably incompatible with the version of " + modid + " that you have currently installed!");
		CMLogger.warning("Please check for a newer Craftmistry and / or a newer " + modid + " version.");
		if(debug)
		{
			e.printStackTrace();
		}
		else
		{
			CMLogger.warning("If you would like to report this, set 'craftmistry.general.debug' in the Craftmistry config to true and restart Minecraft.");
			CMLogger.warning("Then, report this error with the complete ForgeModLoader-client-0.log.");
		}
	}

	private static void printClassLoadingWarning(Throwable e, String modid)
	{
		CMLogger.warning("Found " + modid + "installation but failed to load " + modid + " classes!");
		CMLogger.warning("This Craftmistry version (" + StringLib.VERSION + ") is probably incompatible with the version of " + modid + " that you have currently installed!");
		CMLogger.warning("Please check for a newer Craftmistry and / or a newer " + modid + " version.");
		if(debug)
		{
			e.printStackTrace();
		}
		else
		{
			CMLogger.warning("If you would like to report this, set 'craftmistry.general.debug' in the Craftmistry config to true and restart Minecraft.");
			CMLogger.warning("Then, report this error with the complete ForgeModLoader-client-0.log.");
		}
	}

	private static void printFieldLoadingWarning(Throwable e, String modid)
	{
		CMLogger.warning("Found " + modid + "installation but failed to load " + modid + " fields!");
		CMLogger.warning("This Craftmistry version (" + StringLib.VERSION + ") is probably incompatible with the version of " + modid + " that you have currently installed!");
		CMLogger.warning("Please check for a newer Craftmistry and / or a newer " + modid + " version.");
		if(debug)
		{
			e.printStackTrace();
		}
		else
		{
			CMLogger.warning("If you would like to report this, set 'craftmistry.general.debug' in the Craftmistry config to true and restart Minecraft.");
			CMLogger.warning("Then, report this error with the complete ForgeModLoader-client-0.log.");
		}
	}

//	private static void printMethodLoadingWarning(Throwable e, String modid)
//	{
//		CMLogger.warning("Found " + modid + "installation but failed to load " + modid + " methods!");
//		CMLogger.warning("This Craftmistry version (" + StringLib.VERSION + ") is probably incompatible with the version of " + modid + " that you have currently installed!");
//		CMLogger.warning("Please check for a newer Craftmistry and / or a newer " + modid + " version.");
//		if(debug)
//		{
//			e.printStackTrace();
//		}
//		else
//		{
//			CMLogger.warning("If you would like to report this, set 'craftmistry.general.debug' in the Craftmistry config to true and restart Minecraft.");
//			CMLogger.warning("Then, report this error with the complete ForgeModLoader-client-0.log.");
//		}
//	}

}
