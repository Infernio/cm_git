package com.infernio.Craftmistry.core.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.infernio.Craftmistry.core.lib.StringLib;

public class CMLogger {
	
	private Logger cmLogger = LogManager.getLogger(StringLib.MODID);
	
	private static CMLogger instance = new CMLogger();
	
	public static void log(Level level, String msg)
	{
		instance.doLog(level, msg);
	}
	
	private void doLog(Level level, String msg)
	{
		cmLogger.log(level, msg);
	}
	
	public static void warning(String msg)
	{
		instance.doLog(Level.WARN, msg);
	}

	public static void info(String msg)
	{
		instance.doLog(Level.INFO, msg);
	}

	public static void debug(String msg)
	{
		instance.doLog(Level.DEBUG, msg);
	}

	public static void severe(String msg)
	{
		instance.doLog(Level.FATAL, msg);
	}
}
