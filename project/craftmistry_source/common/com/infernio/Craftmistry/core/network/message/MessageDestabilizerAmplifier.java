package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.tile.TileDestabilizer;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageDestabilizerAmplifier implements IMessage, IMessageHandler<MessageDestabilizerAmplifier, IMessage> {

	private int x;
	private int y;
	private int z;
	private int amplifier;

	public MessageDestabilizerAmplifier() {}

	public MessageDestabilizerAmplifier(int x, int y, int z, int amplifier)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.amplifier = amplifier;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.amplifier = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.amplifier);
    }

    @Override
    public IMessage onMessage(MessageDestabilizerAmplifier message, MessageContext ctx)
    {
        TileEntity tile = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
        if(tile instanceof TileDestabilizer)
        {
        	((TileDestabilizer)tile).amplifier = message.amplifier;
        }
        else
        {
        	CMLogger.warning("Attempted to change the amplifier of a Destabilizer, but there was no Destabilizer at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
