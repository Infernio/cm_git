package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.tile.TileAirFilter;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageAirFilterMode implements IMessage, IMessageHandler<MessageAirFilterMode, IMessage> {

	private int x;
	private int y;
	private int z;
	private int mode;

	public MessageAirFilterMode() {}

	public MessageAirFilterMode(int x, int y, int z, int mode)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.mode = mode;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.mode = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.mode);
    }

    @Override
    public IMessage onMessage(MessageAirFilterMode message, MessageContext ctx)
    {
        TileEntity tile = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
        if(tile instanceof TileAirFilter)
        {
        	((TileAirFilter)tile).mode = message.mode;
        	((TileAirFilter)tile).progress = 0;
        }
        else
        {
        	CMLogger.warning("Attempted to change the mode of an Air Filter, but there was no Air Filter at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
