package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.tile.TileDetector;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageDetectorSettings implements IMessage, IMessageHandler<MessageDetectorSettings, IMessage> {

	private int x;
	private int y;
	private int z;
	private int setting;

	public MessageDetectorSettings() {}

	public MessageDetectorSettings(int x, int y, int z, int setting)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.setting = setting;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.setting = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.setting);
    }

    @Override
    public IMessage onMessage(MessageDetectorSettings message, MessageContext ctx)
    {
        TileEntity tile = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
        if(tile instanceof TileDetector)
        {
        	TileDetector tile2 = (TileDetector)tile;
			if(setting >= TileDetector.SETTINGS)
			{
				CMLogger.warning("Invalid or corrupt message: '" + this.getClass().getSimpleName() + "' with addition " + setting + ".");
				CMLogger.warning("Addition size is bigger than allowed size (" + TileDetector.SETTINGS + "). Message ignored.");
				return null;
			}
			if(setting < 0)
			{
				CMLogger.warning("Invalid or corrupt message: '" + this.getClass().getSimpleName() + "' with addition " + setting + ".");
				CMLogger.warning("Addition smaller than 0. Message ignored.");
				return null;
			}
			tile2.setting = message.setting;
        }
        else
        {
        	CMLogger.warning("Attempted to change the setting of a Detector, but there was no Detector at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
