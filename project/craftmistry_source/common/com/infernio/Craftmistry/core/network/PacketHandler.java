package com.infernio.Craftmistry.core.network;

import net.minecraft.entity.player.EntityPlayerMP;

import com.infernio.Craftmistry.core.lib.IntLib;
import com.infernio.Craftmistry.core.lib.StringLib;
import com.infernio.Craftmistry.core.network.message.MessageAirFilterMode;
import com.infernio.Craftmistry.core.network.message.MessageDestabilizerAmplifier;
import com.infernio.Craftmistry.core.network.message.MessageDetectorConfig;
import com.infernio.Craftmistry.core.network.message.MessageDetectorSettings;
import com.infernio.Craftmistry.core.network.message.MessageForesterBoneMeal;
import com.infernio.Craftmistry.core.network.message.MessageOpenResearchGui;
import com.infernio.Craftmistry.core.network.message.MessageStabilizerAmplifier;
import com.infernio.Craftmistry.core.network.message.MessageThermalRCSlider;
import com.infernio.Craftmistry.core.network.message.MessageUpdateClientResearch;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

//Thanks to Pahimar for this setup!
public class PacketHandler {

	private static final SimpleNetworkWrapper wrapper = NetworkRegistry.INSTANCE.newSimpleChannel(StringLib.CHANNEL);

	public static void init()
    {
        wrapper.registerMessage(MessageAirFilterMode.class, MessageAirFilterMode.class, IntLib.PACKET_AIR_FILTER_MODE_SELECTION, Side.SERVER);
        wrapper.registerMessage(MessageDestabilizerAmplifier.class, MessageDestabilizerAmplifier.class, IntLib.PACKET_DESTABILIZER_AMPLIFIER, Side.SERVER);
        wrapper.registerMessage(MessageStabilizerAmplifier.class, MessageStabilizerAmplifier.class, IntLib.PACKET_STABILIZER_AMPLFIER, Side.SERVER);
        wrapper.registerMessage(MessageDetectorSettings.class, MessageDetectorSettings.class, IntLib.PACKET_DETECTOR_CHANGE_SETTINGS, Side.SERVER);
        wrapper.registerMessage(MessageDetectorConfig.class, MessageDetectorConfig.class, IntLib.PACKET_DETECTOR_CHANGE_CONFIG, Side.SERVER);
        wrapper.registerMessage(MessageThermalRCSlider.class, MessageThermalRCSlider.class, IntLib.PACKET_THERMAL_RC_CHANGE_SLIDER, Side.SERVER);
        wrapper.registerMessage(MessageUpdateClientResearch.class, MessageUpdateClientResearch.class, IntLib.PACKET_UPDATE_CLIENT_RESEARCH_PANEL, Side.CLIENT);
        wrapper.registerMessage(MessageOpenResearchGui.class, MessageOpenResearchGui.class, IntLib.PACKET_OPEN_RESEARCH_MB_GUI, Side.SERVER);
        wrapper.registerMessage(MessageForesterBoneMeal.class, MessageForesterBoneMeal.class, IntLib.PACKET_FORESTER_BONE_MEAL, Side.CLIENT);
    }

	public static void sendTo(IMessage message, EntityPlayerMP player)
	{
		wrapper.sendTo(message, player);
	}

	public static void sendToAll(IMessage message)
	{
		wrapper.sendToAll(message);
	}

	public static void sendToAllAround(IMessage message, int dimension, int x, int y, int z, int range)
	{
		sendToAllAround(message, new TargetPoint(dimension, x, y, z, range));
	}

	public static void sendToAllAround(IMessage message, TargetPoint point)
	{
		wrapper.sendToAllAround(message, point);
	}

	public static void sendToDimension(IMessage message, int dimension)
	{
		wrapper.sendToDimension(message, dimension);
	}

	public static void sendToServer(IMessage message)
	{
		wrapper.sendToServer(message);
	}
}
