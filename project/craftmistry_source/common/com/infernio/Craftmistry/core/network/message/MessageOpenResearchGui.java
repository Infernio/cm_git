package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.Craftmistry.common.research.ResearchNetwork;
import com.infernio.Craftmistry.common.tile.IResearchTile;
import com.infernio.Craftmistry.core.logging.CMLogger;
import com.infernio.InfernoCore.gui.GUIRegistry;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageOpenResearchGui implements IMessage, IMessageHandler<MessageOpenResearchGui, IMessage> {

	private int x;
	private int y;
	private int z;
	private short guiID;
	private short modID;
	private String type;

	public MessageOpenResearchGui() {}

	public MessageOpenResearchGui(int x, int y, int z, short guiID, short modID, String type)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.guiID = guiID;
		this.modID = modID;
		this.type = type;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.guiID = buf.readShort();
        this.modID = buf.readShort();
        int length = buf.readInt();
        StringBuilder type = new StringBuilder();
        for(int i = 0; i < length; ++i)
        {
        	type.append(buf.readChar());
        }
        this.type = type.toString();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeShort(this.guiID);
        buf.writeShort(this.modID);
        char[] chars = this.type.toCharArray();
        buf.writeInt(chars.length);
        for(char c : chars)
        {
        	buf.writeChar(c);
        }
    }

    @Override
    public IMessage onMessage(MessageOpenResearchGui message, MessageContext ctx)
    {
    	EntityPlayerMP player = ctx.getServerHandler().playerEntity;
    	World world = player.worldObj;
        TileEntity tile = world.getTileEntity(message.x, message.y, message.z);
        if(tile instanceof IResearchTile)
		{
			ResearchNetwork network = ((IResearchTile)tile).getNetwork();
			if(network != null && network.has(message.type))
			{
				TileEntity part = (TileEntity)network.get(message.type);
				world.playSoundAtEntity(player, "random.click", 0.5F, 1.0F);
				player.openGui(GUIRegistry.getMod(message.modID), message.guiID, world, part.xCoord, part.yCoord, part.zCoord);
			}
		}
        else
        {
        	CMLogger.warning("Attempted to change the mode of an Air Filter, but there was no Air Filter at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
