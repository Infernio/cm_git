package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.tile.TileResearchPanel;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageUpdateClientResearch implements IMessage, IMessageHandler<MessageUpdateClientResearch, IMessage> {

	private int x;
	private int y;
	private int z;
	private int[] addition;

	public MessageUpdateClientResearch() {}

	public MessageUpdateClientResearch(int x, int y, int z, int[] addition)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.addition = addition;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        int length = buf.readInt();
        this.addition = new int[length];
        for(int i = 0; i < length; ++i)
        {
        	this.addition[i] = buf.readInt();
        }
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.addition.length);
        for(int addition : this.addition)
        {
        	buf.writeInt(addition);
        }
    }

    @Override
    public IMessage onMessage(MessageUpdateClientResearch message, MessageContext ctx)
    {
        TileEntity tile = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
		if(tile instanceof TileResearchPanel)
		{
			TileResearchPanel panel = (TileResearchPanel)tile;
			panel.materialCompartmentX = message.addition[0];
			panel.materialCompartmentY = message.addition[1];
			panel.materialCompartmentZ = message.addition[2];
		}
        else
        {
        	CMLogger.warning("Attempted to change the mode of an Air Filter, but there was no Air Filter at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
