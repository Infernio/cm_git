package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.item.ItemDye;
import net.minecraft.world.World;

import com.infernio.InfernoCore.InfernoCore;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageForesterBoneMeal implements IMessage, IMessageHandler<MessageForesterBoneMeal, IMessage>
{
	private int x;
	private int y;
	private int z;

	public MessageForesterBoneMeal() {}

	public MessageForesterBoneMeal(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
    }

    @Override
    public IMessage onMessage(MessageForesterBoneMeal message, MessageContext ctx)
    {
    	World[] worlds = InfernoCore.proxy.getCurrentWorlds();
    	for(World world : worlds)
    	{
    		ItemDye.func_150918_a(world, message.x, message.y, message.z, 0);
    	}
    	return null;
    }
}
