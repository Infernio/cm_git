package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.tile.TileDetector;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageDetectorConfig implements IMessage, IMessageHandler<MessageDetectorConfig, IMessage> {

	private int x;
	private int y;
	private int z;
	private int config;

	public MessageDetectorConfig() {}

	public MessageDetectorConfig(int x, int y, int z, int config)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.config = config;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.config = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.config);
    }

    @Override
    public IMessage onMessage(MessageDetectorConfig message, MessageContext ctx)
    {
        TileEntity tile = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
        if(tile instanceof TileDetector)
        {
        	((TileDetector)tile).redstoneConfig = message.config;
        }
        else
        {
        	CMLogger.warning("Attempted to change the config of a Detector, but there was no Detector at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
