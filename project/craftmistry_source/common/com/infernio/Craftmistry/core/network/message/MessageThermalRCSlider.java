package com.infernio.Craftmistry.core.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;

import com.infernio.Craftmistry.common.tile.TileThermalRegulationChamber;
import com.infernio.Craftmistry.core.logging.CMLogger;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageThermalRCSlider implements IMessage, IMessageHandler<MessageThermalRCSlider, IMessage> {

	private int x;
	private int y;
	private int z;
	private int temperature;

	public MessageThermalRCSlider() {}

	public MessageThermalRCSlider(int x, int y, int z, int temperature)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.temperature = temperature;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.temperature = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.temperature);
    }

    @Override
    public IMessage onMessage(MessageThermalRCSlider message, MessageContext ctx)
    {
        TileEntity tile = ctx.getServerHandler().playerEntity.worldObj.getTileEntity(message.x, message.y, message.z);
        if(tile instanceof TileThermalRegulationChamber)
		{
			TileThermalRegulationChamber chamber = (TileThermalRegulationChamber)tile;
			if(chamber.master != null)
			{
				TileThermalRegulationChamber master = (TileThermalRegulationChamber)chamber.master;
				master.targetTemperature = message.temperature;
				master.hasTargetTemperature = true;
			}
		}
        else
        {
        	CMLogger.warning("Attempted to change the target temperature of a Thermal Regulation Chamber, but there was no Thermal Regulation Chamber at the sent coordinates!");
        	CMLogger.warning("Coordinates: x=" + message.x + ", y=" + message.y + ", z=" + message.z);
        }
        return null;
    }
}
