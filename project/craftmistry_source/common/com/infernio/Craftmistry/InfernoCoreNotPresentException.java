package com.infernio.Craftmistry;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiErrorScreen;

import com.infernio.Craftmistry.core.lib.StringLib;

import cpw.mods.fml.client.CustomModLoadingErrorDisplayException;

public class InfernoCoreNotPresentException extends CustomModLoadingErrorDisplayException {

	private static final long serialVersionUID = 1L;

	@Override
	public void initGui(GuiErrorScreen errorScreen, FontRenderer fontRenderer) {}

	@Override
	public void drawScreen(GuiErrorScreen errorScreen, FontRenderer fontRenderer, int mouseRelX, int mouseRelY, float tickTime)
	{
		errorScreen.drawCenteredString(fontRenderer, StringLib.INFERNO_CORE_NOT_LOADED_MESSAGE_1, errorScreen.width / 2, 90, 16777215);
		errorScreen.drawCenteredString(fontRenderer, StringLib.INFERNO_CORE_NOT_LOADED_MESSAGE_2, errorScreen.width / 2, 110, 16777215);
	}
}
