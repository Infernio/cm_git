package com.infernio.InfernoCore.world;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class WorldGenCustomOre {

    private Block minableBlock;
    private int minableBlockMeta = 0;

    private int numberOfBlocks;
    private Block targetBlock;

    public WorldGenCustomOre(Block block, int meta, int amount, Block target)
    {
    	this.minableBlock = block;
    	this.minableBlockMeta = meta;
    	this.numberOfBlocks = amount;
    	this.targetBlock = target;
    }

    public boolean generate(World world, Random random, int x, int y, int z)
    {
        float f = random.nextFloat() * (float)Math.PI;
        double x1 = (double)((float)(x + 8) + MathHelper.sin(f) * (float)this.numberOfBlocks / 8.0F);
        double x2 = (double)((float)(x + 8) - MathHelper.sin(f) * (float)this.numberOfBlocks / 8.0F);
        double z1 = (double)((float)(z + 8) + MathHelper.cos(f) * (float)this.numberOfBlocks / 8.0F);
        double z2 = (double)((float)(z + 8) - MathHelper.cos(f) * (float)this.numberOfBlocks / 8.0F);
        double y1 = (double)(y + random.nextInt(3) - 2);
        double y2 = (double)(y + random.nextInt(3) - 2);
        for(int i = 0; i <= this.numberOfBlocks; ++i)
        {
            double x3 = x1 + (x2 - x1) * (double)i / (double)this.numberOfBlocks;
            double y3 = y1 + (y2 - y1) * (double)i / (double)this.numberOfBlocks;
            double z3 = z1 + (z2 - z1) * (double)i / (double)this.numberOfBlocks;
            double generated = random.nextDouble() * (double)this.numberOfBlocks / 16.0D;
            double sin1 = (double)(MathHelper.sin((float)i * (float)Math.PI / (float)this.numberOfBlocks) + 1.0F) * generated + 1.0D;
            double sin2 = (double)(MathHelper.sin((float)i * (float)Math.PI / (float)this.numberOfBlocks) + 1.0F) * generated + 1.0D;
            int floorX1 = MathHelper.floor_double(x3 - sin1 / 2.0D);
            int floorY1 = MathHelper.floor_double(y3 - sin2 / 2.0D);
            int floorZ1 = MathHelper.floor_double(z3 - sin1 / 2.0D);
            int floorX2 = MathHelper.floor_double(x3 + sin1 / 2.0D);
            int floorY2 = MathHelper.floor_double(y3 + sin2 / 2.0D);
            int floorZ2 = MathHelper.floor_double(z3 + sin1 / 2.0D);
            for(int finalX = floorX1; finalX <= floorX2; ++finalX)
            {
                double xGen = ((double)finalX + 0.5D - x3) / (sin1 / 2.0D);
                if(xGen * xGen < 1.0D)
                {
                    for(int finalY = floorY1; finalY <= floorY2; ++finalY)
                    {
                        double yGen = ((double)finalY + 0.5D - y3) / (sin2 / 2.0D);
                        if(xGen * xGen + yGen * yGen < 1.0D)
                        {
                            for(int finalZ = floorZ1; finalZ <= floorZ2; ++finalZ)
                            {
                                double zGen = ((double)finalZ + 0.5D - z3) / (sin1 / 2.0D);
                                Block block = world.getBlock(finalX, finalY, finalZ);
                                if(xGen * xGen + yGen * yGen + zGen * zGen < 1.0D && (block != null && block.isReplaceableOreGen(world, finalX, finalY, finalZ, this.targetBlock)))
                                {
                                    world.setBlock(finalX, finalY, finalZ, this.minableBlock, this.minableBlockMeta, 2);
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
