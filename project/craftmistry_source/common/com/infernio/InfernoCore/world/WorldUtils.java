package com.infernio.InfernoCore.world;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.infernio.InfernoCore.multiblock.MultiblockPart;

public class WorldUtils {

	public static String getBlockKey(String blockName)
	{
		return blockName.startsWith("tile.") ? blockName + ".name" : "tile." + blockName + ".name";
	}

	public static Material getMaterial(IBlockAccess world, int x, int y, int z)
	{
		return world.getBlock(x, y, z).getMaterial();
	}

	public static boolean isAirBlock(IBlockAccess world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
		if(block != null)
		{
			return block.isAir(world, x, y, z);
		}
		return true;
	}

	public static boolean canOreGenReplace(World world, int x, int y, int z, Block target)
	{
		Block block = world.getBlock(x, y, z);
		if(block != null)
		{
			return block.isReplaceableOreGen(world, x, y, z, target);
		}
		return false;
	}

	public static boolean tileFits(IBlockAccess world, int x, int y, int z, MultiblockPart part)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart other = (MultiblockPart)tile;
			return part.getID().equals(other.getID());
		}
		return false;
	}

	public static void markNeighborsRenderUpdate(World world, int x, int y, int z)
	{
		world.markBlockForUpdate(x + 1, y, z);
		world.markBlockForUpdate(x - 1, y, z);
		world.markBlockForUpdate(x, y + 1, z);
		world.markBlockForUpdate(x, y - 1, z);
		world.markBlockForUpdate(x, y, z + 1);
		world.markBlockForUpdate(x, y, z - 1);
	}

}
