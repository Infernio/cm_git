package com.infernio.InfernoCore.client.particle;

import java.util.HashMap;

import net.minecraft.client.particle.*;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ParticleRegistry {

	private static ParticleRegistry instance = new ParticleRegistry();

	private HashMap<String, Class<? extends EntityFX>> particles = new HashMap<String, Class<? extends EntityFX>>();
	private HashMap<Class<? extends EntityFX>, String> particleClasses = new HashMap<Class<? extends EntityFX>, String>();
	private boolean initialized = false;

	private ParticleRegistry() {}

	public static Class<? extends EntityFX> getParticleClass(String particleName)
	{
		return instance.particles.get(particleName);
	}

	public static String getParticleName(Class<? extends EntityFX> particle)
	{
		return instance.particleClasses.get(particle);
	}

	public static void registerParticle(String particleName, Class<? extends EntityFX> particle)
	{
		if(!instance.particles.containsKey(particleName) && !instance.particleClasses.containsKey(particle))
		{
			instance.particles.put(particleName, particle);
			instance.particleClasses.put(particle, particleName);
		}
	}

	public static void loadVanillaParticles()
	{
		if(!instance.initialized)
		{
			instance.initialized = true;
			registerParticle("hugeexplosion", EntityHugeExplodeFX.class);
			registerParticle("largeexplode", EntityLargeExplodeFX.class);
			registerParticle("fireworksSpark", EntityFireworkSparkFX.class);
			registerParticle("bubble", EntityBubbleFX.class);
			registerParticle("suspended", EntitySuspendFX.class);
			registerParticle("depthsuspend", EntityAuraFX.class);
			registerParticle("townaura", EntityAuraFX.class);
			registerParticle("crit", EntityCritFX.class);
			registerParticle("magicCrit", EntityCritFX.class);
			registerParticle("smoke", EntitySmokeFX.class);
			registerParticle("mobSpell", EntitySpellParticleFX.class);
			registerParticle("mobSpellAmbient", EntitySpellParticleFX.class);
			registerParticle("spell", EntitySpellParticleFX.class);
			registerParticle("instantSpell", EntitySpellParticleFX.class);
			registerParticle("witchMagic", EntitySpellParticleFX.class);
			registerParticle("note", EntityNoteFX.class);
			registerParticle("portal", EntityPortalFX.class);
			registerParticle("enchantmenttable", EntityEnchantmentTableParticleFX.class);
			registerParticle("explode", EntityExplodeFX.class);
			registerParticle("flame", EntityFlameFX.class);
			registerParticle("footstep", EntityFootStepFX.class);
			registerParticle("splash", EntitySplashFX.class);
			registerParticle("largesmoke", EntitySmokeFX.class);
			registerParticle("cloud", EntityCloudFX.class);
			registerParticle("reddust", EntityReddustFX.class);
			registerParticle("snowballpoof", EntityBreakingFX.class);
			registerParticle("dripWater", EntityDropParticleFX.class);
			registerParticle("dripLava", EntityDropParticleFX.class);
			registerParticle("snowshovel", EntitySnowShovelFX.class);
			registerParticle("slime", EntityBreakingFX.class);
			registerParticle("heart", EntityHeartFX.class);
			registerParticle("angryVillager", EntityHeartFX.class);
			registerParticle("happyVillager", EntityHeartFX.class);
		}
	}

}
