package com.infernio.InfernoCore.client.render;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderUtils {

	private static RenderItem itemRenderer = new RenderItem();

	public static void renderItem(ItemStack item, World world, double x, double y, double z, boolean spinning, int speed)
	{
		if(item != null && item.getItem() != null)
        {
            float rotation = 0.0F;
            if(spinning)
            {
            	rotation = (float) (720.0F * (double)(System.currentTimeMillis() & 0x3FFFL) / (double)0x3FFFL * speed);
            }
            EntityItem entity = new EntityItem(world);
            entity.hoverStart = 0.0F;
            entity.setEntityItemStack(item);
            if(entity.getEntityItem() != null && entity.getEntityItem().getItem() != null)
            {
	            if(Block.getBlockFromItem(item.getItem()) != Blocks.air)
	            {
	                GL11.glTranslatef((float)x + 0.5F, (float)y + 0.25F, (float)z + 0.5F);
	            }
	            else
	            {
	                GL11.glTranslatef((float)x + 0.5F, (float)y + 0.125F, (float)z + 0.5F);
	            }
	            if(Block.getBlockFromItem(item.getItem()) != Blocks.air)
	            {
		            GL11.glScalef(0.9F, 0.9F, 0.9F);   
	            }
	            else
	            {
	            	GL11.glScalef(0.6F, 0.6F, 0.6F);
	            }
	            if(spinning)
	            {
	            	GL11.glRotatef(rotation, 0.0F, 1.0F, 0.0F);
	            }
	            itemRenderer.doRender(entity, 0, 0, 0, 0, 0);
            }
        }
	}

	public static void setupItemRenderer(RenderItem renderer)
	{
		itemRenderer = renderer;
	}

	static
	{
		itemRenderer.setRenderManager(RenderManager.instance);
	}

}
