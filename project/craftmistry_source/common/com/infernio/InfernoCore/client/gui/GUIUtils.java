package com.infernio.InfernoCore.client.gui;

import java.util.Iterator;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import com.infernio.InfernoCore.tile.IFluidTile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GUIUtils {

	private static RenderItem itemRenderer = new RenderItem();
	private static TextureManager textureManager = Minecraft.getMinecraft().renderEngine;
	private static float zLevel = 0.0F;
	private static int color1 = 1347420415;
	private static int color2 = (color1 & 16711422) >> 1 | color1 & -16777216;
	private static int backgroundColor = -267386864;
	private static int stringColor = -1;
	public static final FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;

	public static boolean intersects(int targetX, int targetY, int mouseX, int mouseY, int width, int height, int guiLeft, int guiTop)
	{
        mouseX -= guiLeft;
        mouseY -= guiTop;
        return mouseX >= targetX - 1 && mouseX < targetX + width + 1 && mouseY >= targetY - 1 && mouseY < targetY + height + 1;
	}

	public static boolean isMouseOverSlot(Slot slot, int x, int y, int guiLeft, int guiTop)
    {
    	return intersects(slot.xDisplayPosition, slot.yDisplayPosition, x, y, 16, 16, guiLeft, guiTop);
    }

	public static Slot getSlotAtPosition(Container container, int x, int y, int guiLeft, int guiTop)
	{
        for(int i = 0; i < container.inventorySlots.size(); ++i)
        {
            Slot slot = (Slot)container.inventorySlots.get(i);
            if(isMouseOverSlot(slot, x, y, guiLeft, guiTop))
            {
                return slot;
            }
        }
        return null;
	}

	public static void drawHoveringText(List<String> list, int x, int y, FontRenderer fontRenderer, int guiHeight, int guiWidth)
	{
		if(!list.isEmpty())
	    {
	        GL11.glDisable(GL12.GL_RESCALE_NORMAL);
	        RenderHelper.disableStandardItemLighting();
	        GL11.glDisable(GL11.GL_LIGHTING);
	        GL11.glDisable(GL11.GL_DEPTH_TEST);
	        int biggestWidth = 0;
	        Iterator<String> iterator = list.iterator();
	        while(iterator.hasNext())
	        {
	            String s = (String)iterator.next();
	            int width = fontRenderer.getStringWidth(s);
	            if(width > biggestWidth)
	            {
	                biggestWidth = width;
	            }
	        }
	        int x1 = x + 12;
	        int y1 = y - 12;
	        int k1 = 8;
	        if(list.size() > 1)
	        {
	            k1 += 2 + (list.size() - 1) * 10;
	        }
	        if(x1 + biggestWidth > guiWidth)
	        {
	            x1 -= 28 + biggestWidth;
	        }
	        if(y1 + k1 + 6 > guiHeight)
	        {
	            y1 = guiHeight - k1 - 6;
	        }
	        zLevel = 300.0F;
	        itemRenderer.zLevel = 300.0F;
	        drawGradientRect(x1 - 3, y1 - 4, x1 + biggestWidth + 3, y1 - 3, backgroundColor, backgroundColor);
	        drawGradientRect(x1 - 3, y1 + k1 + 3, x1 + biggestWidth + 3, y1 + k1 + 4, backgroundColor, backgroundColor);
	        drawGradientRect(x1 - 3, y1 - 3, x1 + biggestWidth + 3, y1 + k1 + 3, backgroundColor, backgroundColor);
	        drawGradientRect(x1 - 4, y1 - 3, x1 - 3, y1 + k1 + 3, backgroundColor, backgroundColor);
	        drawGradientRect(x1 + biggestWidth + 3, y1 - 3, x1 + biggestWidth + 4, y1 + k1 + 3, backgroundColor, backgroundColor);
	        drawGradientRect(x1 - 3, y1 - 3 + 1, x1 - 3 + 1, y1 + k1 + 3 - 1, color1, color2);
	        drawGradientRect(x1 + biggestWidth + 2, y1 - 3 + 1, x1 + biggestWidth + 3, y1 + k1 + 3 - 1, color1, color2);
	        drawGradientRect(x1 - 3, y1 - 3, x1 + biggestWidth + 3, y1 - 3 + 1, color1, color1);
	        drawGradientRect(x1 - 3, y1 + k1 + 2, x1 + biggestWidth + 3, y1 + k1 + 3, color2, color2);
	        for(int i = 0; i < list.size(); ++i)
	        {
	            String s1 = (String)list.get(i);
	            fontRenderer.drawStringWithShadow(s1, x1, y1, stringColor);
	            if(i == 0)
	            {
	                y1 += 2;
	            }
	            y1 += 10;
	        }
	        zLevel = 0.0F;
	        itemRenderer.zLevel = 0.0F;
	        GL11.glEnable(GL11.GL_LIGHTING);
	        GL11.glEnable(GL11.GL_DEPTH_TEST);
	        RenderHelper.enableStandardItemLighting();
	        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
	    }
    }

	public static void drawGradientRect(int x1, int y1, int x2, int y2, int color1, int color2)
	{
        float color1Alpha = (float)(color1 >> 24 & 255) / 255.0F;
        float color1Red = (float)(color1 >> 16 & 255) / 255.0F;
        float color1Green = (float)(color1 >> 8 & 255) / 255.0F;
        float color1Blue = (float)(color1 & 255) / 255.0F;
        float color2Alpha = (float)(color2 >> 24 & 255) / 255.0F;
        float color2Red = (float)(color2 >> 16 & 255) / 255.0F;
        float color2Green = (float)(color2 >> 8 & 255) / 255.0F;
        float color2Blue = (float)(color2 & 255) / 255.0F;
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glDisable(GL11.GL_ALPHA_TEST);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA_F(color1Red, color1Green, color1Blue, color1Alpha);
        tessellator.addVertex((double)x2, (double)y1, (double)zLevel);
        tessellator.addVertex((double)x1, (double)y1, (double)zLevel);
        tessellator.setColorRGBA_F(color2Red, color2Green, color2Blue, color2Alpha);
        tessellator.addVertex((double)x1, (double)y2, (double)zLevel);
        tessellator.addVertex((double)x2, (double)y2, (double)zLevel);
        tessellator.draw();
        GL11.glShadeModel(GL11.GL_FLAT);
        GL11.glDisable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_ALPHA_TEST);
        GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

    public static void drawTexturedModelRectFromIcon(int x, int y, IIcon icon, int size1, int size2)
    {
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertexWithUV((double)(x + 0), (double)(y + size2), (double)zLevel, (double)icon.getMinU(), (double)icon.getMaxV());
        tessellator.addVertexWithUV((double)(x + size1), (double)(y + size2), (double)zLevel, (double)icon.getMaxU(), (double)icon.getMaxV());
        tessellator.addVertexWithUV((double)(x + size1), (double)(y + 0), (double)zLevel, (double)icon.getMaxU(), (double)icon.getMinV());
        tessellator.addVertexWithUV((double)(x + 0), (double)(y + 0), (double)zLevel, (double)icon.getMinU(), (double)icon.getMinV());
        tessellator.draw();
    }

    public static void drawTexturedModalRect(int x, int y, int u, int v, int width, int height)
    {
	    float f = 0.00390625F;
	    float f1 = 0.00390625F;
	    Tessellator tessellator = Tessellator.instance;
	    tessellator.startDrawingQuads();
	    tessellator.addVertexWithUV((double)(x + 0), (double)(y + height), (double)zLevel, (double)((float)(u + 0) * f), (double)((float)(v + height) * f1));
	    tessellator.addVertexWithUV((double)(x + width), (double)(y + height), (double)zLevel, (double)((float)(u + width) * f), (double)((float)(v + height) * f1));
	    tessellator.addVertexWithUV((double)(x + width), (double)(y + 0), (double)zLevel, (double)((float)(u + width) * f), (double)((float)(v + 0) * f1));
	    tessellator.addVertexWithUV((double)(x + 0), (double)(y + 0), (double)zLevel, (double)((float)(u + 0) * f), (double)((float)(v + 0) * f1));
	    tessellator.draw();
    }


	public static IIcon getFluidIcon(FluidStack fluidStack, boolean flowingIcon)
	{
		if(fluidStack == null)
		{
			return null;
		}
		return getFluidIcon(fluidStack.getFluid(), flowingIcon);
	}

	public static IIcon getFluidIcon(Fluid fluid, boolean flowingIcon)
	{
		if(fluid == null)
		{
			return null;
		}
		if(flowingIcon)
		{
			return fluid.getFlowingIcon();
		}
		else
		{
			return fluid.getStillIcon();
		}
	}

	public static float scaleTank(IFluidTile tile)
	{
		int capacity = tile.getCapacity();
		return Math.min(tile.getFluidAmount(), capacity) / (float)capacity;
	}

	public static void renderTank(int x, int y, int y1, int x1, int scaled, FluidStack fluid)
	{
		renderTank(x, y, y1, x1, scaled, fluid.getFluid());
	}

	public static void renderTank(int x, int y, int y1, int x1, int scaled, Fluid fluid)
	{
		//Code adapted from BuildCraft
		IIcon liquidIcon = getFluidIcon(fluid, false);
		textureManager.bindTexture(TextureMap.locationBlocksTexture);
		int start = 0;
		int done = 0;
		while(true)
		{
			int y2;
			if(scaled > 16)
			{
				y2 = 16;
				scaled -= 16;
			}
			else
			{
				y2 = scaled;
				scaled = 0;
			}
			drawTexturedModelRectFromIcon(x + x1, y + y1 + 58 - y2 - start, liquidIcon, 16, y2);
			start += 16;
			if(y2 == done || scaled == done)
			{
				break;
			}
		}
	}

	public static void setupItemRenderer(RenderItem renderer)
	{
		itemRenderer = renderer;
	}

	public static void setZLevel(float to)
	{
		zLevel = to;
	}

	public static void setColorSelection(int newColor1, int newColor2)
	{
		color1 = newColor1;
		color2 = newColor2;
	}

	public static void setBackgroundColor(int newColor)
	{
		backgroundColor = newColor;
	}

	public static void setStringColor(int newColor)
	{
		stringColor = newColor;
	}

	public static void resetColorSelection()
	{
		color1 = 1347420415;
		color2 = (color1 & 16711422) >> 1 | color1 & -16777216;
		backgroundColor = -267386864;
		stringColor = -1;
	}

	static
	{
		itemRenderer.setRenderManager(RenderManager.instance);
	}

}
