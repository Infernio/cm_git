package com.infernio.InfernoCore.client.icon;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.renderer.texture.TextureMap;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class IconRegistry {

	private List<IIconSubscriber> subscribers = new ArrayList<IIconSubscriber>();

	private static IconRegistry instance = new IconRegistry();

	private IconRegistry() {}

	public static IconRegistry getInstance()
	{
		return instance;
	}

	public void registerSubscriber(IIconSubscriber subscriber)
	{
		if(!subscribers.contains(subscriber))
		{
			subscribers.add(subscriber);
		}
	}

	public void loadIcons(TextureMap map)
	{
		if(map != null)
		{
			for(IIconSubscriber subscriber : subscribers)
			{
				if(subscriber != null && subscriber.getTextureType() == map.getTextureType())
				{
					subscriber.loadIcons(map);
				}
			}
		}
	}

	public void postLoadIcons(TextureMap map)
	{
		if(map != null)
		{
			for(IIconSubscriber subscriber : subscribers)
			{
				if(subscriber != null && subscriber.shouldPostLoad() && subscriber.getTextureType() == map.getTextureType())
				{
					subscriber.postLoadIcons(map);
				}
			}
		}
	}

}
