package com.infernio.InfernoCore.client.icon;

import net.minecraft.client.renderer.texture.IIconRegister;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public interface IIconSubscriber {

	void loadIcons(IIconRegister iconRegister);

	void postLoadIcons(IIconRegister iconRegister);

	boolean shouldPostLoad();

	int getTextureType();
}
