package com.infernio.InfernoCore.key;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public interface IKeyBoundItem {

	void onKeyPressed(String description, int keyCode, String category, EntityPlayer player, ItemStack stack);

}
