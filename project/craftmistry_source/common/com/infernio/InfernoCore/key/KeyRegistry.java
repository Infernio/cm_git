package com.infernio.InfernoCore.key;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.message.MessageKeyPressed;
import com.infernio.InfernoCore.network.message.MessageKeyPressed.KeyBindingType;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;

public class KeyRegistry {

	private static HashMap<String, KeyBinding> keyBindings = new HashMap<String, KeyBinding>();
	private static HashMap<String, ArrayList<IKeyTrigger>> keyTriggers = new HashMap<String, ArrayList<IKeyTrigger>>();

	public static void registerKeyBinding(KeyBinding kb)
	{
		if(!keyBindings.containsKey(kb.getKeyDescription()))
		{
			keyBindings.put(kb.getKeyDescription(), kb);
		}
	}

	public static void registerKeyTrigger(String key, IKeyTrigger trigger)
	{
		if(!keyTriggers.containsKey(key))
		{
			ArrayList<IKeyTrigger> triggers = new ArrayList<IKeyTrigger>();
			triggers.add(trigger);
			keyTriggers.put(key, triggers);
		}
		else
		{
			keyTriggers.get(key).add(trigger);
		}
	}

	public static KeyBinding getKeyBinding(String keyDescription)
	{
		return keyBindings.get(keyDescription);
	}

	public static List<IKeyTrigger> getTrigger(String key)
	{
		return keyTriggers.get(key);
	}

	private KeyBinding getPressedKeybinding()
    {
        for(KeyBinding kb : keyBindings.values())
        {
        	if(kb != null && kb.isPressed())
        	{
        		return kb;
        	}
        }
        return null;
    }

	@SubscribeEvent
	public void handleKeyInput(InputEvent.KeyInputEvent event)
	{
		KeyBinding kb = this.getPressedKeybinding();
		if(kb != null)
		{
			EntityPlayer player = FMLClientHandler.instance().getClientPlayerEntity();
			if(player != null)
            {
                ItemStack stack = player.getCurrentEquippedItem();
                if(stack != null && stack.getItem() instanceof IKeyBoundItem)
                {
                    if(player.worldObj.isRemote)
                    {
                    	PacketHelper.sendToServer(new MessageKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), KeyBindingType.ITEM));
                    }
                    else
                    {
                        ((IKeyBoundItem)stack.getItem()).onKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), player, stack);
                    }
                }
                else
                {
                	List<IKeyTrigger> triggers = keyTriggers.get(kb.getKeyDescription());
                	if(triggers != null && !triggers.isEmpty())
                	{
                		for(IKeyTrigger trigger : triggers)
                		{
	                		if(player.worldObj.isRemote)
	                		{
		                    	PacketHelper.sendToServer(new MessageKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), KeyBindingType.TRIGGER));
	                		}
	                		else
	                		{
	                			trigger.onKeyPressed(kb.getKeyDescription(), kb.getKeyCode(), kb.getKeyCategory(), player);
	                		}
                		}
                	}
                }
            }
		}
	}

}
