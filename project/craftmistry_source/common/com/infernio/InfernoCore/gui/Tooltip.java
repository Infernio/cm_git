package com.infernio.InfernoCore.gui;

import java.util.ArrayList;

import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

public class Tooltip {

	public ArrayList<String> tooltip;

	public Tooltip()
	{
		this.tooltip = new ArrayList<String>();
	}

	public static Tooltip loadLocalizedTooltip(String tooltipName)
	{
		Tooltip ret = new Tooltip();
		String count = StatCollector.translateToLocal(tooltipName + ".lineCount");
		if(isInteger(count))
		{
			int lines = Integer.parseInt(count);
			for(int line = 0; line < lines; ++line)
			{
				ret.tooltip.add(resolveColors(StatCollector.translateToLocal(tooltipName + ".line" + line)));
			}
		}
		else
		{
			ret.tooltip.add(EnumChatFormatting.RED + "INVALID TOOLTIP FORMAT");
			ret.tooltip.add(EnumChatFormatting.RED + "'" + tooltipName + ".lineCount' must exist and be a number!");
		}
		return ret;
	}

	public static String resolveColors(String raw)
	{
		boolean readingColor = false;
		StringBuilder ret = new StringBuilder();
		StringBuilder color = new StringBuilder();
		for(char c : raw.toCharArray())
		{
			if(c == '$')
			{
				if(readingColor)
				{
					readingColor = false;
					EnumChatFormatting format = EnumChatFormatting.valueOf(color.toString());
					if(format != null)
					{
						ret.append(format.toString());
					}
					color = new StringBuilder();
				}
				else
				{
					readingColor = true;
				}
			}
			else if(readingColor)
			{
				color.append(Character.toUpperCase(c));
			}
			else
			{
				ret.append(c);
			}
		}
		return ret.toString();
	}

	private static boolean isInteger(String s)
	{
		for(char c : s.toCharArray())
		{
			if(!Character.isDigit(c))
			{
				return false;
			}
		}
		return true;
	}

	public void addLine(String line)
	{
		this.tooltip.add(StatCollector.translateToLocal(line));
	}

	public void addUnlocalizedLine(String line)
	{
		this.tooltip.add(line);
	}

}
