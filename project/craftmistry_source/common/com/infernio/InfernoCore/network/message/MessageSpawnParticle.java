package com.infernio.InfernoCore.network.message;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageSpawnParticle implements IMessage, IMessageHandler<MessageSpawnParticle, IMessage> {

	private double x;
	private double y;
	private double z;
	private double velX;
	private double velY;
	private double velZ;
	private String particle;

	public MessageSpawnParticle() {}

	public MessageSpawnParticle(double x, double y, double z, double velX, double velY, double velZ, String particle)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.velX = velX;
		this.velY = velY;
		this.velZ = velZ;
		this.particle = particle;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readDouble();
        this.y = buf.readDouble();
        this.z = buf.readDouble();
        this.velX = buf.readDouble();
        this.velY = buf.readDouble();
        this.velZ = buf.readDouble();
        int length = buf.readInt();
        StringBuilder type = new StringBuilder();
        for(int i = 0; i < length; ++i)
        {
        	type.append(buf.readChar());
        }
        this.particle = type.toString();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeDouble(this.x);
        buf.writeDouble(this.y);
        buf.writeDouble(this.z);
        buf.writeDouble(this.velX);
        buf.writeDouble(this.velY);
        buf.writeDouble(this.velZ);
        char[] chars = this.particle.toCharArray();
        buf.writeInt(chars.length);
        for(char c : chars)
        {
        	buf.writeChar(c);
        }
    }

    @Override
    public IMessage onMessage(MessageSpawnParticle message, MessageContext ctx)
    {
    	FMLClientHandler.instance().getWorldClient().spawnParticle(message.particle, message.x, message.y, message.z, message.velX, message.velY, message.velZ);
        return null;
    }
}
