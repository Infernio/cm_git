package com.infernio.InfernoCore.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

import com.infernio.InfernoCore.InfernoCore;
import com.infernio.InfernoCore.gui.GUIRegistry;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageOpenGUI implements IMessage, IMessageHandler<MessageOpenGUI, IMessage> {

	private int x;
	private int y;
	private int z;
	private int guiID;
	private int modID;

	public MessageOpenGUI() {}

	public MessageOpenGUI(int x, int y, int z, int guiID, int modID)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.guiID = guiID;
		this.modID = modID;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.guiID = buf.readInt();
        this.modID = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.guiID);
        buf.writeInt(this.modID);
    }

    @Override
    public IMessage onMessage(MessageOpenGUI message, MessageContext ctx)
    {
    	if(message.guiID < 0)
		{
			InfernoCore.log.warn("[Network] Attemped to handle a corrupt message: '" + this.getClass().getSimpleName() + "'.");
			InfernoCore.log.warn("[Network] Cause: GUI ID was negative");
			return null;
		}
    	EntityPlayer player = ctx.getServerHandler().playerEntity;
		player.openGui(GUIRegistry.getMod((short)message.modID), message.guiID, player.worldObj, message.x, message.y, message.z);
        return null;
    }
}
