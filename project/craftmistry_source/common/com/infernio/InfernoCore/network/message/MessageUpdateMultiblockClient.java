package com.infernio.InfernoCore.network.message;

import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.InfernoCore.InfernoCore;
import com.infernio.InfernoCore.multiblock.MultiblockPart;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageUpdateMultiblockClient implements IMessage, IMessageHandler<MessageUpdateMultiblockClient, IMessage> {

	private int x;
	private int y;
	private int z;
	private boolean completed;

	public MessageUpdateMultiblockClient() {}

	public MessageUpdateMultiblockClient(int x, int y, int z, boolean completed)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.completed = completed;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.completed = buf.readBoolean();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeBoolean(this.completed);
    }

    @Override
    public IMessage onMessage(MessageUpdateMultiblockClient message, MessageContext ctx)
    {
    	World[] worlds = InfernoCore.proxy.getCurrentWorlds();
    	for(World world : worlds)
    	{
	    	if(world.getChunkProvider().chunkExists(message.x, message.z))
			{
				TileEntity tile = world.getTileEntity(message.x, message.y, message.z);
				if(tile instanceof MultiblockPart)
				{
					((MultiblockPart)tile).clientCompleted = message.completed;
				}
			}
    	}
        return null;
    }
}
