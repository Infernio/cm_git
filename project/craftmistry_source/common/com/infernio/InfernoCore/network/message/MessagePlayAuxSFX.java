package com.infernio.InfernoCore.network.message;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessagePlayAuxSFX implements IMessage, IMessageHandler<MessagePlayAuxSFX, IMessage> {

	private int x;
	private int y;
	private int z;
	private int sfxID;
	private int data;

	public MessagePlayAuxSFX() {}

	public MessagePlayAuxSFX(int x, int y, int z, int sfxID, int data)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.sfxID = sfxID;
		this.data = data;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.sfxID = buf.readInt();
        this.data = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
        buf.writeInt(this.x);
        buf.writeInt(this.y);
        buf.writeInt(this.z);
        buf.writeInt(this.sfxID);
        buf.writeInt(this.data);
    }

    @Override
    public IMessage onMessage(MessagePlayAuxSFX message, MessageContext ctx)
    {
		FMLClientHandler.instance().getWorldClient().playAuxSFX(message.sfxID, message.x, message.y, message.z, message.data);
        return null;
    }
}
