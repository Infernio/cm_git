package com.infernio.InfernoCore.network.message;

import io.netty.buffer.ByteBuf;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import com.infernio.InfernoCore.key.IKeyBoundItem;
import com.infernio.InfernoCore.key.IKeyTrigger;
import com.infernio.InfernoCore.key.KeyRegistry;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class MessageKeyPressed implements IMessage, IMessageHandler<MessageKeyPressed, IMessage> {

	private String description;
	private int keyCode;
	private String category;
	private KeyBindingType type;

	public MessageKeyPressed() {}

	public MessageKeyPressed(String description, int keyCode, String category, KeyBindingType type)
	{
		this.description = description;
		this.keyCode = keyCode;
		this.category = category;
		this.type = type;
	}

	@Override
    public void fromBytes(ByteBuf buf)
    {
		this.keyCode = buf.readInt();
		this.type = KeyBindingType.values()[buf.readInt()];
		int length = buf.readInt();
		StringBuilder builder = new StringBuilder();
        for(int i = 0; i < length; ++i)
        {
        	builder.append(buf.readChar());
        }
        this.description = builder.toString();
        length = buf.readInt();
        builder = new StringBuilder();
        for(int i = 0; i < length; ++i)
        {
        	builder.append(buf.readChar());
        }
        this.category = builder.toString();
    }

    @Override
    public void toBytes(ByteBuf buf)
    {
    	buf.writeInt(this.keyCode);
    	buf.writeInt(this.type.ordinal());
    	char[] chars = this.description.toCharArray();
        buf.writeInt(chars.length);
        for(char c : chars)
        {
        	buf.writeChar(c);
        }
        chars = this.category.toCharArray();
        buf.writeInt(chars.length);
        for(char c : chars)
        {
        	buf.writeChar(c);
        }
    }

    @Override
    public IMessage onMessage(MessageKeyPressed message, MessageContext ctx)
    {
    	EntityPlayer player = ctx.getServerHandler().playerEntity;
    	if(message.type.equals(KeyBindingType.ITEM))
        {
        	ItemStack stack = player.getCurrentEquippedItem();
        	if(stack.getItem() instanceof IKeyBoundItem)
        	{
                ((IKeyBoundItem)stack.getItem()).onKeyPressed(message.description, message.keyCode, message.category, player, stack);
        	}
        }
        else if(message.type.equals(KeyBindingType.TRIGGER))
        {
        	List<IKeyTrigger> triggers = KeyRegistry.getTrigger(message.description);
        	if(triggers != null && !triggers.isEmpty())
        	{
        		for(IKeyTrigger trigger : triggers)
        		{
        			trigger.onKeyPressed(message.description, message.keyCode, message.category, player);
        		}
        	}
        }
        return null;
    }

    public static enum KeyBindingType
    {
    	ITEM,
    	TRIGGER;
    }
}
