package com.infernio.InfernoCore.network;

import net.minecraft.entity.player.EntityPlayerMP;

import com.infernio.InfernoCore.Resources;
import com.infernio.InfernoCore.network.message.MessageFacing;
import com.infernio.InfernoCore.network.message.MessageKeyPressed;
import com.infernio.InfernoCore.network.message.MessageNeighborRenderUpdate;
import com.infernio.InfernoCore.network.message.MessageOpenGUI;
import com.infernio.InfernoCore.network.message.MessagePlayAuxSFX;
import com.infernio.InfernoCore.network.message.MessageSpawnParticle;
import com.infernio.InfernoCore.network.message.MessageUpdateMultiblockClient;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class PacketHelper {

	private static final SimpleNetworkWrapper wrapper = NetworkRegistry.INSTANCE.newSimpleChannel(Resources.CHANNEL);

	public static void init()
    {
        wrapper.registerMessage(MessageSpawnParticle.class, MessageSpawnParticle.class, Packets.PACKET_PARTICLE_SPAWN, Side.CLIENT);
        wrapper.registerMessage(MessagePlayAuxSFX.class, MessagePlayAuxSFX.class, Packets.PACKET_PLAY_AUX_SFX, Side.CLIENT);
        wrapper.registerMessage(MessageNeighborRenderUpdate.class, MessageNeighborRenderUpdate.class, Packets.PACKET_RENDER_NEIGHBOR_UPDATE, Side.CLIENT);
        wrapper.registerMessage(MessageOpenGUI.class, MessageOpenGUI.class, Packets.PACKET_OPEN_GUI, Side.SERVER);
        wrapper.registerMessage(MessageUpdateMultiblockClient.class, MessageUpdateMultiblockClient.class, Packets.PACKET_UPDATE_MULTIBLOCK_CLIENT, Side.CLIENT);
        wrapper.registerMessage(MessageKeyPressed.class, MessageKeyPressed.class, Packets.PACKET_KEY_PRESSED, Side.SERVER);
        wrapper.registerMessage(MessageFacing.class, MessageFacing.class, Packets.PACKET_SET_CLIENT_FACING, Side.CLIENT);
    }

	public static void sendTo(IMessage message, EntityPlayerMP player)
	{
		wrapper.sendTo(message, player);
	}

	public static void sendToAll(IMessage message)
	{
		wrapper.sendToAll(message);
	}

	public static void sendToAllAround(IMessage message, int dimension, int x, int y, int z, int range)
	{
		sendToAllAround(message, new TargetPoint(dimension, x, y, z, range));
	}

	public static void sendToAllAround(IMessage message, TargetPoint point)
	{
		wrapper.sendToAllAround(message, point);
	}

	public static void sendToDimension(IMessage message, int dimension)
	{
		wrapper.sendToDimension(message, dimension);
	}

	public static void sendToServer(IMessage message)
	{
		wrapper.sendToServer(message);
	}
}
