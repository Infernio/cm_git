package com.infernio.InfernoCore.proxy;

import net.minecraft.client.audio.SoundCategory;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

import com.infernio.InfernoCore.event.EventListener;

public class Proxy {

	public void registerListener()
	{
		MinecraftForge.EVENT_BUS.register(new EventListener());
	}

	public void loadVanillaParticles() {}

	public void registerKeyHandler() {}

	public void tick()
	{
		MinecraftServer.getServer().tick();
	}

	public float getSoundVolume(SoundCategory category)
	{
		return 0.5F;
	}

	public int addArmor(String armor)
	{
		return 0;
	}

	public World[] getCurrentWorlds()
	{
		return MinecraftServer.getServer().worldServers;
	}

}
