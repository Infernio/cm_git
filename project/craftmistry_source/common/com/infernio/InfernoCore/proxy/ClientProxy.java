package com.infernio.InfernoCore.proxy;

import net.minecraft.client.audio.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

import com.infernio.InfernoCore.client.particle.ParticleRegistry;
import com.infernio.InfernoCore.event.ClientEventListener;
import com.infernio.InfernoCore.key.KeyRegistry;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;

public class ClientProxy extends Proxy {

	@Override
	public void registerListener()
	{
		MinecraftForge.EVENT_BUS.register(new ClientEventListener());
	}

	@Override
	public void loadVanillaParticles()
	{
		ParticleRegistry.loadVanillaParticles();
	}

	@Override
	public int addArmor(String armor)
	{
		return RenderingRegistry.addNewArmourRendererPrefix(armor);
	}

	@Override
	public void tick()
	{	
		FMLClientHandler.instance().getClient().runTick();
	}

	@Override
	public float getSoundVolume(SoundCategory category)
	{
		return FMLClientHandler.instance().getClient().gameSettings.getSoundLevel(category);
	}

	@Override
	public void registerKeyHandler()
	{
		FMLCommonHandler.instance().bus().register(new KeyRegistry());
	}

	@Override
	public World[] getCurrentWorlds()
	{
		return new World[]{FMLClientHandler.instance().getWorldClient()};
	}

}
