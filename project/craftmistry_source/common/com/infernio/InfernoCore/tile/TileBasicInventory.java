package com.infernio.InfernoCore.tile;

import com.infernio.InfernoCore.util.NBTLib;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public abstract class TileBasicInventory extends TileEntity implements IInventory {

	public static final int STACK_LIMIT = 64;

    protected ItemStack[] inv;
    private String customName;

	public TileBasicInventory(int size)
	{
		this.inv = new ItemStack[size];
	}

    @Override
    public int getSizeInventory()
    {
        return this.inv.length;
    }

    @Override
    public ItemStack getStackInSlot(int slot)
    {
        return this.inv[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount)
    {
		ItemStack stack = this.getStackInSlot(slot);
		if(stack != null) 
		{
			if(stack.stackSize <= amount) 
			{
				this.setInventorySlotContents(slot, null);
			}
			else
			{
				stack = stack.splitStack(amount);
				if(stack.stackSize <= 0)
				{
					this.setInventorySlotContents(slot, null);
				}
			}
		}
		this.markDirty();
		return stack;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot)
    {
    	ItemStack stack = this.getStackInSlot(slot);
    	if(stack != null)
    	{
    		this.setInventorySlotContents(slot, null);
    	}
    	return stack;
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack stack)
    {
        this.inv[slot] = stack;
        this.markDirty();
        if(stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }
    }

    @Override
    public String getInventoryName()
    {
        return this.hasCustomInventoryName() ? this.customName : this.getDefaultInvName();
    }

    @Override
    public boolean hasCustomInventoryName()
    {
        return this.customName != null && !this.customName.isEmpty();
    }

    @Override
    public int getInventoryStackLimit()
    {
        return STACK_LIMIT;
    }

    @Override
    public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
    {
        return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : par1EntityPlayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }
    
    @Override
    public void openInventory() {}

    @Override
    public void closeInventory() {}

    @Override
    public boolean isItemValidForSlot(int slot, ItemStack stack)
    {
        return true;
    }

    @Override
    public void readFromNBT(NBTTagCompound tag)
    {
    	super.readFromNBT(tag);
    	NBTTagList nbttaglist = tag.getTagList("Inventory", NBTLib.NBT_COMPOUND);
        this.inv = new ItemStack[this.getSizeInventory()];
        for(int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound tag2 = nbttaglist.getCompoundTagAt(i);
            byte b = tag2.getByte("Slot");
            if(b >= 0 && b < this.inv.length)
            {
                this.inv[b] = ItemStack.loadItemStackFromNBT(tag2);
            }
        }
        if(tag.hasKey("CustomName"))
        {
            this.customName = tag.getString("CustomName");
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound tag)
    {
    	super.writeToNBT(tag);
    	NBTTagList tagList = new NBTTagList();
        for(int i = 0; i < this.inv.length; ++i)
        {
            if(this.inv[i] != null)
            {
                NBTTagCompound tag2 = new NBTTagCompound();
                tag2.setByte("Slot", (byte)i);
                this.inv[i].writeToNBT(tag2);
                tagList.appendTag(tag2);
            }
        }
        tag.setTag("Inventory", tagList);
        if(this.hasCustomInventoryName())
        {
            tag.setString("CustomName", this.customName);
        }
    }

    public void setInventoryName(String to)
    {
        this.customName = to;
    }

    public abstract String getDefaultInvName();

}
