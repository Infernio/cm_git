package com.infernio.InfernoCore.tile;

public abstract class TileMachineInventory extends TileInventoryElectric {

    public boolean disabled;

    public TileMachineInventory(int size)
    {
    	super(size);
    	this.disabled = false;
    }

    @Override
    public void updateEntity()
    {
    	super.updateEntity();
    	if(!this.worldObj.isRemote)
    	{
	    	if(this.isRedstoneControlled())
	    	{
		    	this.checkRedstone();
		    	if(!this.disabled)
		    	{
		    		this.fillBuffer();
		    		this.onUpdate();
		    	}
	    	}
	    	else
	    	{
	    		this.fillBuffer();
	    		this.onUpdate();
	    	}
    	}
    }

    private void checkRedstone()
    {
		if(this.worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord))
		{
			this.disabled = true;
			return;
		}
		this.disabled = false;
	}

    public abstract void onUpdate();

    public abstract boolean isRedstoneControlled();

}
