package com.infernio.InfernoCore.tile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;

import com.infernio.InfernoCore.math.MathUtils;

public class TileSingleFluid extends TileEntity implements IFluidTile {

	private FluidTank tank;

	public TileSingleFluid(int capacity)
	{
		this(null, capacity);
	}

	public TileSingleFluid(Fluid fluid, int amount, int capacity)
	{
		this(new FluidStack(fluid, amount), capacity);
	}

	public TileSingleFluid(FluidStack fluid, int capacity)
	{
		this.tank = new FluidTank(fluid, capacity);
	}

	@Override
	public FluidStack getFluid()
	{
		return this.tank.getFluid();
	}

	@Override
	public void emptyTank()
	{
		this.tank.setFluid(null);
	}

	@Override
	public void fillTank()
	{
		this.tank.getFluid().amount = this.tank.getCapacity();
	}

	@Override
	public int getFluidAmount()
	{
		return this.tank.getFluidAmount();
	}

	@Override
	public void decreaseFluidAmount(int by)
	{
		if(this.getFluidAmount() - by <= 0)
		{
			this.emptyTank();
		}
		else
		{
			this.getFluid().amount -= by;
		}
	}

	@Override
	public void increaseFluidAmount(int by)
	{
		if(this.getFluidAmount() + by > this.getCapacity())
		{
			this.fillTank();
		}
		else
		{
			this.getFluid().amount += by;
		}
	}

	@Override
	public void setFluidAmount(int to)
	{
		this.tank.getFluid().amount = MathHelper.clamp_int(to, 0, this.getCapacity());
	}

	@Override
	public int getCapacity()
	{
		return this.tank.getCapacity();
	}

	@Override
	public void decreaseCapacity(int by)
	{
		if(this.getCapacity() - by < 0)
		{
			this.setCapacity(0);
		}
		else
		{
			this.tank.setCapacity(this.getCapacity() - by);
		}
	}

	@Override
	public void increaseCapacity(int by)
	{
		if(this.getCapacity() + by < 0)
		{
			this.setCapacity(0);
		}
		else
		{
			this.tank.setCapacity(this.getCapacity() + by);
		}
	}

	@Override
	public void setCapacity(int to)
	{
		this.tank.setCapacity(MathUtils.ensurePositive(to));
	}

	@Override
	public int getScaledAmount(int scaleBy)
	{
		return (int)(((float)this.getFluidAmount() / (float)this.getCapacity()) * scaleBy);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.tank.readFromNBT(tag.getCompoundTag("Fluid"));
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		tag.setTag("Fluid", new NBTTagCompound());
		this.tank.writeToNBT(tag.getCompoundTag("Fluid"));
	}

	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		return this.tank.fill(resource, doFill);
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		if(resource.isFluidEqual(this.tank.getFluid()))
		{
			return this.tank.drain(resource.amount, doDrain);
		}
		return null;
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		return this.tank.drain(maxDrain, doDrain);
	}

	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		return this.tank.getFluid().isFluidEqual(new FluidStack(fluid, 0));
	}

	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		return this.tank.getFluid().isFluidEqual(new FluidStack(fluid, 0));
	}

	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		return new FluidTankInfo[]{this.tank.getInfo()};
	}
}
