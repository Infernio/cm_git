package com.infernio.InfernoCore.tile;

public interface IRotatable {

	boolean acceptsRotation();

	void rotate(int facing);

	int getFacing();
}
