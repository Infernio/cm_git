package com.infernio.InfernoCore;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.Packets;
import com.infernio.InfernoCore.proxy.Proxy;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Resources.MODID, name = Resources.NAME, version = Resources.VERSION, dependencies = Resources.DEPENDENCIES, useMetadata = true)
public class InfernoCore {

	public static Logger log = LogManager.getLogger(Resources.NAME);
    public static boolean showRawNames = false;

	@SidedProxy(serverSide = "com.infernio.InfernoCore.proxy.Proxy", clientSide = "com.infernio.InfernoCore.proxy.ClientProxy")
	public static Proxy proxy;

	@EventHandler
	public void preLoad(FMLPreInitializationEvent event)
	{
		log.info("Loading InfernoCore version " + Resources.VERSION);

		this.loadConfig(event);

		proxy.registerListener();

		proxy.loadVanillaParticles();
	}

	private void loadConfig(FMLPreInitializationEvent event)
	{
		File dir = event.getModConfigurationDirectory();
		Configuration config = new Configuration(new File(dir.getName() + "/InfernoCore.cfg"));

		config.load();

		showRawNames = config.get("General", "infernoCore.general.showRawNames", true, "Setting this to true will show which mod an item comes from when hovering over it. Will also show raw names of items and blocks while holding shift in gold. These are the names that minecraft now uses instead of numbers.").getBoolean(false);
		Packets.PACKET_SPAWN_PARTICLE_RANGE = config.get("Graphics", "infernoCore.graphics.particleRange", 32, "The range in which particles spawned by InfernoCore mods will be visible.").getInt(32);
		Packets.PACKET_PLAY_AUX_SFX_RANGE = config.get("Graphics", "infernoCore.graphics.auxSFXRange", 32, "The range in which Aux SFX effects played by InfernoCore mods will be visible / audible.").getInt(16);

		config.save();
	}

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		PacketHelper.init();
	}

	@EventHandler
	public void postLoad(FMLPostInitializationEvent event)
	{
		proxy.registerKeyHandler();
	}

}
