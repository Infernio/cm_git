package com.infernio.InfernoCore.multiblock;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.InfernoCore.math.Vector;
import com.infernio.InfernoCore.network.PacketHelper;
import com.infernio.InfernoCore.network.message.MessageNeighborRenderUpdate;
import com.infernio.InfernoCore.network.message.MessageUpdateMultiblockClient;

public final class Multiblock {

	public final MultiblockSize size;
	public boolean modified;
	public boolean needsRefresh;
	public boolean needsNewMaster;
	public boolean needsNotifying;
	private int scanCounter;
	private int fullScanCounter;
	private World world;
	private MultiblockPart master;
	public final String type;
	public EnumMultiblockState state;
	private TreeMap<Vector, MultiblockPart> parts;

	public Multiblock(String type, int xSize, int ySize, int zSize, World world)
	{
		this(type, new MultiblockSize(xSize, ySize, zSize), world);
	}

	public Multiblock(String type, MultiblockSize size, World world)
	{
		this.type = type;
		this.size = size;
		this.state = EnumMultiblockState.UNKNOWN;
		this.world = world;
		this.parts = new TreeMap<Vector, MultiblockPart>();
	}

	public boolean addPart(int x, int y, int z, MultiblockPart part)
	{
		return this.addPart(new Vector(x, y, z), part);
	}

	public boolean addPart(Vector vector, MultiblockPart part)
	{
		if(vector != null && part != null && this.parts.size() < this.size.totalSize)
		{
			Entry<Vector, MultiblockPart> entry;
			Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
			while(ite.hasNext())
			{
				entry = ite.next();
				Vector vector2 = entry.getKey();
				int xDistance = vector.x - vector2.x;
				if(xDistance >= this.size.sizeX || xDistance <= -this.size.sizeX)
				{
					return false;
				}
				int yDistance = vector.y - vector2.y;
				if(yDistance >= this.size.sizeY || yDistance <= -this.size.sizeY)
				{
					return false;
				}
				int zDistance = vector.z - vector2.z;
				if(zDistance >= this.size.sizeZ || zDistance <= -this.size.sizeZ)
				{
					return false;
				}
			}
			part.multiblock = this;
			part.master = this.master;
			part.isMaster = part.equals(this.master);
			this.parts.put(vector, part);
			this.needsNotifying = true;
			return true;
		}
		return false;
	}

	public MultiblockPart removePart(int x, int y, int z)
	{
		return this.removePart(new Vector(x, y, z));
	}

	public MultiblockPart removePart(Vector vector)
	{
		this.state = EnumMultiblockState.UNKNOWN;
		this.modified = true;
		MultiblockPart part = this.parts.get(vector);
		if(part == null)
		{
			return null;
		}
		if(part.equals(this.master))
		{
			this.needsNewMaster = true;
			this.needsNotifying = true;
			part.multiblock = null;
			part.master = null;
			part.isMaster = false;
			return this.parts.remove(vector);
		}
		this.needsNotifying = true;
		part.multiblock = null;
		part.master = null;
		part.isMaster = false;
		return this.parts.remove(vector);
	}

	public boolean hasPart(int x, int y, int z)
	{
		return this.hasPart(new Vector(x, y, z));
	}

	public boolean hasPart(Vector vector)
	{
		return this.parts.containsKey(vector);
	}

	public void tick()
	{
		if(this.master == null || this.needsNewMaster)
		{
			this.assignMaster();
			this.needsNewMaster = false;
		}
		if(this.needsRefresh)
		{
			this.refresh();
			this.needsRefresh = false;
		}
		if(this.needsNotifying)
		{
			this.notifyAllNeighbors();
			this.needsNotifying = false;
		}
		if(this.modified)
		{
			this.fullScan();
			this.modified = false;
			return;
		}
		if(this.scanCounter < 40)
		{
			++this.scanCounter;
		}
		else
		{
			this.scanCounter = 0;
			this.rescan();
		}
		if(this.fullScanCounter < 80)
		{
			++this.fullScanCounter;
		}
		else
		{
			this.fullScanCounter = 0;
			this.fullScan();
		}
	}

	public void fullScan()
	{
		this.needsRefresh = true;
		this.needsNotifying = true;
		Entry<Vector, MultiblockPart> entry;
		Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
		while(ite.hasNext())
		{
			entry = ite.next();
			MultiblockPart part = entry.getValue();
			Vector vector = entry.getKey();
			Entry<Vector, MultiblockPart> entry2;
			Iterator<Entry<Vector, MultiblockPart>> ite2 = this.parts.entrySet().iterator();
			while(ite2.hasNext())
			{
				entry2 = ite2.next();
				Vector vector2 = entry2.getKey();
				if(vector2.equals(vector))
				{
					continue;
				}
				boolean removed = false;
				int xDistance = vector.x - vector2.x;
				if(xDistance >= this.size.sizeX || xDistance <= -this.size.sizeX)
				{
					removed = true;
					if(part.isMaster)
					{
						this.needsNewMaster =  true;
					}
					ite.remove();
				}
				int yDistance = vector.y - vector2.y;
				if(yDistance >= this.size.sizeY || yDistance <= -this.size.sizeY)
				{
					if(!removed)
					{
						if(part.isMaster)
						{
							this.needsNewMaster =  true;
						}
						ite.remove();
						removed = true;
					}
				}
				int zDistance = vector.z - vector2.z;
				if(zDistance >= this.size.sizeZ || zDistance <= -this.size.sizeZ)
				{
					if(!removed)
					{
						if(part.isMaster)
						{
							this.needsNewMaster = true;
						}
						ite.remove();
					}
				}
			}
		}
	}

	public void rescan()
	{
		this.needsNotifying = true;
		boolean hasMaster = false;
		Entry<Vector, MultiblockPart> entry;
		Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
		boolean assignMaster = false;
		while(ite.hasNext())
		{
			entry = ite.next();
			Vector vector = entry.getKey();
			MultiblockPart part = entry.getValue();
			boolean removed = false;
			if(!vector.blockExists(this.world))
			{
				if(part.isMaster)
				{
					assignMaster = true;
				}
				part.multiblock = null;
				part.master = null;
				part.isMaster = false;
				ite.remove();
				removed = true;
				this.state = EnumMultiblockState.INCOMPLETE;
				this.modified = true;
			}
			if(!part.getID().equals(this.type))
			{
				if(!removed)
				{
					if(part.isMaster)
					{
						assignMaster = true; 
					}
					part.multiblock = null;
					part.master = null;
					part.isMaster = false;
					ite.remove();
					removed = true;
					this.state = EnumMultiblockState.INCOMPLETE;
					this.modified = true;
				}
			}
			if(part.isMaster)
			{
				if(removed)
				{
					assignMaster = true;
				}
				if(!hasMaster)
				{
					this.master = part;
					hasMaster = true;
				}
				else
				{
					part.isMaster = false;
					part.master = this.master;
				}
			}
		}
		if(this.parts.size() == this.size.totalSize)
		{
			this.state = EnumMultiblockState.COMPLETE;
		}
		else
		{
			this.state = EnumMultiblockState.INCOMPLETE;
		}
		if(assignMaster)
		{
			this.needsNewMaster = true;
		}
		if(!hasMaster)
		{
			this.needsNewMaster = true;
		}
		else
		{
			this.needsRefresh = true;
		}
	}

	public void refresh()
	{
		Entry<Vector, MultiblockPart> entry;
		Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
		while(ite.hasNext())
		{
			entry = ite.next();
			MultiblockPart part = entry.getValue();
			Vector vector = entry.getKey();
			part.multiblock = this;
			part.master = this.master;
			part.isMaster = part.equals(this.master);
			boolean completed = this.isComplete();
			PacketHelper.sendToDimension(new MessageUpdateMultiblockClient(vector.x, vector.y, vector.z, completed), this.world.provider.dimensionId);
		}
	}

	public void assignMaster()
	{
		Entry<Vector, MultiblockPart> entry;
		Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
		while(ite.hasNext())
		{
			entry = ite.next();
			MultiblockPart part = entry.getValue();
			if(!part.isMaster)
			{
				part.isMaster = true;
				part.master = part;
				this.master = part;
				break;
			}
		}
		this.needsNotifying = true;
		this.needsRefresh = true;
	}

	public void combineMultiblocks(Multiblock other)
	{
		if(other == null || this.parts == null || other.parts == null)
		{
			return;
		}
		int size = this.parts.size();
		int size2 = other.parts.size();
		if(size > size2)
		{
			this.doCombineMultiblocks(other);
			return;
		}
		else if(size < size2)
		{
			other.doCombineMultiblocks(this);
			return;
		}
		else if(size == size2)
		{
			boolean choice = this.world.rand.nextBoolean();
			if(choice)
			{
				other.doCombineMultiblocks(this);
			}
			else
			{
				this.doCombineMultiblocks(other);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void doCombineMultiblocks(Multiblock other)
	{
		Set<Entry<Vector, MultiblockPart>> entrySet = other.parts.entrySet();
		Entry<Vector, MultiblockPart>[] entries = entrySet.toArray(new Entry[entrySet.size()]);
		for(int i = 0; i < entrySet.size(); ++i)
		{
			Entry<Vector, MultiblockPart> entry = entries[i];
			Vector vector = entry.getKey();
			Entry<Vector, MultiblockPart> entry2;
			Iterator<Entry<Vector, MultiblockPart>> ite2 = this.parts.entrySet().iterator();
			while(ite2.hasNext())
			{
				entry2 = ite2.next();
				Vector vector2 = entry2.getKey();
				int xDistance = vector.x - vector2.x;
				if(xDistance == 1 || xDistance == -1)
				{
					other.parts.remove(vector);
					this.addPart(vector, entry.getValue());
					break;
				}
				int yDistance = vector.y - vector2.y;
				if(yDistance == 1 || yDistance == -1)
				{
					other.parts.remove(vector);
					this.addPart(vector, entry.getValue());
					break;
				}
				int zDistance = vector.z - vector2.z;
				if(zDistance == 1 || zDistance == -1)
				{
					other.parts.remove(vector);
					this.addPart(vector, entry.getValue());
					break;
				}
			}
		}
	}

	public World getWorld()
	{
		return this.world;
	}

	public void setWorld(World world)
	{
		this.world = world;
	}

	public void setMaster(MultiblockPart master)
	{
		if(!master.getID().equals(this.type))
		{
			return;
		}
		if(this.master == null)
		{
			this.master = master;
			TileEntity tile = (TileEntity)master;
			Vector vector = new Vector(tile.xCoord, tile.yCoord, tile.zCoord);
			if(!this.hasPart(vector))
			{
				this.addPart(vector, master);
			}
		}
	}

	public boolean isComplete()
	{
		return this.state.equals(EnumMultiblockState.COMPLETE);
	}

	public void notifyAllNeighbors()
	{
		Entry<Vector, MultiblockPart> entry;
		Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
		while(ite.hasNext())
		{
			entry = ite.next();
			if(entry != null)
			{
				Vector vector = entry.getKey();
				if(vector != null)
				{
					vector.notifyNeighbors(this.world);
					PacketHelper.sendToDimension(new MessageNeighborRenderUpdate(vector.x, vector.y, vector.z), this.world.provider.dimensionId);
				}
			}
		}
	}

	public void checkStatus()
	{
		Entry<Vector, MultiblockPart> entry;
		Iterator<Entry<Vector, MultiblockPart>> ite = this.parts.entrySet().iterator();
		while(ite.hasNext())
		{
			entry = ite.next();
			if(entry == null || entry.getKey() == null || entry.getValue() == null)
			{
				this.state = EnumMultiblockState.INCOMPLETE;
				return;
			}
		}
		if(this.parts.size() == this.size.totalSize)
		{
			this.state = EnumMultiblockState.COMPLETE;
		}
		else
		{
			this.state = EnumMultiblockState.INCOMPLETE;
		}
	}

	public static enum EnumMultiblockState {

		COMPLETE, INCOMPLETE, UNKNOWN;

	}

}
