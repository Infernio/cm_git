package com.infernio.InfernoCore.multiblock;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.infernio.InfernoCore.block.BlockMetaTile;

import cpw.mods.fml.common.FMLCommonHandler;

public abstract class BlockMultiblock extends BlockMetaTile {

	protected Block block;

	public BlockMultiblock(Block block, Material material)
	{
		super(material);
		this.block = block;
	}

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int face, float hitX, float hitY, float hitZ)
    {
    	if(world.isRemote)
    	{
    		return true;
    	}
    	try
    	{
			TileEntity tile = world.getTileEntity(x, y, z);
			if(tile == null || !(tile instanceof MultiblockPart)) 
			{
				return false;
			}
			if(player.isSneaking() && player.getCurrentEquippedItem() != null)
			{
				return false;
			}
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock == null || part.master == null)
			{
				return false;
			}
			if(part.isMaster)
			{
				if(!part.multiblock.isComplete())
				{
					return false;
				}
				player.swingItem();
				this.openGUI(player, world, x, y, z);
				return true;
			}
			if(player.isSneaking() && player.getCurrentEquippedItem() == null)
			{
				MultiblockPart master = (MultiblockPart)part.master;
				this.block.onBlockActivated(world, master.xCoord, master.yCoord, master.zCoord, player, face, hitX, hitY, hitZ);
				return true;
			}
			if(player.getCurrentEquippedItem() == null)
			{
				MultiblockPart master = (MultiblockPart)part.master;
				this.block.onBlockActivated(world, master.xCoord, master.yCoord, master.zCoord, player, face, hitX, hitY, hitZ);
				return true;
			}
			MultiblockPart master = (MultiblockPart)part.master;
			this.block.onBlockActivated(world, master.xCoord, master.yCoord, master.zCoord, player, face, hitX, hitY, hitZ);
			return true;
    	}
    	catch(Throwable e)
    	{
    		return false;
    	}
    }

    @Override
	public boolean canCreatureSpawn(EnumCreatureType type, IBlockAccess world, int x, int y, int z)
    {
    	if(FMLCommonHandler.instance().getEffectiveSide().isClient())
    	{
    		return false;
    	}
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile instanceof MultiblockPart)
		{
			return ((MultiblockPart)tile).canCreatureSpawn(type);
		}
		return true;
    }

    @Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
    	if(!world.isRemote)
    	{
	    	TileEntity tile = world.getTileEntity(x, y, z);
	    	if(tile instanceof MultiblockPart)
	    	{
	    		MultiblockPart part = (MultiblockPart)tile;
	    		TileEntity tileTop = world.getTileEntity(x, y + 1, z);
	    		TileEntity tileBottom = world.getTileEntity(x, y - 1, z);
	    		TileEntity tileSide1 = world.getTileEntity(x + 1, y, z);
	    		TileEntity tileSide2 = world.getTileEntity(x - 1, y, z);
	    		TileEntity tileSide3 = world.getTileEntity(x, y, z + 1);
				TileEntity tileSide4 = world.getTileEntity(x, y, z - 1);
				if(this.canPlace(tileTop) && this.canPlace(tileBottom) && this.canPlace(tileSide1) && this.canPlace(tileSide2) && this.canPlace(tileSide3) && this.canPlace(tileSide4))
				{
		    		part.scanArea();
		    		if(part.multiblock == null)
		    		{
		    			world.notifyBlocksOfNeighborChange(x, y, z, world.getBlock(x, y, z));
		    		}
		    		else
		    		{
		    			part.multiblock.needsNotifying = true;
		    		}
		    		return;
				}
				world.getBlock(x, y, z).dropBlockAsItem(world, x, y, z, world.getBlockMetadata(x, y, z), 0);
				world.setBlockToAir(x, y, z);
	    	}
    	}
    }

    protected boolean canPlace(TileEntity tile)
    {
    	if(tile != null && tile instanceof MultiblockPart)
    	{
    		MultiblockPart part = (MultiblockPart)tile;
    		if(part.multiblock != null)
    		{
	    		part.multiblock.rescan();
    			return !part.multiblock.isComplete();
    		}
    		return true;
    	}
    	return true;
    }

    @Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
    {
    	MultiblockPart tile = (MultiblockPart)world.getTileEntity(x, y, z);
    	if(tile != null && tile.multiblock != null)
    	{
    		tile.multiblock.removePart(tile.vector);
    	}
    	super.breakBlock(world, x, y, z, block, meta);
    }
}
