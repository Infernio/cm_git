package com.infernio.InfernoCore.multiblock;

import net.minecraft.entity.EnumCreatureType;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

import com.infernio.InfernoCore.math.Vector;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public abstract class MultiblockPart extends TileEntity {

	public MultiblockPart master;
	public Multiblock multiblock;
	public Vector vector;
	public boolean isMaster;
	@SideOnly(Side.CLIENT)
	public boolean clientMaster;
	@SideOnly(Side.CLIENT)
	public boolean clientCompleted;
	private boolean createMultiblock;
	private int ticksSinceRestoration;
	private int ticksSinceSync;
	private int ticksSinceCombining;
	private int ticksSinceAssignment;
	private int ticksSinceScan;

	@Override
	public void updateEntity()
	{
		if(this.worldObj.isRemote)
		{
			return;
		}
		if(this.vector == null)
		{
			this.vector = new Vector(this.xCoord, this.yCoord, this.zCoord);
		}
		if(this.createMultiblock && this.multiblock == null)
		{
			this.multiblock = new Multiblock(this.getID(), this.getSize(), this.worldObj);
			this.multiblock.setMaster(this);
			this.master = this;
			this.isMaster = true;
			this.createMultiblock = false;
		}
		if(this.multiblock != null)
		{
			if(this.isMaster)
			{
				this.multiblock.tick();
				return;
			}
			if(this.master == null)
			{
				this.multiblock.needsNewMaster = true;
				if(this.master != null)
				{
					this.ticksSinceRestoration = 0;
				}
				else
				{
					if(this.ticksSinceRestoration < 100)
					{
						++this.ticksSinceRestoration;
					}
					else
					{
						this.ticksSinceRestoration = 0;
						this.multiblock.assignMaster();
						this.multiblock.refresh();
					}
				}
			}
			if(!this.multiblock.isComplete())
			{
				if(this.ticksSinceCombining < 50)
				{
					++this.ticksSinceCombining;
				}
				else
				{
					this.ticksSinceCombining = 0;
					this.combineMultiblocks();
				}
			}
			if(!this.isMaster)
			{
				if(this.ticksSinceSync < 10)
				{
					++this.ticksSinceSync;
				}
				else
				{
					this.ticksSinceSync = 0;
					this.receiveInformation();
				}
			}
			return;
		}
		else
		{
			if(this.ticksSinceScan < 30)
			{
				++this.ticksSinceScan;
			}
			else
			{
				this.ticksSinceScan = 0;
				this.scanArea();
			}
			if(this.ticksSinceAssignment < 100)
			{
				++this.ticksSinceAssignment;
				return;
			}
			this.ticksSinceAssignment = 0;
			MultiblockSize size = this.getSize();
			this.multiblock = new Multiblock(this.getID(), size, this.worldObj);
			this.multiblock.setMaster(this);
		}
	}

	public void scanArea()
	{
		if(this.vector == null)
		{
			this.vector = new Vector(this.xCoord, this.yCoord, this.zCoord);
		}
		TileEntity tile = this.worldObj.getTileEntity(this.xCoord + 1, this.yCoord, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()))
			{
				if(part.multiblock.addPart(this.vector, this))
				{
					return;
				}
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord - 1, this.yCoord, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()))
			{
				if(part.multiblock.addPart(this.vector, this))
				{
					return;
				}
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord + 1, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()))
			{
				if(part.multiblock.addPart(this.vector, this))
				{
					return;
				}
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord - 1, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()))
			{
				if(part.multiblock.addPart(this.vector, this))
				{
					return;
				}
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord + 1);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()))
			{
				if(part.multiblock.addPart(this.vector, this))
				{
					return;
				}
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord - 1);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()))
			{
				if(part.multiblock.addPart(this.vector, this))
				{
					return;
				}
			}
		}
	}

	public void combineMultiblocks()
	{
		TileEntity tile = this.worldObj.getTileEntity(this.xCoord + 1, this.yCoord, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()) && !part.multiblock.equals(this.multiblock));
			{
				this.multiblock.combineMultiblocks(part.multiblock);
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord - 1, this.yCoord, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()) && !part.multiblock.equals(this.multiblock));
			{
				this.multiblock.combineMultiblocks(part.multiblock);
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord + 1, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()) && !part.multiblock.equals(this.multiblock));
			{
				this.multiblock.combineMultiblocks(part.multiblock);
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord - 1, this.zCoord);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()) && !part.multiblock.equals(this.multiblock));
			{
				this.multiblock.combineMultiblocks(part.multiblock);
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord + 1);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()) && !part.multiblock.equals(this.multiblock));
			{
				this.multiblock.combineMultiblocks(part.multiblock);
			}
		}
		tile = this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord - 1);
		if(tile instanceof MultiblockPart)
		{
			MultiblockPart part = (MultiblockPart)tile;
			if(part.multiblock != null && part.getID().equals(this.getID()) && !part.multiblock.equals(this.multiblock));
			{
				this.multiblock.combineMultiblocks(part.multiblock);
			}
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			return;
		}
		super.readFromNBT(tag);
		this.readMBFromNBT(tag);
		this.ticksSinceScan = tag.getShort("ScanTicks");
		this.ticksSinceAssignment = tag.getShort("AssignmentTicks");
		this.ticksSinceCombining = tag.getShort("CominingTicks");
		this.ticksSinceRestoration = tag.getShort("RestorationTicks");
		this.ticksSinceSync = tag.getShort("SyncTicks");
		this.isMaster = tag.getBoolean("Master");
		if(this.isMaster && this.multiblock != null)
		{
			this.createMultiblock = true;
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			return;
		}
		super.writeToNBT(tag);
		this.writeMBToNBT(tag);
		tag.setShort("ScanTicks", (short)this.ticksSinceScan);
		tag.setShort("AssignmentTicks", (short)this.ticksSinceAssignment);
		tag.setShort("CominingTicks", (short)this.ticksSinceCombining);
		tag.setShort("RestorationTicks", (short)this.ticksSinceRestoration);
		tag.setShort("SyncTicks", (short)this.ticksSinceSync);
		tag.setBoolean("Master", this.isMaster);
	}

	protected void receiveInformation()
	{
		if(this.master != null && !this.isMaster && !this.equals(this.master))
		{
			NBTTagCompound tag = new NBTTagCompound();
			this.master.writeMBToNBT(tag);
			this.readMBFromNBT(tag);
		}
	}

	public boolean canCreatureSpawn(EnumCreatureType type)
	{
		return this.multiblock == null ? true : this.multiblock.isComplete();
	}

	public abstract String getID();

	public abstract MultiblockSize getSize();

	protected abstract void readMBFromNBT(NBTTagCompound tag);

	protected abstract void writeMBToNBT(NBTTagCompound tag);

}
