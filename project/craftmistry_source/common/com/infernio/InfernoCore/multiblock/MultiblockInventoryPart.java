package com.infernio.InfernoCore.multiblock;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.inventory.ISidedInventory;

import com.infernio.Craftmistry.core.helper.ItemHelper;
import com.infernio.InfernoCore.util.NBTLib;

import cpw.mods.fml.common.FMLCommonHandler;

public abstract class MultiblockInventoryPart extends MultiblockPart implements ISidedInventory {

	protected ItemStack[] inv;
	protected final int[] ALL_SLOTS;
	protected static final int[] NO_SLOTS = new int[]{};

	public MultiblockInventoryPart(int inventorySize)
	{
		this.inv = new ItemStack[inventorySize];
		this.ALL_SLOTS = new int[inventorySize];
		for(int i = 0; i < this.ALL_SLOTS.length; ++i)
		{
			this.ALL_SLOTS[i] = i;
		}
	}

	@Override
	public int getSizeInventory()
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				return this.inv.length;
			}
			return 0;
		}
		if(this.isMaster)
		{
			if(!this.multiblock.isComplete())
			{
				return 0;
			}
			return this.inv.length;
		}
		return this.master == null ? 0 : ((MultiblockInventoryPart)this.master).getSizeInventory();
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				return this.inv[slot];
			}
			return null;
		}
		if(this.isMaster)
		{
			if(!this.multiblock.isComplete())
			{
				return null;
			}
			return this.inv[slot];
		}
		return this.master == null ? null : ((MultiblockInventoryPart)this.master).getStackInSlot(slot);
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount)
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				ItemStack stack = getStackInSlot(slot);
				if(stack != null) 
				{
					if(stack.stackSize <= amount) 
					{
						this.setInventorySlotContents(slot, null);
					}
					else
					{
						stack = stack.splitStack(amount);
						if(stack.stackSize <= 0)
						{
							this.setInventorySlotContents(slot, null);
						}
					}
				}
				return stack;
			}
			return null;
		}
		if(this.isMaster)
		{
			if(!this.multiblock.isComplete())
			{
				return null;
			}
			ItemStack stack = getStackInSlot(slot);
			if(stack != null) 
			{
				if(stack.stackSize <= amount) 
				{
					this.setInventorySlotContents(slot, null);
				}
				else
				{
					stack = stack.splitStack(amount);
					if(stack.stackSize <= 0)
					{
						this.setInventorySlotContents(slot, null);
					}
				}
			}
			return stack;
		}
		return this.master == null ? null : ((MultiblockInventoryPart)this.master).decrStackSize(slot, amount);
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				ItemStack stack = getStackInSlot(slot);
				if (stack != null) 
				{
					setInventorySlotContents(slot, null);
				}
				return stack;
			}
			return null;
		}
		if(this.isMaster)
		{
			ItemStack stack = getStackInSlot(slot);
			if (stack != null) 
			{
				setInventorySlotContents(slot, null);
			}
			return stack;
		}
		return this.master == null ? null : ((MultiblockInventoryPart)this.master).getStackInSlotOnClosing(slot);
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack)
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				this.inv[slot] = stack;
			}
			return;
		}
		if(this.isMaster)
		{
			this.inv[slot] = stack;
			return;
		}
		if(this.master != null)
		{
			((MultiblockInventoryPart)this.master).setInventorySlotContents(slot, stack);
		}
	}

	@Override
	public String getInventoryName()
	{
		return "container." + this.getID();
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return false;
	}

	@Override
	public int getInventoryStackLimit()
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				return 64;
			}
			return 0;
		}
		if(this.isMaster)
		{
			if(!this.multiblock.isComplete())
			{
				return 0;
			}
			return 64;
		}
		return this.master == null ? 0 : ((MultiblockInventoryPart)this.master).getInventoryStackLimit();
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this && player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
			}
			return false;
		}
		if(this.isMaster)
		{
			if(!this.multiblock.isComplete())
			{
				return false;
			}
			return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this && player.getDistanceSq(xCoord + 0.5, yCoord + 0.5, zCoord + 0.5) < 64;
		}
		return this.master == null ? false : ((MultiblockInventoryPart)this.master).isUseableByPlayer(player);
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack stack)
	{
		if(this.worldObj.isRemote)
		{
			if(this.clientMaster)
			{
				return ItemHelper.isTestTubeAndFilled(stack);
			}
			return false;
		}
		if(this.isMaster)
		{
			if(!this.multiblock.isComplete())
			{
				return false;
			}
			return ItemHelper.isTestTubeAndFilled(stack);
		}
		return this.master == null ? false : ((MultiblockInventoryPart)this.master).isItemValidForSlot(slot, stack);
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		if(this.isMaster)
		{
			if(this.multiblock == null || !this.multiblock.isComplete())
			{
				return NO_SLOTS;
			}
			return this.ALL_SLOTS;
		}
		return this.master == null ? NO_SLOTS : ((MultiblockInventoryPart)this.master).getAccessibleSlotsFromSide(side);
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack stack, int side)
	{
		return true;
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack stack, int side)
	{
		return true;
	}

	@Override
	protected void writeMBToNBT(NBTTagCompound tag)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			return;
		}
		NBTTagList list = new NBTTagList();
		for(int i = 0; i < inv.length; i++)
		{
			ItemStack stack = inv[i];
			if(stack != null)
			{
				NBTTagCompound slot = new NBTTagCompound();
				slot.setByte("Slot", (byte)i);
				stack.writeToNBT(slot);
				list.appendTag(slot);
			}
		}
		tag.setTag("Inventory", list);
	}

	@Override
	protected void readMBFromNBT(NBTTagCompound tag)
	{
		if(FMLCommonHandler.instance().getEffectiveSide().isClient())
		{
			return;
		}
		NBTTagList list = tag.getTagList("Inventory", NBTLib.NBT_COMPOUND);
		for(int i = 0; i < list.tagCount(); i++)
		{
			NBTTagCompound slot = list.getCompoundTagAt(i);
			byte b = slot.getByte("Slot");
			if(b >= 0 && b < inv.length)
			{
				inv[b] = ItemStack.loadItemStackFromNBT(slot);
			}
		}
	}

}
