package com.infernio.InfernoCore.event;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

import com.infernio.InfernoCore.InfernoCore;
import com.infernio.InfernoCore.client.icon.IconRegistry;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.GameRegistry.UniqueIdentifier;

public class ClientEventListener {

	@SubscribeEvent
	public void loadTextures(TextureStitchEvent.Pre event)
	{
		IconRegistry.getInstance().loadIcons(event.map);
	}

	@SubscribeEvent
	public void postLoadTextures(TextureStitchEvent.Post event)
	{
		IconRegistry.getInstance().postLoadIcons(event.map);
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void handleTooltips(ItemTooltipEvent event)
	{
		if(InfernoCore.showRawNames)
		{
			UniqueIdentifier uid = GameRegistry.findUniqueIdentifierFor(event.itemStack.getItem());
			if(uid != null)
			{
				event.toolTip.add(EnumChatFormatting.BLUE + uid.modId);
				if(GuiScreen.isShiftKeyDown())
				{
					event.toolTip.add(EnumChatFormatting.YELLOW + "" + EnumChatFormatting.ITALIC + uid.modId + ":" + uid.name);
				}
			}
		}
	}

}
