package com.infernio.InfernoCore.collect;

public interface IFilter<E> {

	boolean accept(E element);

}
