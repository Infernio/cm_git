package com.infernio.InfernoCore.collect;

import java.util.Set;

public interface IFilteredSet<E> extends Set<E> {

}
