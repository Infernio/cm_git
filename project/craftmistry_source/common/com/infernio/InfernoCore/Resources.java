package com.infernio.InfernoCore;

public class Resources {

	public static final String MODID = "InfernoCore";
	public static final String NAME = "Inferno Core";
	public static final String CHANNEL = "InfernoCore";
	public static final String VERSION_PREFIX = "A"; //Changes from A to B to R when state of development changes (Alpha, Beta, Release)
	public static final String VESION_RAW = "0"; //Incremented in case of major becoming 10 or VERY big changes
	public static final String VERSION_MAJOR = "1"; //Incremented in case of big changes (MC update, API rewrite etc.)
	public static final String VERSION_MINOR = "4"; //Incremented in case of medium changes (e.g. Function addition)
	public static final String VERSION = VERSION_PREFIX + VESION_RAW + "." + VERSION_MAJOR + "." + VERSION_MINOR;
	public static final String DEPENDENCIES = "required-after:Forge@[9.11.0.879,)";
	public static final int MIN_BLOCK_ID = 0;
    public static final int MAX_BLOCK_ID = 4095;
    public static final int MIN_ITEM_ID = 4096;
    public static final int MAX_ITEM_ID = 31999;
}
