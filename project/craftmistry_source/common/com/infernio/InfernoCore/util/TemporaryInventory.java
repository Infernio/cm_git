package com.infernio.InfernoCore.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class TemporaryInventory implements IInventory {

	private int start;
	private int length;
	private ItemStack[] originalInventory;

	public static TemporaryInventory wrap(ItemStack... inv)
	{
		return new TemporaryInventory(inv);
	}

	public static TemporaryInventory wrap(int length, ItemStack... inv)
	{
		TemporaryInventory ret = new TemporaryInventory(inv);
		ret.length = length;
		return ret;
	}

	public static TemporaryInventory wrap(int start, int length, ItemStack... inv)
	{
		TemporaryInventory ret = new TemporaryInventory(inv);
		ret.length = length;
		ret.start = start;
		return ret;
	}

	public TemporaryInventory(ItemStack... inv)
	{
		this.originalInventory = inv;
	}

	@Override
	public int getSizeInventory()
	{
        return this.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
        return this.originalInventory[this.start + slot];
	}

	@Override
	public ItemStack decrStackSize(int slot, int amount)
	{
		slot += this.start;
		ItemStack stack = this.getStackInSlot(slot);
		if(stack != null) 
		{
			if(stack.stackSize <= amount) 
			{
				this.setInventorySlotContents(slot, null);
			}
			else
			{
				stack = stack.splitStack(amount);
				if(stack.stackSize <= 0)
				{
					this.setInventorySlotContents(slot, null);
				}
			}
		}
		return stack;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		slot += this.start;
		ItemStack stack = this.getStackInSlot(slot);
    	if(stack != null)
    	{
    		this.setInventorySlotContents(slot, null);
    	}
    	return stack;
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack stack)
	{
		slot += this.start;
		this.originalInventory[slot] = stack;
        if(stack != null && stack.stackSize > this.getInventoryStackLimit())
        {
            stack.stackSize = this.getInventoryStackLimit();
        }
	}

	@Override
	public String getInventoryName()
	{
		return "TEMPORARY_INVENTORY";
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return false;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public void markDirty() {}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return true;
	}

	@Override
	public void openInventory() {}

	@Override
	public void closeInventory() {}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack stack)
	{
		return true;
	}
}
