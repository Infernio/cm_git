package com.infernio.InfernoCore.util;

import java.util.ArrayList;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ItemUtils {

	public static String getItemKey(String itemName)
	{
		return "item." + itemName + ".name";
	}

	public static ItemStack addItemStack(ItemStack stack, IInventory inv)
	{
		if(stack != null && stack.getItem() != null)
		{
			if(!stack.getItem().getHasSubtypes())
			{
				if(stack.getItem().isItemTool(stack))
				{
					int slot = findEmptySlot(inv);
					if(slot > 0)
					{
						inv.setInventorySlotContents(slot, stack);
						return null;
					}
				}
				else
				{
					int slot = findMatchingSlot(inv, stack.getItem(), 0);
					if(slot >= 0)
					{
						ItemStack stack2 = inv.getStackInSlot(slot);
						while(stack2.stackSize < stack2.getMaxStackSize() && stack.stackSize > 0)
						{
							--stack.stackSize;
							++stack2.stackSize;
						}
					}
					else
					{
						slot = findEmptySlot(inv);
						if(slot >= 0)
						{
							inv.setInventorySlotContents(slot, stack);
							return null;
						}
					}
				}
				return stack;
			}
			else
			{
				int meta = stack.getItemDamage();
				int slot = findMatchingSlot(inv, stack.getItem(), meta);
				if(slot >= 0)
				{
					ItemStack stack2 = inv.getStackInSlot(slot);
					while(stack2.stackSize < stack2.getMaxStackSize() && stack.stackSize > 0)
					{
						--stack.stackSize;
						++stack2.stackSize;
					}
					return stack;
				}
				else
				{
					slot = findEmptySlot(inv);
					if(slot >= 0)
					{
						inv.setInventorySlotContents(slot, stack);
						return null;
					}
				}
			}
		}
		return stack;
	}

	public static int findEmptySlot(IInventory inv)
	{
		for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
		{
			if(inv.getStackInSlot(slot) == null)
			{
				return slot;
			}
		}
		return -1;
	}

	public static int findMatchingSlot(IInventory inv, Item item, int meta)
	{
		for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
		{
			ItemStack stack = inv.getStackInSlot(slot);
			if(stack != null && stack.getItem() != null && stack.getItem() == item && stack.getItemDamage() == meta && stack.stackSize < inv.getInventoryStackLimit())
			{
				return slot;
			}
		}
		return -1;
	}

	public static boolean areItemStacksEqual(ItemStack stack1, ItemStack stack2, boolean compareStackSize)
	{
		if(stack1 == null && stack2 == null)
		{
			return true;
		}
		else if((stack1 == null) != (stack2 == null))
		{
			return false;
		}
		else if(stack1.getItem() == null && stack2.getItem() == null)
		{
			return true;
		}
		else if((stack1.getItem() == null) != (stack2.getItem() == null))
		{
			return false;
		}
		else if(stack1.getHasSubtypes() != stack2.getHasSubtypes())
		{
			return false;
		}
		else
		{
			if(stack1.getHasSubtypes())
			{
				if(stack1.getItem() == stack2.getItem() && stack1.getItemDamage() == stack2.getItemDamage())
				{
					if(compareStackSize)
					{
						return stack1.stackSize == stack2.stackSize;
					}
					return true;
				}
			}
			else
			{
				if(stack1.getItem() == stack2.getItem())
				{
					if(compareStackSize)
					{
						return stack1.stackSize == stack2.stackSize;
					}
					return true;
				}
			}
		}
		return false;
	}

	public static boolean areItemStacksEqualNBT(ItemStack stack1, ItemStack stack2, boolean compareStackSize)
	{
		if(stack1 == null && stack2 == null)
		{
			return true;
		}
		else if((stack1 == null) != (stack2 == null))
		{
			return false;
		}
		else if(stack1.getItem() == null && stack2.getItem() == null)
		{
			return true;
		}
		else if((stack1.getItem() == null) != (stack2.getItem() == null))
		{
			return false;
		}
		else if(stack1.getHasSubtypes() != stack2.getHasSubtypes())
		{
			return false;
		}
		else
		{
			if(stack1.getHasSubtypes())
			{
				if(stack1.getItem() == stack2.getItem() && stack1.getItemDamage() == stack2.getItemDamage())
				{
					if(areNBTTagsEqual(stack1.getTagCompound(), stack2.getTagCompound()))
					{
						if(compareStackSize)
						{
							return stack1.stackSize == stack2.stackSize;
						}
						return true;
					}
				}
			}
			else
			{
				if(stack1.getItem() == stack2.getItem())
				{
					if(areNBTTagsEqual(stack1.getTagCompound(), stack2.getTagCompound()))
					{
						if(compareStackSize)
						{
							return stack1.stackSize == stack2.stackSize;
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean areNBTTagsEqual(NBTTagCompound tag1, NBTTagCompound tag2)
	{
		if(tag1 == null && tag2 == null)
		{
			return true;
		}
		else if((tag1 == null) != (tag2 == null))
		{
			return false;
		}
		else
		{
			return tag1.equals(tag2);
		}
	}

	public static ArrayList<ItemStack> findItems(IInventory inv, Item item, int meta)
	{
		ArrayList<ItemStack> ret = new ArrayList<ItemStack>();
		ItemStack stack1 = new ItemStack(item, 1, meta);
		for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
		{
			ItemStack stack = inv.getStackInSlot(slot);
			if(areItemStacksEqual(stack, stack1, false))
			{
				ret.add(stack);
			}
		}
		return ret;
	}

	public static ArrayList<Integer> findItemSlots(IInventory inv, Item item, int meta)
	{
		ArrayList<Integer> ret = new ArrayList<Integer>();
		ItemStack stack1 = new ItemStack(item, 1, meta);
		for(int slot = 0; slot < inv.getSizeInventory(); ++slot)
		{
			ItemStack stack = inv.getStackInSlot(slot);
			if(areItemStacksEqual(stack, stack1, false))
			{
				ret.add(slot);
			}
		}
		return ret;
	}

	public static void dropItem(World world, double x, double y, double z, ItemStack stack)
	{
		if(!world.isRemote)
		{
			EntityItem item = new EntityItem(world, x, y, z, stack);
			item.delayBeforeCanPickup = 10;
			world.spawnEntityInWorld(item);
		}
	}

	public static void dropStacks(World world, double x, double y, double z, Item item, int meta, int amount)
	{
		dropStacks(world, x, y, z, item, meta, amount, 64);
	}

	public static void dropStacks(World world, double x, double y, double z, Item item, int meta, int amount, int maxStackSize)
	{
		if(amount < maxStackSize)
		{
			dropItem(world, x, y, z, new ItemStack(item, amount, meta));
			return;
		}
		else
		{
			int numStacks = amount / maxStackSize;
			int left = amount % maxStackSize;
			for(int i = 0; i < numStacks; ++i)
			{
				dropItem(world, x, y, z, new ItemStack(item, maxStackSize, meta));
			}
			if(left > 0)
			{
				dropItem(world, x, y, z, new ItemStack(item, left, meta));
			}
		}
	}

}
