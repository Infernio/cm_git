package com.infernio.InfernoCore.util;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.MathHelper;

public class PotionUtils {

	private static ArrayList<PotionEffect> negativePotions = new ArrayList<PotionEffect>();
	private static ArrayList<PotionEffect> positivePotions = new ArrayList<PotionEffect>();
	private static ArrayList<PotionEffect> allPotions = new ArrayList<PotionEffect>();

	public static PotionEffect getRandomEffect(Random rand, int duration)
	{
		PotionEffect ret = allPotions.get(rand.nextInt(allPotions.size() - 1));
		return new PotionEffect(ret.getPotionID(), duration, ret.getAmplifier(), ret.getIsAmbient());
	}

	public static PotionEffect getRandomNegativeEffect(Random rand, int duration)
	{
		PotionEffect ret = negativePotions.get(rand.nextInt(negativePotions.size() - 1));
		return new PotionEffect(ret.getPotionID(), duration, ret.getAmplifier(), ret.getIsAmbient());
	}

	public static PotionEffect getRandomPositiveEffect(Random rand, int duration)
	{
		PotionEffect ret = positivePotions.get(rand.nextInt(positivePotions.size() - 1));
		return new PotionEffect(ret.getPotionID(), duration, ret.getAmplifier(), ret.getIsAmbient());
	}

	public static void improveEffect(PotionEffect effect)
	{
		if(effect == null)
		{
			return;
		}
		int id = effect.getPotionID();
		int duration = effect.getDuration();
		int finalDuration = 0;
		int amplifier = effect.getAmplifier();
		boolean isAmbient = effect.getIsAmbient();
		if(id == Potion.blindness.id || id == Potion.confusion.id || id == Potion.nightVision.id)
		{
			finalDuration = MathHelper.clamp_int(duration, 0, 200);
		}
		else if(id == Potion.harm.id || id == Potion.heal.id)
		{
			finalDuration = 0;
		}
		else if(id == Potion.hunger.id || id == Potion.field_76443_y.id)
		{
			finalDuration = MathHelper.clamp_int(duration, 0, 500);
		}
		else if(id == Potion.poison.id || id == Potion.regeneration.id)
		{
			finalDuration = MathHelper.clamp_int(duration, 0, 350);
			return;
		}
		else
		{
			finalDuration = MathHelper.clamp_int(duration, 0, 500);
		}
		effect = new PotionEffect(id, finalDuration, amplifier, isAmbient);
	}

	static
	{
		positivePotions.add(new PotionEffect(Potion.damageBoost.id, 0));
		positivePotions.add(new PotionEffect(Potion.digSpeed.id, 0));
		positivePotions.add(new PotionEffect(Potion.field_76434_w.id, 0));
		positivePotions.add(new PotionEffect(Potion.field_76444_x.id, 0));
		positivePotions.add(new PotionEffect(Potion.field_76443_y.id, 0));
		positivePotions.add(new PotionEffect(Potion.fireResistance.id, 0));
		positivePotions.add(new PotionEffect(Potion.heal.id, 0));
		positivePotions.add(new PotionEffect(Potion.invisibility.id, 0));
		positivePotions.add(new PotionEffect(Potion.jump.id, 0));
		positivePotions.add(new PotionEffect(Potion.moveSpeed.id, 0));
		positivePotions.add(new PotionEffect(Potion.nightVision.id, 0));
		positivePotions.add(new PotionEffect(Potion.regeneration.id, 0));
		positivePotions.add(new PotionEffect(Potion.resistance.id, 0));
		positivePotions.add(new PotionEffect(Potion.waterBreathing.id, 0));
		negativePotions.add(new PotionEffect(Potion.blindness.id, 0));
		negativePotions.add(new PotionEffect(Potion.confusion.id, 0));
		negativePotions.add(new PotionEffect(Potion.digSlowdown.id, 0));
		negativePotions.add(new PotionEffect(Potion.harm.id, 0));
		negativePotions.add(new PotionEffect(Potion.hunger.id, 0));
		negativePotions.add(new PotionEffect(Potion.moveSlowdown.id, 0));
		negativePotions.add(new PotionEffect(Potion.poison.id, 0));
		negativePotions.add(new PotionEffect(Potion.weakness.id, 0));
		negativePotions.add(new PotionEffect(Potion.wither.id, 0));
		allPotions.addAll(positivePotions);
		allPotions.addAll(negativePotions);
	}

}
