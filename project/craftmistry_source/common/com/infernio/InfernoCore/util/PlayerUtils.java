package com.infernio.InfernoCore.util;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import cpw.mods.fml.common.ObfuscationReflectionHelper;

public class PlayerUtils {

	public static int getSlotForStack(InventoryPlayer inv, Item item, int meta)
    {
		for(int i = 0; i < inv.getSizeInventory(); ++i)
		{
			ItemStack stack = inv.getStackInSlot(i);
			if(stack != null && stack.getItem() == item && stack.getItemDamage() == meta)
			{
				return i;
			}
		}
		return -1;
    }

	public static boolean consumeInventoryItemWithMeta(InventoryPlayer inv, Item item, int meta)
	{
		int slot = getSlotForStack(inv, item, meta);
		if(slot < 0 || slot > inv.getSizeInventory())
		{
			return false;
		}
		else
		{
			ItemStack stack = inv.getStackInSlot(slot);
			if(stack != null)
			{
				--stack.stackSize;
				if(stack.stackSize <= 0)
				{
					inv.setInventorySlotContents(slot, (ItemStack)null);
				}
				return true;
			}
		}
		return false;
	}

	public static int getSlotForStackSize(InventoryPlayer inv, Item item, int meta, int amount)
    {
		for(int i = 0; i < inv.getSizeInventory(); ++i)
		{
			ItemStack stack = inv.getStackInSlot(i);
			if(stack != null && stack.getItem() == item && stack.getItemDamage() == meta && stack.stackSize >= amount)
			{
				return i;
			}
		}
		return -1;
    }

	public static boolean consumeInventoryItemsWithMeta(InventoryPlayer inv, Item item, int meta, int amount)
	{
		int slot = getSlotForStack(inv, item, meta);
		if(slot < 0 || slot > inv.getSizeInventory())
		{
			return false;
		}
		else
		{
			ItemStack stack = inv.getStackInSlot(slot);
			if(stack != null)
			{
				stack.stackSize -= amount;
				if(stack.stackSize <= 0)
				{
					inv.setInventorySlotContents(slot, (ItemStack)null);
				}
				return true;
			}
		}
		return false;
	}

	public static void giveItem(ItemStack item, World world, EntityPlayer player)
	{
		if(!player.inventory.addItemStackToInventory(item) && !world.isRemote)
		{
			world.spawnEntityInWorld(new EntityItem(world, player.posX, player.posY, player.posZ, item));
		}
	}

	public static MovingObjectPosition getFromPlayer(World world, EntityPlayer player, boolean ignoreHitbox)
	{
        float f = 1.0F;
        float f1 = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * f;
        float f2 = player.prevRotationYaw + (player.rotationYaw - player.prevRotationYaw) * f;
        double d0 = player.prevPosX + (player.posX - player.prevPosX) * (double)f;
        double d1 = player.prevPosY + (player.posY - player.prevPosY) * (double)f + 1.62D - (double)player.yOffset;
        double d2 = player.prevPosZ + (player.posZ - player.prevPosZ) * (double)f;
        Vec3 vec3 = world.getWorldVec3Pool().getVecFromPool(d0, d1, d2);
        float f3 = MathHelper.cos(-f2 * 0.017453292F - (float)Math.PI);
        float f4 = MathHelper.sin(-f2 * 0.017453292F - (float)Math.PI);
        float f5 = -MathHelper.cos(-f1 * 0.017453292F);
        float f6 = MathHelper.sin(-f1 * 0.017453292F);
        float f7 = f4 * f5;
        float f8 = f3 * f5;
        double d3 = 5.0D;
        if (player instanceof EntityPlayerMP)
        {
            d3 = ((EntityPlayerMP)player).theItemInWorldManager.getBlockReachDistance();
        }
        Vec3 vec31 = vec3.addVector((double)f7 * d3, (double)f6 * d3, (double)f8 * d3);
        return world.func_147447_a(vec3, vec31, ignoreHitbox, !ignoreHitbox, false);
	}


	public static boolean isPlayerJumping(EntityPlayer player)
	{
		Object ret = ObfuscationReflectionHelper.getPrivateValue(EntityLivingBase.class, player, 41);
		if(ret != null && ret instanceof Boolean)
		{
			return ((Boolean)ret).booleanValue();
		}
		return false;
	}

	public static int getPlayerFireTime(EntityPlayer player)
	{
		Object ret = ObfuscationReflectionHelper.getPrivateValue(Entity.class, player, 48);
		if(ret != null && ret instanceof Integer)
		{
			return ((Integer)ret).intValue();
		}
		return 0;
	}

}
