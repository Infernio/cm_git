package com.infernio.InfernoCore.util;

public class FluidUtils {

	public static String getFluidKey(String fluidName)
	{
		return fluidName.startsWith("fluid.") ? fluidName : "fluid." + fluidName;
	}

}
