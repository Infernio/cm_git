package com.infernio.InfernoCore.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import com.infernio.InfernoCore.InfernoCore;

public class IOUtils {

	public static String getCurrentLocation()
	{
		return new File("").getAbsolutePath();
	}

	public static boolean getBoolean(String dataLocation, String fileName, String bool)
	{
		try
		{
			File dir = new File(IOUtils.getCurrentLocation() + File.separator + "mods" + File.separator + dataLocation);
			dir.mkdirs();
			File file = new File(dir.getAbsolutePath() + File.separator + fileName);
			if(!file.exists())
			{
				file.createNewFile();
				return false;
			}
			Scanner s = new Scanner(file);
			String line;
			if(!s.hasNext())
			{
				s.close();
				return false;
			}
			while(s.hasNextLine())
			{
				line = s.nextLine().trim();
				if(line.startsWith(bool))
				{
					try
					{
						Boolean b = Boolean.parseBoolean(line.substring(line.indexOf("=")));
						if(b != null)
						{
							s.close();
							return b.booleanValue();
						}
						s.close();
						return false;
					}
					catch(Throwable e)
					{
						s.close();
						return false;
					}
				}
			}
			s.close();
			return false;
		}
		catch(IOException e)
		{
			return false;
		}
	}

	public static void setBoolean(String dataLocation, String fileName, String bool, boolean value)
	{
		try
		{
			File dir = new File(IOUtils.getCurrentLocation() + File.separator + "mods" + File.separator + dataLocation);
			dir.mkdirs();
			File file = new File(dir.getAbsolutePath() + File.separator + fileName);
			if(!file.exists())
			{
				file.createNewFile();
				return;
			}
			if(!file.canWrite())
			{
				if(!file.setWritable(true))
				{
					InfernoCore.log.warn("InfernoCore was denied write permission for file '" + fileName + "'.");
					return;
				}
			}
			clearFile(file);
			FileWriter fw = new FileWriter(file);
			BufferedWriter writer = new BufferedWriter(fw);
			writer.write(bool + "=" + value);
			writer.close();
		}
		catch(IOException e)
		{
			InfernoCore.log.warn("Failed to set boolean '" + bool + "' to value '" + value + "' in file '" + fileName + "'.");
			e.printStackTrace();
		}
	}

	public static void clearFile(File file) throws IOException
	{
		file.delete();
		file.createNewFile();
	}

}
