package com.infernio.InfernoCore.math;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

import com.infernio.InfernoCore.world.WorldUtils;

public class Vector implements Comparable<Vector> {

	public int x;
	public int y;
	public int z;

	public Vector(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public boolean isChunkLoaded(World world)
	{
		return world.getChunkProvider().chunkExists(this.x, this.z);
	}

	public boolean blockExists(World world)
	{
		return world.blockExists(this.x, this.y, this.z);
	}

	public TileEntity getTile(World world)
	{
		return world.getTileEntity(this.x, this.y, this.z);
	}

	public Block getBlock(World world)
	{
		return world.getBlock(this.x, this.y, this.z);
	}

	public int getMeta(World world)
	{
		return world.getBlockMetadata(this.x, this.y, this.z);
	}

	public Material getMaterial(World world)
	{
		return WorldUtils.getMaterial(world, this.x, this.y, this.z);
	}

	public void notifyNeighbors(World world)
	{
		world.notifyBlocksOfNeighborChange(this.x, this.y, this.z, this.getBlock(world));
	}

	public void markNeighborsRenderUpdate(World world)
	{
		WorldUtils.markNeighborsRenderUpdate(world, this.x, this.y, this.z);
	}

	public int distanceTo(Vector to)
	{
		return MathUtils.distance(this, to);
	}

	public int distanceFrom(Vector from)
	{
		return MathUtils.distance(from, this);
	}

	public Vector2D toVector2D()
	{
		return new Vector2D(this.x, this.y);
	}

	public Vec3 toVec3()
	{
		return Vec3.createVectorHelper(this.x, this.y, this.z);
	}

	public DoubleVector toDoubleVector()
	{
		return new DoubleVector(this.x, this.y, this.z);
	}

	@Override
	public int compareTo(Vector vec)
	{
		int ret = 0;
		if(vec.x < this.x)
		{
			ret = -1;
		}
		else if(vec.x > this.x)
		{
			ret = 1;
		}
		if(ret == 0)
		{
			if(vec.y < this.y)
			{
				ret = -1;
			}
			else if(vec.y > this.y)
			{
				ret = 1;
			}
		}
		if(ret == 0)
		{
			if(vec.z < this.z)
			{
				ret = -1;
			}
			else if(vec.z > this.z)
			{
				ret = 1;
			}
		}
		return ret;
	}


	@Override
	public int hashCode()
	{
		int ret = 0;
		ret += 31 * this.x;
		ret += 31 * this.y;
		ret += 31 * this.z;
		return ret;
	}

	@Override
	public boolean equals(Object other)
	{
		if(!(other instanceof Vector))
		{
			return false;
		}
		Vector vector = (Vector)other;
		return vector.x == this.x && vector.y == this.y && vector.z == this.z;
	}

	@Override
	public String toString()
	{
		return "vec3D[" + this.x + "," + this.y + "," + this.z + "]";
	}

	public static class DoubleVector implements Comparable<DoubleVector> {

		public double x;
		public double y;
		public double z;

		public DoubleVector(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public double distanceTo(DoubleVector to)
		{
			return MathUtils.distance_double(this, to);
		}

		public double distanceFrom(DoubleVector from)
		{
			return MathUtils.distance_double(from, this);
		}

		public Vector toVector()
		{
			return new Vector((int)this.x, (int)this.y, (int)this.z);
		}

		public Vector2D toVector2D()
		{
			return new Vector2D((int)this.x, (int)this.y);
		}

		public Vec3 toVec3()
		{
			return Vec3.createVectorHelper(this.x, this.y, this.z);
		}

		@Override
		public int compareTo(DoubleVector vec)
		{
			int ret = 0;
			if(vec.x < this.x)
			{
				ret = -1;
			}
			else if(vec.x > this.x)
			{
				ret = 1;
			}
			if(ret == 0)
			{
				if(vec.y < this.y)
				{
					ret = -1;
				}
				else if(vec.y > this.y)
				{
					ret = 1;
				}
			}
			if(ret == 0)
			{
				if(vec.z < this.z)
				{
					ret = -1;
				}
				else if(vec.z > this.z)
				{
					ret = 1;
				}
			}
			return ret;
		}

		@Override
		public int hashCode()
		{
			double ret = 0;
			ret += 31D * this.x;
			ret += 31D * this.y;
			ret += 31D * this.z;
			return (int)ret;
		}

		@Override
		public boolean equals(Object other)
		{
			if(!(other instanceof Vector))
			{
				return false;
			}
			Vector vector = (Vector)other;
			return vector.x == this.x && vector.y == this.y && vector.z == this.z;
		}

		@Override
		public String toString()
		{
			return "vec3D[" + this.x + "," + this.y + "," + this.z + "]";
		}

	}

}
