package com.infernio.InfernoCore.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.infernio.InfernoCore.tile.TileBasicInventory;

public abstract class BlockMetaTile extends Block {

	protected Random rand = new Random();

	public BlockMetaTile(Material material)
	{
		super(material);
	}

	@Override
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack stack)
    {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile != null && tile instanceof TileBasicInventory)
		{
			if(stack.hasDisplayName())
			{
				((TileBasicInventory)tile).setInventoryName(stack.getDisplayName());
			}
	    }
    }

    @Override
    @Deprecated
    public boolean hasTileEntity()
    {
    	return true;
    }

    @Override
    public boolean hasTileEntity(int meta)
    {
    	return true;
    }

	@Override
	public int damageDropped(int meta)
	{
		return meta;
	}

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int face, float par7, float par8, float par9)
    {
		TileEntity tile = world.getTileEntity(x, y, z);
		if(tile == null || !(tile instanceof TileBasicInventory))
		{
			return false;
		}
		if(player.isSneaking() && player.getCurrentEquippedItem() != null)
		{
			return false;
		}
		if(player.isSneaking() && player.getCurrentEquippedItem() == null)
		{
			this.openGUI(player, world, x, y, z);
			return true;
		}
		if(player.getCurrentEquippedItem() == null)
		{
			this.openGUI(player, world, x, y, z);
			return true;
		}
		this.openGUI(player, world, x, y, z);
		return true;
    }

    public abstract void openGUI(EntityPlayer player, World world, int x, int y, int z);

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta)
    {
    	if(world.getTileEntity(x, y, z) instanceof IInventory)
    	{
	    	IInventory tile = (IInventory)world.getTileEntity(x, y, z);
	        if(tile != null)
	        {
	            for(int i = 0; i < tile.getSizeInventory(); ++i)
	            {
	                ItemStack stack = tile.getStackInSlot(i);
	                if(stack != null)
	                {
	                    float f = this.rand.nextFloat() * 0.8F + 0.1F;
	                    float f1 = this.rand.nextFloat() * 0.8F + 0.1F;
	                    EntityItem item;
	                    for(float f2 = this.rand.nextFloat() * 0.8F + 0.1F; stack.stackSize > 0; world.spawnEntityInWorld(item))
	                    {
	                        int k1 = this.rand.nextInt(21) + 10;
	                        if(k1 > stack.stackSize)
	                        {
	                            k1 = stack.stackSize;
	                        }
	                        stack.stackSize -= k1;
	                        item = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(stack.getItem(), k1, stack.getItemDamage()));
	                        float f3 = 0.05F;
	                        item.motionX = (double)((float)this.rand.nextGaussian() * f3);
	                        item.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
	                        item.motionZ = (double)((float)this.rand.nextGaussian() * f3);
	                        if(stack.hasTagCompound())
	                        {
	                            item.getEntityItem().setTagCompound((NBTTagCompound)stack.getTagCompound().copy());
	                        }
	                    }
	                }
	            }
	            world.func_147453_f(x, y, z, block);
	        }
    	}
        super.breakBlock(world, x, y, z, block, meta);
    }

    @Override
	public abstract TileEntity createTileEntity(World world, int meta);
}
