package codechicken.nei.recipe;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.Collection;
import java.awt.Rectangle;

import codechicken.nei.PositionedStack;
import net.minecraft.item.ItemStack;

public class TemplateRecipeHandler implements ICraftingHandler, IUsageHandler
{
	public int cycleticks;
	public ArrayList<CachedRecipe> arecipes = new ArrayList<CachedRecipe>();
	public LinkedList<RecipeTransferRect> transferRects = new LinkedList<RecipeTransferRect>();

	public String getRecipeName() { return null; }

	public String getGuiTexture() { return null; }

	public String getOverlayIdentifier() { return null; }

	public void onUpdate() {}

	public void drawBackground(int recipe) {}

	public void drawExtras(int recipe) {}

	public void loadTransferRects() {}

	public List<PositionedStack> getCycledIngredients(int cycle, List<PositionedStack> ingredients) { return null; }

	public void loadCraftingRecipes(String outputId, Object... results) {}

	public void loadCraftingRecipes(ItemStack result) {}

	public void loadUsageRecipes(ItemStack ingredient) {}

	public class CachedRecipe
	{
		public List<PositionedStack> getIngredients() { return null; }

		public PositionedStack getResult() { return null; }

		public void setIngredientPermutation(Collection<PositionedStack> o, ItemStack o2) {}
	}

	public class RecipeTransferRect
	{
		public RecipeTransferRect(Rectangle r, String s, Object[] o) {}
	}
}