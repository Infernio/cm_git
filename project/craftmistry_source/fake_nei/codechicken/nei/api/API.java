package codechicken.nei.api;

import codechicken.nei.recipe.ICraftingHandler;
import codechicken.nei.recipe.IUsageHandler;

public class API
{
	public static void registerRecipeHandler(ICraftingHandler o) {}

	public static void registerUsageHandler(IUsageHandler o) {}

	public static void registerGuiOverlay(Class<?> c, String o) {}

	public static void registerGuiOverlayHandler(Class<?> c, IOverlayHandler o, String o2) {}
}